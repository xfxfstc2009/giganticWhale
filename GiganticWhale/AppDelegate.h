//
//  AppDelegate.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

