//
//  ConnectionQQ.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TencentOpenAPI/QQApiInterface.h"           // qq

@interface ConnectionQQ : NSObject

+(void)callQQWithNumber:(NSString *)qqNumber;
+ (void)handleSendResult:(QQApiSendResultCode)sendResult ;

@end
