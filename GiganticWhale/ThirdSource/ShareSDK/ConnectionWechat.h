//
//  ConnectionWechat.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

typedef void(^respBlock)(NSDictionary *dic);

@interface ConnectionWechat : NSObject
+(void)appRegist;
+ (void)wxResp:(BaseResp *)resp ;

-(void)loginWithWechatWithBlock:(respBlock)block;

+ (NSData *)httpSend:(NSString *)url method:(NSString *)method data:(NSString *)data ;
+ (void)httpSend:(NSString *)url method:(NSString *)method params:(NSString *)params complication:(void(^)(NSData *response))complication ;
@end
