//
//  ConnectionWechat.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ConnectionWechat.h"
#import <objc/runtime.h>

static const char *wxRespKey = "wxRespKey";

@interface ConnectionWechat()
@property (nonatomic, strong) SendAuthReq *sendAuthReq;

@end

@implementation ConnectionWechat

-(void)loginWithWechatWithBlock:(respBlock)block{
    if ([WXApi isWXAppInstalled]) {         // 【判断安装了微信】
        self.sendAuthReq = [[SendAuthReq alloc] init];
        self.sendAuthReq.scope = @"snsapi_userinfo";
        self.sendAuthReq.state = @"panda_ios";
        [WXApi sendReq:self.sendAuthReq];
        if (block) {
            objc_setAssociatedObject(self.class, wxRespKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        }
    } else {
        
    }
}
+(void)appRegist{
//    [WXApi registerApp:WeChatAppID withDescription:@"PandaOL"];
}

+ (void)wxResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *resp_ = (SendAuthResp *)resp;
        if (resp_.errCode == 0) {
            NSMutableString *dataStr = [NSMutableString string];
            [dataStr appendFormat:@"appid=%@&secret=%@&code=%@&grant_type=authorization_code",WeChatAppID,WeChatAppSecret,resp_.code];

            __weak typeof(self) weakSelf = self;
        
            [ConnectionWechat httpSend:[NSString stringWithFormat:@"https://%@%@",WECHAT_HOST,weChat_Token] method:@"POST" params:dataStr complication:^(NSData *response) {
                if (!weakSelf) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (response) {
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
                    [AccountModel sharedAccountModel].wx_accessToken = dic[@"access_token"];
                    [AccountModel sharedAccountModel].wx_refreshToken = dic[@"refresh_token"];
                    [AccountModel sharedAccountModel].wx_openId = dic[@"openid"];
                    [strongSelf wxLogin];
                }
            }];
        }
    }
}

+ (void)wxLogin {
    NSMutableString *dataStrForUser = [NSMutableString string];
    [dataStrForUser appendFormat:@"access_token=%@&openid=%@",[AccountModel sharedAccountModel].wx_accessToken,[AccountModel sharedAccountModel].wx_openId];
    
    __weak typeof(self) weakSelf = self;
    [ConnectionWechat httpSend:[NSString stringWithFormat:@"https://%@/userinfo",WECHAT_HOST] method:@"POST" params:dataStrForUser complication:^(NSData *response) {
        if (!weakSelf) {
            return ;
        }
        if (response) {
            NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:userInfo];
            [mutableDic setObject:[AccountModel sharedAccountModel].wx_openId forKey:@"open_id"];
            respBlock respBlock = objc_getAssociatedObject([weakSelf class], wxRespKey);
            if (respBlock) {
                respBlock([mutableDic copy]);
            }
        }
    }];
}


+ (NSData *)httpSend:(NSString *)url method:(NSString *)method data:(NSString *)data {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    //设置提交方式
    [request setHTTPMethod:method];
    //设置数据类型
    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    //设置编码
    [request setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    //如果是POST
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    return response;
}

+ (void)httpSend:(NSString *)url method:(NSString *)method params:(NSString *)params complication:(void(^)(NSData *response))complication {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    //设置提交方式
    [request setHTTPMethod:method];
    //设置数据类型
    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    //设置编码
    [request setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    //如果是POST
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (complication) {
        complication(response);
    }
}



@end
