//
//  ShareSDKManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/4/25.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>

typedef NS_ENUM(NSInteger,thirdLoginType) {
    thirdLoginTypeQQ,
    thirdLoginTypeWechat,
    thirdLoginTypeWeibo,
};

@interface ShareSDKManager : FetchModel

+(void)registeShareSDK;                             // 注册ShareSDK

+(void)thirdLoginWithType:(thirdLoginType)thirdLoginType successBlock:(void(^)(SSDKUser *user))successHandle failBlock:(void(^)())failHandle;

@end
