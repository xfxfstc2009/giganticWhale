//
//  ShareSDKManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/4/25.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "ShareSDKManager.h"
#import <ShareSDK/ShareSDK.h>
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <ShareSDKConnector/ShareSDKConnector.h>


@implementation ShareSDKManager

+(void)registeShareSDK{
    [ShareSDK registerApp:@"194eb534a00f8"
          activePlatforms:@[@(SSDKPlatformTypeSinaWeibo),@(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType) {
                     
                     switch (platformType) {
                         case SSDKPlatformTypeQQ:
                             [ShareSDKConnector connectQQ:[QQApiInterface class]
                                        tencentOAuthClass:[TencentOAuth class]];
                             break;
                         default:
                             break;
                     }
                 }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
              
              switch (platformType) {
                  case SSDKPlatformTypeSinaWeibo:
                      //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                      [appInfo SSDKSetupSinaWeiboByAppKey:@"568898243"
                                                appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                                              redirectUri:@"http://www.sharesdk.cn"
                                                 authType:SSDKAuthTypeBoth];
                      break;
                      
                  case SSDKPlatformTypeQQ:
                      [appInfo SSDKSetupQQByAppId:@"1105501406"
                                           appKey:@"a0zBvC0I5boEIliE"
                                         authType:SSDKAuthTypeBoth];
                      break;
                  default:
                      break;
              }
          }];
    

}

+(void)thirdLoginWithType:(thirdLoginType)thirdLoginType successBlock:(void(^)(SSDKUser *user))successHandle failBlock:(void(^)())failHandle{
    if (thirdLoginType == thirdLoginTypeQQ){
        [SSEThirdPartyLoginHelper loginByPlatform:SSDKPlatformTypeQQ onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
            if (successHandle){
                successHandle(user);
            }
        } onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                if (successHandle){
                    successHandle(nil);
                }
            } else if (state == SSDKResponseStateFail){
                if (failHandle){
                    failHandle();
                }
            }
        }];
    }
}


@end
