//
//  AppDelegate.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AppDelegate.h"
#import "GWSideMenu.h"                              // 主框架
#import "PushManager.h"                             // 推送消息
#import "GWMapViewSetupManager.h"                   // 地图设置
#import "GWBaseMapViewController.h"                 // 地图
#import "GWLaunchViewController.h"                  // laungch
#import "ConnectionWechat.h"                        // 微信方法
#import "GWNewFeatureViewController.h"              // 新特性
#import "HomeSliderViewController.h"                // 左侧的slider
#import "ShareSDKManager.h"                         // 分享三方登录
#import "AliChatManager.h"                          // IM

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self mainSetup];                                                       // 1. window 层设置配置
    [self createStartAnimation];                                            // 2. 进入主视图
    [GWMapViewSetupManager authorizineMap];                                 // 3. 创建地图
    [ShareSDKManager registeShareSDK];                                      // 4. 微信注册
    [GWStupSQLiteManager steupSqlite];                                      // 5. 数据库
    [[GWMusicPlayerMethod sharedLocationManager] musicWithBackPlayer];      // 6.音乐后台播放
    [[AliChatManager sharedInstance] callThisInDidFinishLaunching];         // 7.即时通讯
    
    [[PushManager shareInstance] registWithPUSHWithApplication:application launchOptions:launchOptions callbackBlock:^(NSDictionary *params) {
        NSLog(@"%@",params);
    }];
    

    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {

}


#pragma mark - 3. APNS
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken{
    [PushManager application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

#pragma mark 3.1 注册失败回调
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(nonnull NSError *)error{
    [PushManager applicationDidFailToRegisterForRemoteNotificationsWithError:error];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo{
    [PushManager application:application didReceiveRemoteNotification:userInfo];
}















#pragma mark - createMainView
-(void)mainSetup{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIImage *backgroundImage = [GWTool createImageWithColor:[UIColor blackColor] frame:CGRectMake(0, 0, kScreenBounds.size.width, 44)];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
}

#pragma mark - 判断是否是第一次使用
-(void)createStartAnimation{
    // 1. 判断是否第一次使用此版本
    NSString *versionKey = @"CFBundleShortVersionString";
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:versionKey];
    NSString *currentVersionCode = [[NSBundle mainBundle].infoDictionary objectForKey:versionKey];
    if ([lastVersionCode isEqualToString:currentVersionCode]) {                 // 非第一次使用软件
        [UIApplication sharedApplication].statusBarHidden = NO ;
        [self startAnimtion:YES];
    } else {                                                                    // 第一次使用软件
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:versionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 跳转开场动画
        GWNewFeatureViewController *newFeatureViewController = [[GWNewFeatureViewController alloc] init];
        newFeatureViewController.startBlock = ^(){
            [self startAnimtion:NO];
        };
        self.window.rootViewController = newFeatureViewController;
    }
}

#pragma mark 跳转主界面
-(void)startAnimtion:(BOOL)isAnimation{
    [UIApplication sharedApplication].statusBarHidden = NO;
    self.window.rootViewController = [GWSideMenu setupSlider];
    
    if (isAnimation == NO){
        [self sendRequestToGetInfo];
    }
    
    // 增加动画页面
    if(isAnimation){
        GWLaunchViewController *launchVC = [[GWLaunchViewController alloc] init];
        [self.window addSubview:launchVC.view];
    }
}

#pragma mark - 请求接口
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:base_setting requestParams:nil responseObjectClass:[GWBaseSettingModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            GWBaseSettingModel *baseSettingModel = (GWBaseSettingModel *)responseObject;
            
            // 设置slider
            [AccountModel sharedAccountModel].slider = baseSettingModel.sliderBg;
            [GWTool userDefaulteWithKey:Main_Slider Obj:baseSettingModel.sliderBg];
            
            // slider 设置
            HomeSliderViewController *sliderViewController = (HomeSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
            [sliderViewController updateList:baseSettingModel.sliderList];
        }
    }];
}



@end
