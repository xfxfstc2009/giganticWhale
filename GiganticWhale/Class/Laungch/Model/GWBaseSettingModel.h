//
//  GWBaseSettingModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWSettingSliderListModel.h"
@interface GWBaseSettingModel : FetchModel

@property (nonatomic,copy)NSString *cons;                   /**< 内容*/
@property (nonatomic,copy)NSString *icon;                   /**< iconURL*/;
@property (nonatomic,assign)BOOL otherOpen;                 /**< 判断其他是否打开*/
@property (nonatomic,assign)BOOL isApple;                   /**< 是否审核*/
@property (nonatomic,copy)NSString *laungch;                /**< 内容*/
@property (nonatomic,copy)NSString *sliderBg;               /**< 侧边栏北京*/
@property (nonatomic,assign)NSInteger launchType;           /**< 设置laungch类型*/
@property (nonatomic,strong)NSArray<GWSettingSliderSingleModel> *sliderList;

@end
