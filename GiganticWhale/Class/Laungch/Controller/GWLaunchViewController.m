//
//  GWLaunchViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWLaunchViewController.h"
#import "GWBaseSettingModel.h"
#import "HomeSliderViewController.h"
@interface GWLaunchViewController()
@property (nonatomic,strong)GWImageView *launchImageView;
@property (nonatomic,strong)GWImageView *logoImageView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *moonLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;



@end

@implementation GWLaunchViewController

-(void)dealloc{
    NSLog(@"释放");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.launchImageView];
    [self.view bringSubviewToFront:self.launchImageView];
    [self createView];
    __weak typeof(self)waekSelf = self;
    [waekSelf sendRequestGetLaunchImageInfo];
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:120];
    self.numberLabel.autoresizesSubviews = YES;
    self.numberLabel.text = [NSDate getCurrentTimeWithDay];
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.numberLabel];
    
    // Moon
    self.moonLabel = [[UILabel alloc] init];
    self.moonLabel.textColor = [UIColor whiteColor];
    self.moonLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
    
    NSString *moon =  [NSDate getCurrentTimeWithMoon];
    NSString *realMoon = @"";
    if ([moon isEqualToString:@"1"]){
        realMoon = @"Jan.";
    } else if ([moon isEqualToString:@"2"]){
        realMoon = @"Feb.";
    } else if ([moon isEqualToString:@"3"]){
        realMoon = @"Mar.";
    } else if ([moon isEqualToString:@"4"]){
        realMoon = @"Apr.";
    } else if ([moon isEqualToString:@"5"]){
        realMoon = @"May.";
    } else if ([moon isEqualToString:@"6"]){
        realMoon = @"Jun.";
    } else if ([moon isEqualToString:@"7"]){
        realMoon = @"Jul.";
    } else if ([moon isEqualToString:@"8"]){
        realMoon = @"Aug.";
    } else if ([moon isEqualToString:@"9"]){
        realMoon = @"Sep.";
    } else if ([moon isEqualToString:@"10"]){
        realMoon = @"Oct.";
    } else if ([moon isEqualToString:@"11"]){
        realMoon = @"Nov.";
    } else if ([moon isEqualToString:@"12"]){
        realMoon = @"Dec.";
    }
    
    self.moonLabel.text = realMoon;
    [self.view addSubview:self.moonLabel];
    
    // Moon
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.contentLabel.numberOfLines = 0;
    [self.view addSubview:self.contentLabel];

    // 4. 创建手势
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(animationStopTapManager)];
    [self.view addGestureRecognizer:self.tapGesture];
}

#pragma mark - updateImageView
-(void)updateLanunchImage{
    
}

-(void)sendRequestGetLaunchImageInfo{
    GWBaseSettingModel *baseSettingModel = [[GWBaseSettingModel alloc]init];
    __weak typeof(self)weakSelf = self;
    
    [baseSettingModel fetchWithPath:base_setting completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            // 设置slider
            [AccountModel sharedAccountModel].slider = baseSettingModel.sliderBg;
            [GWTool userDefaulteWithKey:Main_Slider Obj:baseSettingModel.sliderBg];
            
            // slider 设置
            
            HomeSliderViewController *sliderViewController = (HomeSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
            [sliderViewController updateList:baseSettingModel.sliderList];
            
            [strongSelf.launchImageView uploadImageWithLaungchURL:baseSettingModel.laungch placeholder:nil callback:^(UIImage *image) {
                [strongSelf.view addSubview:strongSelf.logoImageView];
                
                // content
                strongSelf.contentLabel.text = baseSettingModel.cons;
                CGSize contentSize = [strongSelf.contentLabel.text sizeWithCalcFont:strongSelf.contentLabel.font constrainedToSize:CGSizeMake((kScreenBounds.size.width - 2 * LCFloat(11)), CGFLOAT_MAX)];
                strongSelf.contentLabel.frame = CGRectMake(LCFloat(11), kScreenBounds.size.height - LCFloat(30) - contentSize.height, (kScreenBounds.size.width - 2 * LCFloat(11)), contentSize.height);
                
                // day
                CGSize numberSize = [strongSelf.numberLabel.text sizeWithCalcFont:strongSelf.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
                self.numberLabel.frame = CGRectMake(LCFloat(11), strongSelf.contentLabel.orgin_y - LCFloat(25) - 100, numberSize.width, 100);
                
                // Moon
                CGSize moonSize = [strongSelf.moonLabel.text sizeWithCalcFont:strongSelf.moonLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
                strongSelf.moonLabel.frame = CGRectMake(CGRectGetMaxX(strongSelf.numberLabel.frame), strongSelf.numberLabel.orgin_y + (strongSelf.numberLabel.size_height - [NSString contentofHeight:strongSelf.moonLabel.font]), moonSize.width, [NSString contentofHeight:strongSelf.moonLabel.font]);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:3 animations:^{
                        strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                        strongSelf.launchImageView.alpha = 0;
                        strongSelf.view.alpha = 0;
                    } completion:^(BOOL finished) {
                        [strongSelf.view removeFromSuperview];
                    }];
                });
            }];
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:3 animations:^{
                    strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                    strongSelf.launchImageView.alpha = 0;
                    strongSelf.view.alpha = 0;
                } completion:^(BOOL finished) {
                    [strongSelf.view removeFromSuperview];
                }];
            });
        }
    }];
}

#pragma mark - successSetting
-(void)successSettingWithModel:(GWBaseSettingModel *)baseSettingModel{
    __weak typeof(self)weakSelf = self;
    [self.launchImageView uploadImageWithLaungchURL:baseSettingModel.laungch placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view addSubview:strongSelf.logoImageView];
        strongSelf.launchImageView.image = image;
        
        // content
        strongSelf.contentLabel.text = baseSettingModel.cons;
        CGSize contentSize = [strongSelf.contentLabel.text sizeWithCalcFont:strongSelf.contentLabel.font constrainedToSize:CGSizeMake((kScreenBounds.size.width - 2 * LCFloat(11)), CGFLOAT_MAX)];
        strongSelf.contentLabel.frame = CGRectMake(LCFloat(11), kScreenBounds.size.height - LCFloat(30) - contentSize.height, (kScreenBounds.size.width - 2 * LCFloat(11)), contentSize.height);
        
        // day
        CGSize numberSize = [strongSelf.numberLabel.text sizeWithCalcFont:strongSelf.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
        self.numberLabel.frame = CGRectMake(LCFloat(11), strongSelf.contentLabel.orgin_y - LCFloat(25) - 100, numberSize.width, 100);
        
        // Moon
        CGSize moonSize = [strongSelf.moonLabel.text sizeWithCalcFont:strongSelf.moonLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
        strongSelf.moonLabel.frame = CGRectMake(CGRectGetMaxX(strongSelf.numberLabel.frame), strongSelf.numberLabel.orgin_y + (strongSelf.numberLabel.size_height - [NSString contentofHeight:strongSelf.moonLabel.font]), moonSize.width, [NSString contentofHeight:strongSelf.moonLabel.font]);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:3 animations:^{
                strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                strongSelf.launchImageView.alpha = 0;
                strongSelf.view.alpha = 0;
            } completion:^(BOOL finished) {
                [strongSelf.view removeFromSuperview];
            }];
        });
    }];
}



#pragma mark - getter and setter

- (GWImageView *)launchImageView{
    
    if (_launchImageView == nil) {
        _launchImageView = [[GWImageView alloc] initWithFrame:kScreenBounds];
        _launchImageView.image = [UIImage imageNamed:@"bg1.jpg"];
    }
    
    return _launchImageView;
}

- (GWImageView *)logoImageView{
    
    if (_logoImageView == nil) {
        _logoImageView = [[GWImageView alloc] initWithFrame:CGRectMake(95, 458, 128, 100)];
        _logoImageView.image = [UIImage imageNamed:@"bg1.jpg"];
    }
    
    return _logoImageView;
}

#pragma mark - 手势点击页面消失
-(void)animationStopTapManager{
    [self dismissAnimationManager];
}


#pragma mark - dismissManager
-(void)dismissAnimationManager{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:keywindow.center duration:.9f completion:^{
       
    }];
}



@end
