//
//  GWNewFeatureViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/4/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWNewFeatureViewController.h"
#import "GWMovieView.h"
#import "TextAnimation.h"
#import "GWBaseSettingModel.h"
#import "HomeSliderViewController.h"

@interface GWNewFeatureViewController()

@property (nonatomic,strong)UIButton *tempButton;
@property (nonatomic,strong)GWMovieView *movieView;                     /**< 背景视频*/
@property (nonatomic,strong)TextAnimation *textAnimationView;           /**< 文字动画*/
@property (nonatomic,strong)UILabel *networkLabel;                      /**< 地址label*/
@property (nonatomic,strong)UILabel *madeLabel;                         /**< madelabel*/
@end

@implementation GWNewFeatureViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [UIView animateWithDuration:4 animations:^{
        [self.movieView setAlpha:1];
    } completion:^(BOOL finished) {
        [self.movieView startAnimation];
    }];
    
    [self.textAnimationView showAnimationWithFinishBlock:NULL];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self createBgView];
    
    [self createView];
    [self giganticWhale];
}

#pragma mark - createNew
-(void)createBgView{
    self.movieView = [[GWMovieView alloc]initWithFrame:self.view.frame];
    self.movieView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.movieView.alpha = 0;
    [self.view addSubview:self.movieView];
}

#pragma mark - createView
-(void)createView{
    // 1. 创建label
    self.networkLabel = [[UILabel alloc]init];
    self.networkLabel.backgroundColor = [UIColor clearColor];
    self.networkLabel.text = [NSString stringWithFormat:@"http://%@",API_HOST];
    self.networkLabel.textColor = [UIColor whiteColor];
    self.networkLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.networkLabel.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(11) - [NSString contentofHeight:self.networkLabel.font], kScreenBounds.size.width, [NSString contentofHeight:self.networkLabel.font]);
    self.networkLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.networkLabel];
    
    self.tempButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tempButton.backgroundColor = [UIColor clearColor];
    [self.tempButton setTitle:@"进入首页" forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.tempButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf dismissAnimation];
    }];
    [self.view addSubview:self.tempButton];
    self.tempButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.tempButton.layer.borderWidth = 2.;
    CGSize contentOfTemp = [@"进入首页" sizeWithCalcFont:self.tempButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.tempButton.titleLabel.font])];
    CGFloat sizeWidth = contentOfTemp.width + 2 * 30;
    CGFloat sizeHeight = [NSString contentofHeight:self.tempButton.titleLabel.font] + 2 * LCFloat(10);
    
    self.tempButton.frame = CGRectMake((kScreenBounds.size.width - sizeWidth) / 2., self.networkLabel.orgin_y - LCFloat(50) - sizeHeight, sizeWidth, sizeHeight);
    self.tempButton.alpha = 0;
    
    [UIView animateWithDuration:1. animations:^{
        self.tempButton.alpha = 1;
    } completion:^(BOOL finished) {
        self.tempButton.alpha = 1;
    }];
}

-(void)giganticWhale{
    CGRect animationViewRect = CGRectMake(100, 200, 200, 80);
    self.textAnimationView = [[TextAnimation alloc]initWithFrame:animationViewRect title:@"GiganticWhale"];
    self.textAnimationView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.textAnimationView];
    
    // 重置frame
    self.textAnimationView.frame = CGRectMake((kScreenBounds.size.width - self.textAnimationView.size_width) / 2.,kScreenBounds.size.height / 2. - LCFloat(140),self.textAnimationView.size_width,self.textAnimationView.size_height);
}

#pragma mark - 消失动画
-(void)dismissAnimation{
    
    [UIView animateWithDuration:.5f animations:^{
        self.movieView.alpha = 0;
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        if (self.startBlock){
            self.startBlock();
        }
    }];
}

@end
