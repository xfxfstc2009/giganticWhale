//
//  GWNewFeatureViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/4/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface GWNewFeatureViewController : AbstractViewController

@property (nonatomic, copy) void (^startBlock)();         // 是否登录

@end
