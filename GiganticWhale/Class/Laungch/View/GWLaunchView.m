//
//  GWLaunchView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWLaunchView.h"

@interface GWLaunchView()
@property (nonatomic,strong)GWImageView *imageView;

@end

@implementation GWLaunchView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createLaunchImageView];
    }
    return self;
}

-(void)createLaunchImageView{
    self.imageView = [[GWImageView alloc]init];
    self.imageView.backgroundColor = [UIColor clearColor];
    self.imageView.frame = kScreenBounds;
    [self addSubview:self.imageView];
}


-(void)updateLanunchImage{
    __weak typeof(self)weakSelf = self;
    [weakSelf.imageView uploadImageWithURL:@"http://pic1.zhimg.com/17d28443817ba199dea86a6b6da44cc4.jpg"  placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [UIView animateWithDuration:1 animations:^{
            strongSelf.imageView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}


@end
