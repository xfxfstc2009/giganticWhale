//
//  GWStudyScrollView.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhaleView.h"

@protocol GWStudyScrollViewDelegate <NSObject>
@optional
-(void)recievedMBTwitterScrollEvent;
-(void)recievedMBTwitterScrollButtonClicked;
-(void)didselectToDirectUrl:(NSString *)url;
@end


@interface GWStudyScrollView : UIView <UIScrollViewDelegate, GWStudyScrollViewDelegate >

- (GWStudyScrollView *)initScrollViewWithBackgound:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle contentHeight:(CGFloat)height;


@property (strong, nonatomic) WhaleView *avatarImage;
@property (strong, nonatomic) UIView *header;
@property (strong, nonatomic) UILabel *headerLabel;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (nonatomic,strong)UIScrollView *tableViewMainScrollView;  /**< 存放数据内容的scrollView*/
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) GWImageView *headerImageView;
@property (strong, nonatomic) UIButton *headerButton;

@property (nonatomic,strong)UILabel * subtitleLabel;
@property (nonatomic,strong)UILabel * titleLabel;

@property (nonatomic, strong) NSMutableArray *blurImages;
@property (nonatomic,strong) id<GWStudyScrollViewDelegate>delegate;


@end
