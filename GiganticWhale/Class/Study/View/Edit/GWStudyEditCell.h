//
//  GWStudyEditCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWStudyItemSingleModel.h"
#import "GWStudyInfoSingleModel.h"

@interface GWStudyEditCell : UITableViewCell

@property (nonatomic,strong)GWStudyItemSingleModel *transferItemSingleModel;
@property (nonatomic,strong)GWStudyInfoSingleModel *transferInfoSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

@end
