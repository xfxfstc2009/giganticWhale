//
//  GWStudyEditCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWStudyEditCell.h"

@interface GWStudyEditCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *numberLabel;


@end

@implementation GWStudyEditCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建数量
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.numberLabel];
}

-(void)setTransferItemSingleModel:(GWStudyItemSingleModel *)transferItemSingleModel{
    _transferItemSingleModel = transferItemSingleModel;
    
    self.fixedLabel.text = transferItemSingleModel.name;
    CGSize contentOfSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, contentOfSize.width, self.transferCellHeight);
    
    self.numberLabel.text = [NSString stringWithFormat:@"%li",transferItemSingleModel.count];
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 25, 0, 25, self.transferCellHeight);
    
}

-(void)setTransferInfoSingleModel:(GWStudyInfoSingleModel *)transferInfoSingleModel{
    _transferInfoSingleModel = transferInfoSingleModel;
    self.fixedLabel.text = transferInfoSingleModel.title;
    
    CGSize contentOfSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, contentOfSize.width, self.transferCellHeight);
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

@end
