//
//  GWStudyTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWStudyTableViewCell.h"
#import "GWStudyItem.h"
#import <pop/POP.h>

@interface GWStudyTableViewCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation GWStudyTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.numberOfLines = 1;
    self.fixedLabel.textColor = [UIColor blackColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建timeLabel
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.textColor = [UIColor lightGrayColor];
    self.timeLabel.font = [UIFont systemFontOfCustomeSize:12.];
    [self addSubview:self.timeLabel];
    
    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.textColor = [UIColor grayColor];
    self.dymicLabel.font = [UIFont AvenirLightWithFontSize:9.f];
    [self addSubview:self.dymicLabel];
}

-(void)setTransferIndexPath:(NSIndexPath *)transferIndexPath{
    _transferIndexPath = transferIndexPath;
}

-(void)setTransferStudySingleModel:(GWStudyInfoSingleModel *)transferStudySingleModel{
    _transferStudySingleModel = transferStudySingleModel;
    
    GWStudyItem *item = [GWStudyItem itemWithName:transferStudySingleModel.title object:@"123"];
    item.index = self.transferIndexPath.row + 1;
    [item createAttributedString];
    self.fixedLabel.attributedText = item.nameString;
    
    self.dymicLabel.text = transferStudySingleModel.details;
    
    // 2. 进行调整位置
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - timeSize.width, LCFloat(5), timeSize.width, [NSString contentofHeight:self.timeLabel.font]);
    
    self.fixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(5), self.timeLabel.orgin_x - 2 * LCFloat(11), [NSString contentofHeight:self.timeLabel.font]);
    
    // 3. 设置背景颜色
    if (self.transferIndexPath.row % 2){
        self.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.05f];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont systemFontOfCustomeSize:15.]];
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont systemFontOfCustomeSize:12.]];
    cellHeight += LCFloat(5);
    return cellHeight;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (self.highlighted) {
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.fixedLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.fixedLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}
@end
