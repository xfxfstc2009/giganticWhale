//
//  GWStudyScrollView.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWStudyScrollView.h"
#import "UIScrollView+TwitterCover.h"
#import "HTHorizontalSelectionList.h"
#import "GWStudyTableViewCell.h"

#import "GWStudyInfoListModel.h"
#import "GWStudyItemListModel.h"
#import "GCD.h"

CGFloat const offset_HeaderStop = 40.0;
CGFloat const offset_B_LabelHeader = 95.0;
CGFloat const distance_W_LabelHeader = 35.0;

@interface GWStudyScrollView()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *segmentArr;         /**< segmentArr*/

@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;
@property (nonatomic,strong)NSMutableArray *currentMutableArr;
@end


@implementation GWStudyScrollView

- (GWStudyScrollView *)initScrollViewWithBackgound:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle contentHeight:(CGFloat)height {
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    self = [[GWStudyScrollView alloc] initWithFrame:bounds];
    [self setupView:backgroundImage avatarImage:avatarImage titleString:titleString subtitleString:subtitleString buttonTitle:buttonTitle scrollHeight:height];
    [self arrayWithInit];
    
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetInfoWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf hasTableAndManagerWithIndex:0];
    }];      
    
    return self;
}

- (void) setupView:(UIImage*)backgroundImage avatarImage:(UIImage *)avatarImage titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString buttonTitle:(NSString *)buttonTitle scrollHeight:(CGFloat)height{
    // 【1. 创建头部内容】
    self.header = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 120)];
    [self addSubview:self.header];
    
    // 【2.头部文字】
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.header.frame.size.height - 5, self.frame.size.width, 25)];
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.text = @"GiganticWhale";
    self.headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18];
    self.headerLabel.textColor = [UIColor whiteColor];
    [self.header addSubview:self.headerLabel];
    
    // 【2.创建内容体】
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)];
    self.scrollView.showsVerticalScrollIndicator = YES;
    [self addSubview:self.scrollView];
    CGSize newSize = CGSizeMake(self.frame.size.width, height);
    self.scrollView.contentSize = newSize;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.delegate = self;
    
    // 【3. 创建头像】
    self.avatarImage = [[WhaleView alloc] initWithFrame:CGRectMake(10, 79, 69, 69)];
    self.avatarImage.layer.cornerRadius = 10;
    self.avatarImage.layer.borderWidth = 2;
    self.avatarImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarImage.clipsToBounds = YES;
    
    // 【4. 创建标题】
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.avatarImage.orgin_x, CGRectGetMaxY(self.avatarImage.frame), self.avatarImage.size_width, 25)];
    self.titleLabel.size_height = [NSString contentofHeight:self.titleLabel.font];
    self.titleLabel.text = @"巨鲸";
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor lightGrayColor];
    
    // 【5. 创建subTitle】
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 177, 250, 25)];
    self.subtitleLabel.text = subtitleString;
    self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    self.subtitleLabel.textColor = [UIColor lightGrayColor];
    
    [self.scrollView addSubview:self.avatarImage];
    [self.scrollView addSubview:self.titleLabel];

    // 【6.创建头部图片】
    self.headerImageView = [[GWImageView alloc] initWithFrame:self.header.frame];
    self.headerImageView.image = backgroundImage;
    [self.headerImageView uploadImageWithLaungchURL:[AccountModel sharedAccountModel].laungch placeholder:nil callback:NULL];
    
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.header insertSubview:self.headerImageView aboveSubview:self.headerLabel];
    self.header.clipsToBounds = YES;
    
    self.avatarImage.layer.cornerRadius = 10;
    self.avatarImage.layer.borderWidth = 3;
    self.avatarImage.layer.borderColor = [UIColor whiteColor].CGColor;

    self.blurImages = [[NSMutableArray alloc] init];
    
    if (backgroundImage != nil) {
        GCDQueue *concurrentQueue = [[GCDQueue alloc] initConcurrent];
        __weak typeof(self)weakSelf = self;
        [concurrentQueue execute:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf prepareForBlurImages];
        } afterDelay:0.f];
    } else {
        self.headerImageView.backgroundColor = [UIColor whiteColor];
    }
    
    if (!self.segmentList){
        self.segmentList = [[HTHorizontalSelectionList alloc]init];
        self.segmentList.delegate = self;
        self.segmentList.dataSource = self;

        self.segmentList.frame = CGRectMake(CGRectGetMaxX(self.avatarImage.frame)+ 10, CGRectGetMaxY(self.header.frame), kScreenBounds.size.width - CGRectGetMaxX(self.avatarImage.frame) - 20, 44);
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"蓝"];
        self.segmentList.bottomTrimColor = [UIColor clearColor];
        [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        self.segmentList.backgroundColor = [UIColor clearColor];
    }
    
//     创建scrollView
    [self createMainScrollView];
    
    [self addSubview:self.segmentList];
}


#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    GWStudyItemSingleModel *studyItemSingleModel = [self.segmentArr objectAtIndex:index];
    return studyItemSingleModel.name;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.tableViewMainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    [self hasTableAndManagerWithIndex:index];
}

#pragma makr - 临时创建一个tableView
-(void)hasTableAndManagerWithIndex:(NSInteger)index{
    // 2. 判断当前是否存在tableView
    if (![self.tableViewMutableDic.allKeys containsObject:[NSString stringWithFormat:@"%li",index]]){            // 如果不存在
        if (self.currentMutableArr.count){
            [self.currentMutableArr removeAllObjects];
        }
        UITableView *tableView = [self createTableView];                            // 创建tableView
        tableView.orgin_x = index *kScreenBounds.size.width;                        // 修改frame
        [self.tableViewMutableDic setObject:tableView forKey:[NSString stringWithFormat:@"%li",index]];
        
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        GWStudyItemSingleModel *studyItemSingleModel = [self.segmentArr objectAtIndex:index];
        [weakSelf sendRequestToGetStudyListWithItemId:studyItemSingleModel.itemId];
    }
}

-(void)createMainScrollView{
    if (!self.tableViewMainScrollView){
        self.tableViewMainScrollView = [[UIScrollView alloc]init];
        self.tableViewMainScrollView.frame = self.scrollView.bounds;
        self.tableViewMainScrollView.orgin_y += 5;
        self.tableViewMainScrollView.delegate = self;
        self.tableViewMainScrollView.backgroundColor = [UIColor clearColor];
        self.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width , self.scrollView.size_height);
        self.tableViewMainScrollView.pagingEnabled = YES;
        [self addSubview:self.tableViewMainScrollView];
    }
    [self createTableView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentArr = [NSMutableArray array];
    self.tableViewMutableDic = [NSMutableDictionary dictionary];
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];
    self.currentMutableArr = [NSMutableArray array];
}


#pragma mark - createTableView
-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.tableViewMainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.orgin_y = MAX(CGRectGetMaxY(self.segmentList.frame), CGRectGetMaxY(self.titleLabel.frame));
    tableView.size_height = kScreenBounds.size.height - (MAX(CGRectGetMaxY(self.segmentList.frame), CGRectGetMaxY(self.titleLabel.frame)));
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self.tableViewMainScrollView addSubview:tableView];
    return tableView;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.currentMutableArr.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdengifyWithRowOne = @"cellIdentifyWithRowOne";
    GWStudyTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWStudyTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferIndexPath = indexPath;
    
    // 1. 获取对应的tableView
    for (int i = 0 ;i < self.tableViewMutableDic.allKeys.count;i++){
        NSString *key = [self.tableViewMutableDic.allKeys objectAtIndex:i];
        UITableView *currentTableView = [self.tableViewMutableDic objectForKey:key];
        if (tableView == currentTableView){         // 获取当前的model
            GWStudyItemSingleModel *studyItemSingleModel = [self.segmentArr objectAtIndex:i];
            NSArray *currentArr = [self.dataSourceMutableDic objectForKey:studyItemSingleModel.itemId];
            GWStudyInfoSingleModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
            cellWithRowOne.transferStudySingleModel = studyInfoSingleModel;
        }
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 1. 获取对应的tableView
    for (int i = 0 ;i < self.tableViewMutableDic.allKeys.count;i++){
        NSString *key = [self.tableViewMutableDic.allKeys objectAtIndex:i];
        UITableView *currentTableView = [self.tableViewMutableDic objectForKey:key];
        if (tableView == currentTableView){         // 获取当前的model
            GWStudyItemSingleModel *studyItemSingleModel = [self.segmentArr objectAtIndex:i];
            NSArray *currentArr = [self.dataSourceMutableDic objectForKey:studyItemSingleModel.itemId];
            GWStudyInfoSingleModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
            [self.delegate didselectToDirectUrl:studyInfoSingleModel.url];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWStudyTableViewCell calculationCellHeight];
}

#pragma mark - Interface
-(void)sendRequestToGetStudyListWithItemId:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"itemId":itemId,@"page":@(1),@"size":@"1000"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_info_list requestParams:params responseObjectClass:[GWStudyInfoListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWStudyInfoListModel *studyInfoList = (GWStudyInfoListModel *)responseObject;
            
            if (strongSelf.currentMutableArr.count){
                [strongSelf.currentMutableArr removeAllObjects];
            }
            
            if (![strongSelf.dataSourceMutableDic.allKeys containsObject:itemId]){
                [strongSelf.dataSourceMutableDic setObject:studyInfoList.studyInfo forKey:itemId];
            }
            
            NSArray *currentDataSourceArr = [strongSelf.dataSourceMutableDic objectForKey:itemId];
            [strongSelf.currentMutableArr addObjectsFromArray:currentDataSourceArr];
            
            //1. 找到当前的tableView
            UITableView *currentTableView = (UITableView *)[strongSelf.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",strongSelf.segmentList.selectedButtonIndex]];
            
            [currentTableView reloadData];
        } else {
            
        }
    }];
}

#pragma mark - Interface
-(void)sendRequestToGetInfoWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_item_list requestParams:nil responseObjectClass:[GWStudyItemListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWStudyItemListModel *studyItemListModel = (GWStudyItemListModel *)responseObject;
            [strongSelf.segmentArr addObjectsFromArray:studyItemListModel.itemList];
            strongSelf.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width *strongSelf.segmentArr.count , strongSelf.scrollView.size_height);
            [strongSelf.segmentList reloadData];
            if (block){
                block();
            }
        }
    }];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.y;
    if ([scrollView isKindOfClass:[UITableView class]]){
        [self animationForScroll:offset];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.tableViewMainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
        
        [self hasTableAndManagerWithIndex:page];
    }
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    CGFloat offset = self.tableView.contentOffset.y;
//    [self animationForScroll:offset];
//}


- (void) animationForScroll:(CGFloat) offset {
    
    CATransform3D headerTransform = CATransform3DIdentity;
    CATransform3D avatarTransform = CATransform3DIdentity;
    
    // DOWN -----------------
    
    if (offset < 0) {
        
        CGFloat headerScaleFactor = -(offset) / self.header.bounds.size.height;
        CGFloat headerSizevariation = ((self.header.bounds.size.height * (1.0 + headerScaleFactor)) - self.header.bounds.size.height)/2.0;
        headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0);
        headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0);
        
        self.header.layer.transform = headerTransform;
        
        if (offset < -self.frame.size.height/3.5) {
            //            [self recievedMBTwitterScrollEvent];
        }
        self.tableViewMainScrollView.orgin_y = 0;
        self.tableViewMainScrollView.size_height = kScreenBounds.size.height - self.tableViewMainScrollView.orgin_y;
    }
    
    // SCROLL UP/DOWN ------------
    
    else {
        
        // Header -----------
        headerTransform = CATransform3DTranslate(headerTransform, 0, MAX(-offset_HeaderStop, -offset), 0);
        
        //  ------------ Label
        CATransform3D labelTransform = CATransform3DMakeTranslation(0, MAX(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
        self.headerLabel.layer.transform = labelTransform;
        self.headerLabel.layer.zPosition = 2;
        
        // Avatar -----------
        CGFloat avatarScaleFactor = (MIN(offset_HeaderStop, offset)) / self.avatarImage.bounds.size.height / 1.4; // Slow down the animation
        CGFloat avatarSizeVariation = ((self.avatarImage.bounds.size.height * (1.0 + avatarScaleFactor)) - self.avatarImage.bounds.size.height) / 2.0;
        avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0);
        avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0);
        
        if (offset <= offset_HeaderStop) {
            if (self.avatarImage.layer.zPosition <= self.headerImageView.layer.zPosition) {
                self.header.layer.zPosition = 0;
            }
            self.tableViewMainScrollView.orgin_y = -offset;
            self.tableViewMainScrollView.size_height = kScreenBounds.size.height - self.tableViewMainScrollView.orgin_y;
            
        }else {
            if (self.avatarImage.layer.zPosition >= self.headerImageView.layer.zPosition) {
                self.header.layer.zPosition = 2;
            }
            self.tableViewMainScrollView.orgin_y = -offset_HeaderStop;
            self.tableViewMainScrollView.size_height = kScreenBounds.size.height - self.tableViewMainScrollView.orgin_y;
        }
    }
    // 高斯模糊
    if (self.headerImageView.image != nil) {
        [self blurWithOffset:offset];
    }
    self.header.layer.transform = headerTransform;
    self.avatarImage.layer.transform = avatarTransform;
    self.segmentList.orgin_y = CGRectGetMaxY(self.header.frame) ;
    self.scrollView.orgin_y = -offset;
    

    CGFloat segmentNormal = (CGRectGetMaxX(self.avatarImage.frame)+ 10);
    
    if (offset > CGRectGetMaxY(self.titleLabel.frame) - CGRectGetMaxY(self.header.frame)){
        if (self.segmentList.orgin_x <= segmentNormal){
            [UIView animateWithDuration:.4f animations:^{
                self.segmentList.orgin_x = 0;
                self.segmentList.size_width = kScreenBounds.size.width;
            }];
        }
    } else {
        if (self.segmentList.orgin_x >=0){
            [UIView animateWithDuration:.4f animations:^{
                self.segmentList.orgin_x = segmentNormal;
                self.segmentList.size_width = kScreenBounds.size.width - segmentNormal;
            }];
        }
    }
}



- (void)prepareForBlurImages {
    CGFloat factor = 0.1;
    [self.blurImages addObject:self.headerImageView.image];
    for (NSUInteger i = 0; i < self.headerImageView.frame.size.height/10; i++) {
        [self.blurImages addObject:[self.headerImageView.image boxblurImageWithBlur:factor]];
        factor+=0.04;
    }
}



- (void) blurWithOffset:(float)offset {
    NSInteger index = offset / 10;
    if (index < 0) {
        index = 0;
    }
    else if(index >= self.blurImages.count) {
        index = self.blurImages.count - 1;
    }
    UIImage *image = self.blurImages[index];
    if (self.headerImageView.image != image) {
        [self.headerImageView setImage:image];
    }
}


- (void) recievedMBTwitterScrollButtonClicked {
    [self.delegate recievedMBTwitterScrollButtonClicked];
}


- (void) recievedMBTwitterScrollEvent {
    [self.delegate recievedMBTwitterScrollEvent];
}
@end
