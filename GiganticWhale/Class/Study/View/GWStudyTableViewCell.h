//
//  GWStudyTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWStudyInfoSingleModel.h"

@interface GWStudyTableViewCell : UITableViewCell

@property (nonatomic,strong)GWStudyInfoSingleModel *transferStudySingleModel;       /**< 传递过来的Model*/
@property (nonatomic,strong)NSIndexPath *transferIndexPath;
+(CGFloat)calculationCellHeight;
@end
