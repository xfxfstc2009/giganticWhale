//
//  GWStudyItem.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWStudyItem : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) id        object;

+ (instancetype)itemWithName:(NSString *)name object:(id)object;

@property (nonatomic)                   NSInteger index;
@property (nonatomic, strong, readonly) NSMutableAttributedString *nameString;

- (void)createAttributedString;

@end
