//
//  GWMyStudyAddInfoDetailViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMyStudyAddInfoDetailViewController.h"

@interface GWMyStudyAddInfoDetailViewController()<UITableViewDataSource,UITableViewDelegate>
{
    UITextField *inputNameField;
}
@property (nonatomic,strong)UITableView *myStudyAddTableView;
@property (nonatomic,strong)NSMutableArray *myStudyAddMutableArr;

@end

@implementation GWMyStudyAddInfoDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToAddItem];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.myStudyAddMutableArr = [NSMutableArray array];
    if (self.transferStudyEditType == MyStudyEditTypeInfo){
        [self.myStudyAddMutableArr addObject:@[@"添加名字"]];
    } else if (self.transferStudyEditType == MyStudyEditTypeItem){
        [self.myStudyAddMutableArr addObject:@[@"添加名字"]];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.myStudyAddTableView){
        self.myStudyAddTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.myStudyAddTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.myStudyAddTableView.delegate = self;
        self.myStudyAddTableView.dataSource = self;
        self.myStudyAddTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.myStudyAddTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.myStudyAddTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.myStudyAddMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.myStudyAddMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transfrCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferTitle = @"专辑名称";
    cellWithRowOne.transferPlaceholeder = @"请输入添加的专辑名称";
    inputNameField = cellWithRowOne.inputTextField;
    
    return cellWithRowOne;
}
\
#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWInputTextFieldTableViewCell calculationCellHeight];
}

#pragma mark - 接口
-(void)sendRequestToAddItem{
    __weak typeof(self)weakSelf = self;
    
    if (!inputNameField.text.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请输入标签名" buttonTitles:@[@"确定"] callBlock:NULL]show];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_item_add requestParams:@{@"name":inputNameField.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"添加成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}
@end
