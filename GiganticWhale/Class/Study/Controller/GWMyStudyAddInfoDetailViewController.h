//
//  GWMyStudyAddInfoDetailViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 1. 添加
#import "AbstractViewController.h"
#import "GWMyStudyEditViewController.h"

@class GWMyStudyEditViewController;
@interface GWMyStudyAddInfoDetailViewController : AbstractViewController

@property (nonatomic,assign)MyStudyEditType transferStudyEditType;

@end
