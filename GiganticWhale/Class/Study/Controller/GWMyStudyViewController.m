//
//  GWMyStudyViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMyStudyViewController.h"
#import "GWStudyScrollView.h"
#import "GWStudyInfoListModel.h"
#import "GWMyStudyEditViewController.h"

@interface GWMyStudyViewController()<GWStudyScrollViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic,strong)GWStudyScrollView *myScrollView;
@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)UIPanGestureRecognizer *pangesture;

@end

@implementation GWMyStudyViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma pageSetting
-(void)pageSetting{
    [self createMainScrollView];                            // 创建主的ScrollView
    [self createMenuButton];                                // 创建左侧按钮
}

#pragma mark - createMenuButton
-(void)createMenuButton{
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    self.myScrollView = [[GWStudyScrollView alloc] initScrollViewWithBackgound:[UIImage imageNamed:@"giganticWhale"] avatarImage:[UIImage imageNamed:@"giganticWhale"] titleString:@"Study" subtitleString:@"Sub title" buttonTitle:@"Follow" contentHeight:self.view.bounds.size.height];
    
    self.myScrollView.delegate = self;
    self.myScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.myScrollView];
    
    self.pangesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGestureRecognizedWithView:)];
    self.pangesture.delegate = self;
    [self.myScrollView.tableViewMainScrollView addGestureRecognizer:self.pangesture];
}

- (void)panGestureRecognizedWithView:(UIPanGestureRecognizer *)recognizer{
    [[RESideMenu shareInstance] panGestureRecognized:recognizer];
    if (recognizer.state == UIGestureRecognizerStateEnded){
        self.myScrollView.tableViewMainScrollView.scrollEnabled = YES;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.pangesture && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
        CGPoint point = [touch locationInView:gestureRecognizer.view];
        CGFloat point_X = point.x;
        if (point.x > kScreenBounds.size.width){
            point_X = point.x - kScreenBounds.size.width;
        }
        if (point_X < 50.0) {
            self.myScrollView.tableViewMainScrollView.scrollEnabled = NO;
            return YES;
        } else {
            self.myScrollView.tableViewMainScrollView.scrollEnabled = YES;
            return NO;
        }
    }
    return YES;
}

-(void)didselectToDirectUrl:(NSString *)url{
//    GWMyStudyEditViewController *info = [[GWMyStudyEditViewController alloc]init];
//    info.transferStudyType = MyStudyEditTypeItem;
//    [self.navigationController pushViewController:info animated:YES];
//    return;
    
    WebViewController *webViewController = [[WebViewController alloc]init];
    [webViewController webViewControllerWithAddress:url];
    [self.navigationController pushViewController:webViewController animated:YES];
}


-(void)dealloc{
    
}
@end
