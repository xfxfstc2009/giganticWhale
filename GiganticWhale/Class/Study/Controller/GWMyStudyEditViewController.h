//
//  GWMyStudyEditViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 编辑页面
#import "AbstractViewController.h"
#import "GWStudyItemListModel.h"

typedef NS_ENUM(NSInteger,MyStudyEditType) {
    MyStudyEditTypeItem,                    /**< 修改标签*/
    MyStudyEditTypeInfo,                    /**< 修改内容*/
};


@interface GWMyStudyEditViewController : AbstractViewController

@property (nonatomic,assign)MyStudyEditType transferStudyType;          /**< 传入的类型*/
@property (nonatomic,strong)GWStudyItemSingleModel *transferStudyItemModel;           /**< 传入的itemModel*/
@end
