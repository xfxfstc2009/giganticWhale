//
//  GWMyStudyEditViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMyStudyEditViewController.h"

// 【Model】
#import "GWStudyInfoListModel.h"

// 【cell】
#import "GWStudyEditCell.h"

// 【controller】
#import "GWMyStudyAddInfoDetailViewController.h"


@interface GWMyStudyEditViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *myStudyEditTableView;                  /**< tableView*/
@property (nonatomic,strong)NSMutableArray *myStudyEditMutableArr;              /**< 数据源*/

@end

@implementation GWMyStudyEditViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
    __weak typeof(self)weakSelf = self;
    if (self.transferStudyType == MyStudyEditTypeInfo){
        [weakSelf sendRequestToGetInfoWithItem:self.transferStudyItemModel.itemId];
    } else if (self.transferStudyType == MyStudyEditTypeItem){
        [weakSelf sendRequestToGetItemInfo];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferStudyType == MyStudyEditTypeInfo){
        self.barMainTitle = @"信息列表";
    } else {
        self.barMainTitle = @"修改内容";
    }
    
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        GWMyStudyAddInfoDetailViewController *myStudyAddInfo = [[GWMyStudyAddInfoDetailViewController alloc]init];
        [self.navigationController pushViewController:myStudyAddInfo animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.myStudyEditMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.myStudyEditTableView){
        self.myStudyEditTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.myStudyEditTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.myStudyEditTableView.delegate = self;
        self.myStudyEditTableView.dataSource = self;
        self.myStudyEditTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.myStudyEditTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.myStudyEditTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.myStudyEditMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWStudyEditCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if(!cellWithRowOne){
        cellWithRowOne = [[GWStudyEditCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (self.transferStudyType == MyStudyEditTypeInfo){                 // 修改内容
        cellWithRowOne.transferInfoSingleModel = [[self.myStudyEditMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    } else if (self.transferStudyType == MyStudyEditTypeItem){          // 修改专辑
        cellWithRowOne.transferItemSingleModel = [self.myStudyEditMutableArr objectAtIndex:indexPath.row];
    }

    
    return cellWithRowOne;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

#pragma mark - 删除方法
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self)weakSelf = self;

    // 1. 获取当前删除的model
    GWStudyItemSingleModel *studyItemModel = [self.myStudyEditMutableArr objectAtIndex:indexPath.row];
    
    if(studyItemModel.itemId.length){
        [weakSelf sendRequestDeleteWithItemId:studyItemModel.itemId];
    }
    [self.myStudyEditMutableArr removeObjectAtIndex:indexPath.row];// 传入一个位置，将这个位置上的数据删除
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 修改
    GWMyStudyEditViewController *myStudyEditInfoController = [[GWMyStudyEditViewController alloc]init];
    myStudyEditInfoController.transferStudyItemModel = [self.myStudyEditMutableArr  objectAtIndex:indexPath.row];
    myStudyEditInfoController.transferStudyType = MyStudyEditTypeInfo;
    [self.navigationController pushViewController:myStudyEditInfoController animated:YES];
}


#pragma mark - 接口
-(void)sendRequestToGetItemInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_item_list requestParams:nil responseObjectClass:[GWStudyItemListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWStudyItemListModel *studyItemListModel = (GWStudyItemListModel *)responseObject;
            [strongSelf.myStudyEditMutableArr addObjectsFromArray:studyItemListModel.itemList];
            [strongSelf.myStudyEditTableView reloadData];
            
        }
    }];
}

#pragma mark 获取专辑下的信息
-(void)sendRequestToGetInfoWithItem:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"itemId":itemId,@"page":@(1),@"size":@"1000"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_info_list requestParams:params responseObjectClass:[GWStudyInfoListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.myStudyEditMutableArr.count){
                [strongSelf.myStudyEditMutableArr removeAllObjects];
            }
            
            GWStudyInfoListModel *infoListModel = (GWStudyInfoListModel *)responseObject;
            [strongSelf.myStudyEditMutableArr addObject:infoListModel.studyInfo];
            [strongSelf.myStudyEditTableView reloadData];
        }
    }];
}

#pragma mark  删除专辑
-(void)sendRequestDeleteWithItemId:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:study_item_delete requestParams:@{@"itemId":itemId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"删除成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


@end
