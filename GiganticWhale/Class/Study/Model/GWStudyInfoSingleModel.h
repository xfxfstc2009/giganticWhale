//
//  GWStudyInfoSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/24.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWStudyInfoSingleModel <NSObject>


@end

@interface GWStudyInfoSingleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,copy)NSString *details;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *url;


@end
