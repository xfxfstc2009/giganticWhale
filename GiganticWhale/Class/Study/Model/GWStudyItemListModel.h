//
//  GWStudyItemListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/24.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWStudyItemSingleModel.h"

@interface GWStudyItemListModel : FetchModel

@property (nonatomic,strong)NSArray<GWStudyItemSingleModel> *itemList;           /**< 列表信息*/

@end
