//
//  GWStudyInfoListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/24.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWStudyInfoSingleModel.h"
@interface GWStudyInfoListModel : FetchModel

@property (nonatomic,strong)NSArray<GWStudyInfoSingleModel> *studyInfo;

@end
