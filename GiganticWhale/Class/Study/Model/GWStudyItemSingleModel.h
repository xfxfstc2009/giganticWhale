//
//  GWStudyItemSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/24.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWStudyItemSingleModel <NSObject>


@end

@interface GWStudyItemSingleModel : FetchModel

@property (nonatomic,copy)NSString *itemId;             /**< 编号*/
@property (nonatomic,copy)NSString *name;               /**< 名字*/
@property (nonatomic,assign)NSInteger count;            /**< 数量*/

@end
