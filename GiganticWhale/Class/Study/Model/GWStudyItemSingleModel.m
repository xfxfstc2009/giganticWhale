//
//  GWStudyItemSingleModel.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/24.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWStudyItemSingleModel.h"

@implementation GWStudyItemSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"itemId": @"id"};
}

@end
