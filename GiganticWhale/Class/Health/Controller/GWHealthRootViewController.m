//
//  GWHealthRootViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWHealthRootViewController.h"
#import "GWHealthManager.h"
#import "DBSphereView.h"

@interface GWHealthRootViewController(){
}
@property (nonatomic,strong)NSMutableArray *healthInfoMutableArr;
@property (nonatomic,strong)DBSphereView *sphereView;
@property (nonatomic,strong)GWHealthSingleModel *healthDataModel;
@end

@implementation GWHealthRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self getHealthInfoManager];
//        [self createView1];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的健康";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.healthInfoMutableArr = [NSMutableArray array];
    self.healthDataModel = [[GWHealthSingleModel alloc]init];
}

-(void)getHealthInfoManager{
    GWHealthManager *healghManager = [[GWHealthManager alloc]init];
    __weak typeof(self)weakSelf = self;
    [healghManager gethealthMainManagerWithBlock:^(GWHealthSingleModel *healthModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.healthDataModel = healthModel;
        [self createView];
    }];
}

-(void)createView{
    self.sphereView = [[DBSphereView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.width)];
    for (int i = 0 ; i < 6; i++){
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
        
        if(i == 0){
            [btn setTitle:[NSString stringWithFormat:@"总步数:%@",self.healthDataModel.healthStepCount] forState:UIControlStateNormal];
        } else if (i == 1){
            [btn setTitle:[NSString stringWithFormat:@"爬楼层:%@",self.healthDataModel.healthFlightsClimbed] forState:UIControlStateNormal];
        } else if (i == 2){
            [btn setTitle:[NSString stringWithFormat:@"静息能量:%@",self.healthDataModel.healthBasalEnergyBurned] forState:UIControlStateNormal];
        } else if (i == 3){
            [btn setTitle:[NSString stringWithFormat:@"活动能量:%@",self.healthDataModel.healthActiveEnergyBurned] forState:UIControlStateNormal];
        } else if (i == 4){
            [btn setTitle:[NSString stringWithFormat:@"步行跑步距离:%@",self.healthDataModel.healthDistanceWalkingRunning] forState:UIControlStateNormal];
        } else if (i == 5){
            [btn setTitle:[NSString stringWithFormat:@"骑行距离:%@",self.healthDataModel.healthHKQuantityTypeIdentifierDistanceCycling] forState:UIControlStateNormal];
        }
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:24.];
        btn.frame = CGRectMake(0, 0, 200, 40);
        [btn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.healthInfoMutableArr addObject:btn];
        [self.sphereView addSubview:btn];
    }
    [self.sphereView setCloudTags:self.healthInfoMutableArr];
    self.sphereView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.sphereView];
}





- (void)buttonPressed:(UIButton *)btn
{
    [self.sphereView timerStop];
    
    [UIView animateWithDuration:0.3 animations:^{
        btn.transform = CGAffineTransformMakeScale(2., 2.);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            btn.transform = CGAffineTransformMakeScale(1., 1.);
        } completion:^(BOOL finished) {
            [self.sphereView timerStart];
        }];
    }];
}


@end
