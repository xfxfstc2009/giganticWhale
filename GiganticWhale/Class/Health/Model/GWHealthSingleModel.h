//
//  GWHealthSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface GWHealthSingleModel : FetchModel

@property (nonatomic,copy)NSString *healthStepCount;            /**< 计步数*/
@property (nonatomic,copy)NSString *healthFlightsClimbed;       /**< 爬楼层*/
@property (nonatomic,copy)NSString *healthBasalEnergyBurned;    /**< 静息能量*/
@property (nonatomic,copy)NSString *healthActiveEnergyBurned;   /**< 活动能量*/
@property (nonatomic,copy)NSString *healthDistanceWalkingRunning;/**< 步行跑步距离*/
@property (nonatomic,copy)NSString *healthHKQuantityTypeIdentifierDistanceCycling;  /**< 骑行距离*/


@end
