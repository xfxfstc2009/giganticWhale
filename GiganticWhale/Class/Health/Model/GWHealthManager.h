//
//  GWHealthManager.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWHealthSingleModel.h"

@interface GWHealthManager : FetchModel


-(void)gethealthMainManagerWithBlock:(void (^)(GWHealthSingleModel *healthModel))block;

#pragma mark - 授权
-(void)requestAuthorizationWithBlock:(void(^)(BOOL isSuccess))block;

@end
