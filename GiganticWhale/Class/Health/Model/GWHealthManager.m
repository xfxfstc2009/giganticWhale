//
//  GWHealthManager.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWHealthManager.h"
#import <HealthKit/HealthKit.h>
#import <HealthKit/HealthKit.h>
#import <UIKit/UIDevice.h>

#define HKVersion [[[UIDevice currentDevice] systemVersion] doubleValue]
#define CustomHealthErrorDomain @"com.sdqt.healthError"



@interface GWHealthManager()
@property (nonatomic, strong) HKHealthStore *healthStore;
@property (nonatomic,strong)GWHealthSingleModel *healthModel;
@end

@implementation GWHealthManager

#pragma mark - MainManager
-(void)gethealthMainManagerWithBlock:(void (^)(GWHealthSingleModel *healthModel))block{
    self.healthModel = [[GWHealthSingleModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [self healthKitManagerblock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        block(strongSelf.healthModel);
    }];
}

#pragma mark - 1. 判断设备是否支持HealthKit
-(BOOL)healthKitHasSuport{                  //查看healthKit在设备上是否可用，ipad不支持HealthKit
    BOOL hasSuport = NO;
    if(![HKHealthStore isHealthDataAvailable]) {
        hasSuport = NO;
        NSLog(@"设备不支持healthKit");
    } else {
        hasSuport = YES;
    }
    return hasSuport;
}

#pragma mark - 2. 获取信息
-(void)healthKitManagerblock:(void(^)())block{
    if ([self healthKitHasSuport] == NO){
        return;
    }
    
    self.healthStore = [[HKHealthStore alloc] init];
    HKObjectType *stepCount = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    HKObjectType *stepCount1 = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    HKObjectType *stepCount2 = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceCycling];
    HKObjectType *stepCount3 = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBasalEnergyBurned];
    HKObjectType *stepCount4 = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    HKObjectType *stepCount5 = [HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierFlightsClimbed];
    
    NSSet *healthSet = [NSSet setWithObjects:stepCount,stepCount1,stepCount2,stepCount3,stepCount4,stepCount5,nil];
    
    [self.healthStore requestAuthorizationToShareTypes:nil readTypes:healthSet completion:^(BOOL success, NSError * _Nullable error) {
        if (success) {
            //获取步数后我们调用获取步数的方法
            [self readStepWithIndex:0 countWithBlock:^(NSString *count) {
                self.healthModel.healthStepCount = count;
                [self readStepWithIndex:1 countWithBlock:^(NSString *count) {
                self.healthModel.healthFlightsClimbed = count;
                    [self readStepWithIndex:2 countWithBlock:^(NSString *count) {
                        self.healthModel.healthBasalEnergyBurned = count;
                        [self readStepWithIndex:3 countWithBlock:^(NSString *count) {
                            self.healthModel.healthActiveEnergyBurned = count;
                            [self readStepWithIndex:4 countWithBlock:^(NSString *count) {
                                self.healthModel.healthDistanceWalkingRunning = count;
                                [self readStepWithIndex:5 countWithBlock:^(NSString *count) {
                                    self.healthModel.healthHKQuantityTypeIdentifierDistanceCycling = count;
                                    if (block){
                                        block();
                                    }
                                }];
                            }];
                        }];
                    }];
                }];
            }];
        } else {
            NSLog(@"获取步数权限失败");
        }
    }];
}

#pragma mark - 获取步数
- (void)readStepWithIndex:(NSInteger)index countWithBlock:(void(^)(NSString *count))block{
    HKSampleType *sampleType;
      if (index == 0){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierStepCount];
    } else if (index == 1){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceWalkingRunning];
    } else if (index == 2){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierDistanceCycling];
    } else if (index == 3){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierBasalEnergyBurned];
    } else if (index == 4){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    } else if (index == 5){
        sampleType = [HKQuantityType quantityTypeForIdentifier:HKQuantityTypeIdentifierFlightsClimbed];
    }
    
    //NSSortDescriptors用来告诉healthStore怎么样将结果排序。
    NSSortDescriptor *start = [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierStartDate ascending:NO];
    NSSortDescriptor *end = [NSSortDescriptor sortDescriptorWithKey:HKSampleSortIdentifierEndDate ascending:NO];

    HKSampleQuery *sampleQuery = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:nil limit:1 sortDescriptors:@[start,end] resultsHandler:^(HKSampleQuery * _Nonnull query, NSArray<__kindof HKSample *> * _Nullable results, NSError * _Nullable error) {
        //把结果装换成字符串类型
        if (results.count){
            HKQuantitySample *result = results[0];
            HKQuantity *quantity = result.quantity;
            NSString *stepStr = (NSString *)quantity;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (block){
                    block(stepStr);
                }
            }];
        } else {
            block(@"0");
        }
    }];
    //执行查询
    [self.healthStore executeQuery:sampleQuery];
}



#pragma mark 2.设置授权的类别
- (NSArray *)quantityTypeIdentifiers {
    return @[
             // 能量
             HKQuantityTypeIdentifierStepCount,                  // 步计数
             HKQuantityTypeIdentifierDistanceWalkingRunning,     // 距离
             HKQuantityTypeIdentifierDistanceCycling,            // 骑行距离
             HKQuantityTypeIdentifierBasalEnergyBurned,          // 静息能量
             HKQuantityTypeIdentifierActiveEnergyBurned,         // 活动能量
             HKQuantityTypeIdentifierFlightsClimbed,             // 已爬楼层
             ];
}
@end
