//
//  GWLifeShareInstance.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/28.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,menuType) {
    menuType1,                      /**< 平台登录*/
    menuType2,                      /**< 微信登录*/
};


@interface GWLifeShareInstance : NSObject




+ (instancetype)shareInstance;                                  /**< 单利*/

@end
