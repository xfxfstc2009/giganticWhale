//
//  GWLifeShareInstance.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/28.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWLifeShareInstance.h"

@implementation GWLifeShareInstance

+ (instancetype)shareInstance{
    static GWLifeShareInstance *lifeModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lifeModel = [[GWLifeShareInstance alloc] init];
    });
    
    return lifeModel;
}


@end
