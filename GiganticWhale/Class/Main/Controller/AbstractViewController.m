//
//  AbstractViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import <objc/runtime.h>
#import "HomeSliderViewController.h"
#import "GWMusicPlayerViewController.h"                 // 音乐详情


#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:16.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   200

static char buttonActionBlockKey;

@interface AbstractViewController()
@property (nonatomic,strong)UIView *barTitleView;
@property (nonatomic,strong)UILabel *barMainTitleLabel;
@property (nonatomic,strong)UILabel *barSubTitleLabel;
@property (nonatomic, strong) UIVisualEffectView *effectView;
@property (nonatomic, strong) UIVisualEffectView *vibrancyEffectView;
@property (nonatomic, strong) UIView *naviView;                                 /**< 顶部的view */
@end

@implementation AbstractViewController

-(void)dealloc{
    NSLog(@"释放");
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self createBarTitleView];
    [self customerSet];
    [self navigationBarStup];
    [self.navigationController setEnableBackGesture:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark - customer Set
-(void)customerSet{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    if (self != [self.navigationController.viewControllers firstObject]) {
        __weak typeof(self) weakSelf = self;
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_home_slider"] barHltImage:[UIImage imageNamed:@"icon_home_slider"] action:^{
            [[RESideMenu shareInstance] presentLeftMenuViewController];
        }];
    }
}


#pragma mark - Nav Bar
- (void)createBarTitleView {
    _barTitleView = [[UIView alloc] initWithFrame:CGRectZero];
    _barTitleView.backgroundColor = [UIColor clearColor];
    _barTitleView.clipsToBounds = YES;
    
    _barMainTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barMainTitleLabel.backgroundColor = [UIColor clearColor];
    _barMainTitleLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    _barMainTitleLabel.textColor = [UIColor whiteColor];
    _barMainTitleLabel.text = @"";
    [_barTitleView addSubview:_barMainTitleLabel];
    
    _barSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barSubTitleLabel.backgroundColor = [UIColor clearColor];
    _barSubTitleLabel.font = BAR_SUB_TITLE_FONT;
    _barSubTitleLabel.textColor = [UIColor whiteColor];
    [_barTitleView addSubview:_barSubTitleLabel];
    
    self.navigationItem.titleView = _barTitleView;
}

- (void)setBarMainTitle:(NSString *)barMainTitle {
    _barMainTitle = [barMainTitle copy];
    _barMainTitleLabel.text = _barMainTitle;
    
    [_barMainTitleLabel sizeToFit];
    CGRect rect = _barMainTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barMainTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)setBarSubTitle:(NSString *)barSubTitle {
    _barSubTitle = [barSubTitle copy];
    _barSubTitleLabel.text = _barSubTitle;
    
    [_barSubTitleLabel sizeToFit];
    CGRect rect = _barSubTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barSubTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)resetBarTitleView {
    CGSize mainTitleSize = _barMainTitleLabel.bounds.size;
    CGSize subTitleSize = _barSubTitleLabel.bounds.size;
    
    CGFloat titleViewHeight = mainTitleSize.height + subTitleSize.height;
    if (_barMainTitle.length && _barSubTitle.length)  {
        titleViewHeight += BAR_TITLE_PADDING_TOP;
    }
    
    _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
    if ([GWTool isEmpty:_barSubTitle]) {
        if (titleViewHeight < 44) {
            titleViewHeight = 44;
        }
        _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, 22);
    } else {
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, _barMainTitleLabel.bounds.size.height*0.5);
        _barSubTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, titleViewHeight - _barSubTitleLabel.bounds.size.height*0.5);
    }
}


#pragma mark - CustomerView
- (void)hidesBackButton {
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

- (UIButton *)leftBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor hexChangeFloat:@"f5bbb7"] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor hexChangeFloat:@"f5bbb7"] forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    
    return button;
}

- (void)actionButtonClicked:(UIButton *)sender {
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &buttonActionBlockKey);
    actionBlock();
}


#pragma mark navigationBar
-(void)navigationBarStup{
    //去掉navigationBar底部线
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"白"] renderSize:CGSizeMake(kScreenBounds.size.width, 64)] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = BACKGROUND_VIEW_COLOR;
}

-(void)createCustomerNavView{
    self.naviView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    [self.view addSubview:self.naviView];
    // 添加模糊效果
    self.effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    self.effectView.userInteractionEnabled = NO;
    self.effectView.frame                  = self.naviView.bounds;
    self.effectView.userInteractionEnabled = YES;
    [self.naviView addSubview:self.effectView];
    
    // 需要与作用的effectView的效果一致
    _vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIVibrancyEffect effectForBlurEffect:(UIBlurEffect *)self.effectView.effect]];
    _vibrancyEffectView.frame = self.effectView.bounds;
    [self.effectView.contentView addSubview:self.vibrancyEffectView];
    
    // Back button.
    UIImage  *image      = [UIImage imageNamed:@"backIconTypeTwo"];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 64)];
    backButton.center    = CGPointMake(20, self.naviView.middleY);
    [backButton setImage:image forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popSelf) forControlEvents:UIControlEventTouchUpInside];
    [backButton.imageView setContentMode:UIViewContentModeCenter];
    [self.naviView addSubview:backButton];
    
    // Title label.
    UILabel *headlinelabel      = [UILabel new];
    headlinelabel.font          = [UIFont HeitiSCWithFontSize:20.f];
    headlinelabel.textAlignment = NSTextAlignmentCenter;
    headlinelabel.textColor     = [UIColor colorWithRed:0.329  green:0.329  blue:0.329 alpha:1];
    headlinelabel.text          = self.title;
    [headlinelabel sizeToFit];
    headlinelabel.center        = self.naviView.middlePoint;
    [_vibrancyEffectView.contentView addSubview:backButton];
    [_vibrancyEffectView.contentView addSubview:headlinelabel];
}


-(void)popSelf{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - 登录动画
-(void)loginSuccessManager{
    [self.headerImageView uploadImageWithURL:[AccountModel sharedAccountModel].avatar placeholder:nil callback:^(UIImage *image) {
        self.imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
        self.imageUploadProgress.radius = LCFloat(100);
        self.imageUploadProgress.progressBorderThickness = -10;
        self.imageUploadProgress.trackColor = [UIColor blackColor];
        self.imageUploadProgress.progressColor = [UIColor clearColor];
        self.imageUploadProgress.imageToUpload = [GWTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
        [self.imageUploadProgress show];
        [self performSelector:@selector(showSliderWithAnimation) withObject:nil afterDelay:.5f];
    }];
}

#pragma mark - 显示
-(void)showSliderWithAnimation{
    [[RESideMenu shareInstance] presentLeftMenuViewController];
    [self performSelector:@selector(loginSuccessAnimationWithProView:) withObject:self.imageUploadProgress afterDelay:.3f];
}

#pragma mark - Animation Login
-(void)loginSuccessAnimationWithProView:(TNSexyImageUploadProgress *)progressView{
    HomeSliderViewController *leftViewController = (HomeSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
    // 2. 寻找到view 的位置
    
    //1. 找到对应cell 位置
    GWSliderLeftTableViewCell *cellWithRowOne = (GWSliderLeftTableViewCell *)[leftViewController.sliderTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    // 2. 寻找到view 的位置
    CGRect convertFrame = [cellWithRowOne convertRect:cellWithRowOne.iconImageView.frame toView:self.view.window];
    progressView.imageView.backgroundColor = [UIColor clearColor];
    
    // 3. 创建一个副本图像
    UIImageView *animationImageView = [[UIImageView alloc]init];
    animationImageView.image = progressView.imageView.image;
    animationImageView.frame = progressView.imageView.frame;
    [self.view.window addSubview:animationImageView];
    
    [progressView outroAnimation];
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = convertFrame;
    } completion:^(BOOL finished) {
        animationImageView.hidden = YES;
        [animationImageView removeFromSuperview];
        [cellWithRowOne loginManager];
//        [UIView animateWithDuration:2 animations:^{
//            [leftViewController loginSuccess];
//        }];
    }];
}


#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)())handler{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        handler();
    } else {
        if ([GWTool userDefaultGetWithKey:AUTO_Login_User].length){
            [[AccountModel sharedAccountModel] loginManager:[GWTool userDefaultGetWithKey:AUTO_Login_User] password:nil loginType:[[GWTool userDefaultGetWithKey:AUTO_Login_Type] integerValue] nick:nil icon:nil successBlock:^{
                handler();
            }];
        }
    }
}

@end
