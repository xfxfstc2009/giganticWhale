//
// REFrostedViewController.h
// RESideMenu
//
// Copyright (c) 2013-2014 Roman Efimov (https://github.com/romaonthego)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "UIViewController+RESideMenu.h"

#ifndef IBInspectable
#define IBInspectable
#endif

@protocol RESideMenuDelegate;

@interface RESideMenu : UIViewController <UIGestureRecognizerDelegate>

#if __IPHONE_8_0
@property (strong, readwrite, nonatomic) IBInspectable NSString *contentViewStoryboardID;
@property (strong, readwrite, nonatomic) IBInspectable NSString *leftMenuViewStoryboardID;
@property (strong, readwrite, nonatomic) IBInspectable NSString *rightMenuViewStoryboardID;
#endif

@property (strong, readwrite, nonatomic) UIViewController *contentViewController;
@property (strong, readwrite, nonatomic) UIViewController *leftMenuViewController;
@property (strong, readwrite, nonatomic) UIViewController *rightMenuViewController;
@property (weak, readwrite, nonatomic) id<RESideMenuDelegate> delegate;

@property (assign, readwrite, nonatomic) NSTimeInterval animationDuration;                      /**< 动画时间*/
@property (strong, readwrite, nonatomic) UIImage *backgroundImage;                              /**< 背景图片*/
@property (assign, readwrite, nonatomic) BOOL panGestureEnabled;                                /**< 拖动手势*/
@property (assign, readwrite, nonatomic) BOOL panFromEdge;                                      /**< 是否边缘拖动*/
@property (assign, readwrite, nonatomic) NSUInteger panMinimumOpenThreshold;                    /**< 最小的打开*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL interactivePopGestureRecognizerEnabled; /**< 互动的流行手势识别功能*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL fadeMenuView;                       /**< 淡出菜单视图*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL scaleContentView;                   /**< 尺度内容视图*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL scaleBackgroundImageView;           /**< 尺度背景图像视图*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL scaleMenuView;                      /**< 尺度菜单视图*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL contentViewShadowEnabled;           /**< 阴影*/
@property (strong, readwrite, nonatomic) IBInspectable UIColor *contentViewShadowColor;         /**< 阴影颜色*/
@property (assign, readwrite, nonatomic) IBInspectable CGSize contentViewShadowOffset;          /**< 阴影尺寸*/
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewShadowOpacity;        /**< 阴影不透明度*/
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewShadowRadius;         /**< 阴影半径*/
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewFadeOutAlpha;         /**< 淡出透明度*/
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewScaleValue;           /**< 缩放尺度*/
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewInLandscapeOffsetCenterX;
@property (assign, readwrite, nonatomic) IBInspectable CGFloat contentViewInPortraitOffsetCenterX;
@property (assign, readwrite, nonatomic) IBInspectable CGFloat parallaxMenuMinimumRelativeValue;
@property (assign, readwrite, nonatomic) IBInspectable CGFloat parallaxMenuMaximumRelativeValue;
@property (assign, readwrite, nonatomic) IBInspectable CGFloat parallaxContentMinimumRelativeValue;
@property (assign, readwrite, nonatomic) IBInspectable CGFloat parallaxContentMaximumRelativeValue;
@property (assign, readwrite, nonatomic) CGAffineTransform menuViewControllerTransformation;
@property (assign, readwrite, nonatomic) IBInspectable BOOL parallaxEnabled;                            /**< 视差*/
@property (assign, readwrite, nonatomic) IBInspectable BOOL bouncesHorizontally;                        /**< 弹性*/
@property (assign, readwrite, nonatomic) UIStatusBarStyle menuPreferredStatusBarStyle;
@property (assign, readwrite, nonatomic) IBInspectable BOOL menuPrefersStatusBarHidden;

- (id)initWithContentViewController:(UIViewController *)contentViewController
             leftMenuViewController:(UIViewController *)leftMenuViewController
            rightMenuViewController:(UIViewController *)rightMenuViewController;
- (void)presentLeftMenuViewController;
- (void)presentRightMenuViewController;
- (void)hideMenuViewController;
- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated;
- (void)panGestureRecognized:(UIPanGestureRecognizer *)recognizer;
@end

@protocol RESideMenuDelegate <NSObject>

@optional
- (void)sideMenu:(RESideMenu *)sideMenu didRecognizePanGesture:(UIPanGestureRecognizer *)recognizer;
- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController;
- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController;
- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController;
- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController;

@end
