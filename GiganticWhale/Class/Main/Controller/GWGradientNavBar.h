//
//  GWGradientNavBar.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWGradientNavBar : UINavigationBar

- (void)setBarTintGradientColors:(NSArray *)barTintGradientColors;

@end
