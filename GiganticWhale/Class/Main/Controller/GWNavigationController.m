//
//  GWNavigationController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWNavigationController.h"
#import "GWGradientNavBar.h"

@interface GWNavigationController()<UINavigationControllerDelegate>

@property(nonatomic, retain)UIView *alphaView;

@end

@implementation GWNavigationController

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    self = [super initWithRootViewController:rootViewController];
    if (self){
        [self smart];
    }
    return self;
}


-(void)smart{
    UIColor *firstColor = [UIColor hexChangeFloat:@"b721ff"];
    UIColor *secondColor = [UIColor hexChangeFloat:@"21d4fd"];

    NSArray *colors = [NSArray arrayWithObjects:(id)firstColor.CGColor, (id)secondColor.CGColor, nil];
    
    [[GWGradientNavBar appearance] setBarTintGradientColors:colors];
    [[self navigationBar] setTranslucent:NO];
}

@end
