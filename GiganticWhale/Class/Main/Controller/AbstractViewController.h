//
//  AbstractViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TNSexyImageUploadProgress.h"
#import "GWImageView.h"
#import "GiganticWhale-Swift.h"
@class AppDelegate;
@interface AbstractViewController : UIViewController
@property (nonatomic,copy,nullable)NSString *barMainTitle;
@property (nonatomic,copy,nullable)NSString *barSubTitle;
@property (nonatomic,strong,nullable)TNSexyImageUploadProgress *imageUploadProgress;             /**< progress*/
@property (nonatomic,strong,nullable)GWImageView *headerImageView;                               /**< 创建view */

- (void)hidesBackButton;
-(void)createCustomerNavView;

- (nullable UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock;

- (nullable UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock;


-(void)loginSuccessManager;

#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(nullable void(^)())handler;

@end
