//
//  GWMusicPlayerViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^musicPopManagerBlock)();

@interface GWMusicPlayerViewController : AbstractViewController

@property (nonatomic,strong)GWImageView *musicImageView;            /**< 音乐播放器*/
@property (nonatomic,assign)CGRect transferRect;                    /**< 上个页面传递过来的rect*/

@property (nonatomic,copy)musicPopManagerBlock musicPopManagerBlock;

@end
