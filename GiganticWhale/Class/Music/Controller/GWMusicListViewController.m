//
//  GWMusicListViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicListViewController.h"

#import "GWMusicListHeaderView.h"
#import "KMScrollingHeaderView.h"
#import "GWMusicAuthorCell.h"
#import "GWMusicListModel.h"
#import "GWMusicInfoCell.h"
#import "MusicPlayerManager.h"

// test
#import "GWMusicPlayerMethod.h"

@interface GWMusicListViewController()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>
@property (nonatomic,strong) KMScrollingHeaderView* scrollingHeaderView;            /**< 头部*/
@property (nonatomic,strong) GWMusicListHeaderView *musicList;                      /**< 音乐列表*/
@property (nonatomic,strong)GWAblumSingleModel *tempAblumSingleModel;               /**< 临时使用的专辑Model*/
@property (nonatomic,strong)NSMutableArray *musicArr;                               /**< 音乐数组*/
@end

@implementation GWMusicListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)setupNavbarButtons{
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    
    buttonBack.frame = CGRectMake(LCFloat(15), 20 , 44, 44);
    [buttonBack setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    [buttonBack buttonWithBlock:^(UIButton *button) {
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    
    [self.view addSubview:buttonBack];
    
    self.scrollingHeaderView.navbarView = [self createNavBar];
}

-(UIView *)createNavBar{
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *smartView = [[UIVisualEffectView alloc] initWithEffect:blur];
    smartView.alpha = .8f;
    smartView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
    [self.view addSubview:smartView];
    
    
    
    // 1. 创建回退按钮
//    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [leftButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
//    __weak typeof(self)weakSelf = self;
//    [leftButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        [[RESideMenu shareInstance] presentLeftMenuViewController];
//    }];
//    leftButton.frame = CGRectMake(LCFloat(15), 20, 44, 44);
//    [smartView addSubview:leftButton];
    
//    // 2. 创建label
//    UILabel *fixedLabel = [[UILabel alloc]init];
//    fixedLabel.backgroundColor = [UIColor clearColor];
//    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
//    fixedLabel.frame = CGRectMake(CGRectGetMaxX(leftButton.frame), 20, kScreenBounds.size.width - 2 * (LCFloat(14) + 44), 44);
//    fixedLabel.textAlignment = NSTextAlignmentCenter;
//    fixedLabel.textColor = [UIColor colorWithCustomerName:@"蓝"];
//    fixedLabel.text = @"My Music";
//    [smartView addSubview:fixedLabel];
    
    return smartView;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self pageSetting];
    
    [self setupDetailsPageView];        // 1. 创建主题view
    [self setupNavbarButtons];          // 2. 创建返回按钮
    [self setupMusicList];              // 3. 创建头部信息
    
    // 获取通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(musicStatusNotification:) name:GWMusicNotification object:nil];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.musicArr = [NSMutableArray array];
}

-(void)setupMusicList{
    __weak typeof(self)weakSelf = self;
    self.musicList = [[GWMusicListHeaderView alloc]initWithFrame:CGRectMake(0, 60, kScreenBounds.size.width, 240) withBlock:^(GWAblumSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 更新背景图片
        [strongSelf.scrollingHeaderView.imageView uploadMusicAblumListImageWithURL1:singleModel.img placeholder:nil callback:NULL];
        // 2. 修改信息
        [strongSelf imgMaskWithAblumSingleModel:singleModel];
    }];
    
    // 点击方法
    [self.musicList playerAblumClickManager:^(GWAblumSingleModel *ablumSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WebViewController *webViewController = [[WebViewController alloc]init];
        [webViewController webViewControllerWithAddress:@"https://mr.baidu.com/9ad570e"];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
    
    // 滚动方法
    [self.musicList playerAblumScrollManager:^(GWAblumSingleModel *ablumSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf imgMaskWithAblumSingleModel:ablumSingleModel];
    }];
    
    self.musicList.backgroundColor = [UIColor clearColor];
    [self.scrollingHeaderView addSubview:self.musicList];
}

-(void)imgMaskWithAblumSingleModel:(GWAblumSingleModel *)ablumSingleModel{
    _tempAblumSingleModel = ablumSingleModel;
    
    __weak typeof(self)weakSelf = self;
    [self.scrollingHeaderView.imageView uploadMusicAblumListImageWithURL1:ablumSingleModel.img placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.scrollingHeaderView maskHeaderImage:image];
    }];
    
    // 刷新tableView
    [self sendRequestToGetMusicListWithAblumId:ablumSingleModel.ablumId];
}

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset{
    if (scrollOffset < 0) {
        self.scrollingHeaderView.imageView.transform = CGAffineTransformMakeScale(1 - (scrollOffset / (self.scrollingHeaderView.navbarViewFadingOffset - [GWMusicAuthorCell calculationCellHeight])), 1 - (scrollOffset / (self.scrollingHeaderView.navbarViewFadingOffset - [GWMusicAuthorCell calculationCellHeight])));
    } else {
        self.scrollingHeaderView.imageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.scrollingHeaderView.imageView.orgin_y = - scrollOffset;
    }
}

- (void)setupDetailsPageView {
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.scrollingHeaderView];
    
    self.scrollingHeaderView.navbarViewFadingOffset = 200;
    
    [self.scrollingHeaderView reloadScrollingHeader];
}


#pragma mark - PageSetting
-(void)pageSetting{
    self.barMainTitle = @"Music";
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.musicArr.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){                    // 专辑信息
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWMusicAuthorCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWMusicAuthorCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferAblumSingleModel = self.tempAblumSingleModel;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWMusicInfoCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWMusicInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferMusicSingleModel = [self.musicArr objectAtIndex:(indexPath.row - 1)];
        
        [cellWithRowTwo loadContentWithIndex:indexPath.row];
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        return;
    }
    for (UITableViewCell *cell in tableView.visibleCells){
        if ([cell isKindOfClass:[GWMusicInfoCell class]]){
            GWMusicInfoCell *indexCell = (GWMusicInfoCell *)cell;
            [indexCell stopMusicProgressAnimation];
        }
    }
    
    GWMusicSingleModel *musicSingleModel = [self.musicArr lastObject];
    if (![[GWMusicPlayerMethod sharedLocationManager].musicMutableArr containsObject:musicSingleModel]){    // 【表示换了一个专辑了】
        [[GWMusicPlayerMethod sharedLocationManager] playWithMusicList:self.musicArr playIndex:(indexPath.row - 1)];
        
        // showAnimation
        GWMusicInfoCell *cell = (GWMusicInfoCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:([GWMusicPlayerMethod sharedLocationManager].currentTrackIndex + 1) inSection:0]];
        [cell showMusicProgressAnimation];
        return;
    }
    
    if (([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPaused) || ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerIdle)){                                  // 【如果暂停状态，就播放】
        [[GWMusicPlayerMethod sharedLocationManager] actionPlayPause];
    } else {
        if ([GWMusicPlayerMethod sharedLocationManager].currentTrackIndex == (indexPath.row - 1)){      // 暂停
            if (![GWMusicPlayerMethod sharedLocationManager].streamer){
                [[GWMusicPlayerMethod sharedLocationManager] playWithMusicList:self.musicArr playIndex:(indexPath.row - 1)];
            } else {
                 [[GWMusicPlayerMethod sharedLocationManager] actionPlayPause];
            }
        } else {
            [[GWMusicPlayerMethod sharedLocationManager] playWithMusicList:self.musicArr playIndex:(indexPath.row - 1)];
        }
    }
    
    [self showMusicAnimation];
}

#pragma mark - 显示当前的音乐流动画
-(void)showMusicAnimation{
    NSInteger index = -1;
    for (int i = 0 ; i < self.musicArr.count;i++){
        GWMusicSingleModel *musicSingleModel = [self.musicArr objectAtIndex:i];
        if ([musicSingleModel.musicId isEqualToString:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel.musicId]){
            index = i;
            break;
        }
    }
    if (index == -1 ){
        return;
    }
    
    GWMusicInfoCell *cell = (GWMusicInfoCell *)[self.scrollingHeaderView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(index + 1) inSection:0]];
    [cell showMusicProgressAnimation];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return [GWMusicAuthorCell calculationCellHeight];
    } else {
        return [GWMusicInfoCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.scrollingHeaderView.tableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.row > 0){
            if (indexPath.row == 1){
                separatorType = SeparatorTypeHead;
            } else if (indexPath.row == self.musicArr.count){
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
    
    // 动画
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;
    if (indexPath.row >= 1){
        GWMusicInfoCell *musicInfoCell = (GWMusicInfoCell *)cell;
        [musicInfoCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
    }
}



#pragma mark - KMScrollingHeaderViewDelegate

- (void)detailsPage:(KMScrollingHeaderView *)detailsPageView headerImageView:(GWImageView *)imageView{
    [imageView uploadImageWithURL:[AccountModel sharedAccountModel].laungch placeholder:nil callback:NULL];
}



#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    if ([scrollView isKindOfClass:[UITableView class]]){
        self.musicList.orgin_y = 70 - point.y;
    }
}


#pragma mark - 获取当前的音乐
-(void)sendRequestToGetMusicListWithAblumId:(NSString *)ablumId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"page":@(1),@"size":@(1000),@"ablumId":ablumId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:music_list requestParams:dic responseObjectClass:[GWMusicListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWMusicListModel *musicList = (GWMusicListModel *)responseObject;
        if (strongSelf.musicArr.count){
            [strongSelf.musicArr removeAllObjects];
        }
        [strongSelf.musicArr addObjectsFromArray:musicList.musicList];
        [strongSelf.scrollingHeaderView.tableView reloadData];
        
        // 吧当前的cell  变成动画
        if ([GWMusicPlayerMethod sharedLocationManager].streamer){
            if (([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPaused) || ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerIdle)){
            } else {
                for (UITableViewCell *cell in strongSelf.scrollingHeaderView.tableView.visibleCells){
                    if ([cell isKindOfClass:[GWMusicInfoCell class]]){
                        GWMusicInfoCell *indexCell = (GWMusicInfoCell *)cell;
                        [indexCell stopMusicProgressAnimation];
                    }
                }
                
                [strongSelf showMusicAnimation];
            }
        }
    }];
}

-(void)musicStatusNotification:(NSNotification *)note{
    if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){ // 正在播放中

    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPaused){   // 暂停
    
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerIdle){   // 等待任务
    
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerFinished){   // 完成
        __weak typeof(self)weakSelf = self;
        for (UITableViewCell *cell in self.scrollingHeaderView.tableView.visibleCells){
            if ([cell isKindOfClass:[GWMusicInfoCell class]]){
                GWMusicInfoCell *indexCell = (GWMusicInfoCell *)cell;
                [indexCell stopMusicProgressAnimation];
            }
        }
        
        [[GWMusicPlayerMethod sharedLocationManager] actionNextWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
      
            [strongSelf showMusicAnimation];
        }];

    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerBuffering){   //  缓冲
        for (UITableViewCell *cell in self.scrollingHeaderView.tableView.visibleCells){
            if ([cell isKindOfClass:[GWMusicInfoCell class]]){
                GWMusicInfoCell *indexCell = (GWMusicInfoCell *)cell;
                [indexCell stopMusicProgressAnimation];
            }
        }
        [self showMusicAnimation];
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerError){   //  缓冲
        
    }
}
@end
