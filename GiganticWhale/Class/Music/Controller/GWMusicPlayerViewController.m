//
//  GWMusicPlayerViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMusicPlayerViewController.h"
#import "GWMusicPlayerMethod.h"
#import "GWSlider.h"
#import "GWMusicListModel.h"
#import <objc/runtime.h>

@interface GWMusicPlayerViewController()<GWSliderDataSource,GWMusicSingleModelDelegate>
@property (nonatomic,strong)UIView *musiceImageBgView;              /**< 音乐背景图片*/
@property (nonatomic,strong)UILabel *minLabel;                      /**< 当前进度*/
@property (nonatomic,strong)UILabel *maxLabel;                      /**< 最大进度*/
@property (nonatomic,strong)GWSlider *slider;                       /**< 进度条*/
@property (nonatomic,strong)UIButton *leftButton;                   /**< 上一首歌*/
@property (nonatomic,strong)UIButton *startButton;                  /**< 开始按钮*/
@property (nonatomic,strong)UIButton *rightButton;                  /**< 下一首歌曲*/
@property (nonatomic,strong)GWImageView *bgImageView;               /**< 背景图片*/
@property (nonatomic,strong)UIVisualEffectView *backVisual;         /**< 模糊背景*/
@property (nonatomic,strong)UIView *playerBgView;                   /**< 播放页面增加背景图片*/

// body
@property (nonatomic,strong)UIView *bodyView;                       /**< 中间的view*/

// nav
@property (nonatomic,strong)UIView *navBgView;                      /**< navigationBgView*/
@property (nonatomic,strong)UIButton *navLeftButton;                /**< 返回按钮*/
@property (nonatomic,strong)UILabel *musicNameLabel;                /**< 音乐名称*/
@property (nonatomic,strong)UILabel *musicAuthorLabel;              /**< 音乐作者名字*/
@property (nonatomic,strong)UIButton *navRightButton;               /**< nav右边按钮*/
@property (nonatomic,strong)UIView *leftlineView;
@property (nonatomic,strong)UIView *rightLineView;

@property (nonatomic,retain)CABasicAnimation *basicAnimation;       /**< 动画*/

// flag
@property (nonatomic,assign)BOOL userIsSwip;                        /**< 判断用户是否拖拉*/
@property (nonatomic,strong)NSMutableArray *musicMutableArr;
@property (nonatomic,strong)GWMusicSingleModel *currentMusicModel;
@end

@implementation GWMusicPlayerViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)setupWithPlayerInfo{
    [GWMusicPlayerMethod sharedLocationManager].delegate = self;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(musicStatusNotification:) name:GWMusicNotification object:nil];
    __weak typeof(self)weakSelf = self;
    [weakSelf updateUIInfoWithMusicSingleInfo:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel];
    
    [self musicStatusNotification:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupWithPlayerInfo];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createViewBackgroundView];            // 1. 创建底部的view
    [self createNav];                           // 2. 创建头部
    [self createPlayerView];                    // 3. 创建底部
    [self createView];                          // 4. 创建中部
    [self animataionWithImageView];             // 5. 开始动画
    __weak typeof(self)weakSelf = self;
    if (![GWMusicPlayerMethod sharedLocationManager].musicMutableArr){
        [weakSelf sendRequestToGetMusicList];       // 6. 获取列表
    } else {
        [weakSelf updateUIInfoWithMusicSingleInfo:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel];
        if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){
            self.startButton.enabled = YES;
        }
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"音乐";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.musicMutableArr = [NSMutableArray array];
}

#pragma mark - 1 专辑模糊背景
-(void)createViewBackgroundView{
    self.bgImageView = [[GWImageView alloc]init];
    self.bgImageView.backgroundColor = [UIColor clearColor];
    self.bgImageView.frame = self.view.bounds;
    [self.view addSubview:self.bgImageView];
    
    self.backVisual = [[UIVisualEffectView alloc]initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    self.backVisual.frame = self.view.bounds;
    self.backVisual.alpha = 1;
    [self.bgImageView addSubview:self.backVisual];
}

#pragma mark - 2. 创建头部
-(void)createNav{
    // 1. bgView
    self.navBgView = [[UIView alloc]init];
    self.navBgView.backgroundColor = [UIColor clearColor];
    self.navBgView.frame = CGRectMake(0, - LCFloat(100), kScreenBounds.size.width, LCFloat(100));
    [self.view addSubview:self.navBgView];
    
    // 2. leftNavButton
    self.navLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.navLeftButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [weakSelf.navLeftButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationWithPop];
    }];
    self.navLeftButton.frame = CGRectMake(0, 20, LCFloat(60), LCFloat(44));
    [self.navLeftButton setBackgroundImage:[UIImage imageNamed:@"player_btn_close_normal"] forState:UIControlStateNormal];
    [self.navLeftButton setBackgroundImage:[UIImage imageNamed:@"player_btn_close_highlight"] forState:UIControlStateHighlighted];
    [self.navBgView addSubview:self.navLeftButton];
    
    // 2. rightNavButton
    self.navRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.navRightButton.backgroundColor = [UIColor clearColor];
    [weakSelf.navRightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
    self.navRightButton.frame = CGRectMake(kScreenBounds.size.width - 0 - LCFloat(60), self.navLeftButton.orgin_y, self.navLeftButton.size_width, self.navLeftButton.size_height);
    [self.navRightButton setBackgroundImage:[UIImage imageNamed:@"player_btn_more_normal"] forState:UIControlStateNormal];
    [self.navRightButton setBackgroundImage:[UIImage imageNamed:@"player_btn_more_highlight"] forState:UIControlStateHighlighted];
    [self.navBgView addSubview:self.navRightButton];
    
    // 3. 创建title
    self.musicNameLabel = [[UILabel alloc]init];
    self.musicNameLabel.textAlignment = NSTextAlignmentCenter;
    self.musicNameLabel.font = [UIFont systemFontOfCustomeSize:20];
    [self.musicNameLabel.font boldFont];
    self.musicNameLabel.frame = CGRectMake(CGRectGetMaxX(self.navLeftButton.frame), self.navLeftButton.orgin_y, self.navRightButton.orgin_x - CGRectGetMaxX(self.navLeftButton.frame),self.navLeftButton.size_height);
    self.musicNameLabel.textColor = [UIColor whiteColor];
    [self.navBgView addSubview:self.musicNameLabel];
    
    // 4. 创建作者
    self.musicAuthorLabel = [[UILabel alloc]init];
    self.musicAuthorLabel.textAlignment = NSTextAlignmentCenter;
    self.musicAuthorLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.musicAuthorLabel.textColor = [UIColor whiteColor];
    [self.navBgView addSubview:self.musicAuthorLabel];
    self.musicAuthorLabel.backgroundColor = [UIColor clearColor];
    
    self.leftlineView = [self createAuthorLine];
    [self.navBgView addSubview:self.leftlineView];
    
    self.rightLineView = [self createAuthorLine];
    [self.navBgView addSubview:self.rightLineView];
}

#pragma mark - 创建底部按钮
-(void)createPlayerView{
    // 1. 创建播放按钮背景
    self.playerBgView = [[UIView alloc]init];
    self.playerBgView.backgroundColor = [UIColor clearColor];
    self.playerBgView.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, LCFloat(180));
    [self.view addSubview:self.playerBgView];
    
    // 5. 创建播放按钮
    self.startButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.startButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(64)) / 2., 0 , LCFloat(64), LCFloat(64));
    __weak typeof(self)weakSelf = self;
    [self.startButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.startButton.selected = !strongSelf.startButton.selected;
        [[GWMusicPlayerMethod sharedLocationManager] actionPlayPause];
    }];
    [self.startButton setBackgroundImage:[UIImage imageNamed:@"player_btn_play_normal"] forState:UIControlStateNormal];
    [self.startButton setBackgroundImage:[UIImage imageNamed:@"player_btn_pause_normal"] forState:UIControlStateSelected];
    [self.playerBgView addSubview:self.startButton];
    
    // 5. 创建上一首按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    self.leftButton.frame = CGRectMake(self.startButton.orgin_x - LCFloat(64) - LCFloat(30), self.startButton.orgin_y + (self.startButton.size_height - LCFloat(64)) / 2., LCFloat(64), LCFloat(64));
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf musicPrevious];
    }];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"player_btn_pre_normal"] forState:UIControlStateNormal];
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"player_btn_pre_highlight"] forState:UIControlStateSelected];
    [self.playerBgView addSubview:self.leftButton];
    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    self.rightButton.frame = CGRectMake(CGRectGetMaxX(self.startButton.frame) + LCFloat(30), self.leftButton.orgin_y, self.leftButton.size_width, self.leftButton.size_height);
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf musicNext];
    }];
    [self.rightButton setBackgroundImage:[UIImage imageNamed:@"player_btn_next_normal"] forState:UIControlStateNormal];
    [self.rightButton setBackgroundImage:[UIImage imageNamed:@"player_btn_next_highlight"] forState:UIControlStateSelected];
    [self.playerBgView addSubview:self.rightButton];
    
    // 3. 创建进度条
    self.slider = [[GWSlider alloc]initWithFrame:CGRectMake(LCFloat(30), CGRectGetMaxY(self.startButton.frame) + LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(30) , 20)];
    self.slider.popUpViewCornerRadius = 0.6f;
    [self.slider setMaxFractionDigitsDisplayed:0];
    self.slider.dataSource = self;
    self.slider.popUpViewColor = [UIColor colorWithHue:0.55 saturation:0.8 brightness:0.9 alpha:0.7];
    self.slider.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
    self.slider.textColor = [UIColor colorWithHue:0.55 saturation:1.0 brightness:0.5 alpha:1];
    [self.slider addTarget:self action:@selector(playProgressChange:) forControlEvents:UIControlEventValueChanged];
    [self.slider addTarget:self action:@selector(playProgressChangeEnd:) forControlEvents:UIControlEventTouchUpInside];
    [self.playerBgView addSubview:self.slider];

    
    // 2. 创建左侧文字
    self.minLabel = [self createProgressLabel];
    self.minLabel.textAlignment = NSTextAlignmentLeft;
    self.minLabel.frame = CGRectMake(self.slider.orgin_x, CGRectGetMaxY(self.slider.frame) + LCFloat(10), 100, [NSString contentofHeight:self.minLabel.font]);
    self.minLabel.text = @"";
    [self.playerBgView addSubview:self.minLabel];
    
    // 4. 创建右侧文字
    self.maxLabel = [self createProgressLabel];
    self.maxLabel.textAlignment = NSTextAlignmentRight;
    self.maxLabel.frame = CGRectMake(CGRectGetMaxX(self.slider.frame) - 100, self.minLabel.orgin_y , 100, [NSString contentofHeight:self.maxLabel.font]);
    self.maxLabel.text = @"";
    [self.playerBgView addSubview:self.maxLabel];
}


#pragma mark - 4. 创建中间
-(void)createView{
    
    // 2. 创建播放背景图片
    self.musiceImageBgView = [[UIImageView alloc]init];
    self.musiceImageBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:.33];
    self.musiceImageBgView.frame = self.musiceImageBgView.frame = CGRectMake(LCFloat(20), self.navBgView.size_height + LCFloat(10), kScreenBounds.size.width - 2 * LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(20));
    self.musiceImageBgView.layer.cornerRadius = self.musiceImageBgView.size_height / 2.;
    self.musiceImageBgView.clipsToBounds = YES;
    self.musiceImageBgView.alpha = 0;
    [self.view addSubview:self.musiceImageBgView];
    
    // 创建专辑封面
    self.musicImageView = [[GWImageView alloc]init];
    self.musicImageView.backgroundColor = [UIColor clearColor];
    self.musicImageView.image = [GWTool imageByComposingImage:[UIImage imageNamed:@"demo_1.jpg"] withMaskImage:[UIImage imageNamed:@"round"]];
    self.musicImageView.frame = self.transferRect;
    [self.view addSubview:self.musicImageView];
}

#pragma mark - OtherManager
- (CABasicAnimation *)basicAnimation {
    if (_basicAnimation == nil) {
        self.basicAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        //旋转一圈时长
        self.basicAnimation.duration = 10;
        //开始动画的起始位置
        self.basicAnimation.fromValue = [NSNumber numberWithInt:0];
        self.basicAnimation.byValue = [NSNumber numberWithInt:2];
        //M_PI是180度
        self.basicAnimation.toValue = [NSNumber numberWithInt:M_PI * 2];
        //动画重复次数
        [self.basicAnimation setRepeatCount:NSIntegerMax];
        //播放完毕之后是否逆向回到原来位置
        [self.basicAnimation setAutoreverses:NO];
        //是否叠加（追加）动画效果
        [self.basicAnimation setCumulative:YES];
        //停止动画，速度设置为0
        self.musicImageView.layer.speed = 1;
        //    self.ImageView.layer.speed = 0;
        [self.musicImageView.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    }
    return _basicAnimation;
}

-(UILabel *)createProgressLabel{
    UILabel *progressLabel = [[UILabel alloc]init];
    progressLabel.backgroundColor = [UIColor clearColor];
    progressLabel.font = [UIFont systemFontOfCustomeSize:12.];
    progressLabel.textColor = [UIColor whiteColor];
    progressLabel.textAlignment = NSTextAlignmentCenter;
    return progressLabel;
}

-(UIView *)createAuthorLine{
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor whiteColor];
    lineView.frame = CGRectMake(0, 0, LCFloat(50), 1);
    return lineView;
}





#pragma mark - Action
// 4. 进度条显示
- (void)playProgressChange:(GWSlider *)slider {
    self.userIsSwip = YES;
}

-(void)playProgressChangeEnd:(GWSlider *)slider{
    self.userIsSwip = NO;
    [[GWMusicPlayerMethod sharedLocationManager] actionSliderProgressWithTime:slider.value];
    [self.slider setValue:slider.value animated:YES];
}

- (NSString *)slider:(GWSlider *)slider stringForValue:(float)value{
    return [self updateDateTextWithTime:value];
}


#pragma mark -Animation
-(void)animataionWithImageView{
    [UIView animateWithDuration:.4f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        // 1. 【头部】
        self.navBgView.orgin_y = 0;
        // 2. 【中间】
        self.musiceImageBgView.alpha = .33;
        // 3. 【底部】
        self.playerBgView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(180), kScreenBounds.size.width, LCFloat(180));
        
        self.musicImageView.frame = CGRectMake(self.musiceImageBgView.orgin_x + LCFloat(10),self.musiceImageBgView.orgin_y + LCFloat(10), self.musiceImageBgView.size_width - 2 * LCFloat(10), self.musiceImageBgView.size_width - 2 * LCFloat(10));
    } completion:^(BOOL finished) {
        
        [self basicAnimation];//开始动画
    }];
}


-(void)animationWithPop{
//    self.musicImageView.layer.cornerRadius = self.musicImageView.size_height / 2.;
//    self.musicImageView.clipsToBounds = NO;
    if (self.musicPopManagerBlock){
        self.musicPopManagerBlock();
    }
    
    [UIView animateWithDuration:.4f animations:^{
        // 1. 头部
        self.navBgView.orgin_y = 0 - self.navBgView.size_height;
        // 2. 中间
        self.musiceImageBgView.alpha = 0;
        // 3. 底部
        self.playerBgView.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, LCFloat(180));
        // 4.
        self.musicImageView.frame = CGRectMake(331, 115, 67, 67);
//        self.transferRect;
    } completion:^(BOOL finished) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}

#pragma mark - playerManager
// 播放/暂停
-(void)musicPlay{

}

// 下一首
-(void)musicNext{
    __weak typeof(self)weakSelf = self;
    [[GWMusicPlayerMethod sharedLocationManager] actionNextWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf updateUIInfoWithMusicSingleInfo:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel];
    }];
}
// 上一首
-(void)musicPrevious{
    [[GWMusicPlayerMethod sharedLocationManager] actionPrevious];
}

-(void)musicProgress:(NSInteger)currentprogress duration:(NSInteger)duration{
    if (self.userIsSwip){
        return;
    }
    self.minLabel.text = [self updateDateTextWithTime:currentprogress];
    self.maxLabel.text = [self updateDateTextWithTime:duration];
    [self.slider setValue:currentprogress animated:YES];
    // 设置最大值
    self.slider.maximumValue = duration;
}


-(NSString *)updateDateTextWithTime:(double)time{           // 6.设置文字
    NSDate *pastDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *pastTime = [NSDate getTimeByDate:pastDate byProgress: time];
    return pastTime;
}

-(void)musicStatusNotification:(NSNotification *)note{
    if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){ // 正在播放中
        self.startButton.selected = YES;
        self.slider.maximumValue = [GWMusicPlayerMethod sharedLocationManager].streamer.duration;
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPaused){   // 暂停
        self.startButton.selected = NO;
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerIdle){   // 等待任务
        self.startButton.selected = NO;
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerFinished){   // 完成
        __weak typeof(self)weakSelf = self;
        [[GWMusicPlayerMethod sharedLocationManager] actionNextWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf updateUIInfoWithMusicSingleInfo:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel];
        }];
        self.startButton.selected = YES;
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerBuffering){   //  缓冲
        self.startButton.selected = YES;
        [self updateUIInfoWithMusicSingleInfo:[GWMusicPlayerMethod sharedLocationManager].currentMusicModel];
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerError){   //  缓冲
        
    }
}

#pragma mark - Interface
-(void)sendRequestToGetMusicList{
    GWMusicListModel *musicListModel = [[GWMusicListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:music_list requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.musicMutableArr addObjectsFromArray:musicListModel.musicList];
            [strongSelf musicPlay];
        } else {
            NSLog(@"失败啦");
        }
    }];
}

// 更新UI
-(void)updateUIInfoWithMusicSingleInfo:(GWMusicSingleModel *)musicModel{
    self.musicNameLabel.text = musicModel.name;
    self.musicAuthorLabel.text = musicModel.authorInfo.name;
    [self uploadWithFrame];

    __weak typeof(self)weakSelf = self;
    [self.bgImageView uploadImageWithMusicrURL:musicModel.img placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.bgImageView.image = image;
    }];
    
    GWImageView *imageView = [[GWImageView alloc]init];
    imageView.frame = CGRectMake(1110, 1110, self.musicImageView.size_width, self.musicImageView.size_height);
    [self.view addSubview:imageView];
    
    [imageView uploadImageWithMusicrURL:musicModel.img placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.musicImageView.image = [GWTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
    }];
}

#pragma mark 修改frame 
-(void)uploadWithFrame{
    CGSize musicAuthorSize = [self.musicAuthorLabel.text sizeWithCalcFont:self.musicAuthorLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.musicAuthorLabel.font])];
    self.musicAuthorLabel.frame = CGRectMake((kScreenBounds.size.width - musicAuthorSize.width) / 2., CGRectGetMaxY(self.musicNameLabel.frame), musicAuthorSize.width, [NSString contentofHeight:self.musicNameLabel.font]);
    self.leftlineView.frame = CGRectMake(self.musicAuthorLabel.orgin_x - LCFloat(10) - self.leftlineView.size_width, self.musicAuthorLabel.orgin_y + (self.musicAuthorLabel.size_height / 2.), self.leftlineView.size_width, self.leftlineView.size_height);
    self.rightLineView.frame = CGRectMake(CGRectGetMaxX(self.musicAuthorLabel.frame) + LCFloat(10), self.leftlineView.orgin_y, self.leftlineView.size_width, self.leftlineView.size_height);
    
}
@end
