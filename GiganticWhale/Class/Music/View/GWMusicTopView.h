//
//  GWMusicTopView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWMusicTopView : UIView

@property (nonatomic,strong)GWImageView *flagImageView;             /**< */

-(void)actionClickBlock:(void(^)())block;
-(void)startAnimation;
-(void)stopAnimation;

-(void)imageUploadWithBlock:(void(^)())block;
// 显示的时候弹簧动画
-(void)showWithAnimation;
// 转圈动画
-(void)animationWithRound;
@end
