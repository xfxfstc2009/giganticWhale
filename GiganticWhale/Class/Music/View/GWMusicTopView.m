//
//  GWMusicTopView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicTopView.h"
#import <objc/runtime.h>

@interface GWMusicTopView()
@property (nonatomic,retain)CABasicAnimation *basicAnimation;       /**< 动画*/

@end

static char homeTopViewKey;
@implementation GWMusicTopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithFramge:frame];
    }
    return self;
}

-(void)createViewWithFramge:(CGRect)frame{
    self.flagImageView = [[GWImageView alloc]init];
    self.flagImageView.frame = CGRectMake(LCFloat(5), LCFloat(5), frame.size.height - 2 * LCFloat(5), frame.size.height - 2 * LCFloat(5));
//    self.flagImageView.image = [UIImage imageNamed:@"sub_image.jpg"];
    [self addSubview:self.flagImageView];
    
    // 添加手势
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCLick)];
    [self addGestureRecognizer:tapGesture];
}

#pragma mark - SET
-(void)imageUploadWithBlock:(void(^)())block{
    if ([GWMusicPlayerMethod sharedLocationManager].streamer){
        self.flagImageView.image = [GWMusicPlayerMethod sharedLocationManager].imageHeaderImageView.image;
    }
}


#pragma mark - KVO
-(void)animationWithRound{
    [self createBasicAnimation];
}

#pragma mark - OtherManager
- (CABasicAnimation *)createBasicAnimation {
    if (_basicAnimation == nil) {
        self.basicAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        self.basicAnimation.duration = 30;
        self.basicAnimation.fromValue = [NSNumber numberWithInt:0];
        self.basicAnimation.toValue = [NSNumber numberWithInt:M_PI * 2];
        [self.basicAnimation setRepeatCount:NSIntegerMax];
        [self.basicAnimation setAutoreverses:NO];
        [self.basicAnimation setCumulative:YES];
        self.flagImageView.layer.speed = 1;
        [self.flagImageView.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    } else {
        
        [self.flagImageView.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    }
    return _basicAnimation;
}

-(void)startAnimation{
    CFTimeInterval pausedTime = [self.flagImageView.layer timeOffset];
    self.flagImageView.layer.speed = 1.0;
    self.flagImageView.layer.timeOffset = 0.0;
    self.flagImageView.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.flagImageView.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.flagImageView.layer.beginTime = timeSincePause;
}

-(void)stopAnimation{
    CFTimeInterval pausedTime = [self.flagImageView.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.flagImageView.layer.speed = 0.0;
    self.flagImageView.layer.timeOffset = pausedTime;
}



-(void)showWithAnimation{
    self.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:.2f usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:NULL];
}

//-(void)musicStatusNotification:(NSNotification *)note{
//    self.flagImageView.image = [MusicPlayerManager sharedLocationManager].imageHeaderImageView.image;
//}


#pragma mark - Action
-(void)actionCLick{
    void(^block)() = objc_getAssociatedObject(self, &homeTopViewKey);
    if (block){
        block();
    }
}

-(void)actionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &homeTopViewKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
