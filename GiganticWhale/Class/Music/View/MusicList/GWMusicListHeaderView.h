//
//  GWMusicListHeaderView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWAblumListModel.h"

@interface GWMusicListHeaderView : UIView

-(instancetype)initWithFrame:(CGRect)frame withBlock:(void(^)(GWAblumSingleModel *singleModel))block;

@property (nonatomic,strong)GWImageView *transferImageView;

-(void)playerAblumClickManager:(void(^)(GWAblumSingleModel *ablumSingleModel))block;
-(void)playerAblumScrollManager:(void (^)(GWAblumSingleModel *))block;
@end
