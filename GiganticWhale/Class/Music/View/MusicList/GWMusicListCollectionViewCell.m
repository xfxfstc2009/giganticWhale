//
//  GWMusicListCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicListCollectionViewCell.h"

@interface GWMusicListCollectionViewCell()

@end

@implementation GWMusicListCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.musicAblumImgView = [[GWImageView alloc]init];
    self.musicAblumImgView.frame = CGRectMake(0, 0, LCFloat(100), LCFloat(100));
    self.musicAblumImgView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.musicAblumImgView.layer.borderWidth = 3;
    self.musicAblumImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.musicAblumImgView.layer.cornerRadius = 5;
    self.musicAblumImgView.clipsToBounds = YES;
    [self addSubview:self.musicAblumImgView];
}

-(void)setTransferAblumSingleModel:(GWAblumSingleModel *)transferAblumSingleModel{
    _transferAblumSingleModel = transferAblumSingleModel;
    [self.musicAblumImgView uploadMusicAblumListImageWithURL:transferAblumSingleModel.img placeholder:nil callback:NULL];
}

@end
