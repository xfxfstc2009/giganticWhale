//
//  GWMusicListHeaderView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicListHeaderView.h"
#import "LineLayout.h"
#import "UIView+MaterialDesign.h"
#import "GWMusicListCollectionViewCell.h"
#import <objc/runtime.h>
#import "GWAblumListModel.h"

static char musicHeaderClickKey;                    // 点击
static char musicHeaderScrollKey;                   // 滚动
static char musicInfoKey;
@interface GWMusicListHeaderView()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)GWImageView *bgImageView;
@property (nonatomic,strong)NSMutableArray *musicMutableArr;
@property (nonatomic,strong)UICollectionView *musicCollectionView;           /**< 首页collectionView*/

@end

@implementation GWMusicListHeaderView

-(instancetype)initWithFrame:(CGRect)frame withBlock:(void(^)(GWAblumSingleModel *singleModel))block{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &musicInfoKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self arrayWithInit];
        [self createCollectionView];
        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestGetAblumList];
    } return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.musicMutableArr = [NSMutableArray array];
}

#pragma mark - createCollectionView
-(void)createCollectionView{
    LineLayout *layout = [[LineLayout alloc] init];
    
    self.musicCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    self.musicCollectionView.backgroundColor = [UIColor clearColor];
    self.musicCollectionView.showsVerticalScrollIndicator = NO;
    self.musicCollectionView.delegate = self;
    self.musicCollectionView.dataSource = self;
    self.musicCollectionView.showsHorizontalScrollIndicator = NO;
    self.musicCollectionView.scrollsToTop = YES;
    self.musicCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.musicCollectionView];
    
    [self.musicCollectionView registerClass:[GWMusicListCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.musicMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GWMusicListCollectionViewCell *cell = (GWMusicListCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    
    cell.transferAblumSingleModel = [self.musicMutableArr objectAtIndex:indexPath.row];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(LCFloat(120),LCFloat(120));
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // 1. 下载图片
    GWAblumSingleModel *ablumSingleModel = [self.musicMutableArr objectAtIndex:indexPath.row];
    void(^block)(GWAblumSingleModel *ablumSingleModel) = objc_getAssociatedObject(self, &musicHeaderClickKey);
    if (block){
        block(ablumSingleModel);
    }
}


// 滚动视图减速完成，滚动将停止时，调用该方法。一次有效滑动，只执行一次。
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    NSInteger index = point.x / (kScreenBounds.size.width / 3.33);
    GWAblumSingleModel *ablumSingleModel;
    if (index > self.musicMutableArr.count - 1){
        ablumSingleModel = [self.musicMutableArr lastObject];
    } else {
        ablumSingleModel = [self.musicMutableArr objectAtIndex:index];
    }

    void(^block)(GWAblumSingleModel *ablumSingleModel) = objc_getAssociatedObject(self, &musicHeaderScrollKey);
    if (block){
        block(ablumSingleModel);
    }
}


#pragma mark - block
-(void)playerAblumClickManager:(void(^)(GWAblumSingleModel *ablumSingleModel))block{
    objc_setAssociatedObject(self, &musicHeaderClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)playerAblumScrollManager:(void (^)(GWAblumSingleModel *))block{
    objc_setAssociatedObject(self, &musicHeaderScrollKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - 获取专辑名称 
-(void)sendRequestGetAblumList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:music_AblumList requestParams:@{@"page":@"1",@"size":@"100"} responseObjectClass:[GWAblumListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWAblumListModel *listModel = (GWAblumListModel *)responseObject;
            [strongSelf.musicMutableArr addObjectsFromArray:listModel.ablumList];
            [strongSelf.musicCollectionView reloadData];
            
            // 刷新背景
            GWAblumSingleModel *singleModel = [listModel.ablumList objectAtIndex:0];
            void(^block)(GWAblumSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &musicInfoKey);
            if (block){
                block(singleModel);
            }
        }
    }];
}


@end
