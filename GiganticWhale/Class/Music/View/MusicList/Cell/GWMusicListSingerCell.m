//
//  GWMusicListSingerCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicListSingerCell.h"

@interface GWMusicListSingerCell()
@property (nonatomic,strong)GWImageView *singerView;            /**< 作者*/
@property (nonatomic,strong)UILabel *singerLabel;               /**< 演唱者label*/
@property (nonatomic,strong)UIButton *baiduButton;              /**< 跳转到百度*/

@end

@implementation GWMusicListSingerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.singerView = [[GWImageView alloc]init];
    
}

@end
