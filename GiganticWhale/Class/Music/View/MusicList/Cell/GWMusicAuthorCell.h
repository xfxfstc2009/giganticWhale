//
//  GWMusicAuthorCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 音乐作者cell 
#import <UIKit/UIKit.h>
#import "GWAblumSingleModel.h"
@interface GWMusicAuthorCell : UITableViewCell

@property (nonatomic,strong)GWAblumSingleModel *transferAblumSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;
+(CGFloat)calculationCellHeight;
@end
