//
//  GWMusicInfoCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicInfoCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"
#import "GWMusicWaveView.h"

@interface GWMusicInfoCell()
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *authorNameLabel;
@property (nonatomic,strong)GWMusicWaveView *musicWaveView;         /**< 音乐波浪*/

@end

@implementation GWMusicInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.nameLabel];
    
    self.authorNameLabel = [[UILabel alloc]init];
    self.authorNameLabel.backgroundColor = [UIColor clearColor];
    self.authorNameLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.authorNameLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.authorNameLabel];
    
    // 创建音乐波浪
    self.musicWaveView = [[GWMusicWaveView alloc]init];
    self.musicWaveView.backgroundColor = [UIColor clearColor];
    self.musicWaveView.pillarColor = Main_Color;
    self.musicWaveView.pillarWidth = 4;
    self.musicWaveView.hidden = YES;
    [self addSubview:self.musicWaveView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferMusicSingleModel:(GWMusicSingleModel *)transferMusicSingleModel{
    _transferMusicSingleModel = transferMusicSingleModel;
    
    self.nameLabel.text = transferMusicSingleModel.name;
    self.nameLabel.frame = CGRectMake(LCFloat(11), LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeight:self.nameLabel.font]);
    
    
    NSString *author = transferMusicSingleModel.authorInfo.name;
    self.authorNameLabel.text = author;
    self.authorNameLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeight:self.authorNameLabel.font]);
    
    self.musicWaveView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(22), (self.transferCellHeight - LCFloat(22)) / 2. , LCFloat(22), LCFloat(22));
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(5);
    return cellHeight;
}


-(void)loadContentWithIndex:(NSInteger)index{
    NSString *fullStirng = [NSString stringWithFormat:@"%02ld. %@", (long)index, self.transferMusicSingleModel.name];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont HeitiSCWithFontSize:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.nameLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.authorNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.authorNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}


#pragma mark 飘动过去Animation
- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.nameLabel.transform =  CGAffineTransformMakeTranslation(kScreenWidth, 0);
    self.authorNameLabel.transform =  CGAffineTransformMakeTranslation(kScreenWidth, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.nameLabel.transform = CGAffineTransformIdentity;
        self.authorNameLabel.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - 显示动画
-(void)showMusicProgressAnimation{
    [self.musicWaveView startAnimation];
    self.musicWaveView.hidden = NO;
}

#pragma mark - 隐藏动画
-(void)stopMusicProgressAnimation{
    [self.musicWaveView stopAnimation];
    self.musicWaveView.hidden = YES;
}
@end
