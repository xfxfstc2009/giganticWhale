//
//  GWMusicAuthorCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicAuthorCell.h"
#import "PDLabel.h"
@interface GWMusicAuthorCell()
@property (nonatomic,strong)PDLabel *ablumLabel;            /**< 专辑信息*/
@property (nonatomic,strong)UILabel *countLabel;            /**< 歌曲数量label*/
@property (nonatomic,strong)UILabel *endLabel;              /**< 发行时间*/
@property (nonatomic,strong)UIView *lineView1;
@property (nonatomic,strong)UIView *lineView2;
@end

@implementation GWMusicAuthorCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 1. 创建专辑名称
    self.ablumLabel = [[PDLabel alloc]init];
    self.ablumLabel.backgroundColor = [UIColor clearColor];
    self.ablumLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    self.ablumLabel.textAlignment = NSTextAlignmentCenter;
    self.ablumLabel.glowInColor = [UIColor colorWithCustomerName:@"白"];
    self.ablumLabel.glowInSize = LCFloat(10);
    self.ablumLabel.glowOutColor = [UIColor colorWithCustomerName:@"白"];
    self.ablumLabel.glowOutSize = LCFloat(5);
    self.ablumLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.ablumLabel.glowShow = YES;
    [self addSubview:self.ablumLabel];
    
    // 2. 创建countlabel
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    self.countLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.countLabel];
    
    // 3. 创建结束时间
    self.endLabel = [[UILabel alloc]init];
    self.endLabel.backgroundColor = [UIColor clearColor];
    self.endLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.endLabel.textAlignment = NSTextAlignmentCenter;
    self.endLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [self addSubview:self.endLabel];
    
    // 4. 创建line
    self.lineView1 = [[UIView alloc]init];
    self.lineView1.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView1];
    
    // 4.1
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView2];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferAblumSingleModel:(GWAblumSingleModel *)transferAblumSingleModel{
    _transferAblumSingleModel = transferAblumSingleModel;
    
    // 1. 创建专辑名称
    self.ablumLabel.text = transferAblumSingleModel.name;
    CGSize contentSize = [self.ablumLabel.text sizeWithCalcFont:self.ablumLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.ablumLabel.font])];
    self.ablumLabel.frame = CGRectMake((kScreenBounds.size.width - contentSize.width) / 2., 0, contentSize.width, [NSString contentofHeight:self.ablumLabel.font] + LCFloat(5) * 2);
    
    // 2. 创建line
    self.lineView1.frame = CGRectMake(self.ablumLabel.orgin_x - LCFloat(11) - LCFloat(40), self.ablumLabel.center_y, LCFloat(40), .5f);
    
    // 2.1 line2
    self.lineView2.frame = CGRectMake(CGRectGetMaxX(self.ablumLabel.frame) + LCFloat(11), self.lineView1.orgin_y, LCFloat(40), .5f);
    
    // 3.1创建数量
    self.countLabel.text = [NSString stringWithFormat:@"歌曲数量:%li",(long)transferAblumSingleModel.count];
    self.countLabel.frame = CGRectMake(0, CGRectGetMaxY(self.ablumLabel.frame), kScreenBounds.size.width, [NSString contentofHeight:self.countLabel.font]);
    
    // 4.创建时间
    self.endLabel.text = [NSDate getTimeAblum:transferAblumSingleModel.beginTime];
    self.endLabel.frame = CGRectMake(0, CGRectGetMaxY(self.countLabel.frame), kScreenBounds.size.width, [NSString contentofHeight:self.endLabel.font]);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(5);
    return cellHeight;
}

@end
