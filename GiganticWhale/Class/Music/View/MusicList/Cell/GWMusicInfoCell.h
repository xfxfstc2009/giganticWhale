//
//  GWMusicInfoCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWMusicSingleModel.h"

@interface GWMusicInfoCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)GWMusicSingleModel *transferMusicSingleModel;

-(void)loadContentWithIndex:(NSInteger)index;
+(CGFloat)calculationCellHeight;

- (void)startAnimationWithDelay:(CGFloat)delayTime ;            // 动画

-(void)showMusicProgressAnimation;
-(void)stopMusicProgressAnimation;
@end
