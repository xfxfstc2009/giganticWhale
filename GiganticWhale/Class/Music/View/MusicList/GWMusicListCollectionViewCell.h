//
//  GWMusicListCollectionViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWAblumSingleModel.h"
@interface GWMusicListCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)GWImageView *musicAblumImgView;
@property (nonatomic,strong) GWAblumSingleModel *transferAblumSingleModel;      /**< 传递专辑信息*/
@end
