//
//  GWAblumSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWAblumSingleModel <NSObject>

@end

@interface GWAblumSingleModel : FetchModel

@property (nonatomic,copy)NSString *author_id;          /**< 作者编号*/
@property (nonatomic,assign)NSTimeInterval beginTime;   /**< 开始时间*/
@property (nonatomic,assign)NSInteger count;            /**< 数量*/
@property (nonatomic,copy)NSString *ablumId;            /**< 专辑编号*/
@property (nonatomic,copy)NSString *name;               /**< 专辑名称*/
@property (nonatomic,copy)NSString *img;                /**< 专辑封面*/
@property (nonatomic,copy)NSString *desc;               /**< 详情*/


@end
