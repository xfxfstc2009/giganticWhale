//
//  GWAblumSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAblumSingleModel.h"

@implementation GWAblumSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ablumId": @"id",@"desc":@"description"};
}

@end
