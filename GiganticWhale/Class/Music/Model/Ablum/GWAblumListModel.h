//
//  GWAblumListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWAblumSingleModel.h"

@interface GWAblumListModel : FetchModel

@property (nonatomic,strong)NSArray<GWAblumSingleModel> *ablumList;

@end
