//
//  GWMusicAuthorInfoModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/23.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface GWMusicAuthorInfoModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *desc;

@end
