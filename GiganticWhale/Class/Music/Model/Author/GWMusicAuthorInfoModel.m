//
//  GWMusicAuthorInfoModel.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/23.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicAuthorInfoModel.h"

@implementation GWMusicAuthorInfoModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc": @"description"};
}

@end
