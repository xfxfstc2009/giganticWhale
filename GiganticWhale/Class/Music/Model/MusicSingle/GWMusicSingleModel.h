//
//  GWMusicSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "DOUAudioFile.h"
#import "GWMusicAuthorInfoModel.h"

@protocol GWMusicSingleModel <NSObject>

@end

@interface GWMusicSingleModel : FetchModel<DOUAudioFile>

@property (nonatomic,assign)NSTimeInterval create_time;             /**< 创建时间*/
@property (nonatomic,copy)NSString *musicId;                        /**< 音乐编号*/
@property (nonatomic,copy)NSString *img;                            /**< 图片*/
@property (nonatomic,assign)BOOL islike;                            /**< 判断是否喜欢*/
@property (nonatomic,copy)NSString *lrc;                            /**< lrc*/
@property (nonatomic,copy)NSString *music;
@property (nonatomic,copy)NSString *mv;                             /**< mv的路径*/
@property (nonatomic,copy)NSString *name;                           /**< 音乐名称*/
@property (nonatomic,strong) GWMusicAuthorInfoModel *authorInfo;

@property (nonatomic, strong) NSURL *audioFileURL;

@property (nonatomic,copy)NSString *audioFileHost;

@end
