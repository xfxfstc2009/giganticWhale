//
//  GWMusicListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWMusicSingleModel.h"
@interface GWMusicListModel : FetchModel

@property (nonatomic,strong)NSArray <GWMusicSingleModel> * musicList;

@end
