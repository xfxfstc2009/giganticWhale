//
//  MusicPlayerManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/21.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "MusicPlayerManager.h"
#import <objc/runtime.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MusicPlayerManager()
@property (nonatomic,strong)NSTimer *musicTimer;        /**< 音乐计时器*/

@property (nonatomic,assign)NSInteger currentTrackIndex;           /**< 当前播放的第几首*/

@end
typedef void(^musicUIUpdateManager)(GWMusicSingleModel *musicSingleModel);
static char musicUpdateKey;

static void *kStatusKVOKey = &kStatusKVOKey;
static void *kDurationKVOKey = &kDurationKVOKey;
static void *kBufferingRatioKVOKey = &kBufferingRatioKVOKey;

@implementation MusicPlayerManager

+ (MusicPlayerManager *)sharedLocationManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(void)playWithmusicListArr:(NSArray *)playerListArr{
    self.musicMutableArr = [NSMutableArray arrayWithArray:playerListArr];
    self.musicTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    NSLog(@"声音%.2f",[DOUAudioStreamer volume]);
    [self resetStreamer];
}



#pragma mark - 取消音乐流
- (void)cancelStreamer {
    if (self.streamer != nil) {
        [self.streamer pause];
        [self.streamer removeObserver:self forKeyPath:@"status"];
        [self.streamer removeObserver:self forKeyPath:@"duration"];
        [self.streamer removeObserver:self forKeyPath:@"bufferingRatio"];
        self.streamer = nil;
    }
}

#pragma mark - 重置音乐流
-(void)resetStreamer{
//    [self cancelStreamer];          // 取消
//    if (![self.musicMutableArr count]){
//        NSLog(@"没有数据啦");
//    } else {
//        GWMusicSingleModel *musicSingleModel = [self.musicMutableArr objectAtIndex:self.currentTrackIndex];
////        // 2. 获取当前的歌曲
////        __weak typeof(self)weakSelf = self;
////        [weakSelf sendRequestToGetSingleMusicInfoWithMusicId:musicSingleModel.musicId withSuccessBlock:^(GWMusicSingleModel *musicSingleModel) {
////            if (!weakSelf){
////                return ;
////            }
////            __strong typeof(weakSelf)strongSelf = weakSelf;
//            if (musicSingleModel.url.length){
//                NSString *url = [NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,musicSingleModel.music];;
//                musicSingleModel.url = url;
//                musicSingleModel.audioFileURL = [NSURL URLWithString:[GWTool replaceSpace:url]];
//                self.streamer = [DOUAudioStreamer streamerWithAudioFile:musicSingleModel];
//                // 增加观察者
//                [self.streamer addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:kStatusKVOKey];
//                [self.streamer addObserver:self forKeyPath:@"duration" options:NSKeyValueObservingOptionNew context:kDurationKVOKey];
//                [self.streamer addObserver:self forKeyPath:@"bufferingRatio" options:NSKeyValueObservingOptionNew context:kBufferingRatioKVOKey];
//                
//                [self.streamer play];
//                
//                [self updateBufferingStatus];
//                [self setupHintForStreamerWithModel:musicSingleModel];
//            }
////        } failBlock:NULL];
//    }
}

#pragma mark - 装载流
- (void)setupHintForStreamerWithModel:(GWMusicSingleModel *)musicSingleModel {
    NSUInteger nextIndex = self.currentTrackIndex + 1;
    if (nextIndex >= [self.musicMutableArr count]) {
        nextIndex = 0;
    }
    
    [DOUAudioStreamer setHintWithAudioFile:musicSingleModel];
}

- (void)updateBufferingStatus {
    NSLog(@"%@",[NSString stringWithFormat:@"Received %.2f/%.2f MB (%.2f %%), Speed %.2f MB/s", (double)[_streamer receivedLength] / 1024 / 1024, (double)[_streamer expectedLength] / 1024 / 1024, [_streamer bufferingRatio] * 100.0, (double)[_streamer downloadSpeed] / 1024 / 1024]);
    
    if ([_streamer bufferingRatio] >= 1.0) {
        NSLog(@"sha256: %@", [_streamer sha256]);
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == kStatusKVOKey) {
        [self performSelector:@selector(updateStatus)  onThread:[NSThread mainThread] withObject:nil  waitUntilDone:NO];
    } else if (context == kDurationKVOKey) {
        [self performSelector:@selector(timerAction:) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
    } else if (context == kBufferingRatioKVOKey) {
        [self performSelector:@selector(updateBufferingStatus) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - 进度
- (void)timerAction:(id)timer {
    if (self.delegate){
        if (self.streamer.duration == 0.0){
            [self.delegate musicProgress:0 duration:0];
        } else {
            [self.delegate musicProgress:self.streamer.currentTime duration:self.streamer.duration];
        }
    }
}

#pragma mark 更新当前状态
- (void)updateStatus {
    NSDictionary *resultDic = [NSDictionary dictionary];
    if (self.streamer.status == DOUAudioStreamerPlaying){
        resultDic = @{@"status":@"playing"};
    } else if (self.streamer.status == DOUAudioStreamerPaused){
        resultDic = @{@"status":@"paused"};
    } else if (self.streamer.status == DOUAudioStreamerIdle){
        resultDic = @{@"status":@"idle"};
    } else if (self.streamer.status == DOUAudioStreamerFinished){
        resultDic = @{@"status":@"finished"};
        [[MusicPlayerManager sharedLocationManager] actionNext];
    } else if (self.streamer.status == DOUAudioStreamerBuffering){
        resultDic = @{@"status":@"buffering"};
    } else if (self.streamer.status == DOUAudioStreamerError){
        resultDic = @{@"status":@"error"};
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:GWMusicNotification object:nil userInfo:resultDic];
}


//- (void)viewWillDisappear:(BOOL)animated
//{
//    [_timer invalidate];
//    [_streamer stop];
//    [self _cancelStreamer];
//    
//    [super viewWillDisappear:animated];
//}

#pragma mark - 开始 暂停
- (void)actionPlayPause{
    if ([self.streamer status] == DOUAudioStreamerPaused ||
        [self.streamer status] == DOUAudioStreamerIdle) {
        [self.streamer play];
    } else {
        [self.streamer pause];
    }
}

#pragma mark - 下一首
- (void)actionNext{
    if (++self.currentTrackIndex >= [self.musicMutableArr count]) {
        self.currentTrackIndex = 0;
    }
    
    [self resetStreamer];
}

#pragma mark - 上一首
-(void)actionPrevious{
    if (--self.currentTrackIndex < 1){
        self.currentTrackIndex = self.musicMutableArr.count - 1;
    }
    [self resetStreamer];
}

#pragma mark - 停止
- (void)actionStop {
    [self.streamer stop];
}

#pragma mark - 修改进度
- (void)actionSliderProgressWithTime:(NSTimeInterval)currentTime{
    [self.streamer setCurrentTime:currentTime];
}

#pragma mark - 获取当前音乐 
-(void)sendRequestToGetSingleMusicInfoWithMusicId:(NSString *)musicId withSuccessBlock:(void (^)(GWMusicSingleModel *musicSingleModel))success failBlock:(void (^)())failBlock {
    GWMusicSingleModel *musicSingleModel = [[GWMusicSingleModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:music_Single requestParams: @{@"musicId":musicId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.currentMusicSingleModel = musicSingleModel;
            [strongSelf setLockScreenNowPlayingInfoWithModel:musicSingleModel andImage:nil];
            success(musicSingleModel);
            void (^block) (GWMusicSingleModel *musicSingleModel) = objc_getAssociatedObject(self, &musicUpdateKey);
            if (block){
                block(musicSingleModel);
            }
        } else {
            if (failBlock){
                failBlock();
            }
        }
    }];
}

#pragma mark - 上个页面去更新界面
-(void)updateMusicInfoWithBlock:(void (^)(GWMusicSingleModel *musicSingleModel))block{
    objc_setAssociatedObject(self, &musicUpdateKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 音乐后台播放
-(void)musicWithBackPlayer{
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];
}

//设置锁屏状态，显示的歌曲信息
- (void)setLockScreenNowPlayingInfoWithModel:(GWMusicSingleModel *)musicSingleModel andImage:(UIImage *)image{
//    //更新锁屏时的歌曲信息
//    if (NSClassFromString(@"MPNowPlayingInfoCenter")) {
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//        
//        [dict setObject:musicSingleModel.name forKey:MPMediaItemPropertyTitle];
//        [dict setObject:musicSingleModel.authorInfo.name forKey:MPMediaItemPropertyArtist];
//        [dict setObject:musicSingleModel.ablum.length?musicSingleModel.ablum:@"巨鲸" forKey:MPMediaItemPropertyAlbumTitle];
//        
//        self.imageHeaderImageView = [[GWImageView alloc]init];
//        self.imageHeaderImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.width);
//        __weak typeof(self)weakSelf = self;
//        [self.imageHeaderImageView uploadImageWithMusicrURL:musicSingleModel.main_img placeholder:nil callback:^(UIImage *image) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            UIImage *roundImage = [GWTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
//            strongSelf.imageHeaderImageView.image = roundImage;
//            [dict setObject:[[MPMediaItemArtwork alloc] initWithImage:roundImage] forKey:MPMediaItemPropertyArtwork];
//            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:dict];
//        }];
//    }
}


@end
