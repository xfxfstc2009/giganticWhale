//
//  GWMusicPlayerMethod.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DOUAudioStreamer.h"
#import "DOUAudioStreamer+Options.h"
#import "DOUAudioStreamer.h"
#import "DOUAudioVisualizer.h"

#import "GWMusicSingleModel.h"

@protocol GWMusicSingleModelDelegate <NSObject>

-(void)musicProgress:(NSInteger)currentprogress duration:(NSInteger)duration;

@end

@interface GWMusicPlayerMethod : NSObject

+ (GWMusicPlayerMethod *)sharedLocationManager;
@property (nonatomic,strong) DOUAudioVisualizer *audioVisualizer;               /**< 可视化工具*/
@property (nonatomic,strong) DOUAudioStreamer *streamer;                        /**< 播放器*/
@property (nonatomic,strong) GWMusicSingleModel *currentMusicModel;             /**< 传递的音乐model*/
@property (nonatomic,assign)NSInteger currentTrackIndex;                        /**< 当前正在播放 index */
@property (nonatomic,strong)NSMutableArray *musicMutableArr;                    /**< 音乐数组*/
@property (nonatomic,strong)GWImageView *imageHeaderImageView;                  /**< 图片封面*/

@property (nonatomic,weak)id<GWMusicSingleModelDelegate> delegate;              /**< 代理*/

- (void)playWithMusicList:(NSArray *)musicArr playIndex:(NSInteger)index;                                  /**< 传递一个数组*/
// 方法
- (void)actionPlayPause;                                                        /**< 开始和暂停*/
- (void)actionNextWithBlock:(void(^)())block;                                   /**< 下一首*/
- (void)actionPrevious;                                                         /**< 上一首*/
- (void)actionSliderProgressWithTime:(NSTimeInterval)currentTime;               /**< 移动进度*/
- (void)actionStop ;                                                            /**< 取消释放*/

- (void)musicWithBackPlayer;                                                    /**< 音乐后台播放*/

@end
