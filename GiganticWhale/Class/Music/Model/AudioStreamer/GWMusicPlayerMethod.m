//
//  GWMusicPlayerMethod.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMusicPlayerMethod.h"
#import <objc/runtime.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

static void *kStatusKVOKey = &kStatusKVOKey;
static void *kDurationKVOKey = &kDurationKVOKey;
static void *kBufferingRatioKVOKey = &kBufferingRatioKVOKey;

@interface GWMusicPlayerMethod()
@property (nonatomic,strong)NSTimer *musicTimer;        /**< 音乐计时器*/

@end

@implementation GWMusicPlayerMethod

+ (GWMusicPlayerMethod *)sharedLocationManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - Main
-(void)playerManager{
    GWMusicSingleModel *musicSingleModel = [self.musicMutableArr objectAtIndex:self.currentTrackIndex];
    self.currentMusicModel = musicSingleModel;
    
    musicSingleModel.audioFileURL = [self transformationUrl:musicSingleModel.music];
    
    self.musicTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    [self resetStreamerWithModel:musicSingleModel];
}

-(void)resetStreamerWithModel:(GWMusicSingleModel *)musicSingleModel{
    // 1. 取消上一个音乐流
    [self cancelStreamer];
    // 2. 创建新的音乐播放器
    self.streamer = [DOUAudioStreamer streamerWithAudioFile:musicSingleModel];
    // 3. 创建观察者
    [self.streamer addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:kStatusKVOKey];
    [self.streamer addObserver:self forKeyPath:@"duration" options:NSKeyValueObservingOptionNew context:kDurationKVOKey];
    [self.streamer addObserver:self forKeyPath:@"bufferingRatio" options:NSKeyValueObservingOptionNew context:kBufferingRatioKVOKey];
    // 4. 开始播放
    [self.streamer play];
    // 5.修改当前播放状态
    [self updateBufferingStatus];
    // 6.装载流
    [self setupHintForStreamerWithModel:musicSingleModel];
    // 7. 设置当前
    [self setLockScreenNowPlayingInfoWithModel:musicSingleModel andImage:nil];
    // 8.缓存
    NSLog(@"====url %@        . .. ..... path ======%@",[NSString stringWithFormat:@"%@",_streamer.cachedURL],_streamer.cachedPath);
}

#pragma mark 播放音乐
- (void)playWithMusicList:(NSArray *)musicArr playIndex:(NSInteger)index{
    if (!self.musicMutableArr){
        self.musicMutableArr = [NSMutableArray array];
    }
    if (self.musicMutableArr.count){
        [self.musicMutableArr removeAllObjects];
    }
    [self.musicMutableArr addObjectsFromArray:musicArr];
    self.currentTrackIndex = index;
    self.currentMusicModel = [musicArr objectAtIndex:index];
    [self playerManager];
}

#pragma mark - 装载流
- (void)setupHintForStreamerWithModel:(GWMusicSingleModel *)musicSingleModel {
    NSUInteger nextIndex = self.currentTrackIndex + 1;
    if (nextIndex >= [self.musicMutableArr count]) {
        nextIndex = 0;
    }
    
    [DOUAudioStreamer setHintWithAudioFile:musicSingleModel];
}

#pragma mark - 修改当前状态
- (void)updateBufferingStatus {
    NSLog(@"%@",[NSString stringWithFormat:@"Received %.2f/%.2f MB (%.2f %%), Speed %.2f MB/s", (double)[_streamer receivedLength] / 1024 / 1024, (double)[_streamer expectedLength] / 1024 / 1024, [_streamer bufferingRatio] * 100.0, (double)[_streamer downloadSpeed] / 1024 / 1024]);
    
    if ([_streamer bufferingRatio] >= 1.0) {
        NSLog(@"sha256: %@", [_streamer sha256]);
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == kStatusKVOKey) {
        [self performSelector:@selector(updateStatus)  onThread:[NSThread mainThread] withObject:nil  waitUntilDone:NO];
    } else if (context == kDurationKVOKey) {
        [self performSelector:@selector(timerAction:) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
    } else if (context == kBufferingRatioKVOKey) {
        [self performSelector:@selector(updateBufferingStatus) onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - 进度
- (void)timerAction:(id)timer {
    if (self.delegate){
        if (self.streamer.duration == 0.0){
            [self.delegate musicProgress:0 duration:0];
        } else {
            [self.delegate musicProgress:self.streamer.currentTime duration:self.streamer.duration];
        }
    }
}

#pragma mark 更新当前状态
- (void)updateStatus {
    NSDictionary *resultDic = [NSDictionary dictionary];
    if (self.streamer.status == DOUAudioStreamerPlaying){
        resultDic = @{@"status":@"playing"};
    } else if (self.streamer.status == DOUAudioStreamerPaused){
        resultDic = @{@"status":@"paused"};
    } else if (self.streamer.status == DOUAudioStreamerIdle){
        resultDic = @{@"status":@"idle"};
    } else if (self.streamer.status == DOUAudioStreamerFinished){
        resultDic = @{@"status":@"finished"};
        [[GWMusicPlayerMethod sharedLocationManager] actionNextWithBlock:NULL];
    } else if (self.streamer.status == DOUAudioStreamerBuffering){
        resultDic = @{@"status":@"buffering"};
    } else if (self.streamer.status == DOUAudioStreamerError){
        resultDic = @{@"status":@"error"};
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:GWMusicNotification object:nil userInfo:resultDic];
}


#pragma mark - 公开方法
#pragma mark  开始 暂停
- (void)actionPlayPause{
    if ([self.streamer status] == DOUAudioStreamerPaused ||
        [self.streamer status] == DOUAudioStreamerIdle) {
        [self.streamer play];
    } else {
        [self.streamer pause];
    }
}

#pragma mark 下一首
- (void)actionNextWithBlock:(void(^)())block{
    if (++self.currentTrackIndex >= [self.musicMutableArr count]) {
        self.currentTrackIndex = 0;
    }
    
    [self playerManager];
}

#pragma mark - 上一首
-(void)actionPrevious{
    if (--self.currentTrackIndex < 1){
        self.currentTrackIndex = self.musicMutableArr.count - 1;
    }
    [self playerManager];
}

#pragma mark - 停止
- (void)actionStop {
    [self.streamer stop];
}

#pragma mark - 修改进度
- (void)actionSliderProgressWithTime:(NSTimeInterval)currentTime{
    [self.streamer setCurrentTime:currentTime];
}


#pragma mark - 取消音乐流
- (void)cancelStreamer {
    if (self.streamer != nil) {
        [self.streamer pause];
        [self.streamer removeObserver:self forKeyPath:@"status"];
        [self.streamer removeObserver:self forKeyPath:@"duration"];
        [self.streamer removeObserver:self forKeyPath:@"bufferingRatio"];
        self.streamer = nil;
    }
}

#pragma mark - 音乐后台播放
-(void)musicWithBackPlayer{
    AVAudioSession *audioSession=[AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];
}

#pragma mark 转换url
-(NSURL *)transformationUrl:(NSString *)url{
    NSURL *transferUrl;
    if (![url hasPrefix:@"http://"]){               // 如果不是http 开头就是我们自己的
        transferUrl = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,url] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    } else {
        transferUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
    }
    return transferUrl;
}




//设置锁屏状态，显示的歌曲信息
- (void)setLockScreenNowPlayingInfoWithModel:(GWMusicSingleModel *)musicSingleModel andImage:(UIImage *)image{
    //更新锁屏时的歌曲信息
    if (NSClassFromString(@"MPNowPlayingInfoCenter")) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setObject:musicSingleModel.name forKey:MPMediaItemPropertyTitle];
        [dict setObject:musicSingleModel.authorInfo.name forKey:MPMediaItemPropertyArtist];
        [dict setObject:@"GiganticWhale" forKey:MPMediaItemPropertyAlbumTitle];
        
        self.imageHeaderImageView = [[GWImageView alloc]init];
        self.imageHeaderImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.width);
        __weak typeof(self)weakSelf = self;
        [self.imageHeaderImageView uploadImageWithMusicrURL:musicSingleModel.img placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *roundImage = [GWTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
            strongSelf.imageHeaderImageView.image = roundImage;
            [dict setObject:[[MPMediaItemArtwork alloc] initWithImage:roundImage] forKey:MPMediaItemPropertyArtwork];
            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:dict];
        }];
    }
}

@end
