//
//  MusicPlayerManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/21.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DOUAudioStreamer.h"
#import "DOUAudioStreamer+Options.h"
#import "DOUAudioStreamer.h"
#import "DOUAudioVisualizer.h"
#import "GWMusicSingleModel.h"

@protocol musicPlayerManagerDelegate <NSObject>
-(void)musicProgress:(NSInteger)currentprogress duration:(NSInteger)duration;
@end

@interface MusicPlayerManager : NSObject

+ (MusicPlayerManager *)sharedLocationManager;
@property (nonatomic,strong) DOUAudioVisualizer *audioVisualizer;
@property (nonatomic,strong) DOUAudioStreamer *streamer;                    // 播放器
@property (nonatomic,weak)id<musicPlayerManagerDelegate> delegate;
@property (nonatomic,strong)GWMusicSingleModel *currentMusicSingleModel;
@property (nonatomic,strong)NSMutableArray *musicMutableArr;
@property (nonatomic,strong)GWImageView *imageHeaderImageView;              // 图片封面
-(void)playWithmusicListArr:(NSArray *)playerListArr;

- (void)actionPlayPause;    // 开始暂停

- (void)actionNext;     // 下一首
-(void)actionPrevious;  // 上一首
- (void)actionSliderProgressWithTime:(NSTimeInterval)currentTime;   // 移动进度
- (void)actionStop ;    // 取消释放

-(void)updateMusicInfoWithBlock:(void (^)(GWMusicSingleModel *musicSingleModel))block;

//
-(void)musicWithBackPlayer;
@end
