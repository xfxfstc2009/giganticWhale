//
//  GWPlayerThumbnailManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/12.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWPlayerThumbnailManager : NSObject
+ (UIImage*) thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time;
@end
