//
//  GWDiarySingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/5.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWDiarySingleModel <NSObject>

@end

@interface GWDiarySingleModel : FetchModel

@property (nonatomic,copy)NSString *title;                      /**< 标题*/
@property (nonatomic,copy)NSString *desc;                       /**< 文字内容*/
@property (nonatomic,copy)NSString *location;                   /**< 地址*/
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,copy)NSString *city;                       /**< 城市名字*/
@property (nonatomic,copy)NSString *district;                   /**< 街道名字*/
@property (nonatomic,strong)NSArray *imgArr;                    /**< 图片列表*/
@property (nonatomic,copy)NSString *province;                   /**< 省市街道*/
@property (nonatomic,copy)NSString *township;
@property (nonatomic,copy)NSString *neighborhood;

@property (nonatomic,copy)NSString *avatar;                     /**< 头像*/

@property (nonatomic,assign)NSTimeInterval createTime;          /**< 创建时间*/












// temp
@property (nonatomic,strong)NSArray<OSSSingleFileModel> *aliOSSArr;
@property (nonatomic,strong)NSString *userId;
@property (nonatomic,assign)BOOL showLocation;

@end
