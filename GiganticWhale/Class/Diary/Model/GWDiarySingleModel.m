//
//  GWDiarySingleModel.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/5.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDiarySingleModel.h"

@implementation GWDiarySingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc":@"description"};
}

@end
