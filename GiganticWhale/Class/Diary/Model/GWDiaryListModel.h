//
//  GWDiaryListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/5.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWDiarySingleModel.h"
@interface GWDiaryListModel : FetchModel

@property (nonatomic,strong)NSArray <GWDiarySingleModel > *diaryList;

@end
