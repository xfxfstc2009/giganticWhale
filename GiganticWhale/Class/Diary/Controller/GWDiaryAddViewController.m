//
//  GWDiaryAddViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/3.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDiaryAddViewController.h"
#import "GWAssetsLibraryViewController.h"
#import "GWMenu.h"
#import "GWGradientNavBar.h"
#import "GWUploadFileManager.h"

static char uploadDiaryWithBlockKey;
static NSString *kOriginalScrollViewWithDiary = @"kOriginalScrollViewWithDiary";
static char originalFrameKey;
static NSString *kOriginalImageViewWithDiary = @"kOriginalImageViewWithDiary";
@interface GWDiaryAddViewController()<UIScrollViewDelegate>{

}
@property (nonatomic,strong)UIButton *photoButton;          /**< 照片按钮*/
@property (nonatomic,strong)UIButton *locationButton;       /**< 是否显示地址按钮*/
@property (nonatomic,strong)UILabel *dateLabel;             /**< 日期*/
@property (nonatomic,strong)UIButton *checkButton;          /**< 确定按钮*/
@property (nonatomic,strong)UITextView *complaintTextView;  /**< 输入框*/
@property (nonatomic,strong)NSMutableArray *photoMutableArr;/**< 选择的照片数组*/
@property (nonatomic,strong)UILabel *locationLabel;         /**< 地址label*/
@end

@implementation GWDiaryAddViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createBaseView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加日记";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.photoMutableArr = [NSMutableArray array];
}

#pragma mark createBaseView
-(void)createBaseView{
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.backgroundColor = [UIColor redColor];
    bgView.image = [UIImage imageNamed:@"diary_add_background"];
    bgView.frame = kScreenBounds;
    [self.view addSubview:bgView];
    
    // 1. 创建时间label
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.frame = CGRectMake(LCFloat(22), 20, LCFloat(150), 44);
    self.dateLabel.textColor = [UIColor lightGrayColor];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:14.];
    [self.view addSubview:self.dateLabel];
    self.dateLabel.text = [NSDate getCurrentTime];
    
    // 3. 创建确定按钮
    __weak typeof(self)weakSelf = self;
    self.checkButton = [self createCustomerButtonWithBlock:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 提交图片
        NSMutableArray<OSSSingleFileModel> *aliOSSMutableArr = [NSMutableArray<OSSSingleFileModel> array];
        for (int i = 0 ; i < self.photoMutableArr.count;i++){
            UIImage *img = [self.photoMutableArr objectAtIndex:i];
            OSSSingleFileModel *singleModel = [[OSSSingleFileModel alloc]init];
            singleModel.objcName = [NSString stringWithFormat:@"%@%li",[NSDate getCurrentTimeWithSetting],(long)i];
            singleModel.objcImg = img;
            [aliOSSMutableArr addObject:singleModel];
        }
        GWDiarySingleModel *singleModel = [[GWDiarySingleModel alloc]init];
        singleModel.aliOSSArr = aliOSSMutableArr;
        singleModel.desc = strongSelf.complaintTextView.text;
        if (strongSelf.locationButton.isSelected){
            singleModel.showLocation = YES;
        }
        void(^block)(GWDiarySingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &uploadDiaryWithBlockKey);
        if (block){
            block(singleModel);
        }        
        
        [strongSelf.complaintTextView resignFirstResponder];
        [strongSelf dismissViewControllerAnimated:YES completion:NULL];
    }];
    self.checkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(22) - LCFloat(30), 20 + (44 - LCFloat(30)) /2., LCFloat(30), LCFloat(30));
    [self.checkButton setImage:[UIImage imageNamed:@"pro_icon_add_h"] forState:UIControlStateNormal];
    [self.view addSubview:self.checkButton];
    
    // 4. 创建照片按钮
    self.photoButton = [self createCustomerButtonWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
        CGRect rect = [strongSelf.view convertRect:strongSelf.photoButton.frame toView:window];
        
        [strongSelf showMenu:strongSelf.photoButton WithRect:rect];
        
    }];
    self.photoButton.frame = CGRectMake(self.checkButton.orgin_x - LCFloat(10) - LCFloat(30), self.checkButton.orgin_y, self.checkButton.size_width, self.checkButton.size_height);
    self.photoButton.backgroundColor = [UIColor clearColor];
    [self.photoButton setImage:[UIImage imageNamed:@"chat_btn_camera_n"] forState:UIControlStateNormal];
    [self.view addSubview:self.photoButton];
    
    // 5. 创建location按钮
    self.locationButton = [self createCustomerButtonWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.locationButton.selected = !strongSelf.locationButton.selected;
        if (strongSelf.locationButton.selected){
            strongSelf.locationLabel.text = [NSString stringWithFormat:@"%@·%@·%@",[GWLocationMethod sharedLocationManager].addressComponent.province,[GWLocationMethod sharedLocationManager].addressComponent.city,[GWLocationMethod sharedLocationManager].addressComponent.district];
        } else {
            strongSelf.locationLabel.text = @"";
        }
    }];
    self.locationButton.frame = CGRectMake(self.photoButton.orgin_x - LCFloat(10) - LCFloat(30), self.checkButton.orgin_y, self.checkButton.size_width, self.checkButton.size_height);
    self.locationButton.backgroundColor = [UIColor clearColor];
    [self.locationButton setImage:[UIImage imageNamed:@"diary_location_n"] forState:UIControlStateNormal];
    [self.locationButton setImage:[UIImage imageNamed:@"diary_location_s"] forState:UIControlStateSelected];
    [self.view addSubview:self.locationButton];
    
    // 6. 创建地址label
    self.locationLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.locationLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.locationLabel.frame = CGRectMake(self.dateLabel.orgin_x, CGRectGetMaxY(self.dateLabel.frame), self.dateLabel.size_width, [NSString contentofHeight:self.locationLabel.font]);
    self.locationLabel.text = @"";
    [self.view addSubview:self.locationLabel];
    
    // 2. 创建输入框
    self.complaintTextView = [[UITextView alloc]init];
    self.complaintTextView.textColor = [UIColor blackColor];
    self.complaintTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.complaintTextView.returnKeyType = UIReturnKeyDefault;
    self.complaintTextView.keyboardType = UIKeyboardTypeDefault;
    self.complaintTextView.scrollEnabled = YES;
    self.complaintTextView.frame = CGRectMake(self.dateLabel.orgin_x,  CGRectGetMaxY(self.locationLabel.frame) + LCFloat(7), kScreenBounds.size.width - self.dateLabel.orgin_x - LCFloat(11), kScreenBounds.size.height - CGRectGetMaxY(self.dateLabel.frame) - 2 * LCFloat(11));
    self.complaintTextView.placeholder = @"";
    self.complaintTextView.backgroundColor = [UIColor clearColor];
    self.complaintTextView.limitMax = 10000;
    [self.complaintTextView textViewDidChangeWithBlock:NULL];
    self.complaintTextView.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:self.complaintTextView];
}




#pragma mark - createCustomerButton
-(UIButton *)createCustomerButtonWithBlock:(void (^)())block{
    UIButton *senderButton = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [senderButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    return senderButton;
}

- (void)showMenu:(UIButton *)sender WithRect:(CGRect)rect{
    [self.complaintTextView resignFirstResponder];
    
    __weak typeof(self)weakSelf = self;
    [GWMenu showMenuInView:self.view.window fromRect:rect imgArr:self.photoMutableArr imgBlock:^(NSInteger imgIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf tapImgMangerWithIndex:imgIndex];
    } addImgBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf addImgManager];
    }];
}

-(void)tapImgMangerWithIndex:(NSInteger)index{
    [GWMenu dismissMenu];
    __weak typeof(self)weakSelf = self;
    [[UIActionSheet actionSheetWithTitle:@"图片操作" buttonTitles:@[@"取消",@"查看",@"删除"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            UIImage *img = [self.photoMutableArr objectAtIndex:index];
            [strongSelf imgToZoomWithImg:img];
        } else if (buttonIndex == 1){
            [self.photoMutableArr removeObjectAtIndex:index];
            [self performSelector:@selector(addImgShowItemBlock) withObject:nil afterDelay:.3f];
        } else if (buttonIndex == 2){
            
        }
    }]showInView:self.view];
}

-(void)addImgManager{
    [GWMenu dismissMenu];
    
    GWAssetsLibraryViewController *assetsLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetsLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:(9 - self.photoMutableArr.count) andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.photoMutableArr addObjectsFromArray:selectedImgArr];
        [strongSelf performSelector:@selector(addImgShowItemBlock) withObject:nil afterDelay:.3f];
    }];
    
    [self.navigationController pushViewController:assetsLibraryViewController animated:YES];
}

-(void)addImgShowItemBlock{    // 显示popView
    if (self.photoMutableArr.count > 0){
        [self.photoButton setImage:[UIImage imageNamed:@"chat_btn_camera_s"] forState:UIControlStateNormal];
    } else {
        [self.photoButton setImage:[UIImage imageNamed:@"chat_btn_camera_n"] forState:UIControlStateNormal];
    }
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect = [self.view convertRect:self.photoButton.frame toView:window];
    [self showMenu:self.photoButton WithRect:rect];
}


#pragma mark - 图片放大
-(void)imgToZoomWithImg:(UIImage *)img{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollViewWithDiary];
    if (originalScrollView){
        return;
    }
    CGRect convertItemFrame = [self.photoButton convertRect:self.photoButton.superview.frame toView:self.view.window];
    originalScrollView = [[UIScrollView alloc] initWithFrame:convertItemFrame];
    originalScrollView.stringTag = kOriginalScrollViewWithDiary;
    originalScrollView.backgroundColor = [UIColor blackColor];
    originalScrollView.showsHorizontalScrollIndicator = NO;
    originalScrollView.showsVerticalScrollIndicator = NO;
    originalScrollView.layer.zPosition = MAXFLOAT;
    originalScrollView.delegate = self;
    originalScrollView.layer.masksToBounds = YES;
    originalScrollView.userInteractionEnabled = YES;
    originalScrollView.maximumZoomScale=2.0;
    originalScrollView.minimumZoomScale=0.5;
    originalScrollView.bounces = YES;
    originalScrollView.bouncesZoom = YES;
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissOriginalPhoto)];
    [originalScrollView addGestureRecognizer:tapForHideKeyBoard];
    objc_setAssociatedObject(originalScrollView, &originalFrameKey, [NSValue valueWithCGRect:convertItemFrame], OBJC_ASSOCIATION_RETAIN);
    
    UIImageView *originalImageView = [[UIImageView alloc] initWithFrame:originalScrollView.bounds];
    originalImageView.stringTag = kOriginalImageViewWithDiary;
    UIImage *originalImage = img;
    originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(320, 320*originalImage.size.height/originalImage.size.width)];
    originalImageView.image = originalImage;
    originalImageView.contentMode = UIViewContentModeScaleAspectFit;
    [originalScrollView addSubview:originalImageView];
    [self.view.window addSubview:originalScrollView];

    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
    }
    originalScrollView.minimumZoomScale = 1.;
    // 开启动画
    [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        originalScrollView.frame = self.navigationController.view.bounds;
        originalImageView.frame = self.navigationController.view.bounds;
    } completion:^(BOOL finished) {
        
    }];

}

-(void)actionDismissOriginalPhoto{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollViewWithDiary];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageViewWithDiary];
    CGRect originalFrame = (CGRect)[objc_getAssociatedObject(originalScrollView, &originalFrameKey)CGRectValue];
    
    [UIView animateWithDuration:.5 animations:^{
        originalScrollView.frame = originalFrame;
        originalScrollView.layer.cornerRadius = 5;
        originalScrollView.alpha = 0.;
        originalImageView.frame = CGRectMake(0, 0, CGRectGetWidth(originalFrame), CGRectGetHeight(originalFrame));
    } completion:^(BOOL finished) {
        [originalScrollView removeFromSuperview];
    }];
}

#pragma mark - 提交block
-(void)uploadDiaryWithBlock:(void(^)(GWDiarySingleModel *singleModel))block{
    objc_setAssociatedObject(self, &uploadDiaryWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
