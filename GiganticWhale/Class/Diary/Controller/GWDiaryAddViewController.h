//
//  GWDiaryAddViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/3.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWDiarySingleModel.h"
@interface GWDiaryAddViewController : AbstractViewController

-(void)uploadDiaryWithBlock:(void(^)(GWDiarySingleModel *singleModel))block;

@end
