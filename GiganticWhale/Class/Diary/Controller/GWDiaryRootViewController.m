//
//  GWDiaryRootViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/2.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDiaryRootViewController.h"
#import "GWCustomerModalTransition.h"
#import "GWDiaryAddViewController.h"
#import "GWDiaryListModel.h"
#import "GWGradientNavBar.h"
#import "StickCollectionViewFlowLayout.h"
#import "DiaryPrimerCollectionViewCell.h"
#import "GWUploadFileManager.h"
#import "PopView.h"
#import "GWDiarybackgroundView.h"
@interface GWDiaryRootViewController()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    NSInteger currentPage;
    GWDiarybackgroundView *singleVie;
}
@property (nonatomic,strong)UICollectionView *diaryCollectionView;                      /**< collectionView*/
@property (nonatomic,strong)NSMutableArray *diaryMutableArr;                            /**< 字典数据源*/
@property (nonatomic,strong)GWCustomerModalTransition *transition;

@end


static NSString *const kDemoCell = @"primerCell";
static const CGFloat kCellSizeCoef = .87f;
static const CGFloat kFirstItemTransform = 0.15f;

@implementation GWDiaryRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createCollectionView];
    __weak typeof(self)weakSelf = self;
    [weakSelf getDiaryListWithInterfaceWithHornal:YES];

    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的日记";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_img_add"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf popViewManager];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.diaryMutableArr = [NSMutableArray array];
    currentPage = 1;
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if (!self.diaryCollectionView){
        StickCollectionViewFlowLayout *flowLayout= [[StickCollectionViewFlowLayout alloc]init];
        self.diaryCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.diaryCollectionView.backgroundColor = [UIColor clearColor];
        self.diaryCollectionView.showsVerticalScrollIndicator = NO;
        self.diaryCollectionView.delegate = self;
        self.diaryCollectionView.dataSource = self;
        self.diaryCollectionView.showsHorizontalScrollIndicator = NO;
        self.diaryCollectionView.scrollsToTop = YES;
        self.diaryCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.diaryCollectionView];
        
        
        [self.diaryCollectionView registerClass:[DiaryPrimerCollectionViewCell class] forCellWithReuseIdentifier:kDemoCell];
        StickCollectionViewFlowLayout *stickLayout = (StickCollectionViewFlowLayout *)self.diaryCollectionView.collectionViewLayout;
        stickLayout.firstItemTransform = kFirstItemTransform;
    }
    
    __weak typeof(self)weakSelf = self;
    [self.diaryCollectionView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getDiaryListWithInterfaceWithHornal:YES];
    }];
    
    [self.diaryCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getDiaryListWithInterfaceWithHornal:NO];
    }];
}

#pragma mark -=CollectionView datasource=-
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.diaryMutableArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DiaryPrimerCollectionViewCell *cell = [self.diaryCollectionView dequeueReusableCellWithReuseIdentifier:kDemoCell forIndexPath:indexPath];
    GWDiarySingleModel *singleModel = [self.diaryMutableArr objectAtIndex:indexPath.row];

    
    // height
    CGFloat cellHeight = 0;
    if ([DiaryPrimerCollectionViewCell calculationCellHeightWithModel:singleModel] > CGRectGetHeight(self.diaryCollectionView.bounds) * kCellSizeCoef){
        cellHeight = [DiaryPrimerCollectionViewCell calculationCellHeightWithModel:singleModel];
    } else {
        cellHeight = CGRectGetHeight(self.diaryCollectionView.bounds) * kCellSizeCoef;
    }
    cell.transferCellHeight = cellHeight;
    
    // select
    __weak typeof(self)weakSelf = self;
    [cell locationButtonActionClick:^(GWDiarySingleModel *transferSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        CGFloat xWidth = strongSelf.view.size_width - 50.0f;
        CGFloat yHeight = 272.0f;
        CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
        PopView *poplistview = [[PopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
        poplistview.type = PopViewTypeLocation;
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = transferSingleModel.lat;
        coordinate.longitude = transferSingleModel.lng;
        poplistview.transferCoordinate = coordinate;
        [poplistview viewShow];
    }];
    cell.transferIndex = indexPath.row;
    cell.transferSingleModel = singleModel;
    return cell;
}

#pragma mark -=CollectionView layout=-
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 0;
    GWDiarySingleModel *singleModel = [self.diaryMutableArr objectAtIndex:indexPath.row];
    if ([DiaryPrimerCollectionViewCell calculationCellHeightWithModel:singleModel] > CGRectGetHeight(self.diaryCollectionView.bounds) * kCellSizeCoef){
        cellHeight = [DiaryPrimerCollectionViewCell calculationCellHeightWithModel:singleModel];
    } else {
        cellHeight = CGRectGetHeight(self.diaryCollectionView.bounds) * kCellSizeCoef;
    }
    return CGSizeMake(CGRectGetWidth(self.view.bounds), cellHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}



#pragma mark - PopManager
-(void)sendDiaryWithSingleodel:(GWDiarySingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    [[GWUploadFileManager sharedUploadManager] sendRequesTouploadDiaryInfoWithDiarySingleModel:singleModel block:^(BOOL successed, GWDiarySingleModel *backSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            [strongSelf.diaryMutableArr insertObject:backSingleModel atIndex:0];
            [strongSelf.diaryCollectionView reloadData];
        }
    }];
}

- (void)popViewManager {
    GWDiaryAddViewController *diaryWithAddViewController = [[GWDiaryAddViewController alloc]init];
    
    __weak typeof(self)weakSelf = self;
    [diaryWithAddViewController uploadDiaryWithBlock:^(GWDiarySingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendDiaryWithSingleodel:singleModel];
    }];
    
    
    UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
    UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
    
    GWNavigationController *navigationController = [[GWNavigationController alloc] initWithNavigationBarClass:[GWGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    
    [navigationController setViewControllers:@[diaryWithAddViewController]];
    [[GWGradientNavBar appearance] setBarTintGradientColors:[NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil]];

    self.transition = [[GWCustomerModalTransition alloc]initWithModalViewController:navigationController];
    self.transition.dragable = YES;//---是否可下拉收起
    navigationController.transitioningDelegate = self.transition;
    navigationController.modalPresentationStyle = UIModalPresentationCustom;
    // pop block
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - getList
-(void)getDiaryListWithInterfaceWithHornal:(BOOL)hornal{
    __weak typeof(self)weakSelf = self;
    if (hornal){ // 下拉
        currentPage = 1;
    }
    NSDictionary *params = @{@"page":@(currentPage),@"size":@"10"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:diary_list requestParams:params responseObjectClass:[GWDiaryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWDiaryListModel *diaryListModel = (GWDiaryListModel *)responseObject;
            
            if (hornal){
                [strongSelf.diaryMutableArr removeAllObjects];
            }
            strongSelf->currentPage++;
            
            [strongSelf.diaryMutableArr addObjectsFromArray:diaryListModel.diaryList];
            [strongSelf.diaryCollectionView stopFinishScrollingRefresh];
            [strongSelf.diaryCollectionView stopPullToRefresh];
        
            if (strongSelf.diaryMutableArr.count){
                [strongSelf.diaryCollectionView dismissPrompt];
            } else {
                [strongSelf.diaryCollectionView showPrompt:@"没有我的日记信息" withImage:nil andImageAlignment:MMHPromptImageAlignmentNone];
            }

            [strongSelf.diaryCollectionView reloadData];
        
        }
    }];
}

@end
