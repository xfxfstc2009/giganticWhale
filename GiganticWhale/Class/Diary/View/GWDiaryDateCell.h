//
//  GWDiaryDateCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/5.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWDiaryDateCell : UITableViewCell

@property (nonatomic,copy)NSString *transferDateContent;
@property (nonatomic,assign)CGFloat transferCellHeight;


+(CGFloat)calculationCellHeight;
@end
