//
//  GWDiaryImgCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/2.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDiaryImgCell.h"

@interface GWDiaryImgCell()
@property (nonatomic,strong)UIScrollView *mainScrollView;

@end

@implementation GWDiaryImgCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建scrollView
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, 3 * kScreenBounds.size.height);
        self.mainScrollView.contentOffset = CGPointMake(0, kScreenBounds.size.height);
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bounces = NO;
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.mainScrollView];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}



+(CGFloat)calculationCellHeight{
    return 100;
}
@end
