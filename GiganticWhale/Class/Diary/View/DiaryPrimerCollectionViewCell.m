//
//  DiaryPrimerCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/6.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "DiaryPrimerCollectionViewCell.h"


static char locationButtonActionClickKey;
@interface DiaryPrimerCollectionViewCell()
@property (nonatomic,strong)GWImageView *bgView;
@property (nonatomic,strong)GWImageView *avatarImgView;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *locationLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIView *imgBgView;
@property (nonatomic,strong)UIButton *locationButton;

@end

@implementation DiaryPrimerCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
    } return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[GWImageView alloc]init];
    self.bgView.frame = self.bounds;
    self.bgView.image = [GWTool stretchImageWithName:@"common_card_background"];
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    
    self.avatarImgView = [[GWImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(7), LCFloat(7), LCFloat(65), LCFloat(65));
    [self.bgView addSubview:self.avatarImgView];
    
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), LCFloat(11), self.bgView.size_width - CGRectGetMaxX(self.avatarImgView.frame) - 2 * LCFloat(11), [NSString contentofHeight:self.timeLabel.font]);
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.timeLabel.textColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.bgView addSubview:self.timeLabel];
    
    // location
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), CGRectGetMaxY(self.timeLabel.frame) + LCFloat(11), self.timeLabel.size_width, [NSString contentofHeight:self.locationLabel.font]);
    self.locationLabel.textAlignment = NSTextAlignmentRight;
    self.locationLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self.bgView addSubview:self.locationLabel];
    
    // descLabel
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.descLabel.numberOfLines = 0;
    [self.bgView addSubview:self.descLabel];
    
    // bgView
    self.imgBgView = [[UIView alloc]init];
    self.imgBgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.imgBgView];
    
    self.locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.locationButton.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.locationButton];
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferSingleModel:(GWDiarySingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    self.bgView.backgroundColor = [UIColor clearColor];
    
    // 1. 头像
    [self.avatarImgView uploadImageWithURL:transferSingleModel.avatar placeholder:nil callback:NULL];
    self.avatarImgView.hidden = YES;
    
    // 2.时间
    self.timeLabel.text = [NSDate getTimeWithString:transferSingleModel.createTime];
    
    // 3. location
    NSString *locationStr = @"";
    if (self.transferSingleModel.province.length){              // 省
        locationStr = [locationStr stringByAppendingString:[NSString stringWithFormat:@"%@ · ",self.transferSingleModel.province]];
    }
    if (self.transferSingleModel.city.length){                  // 城市
        if (locationStr.length){
            locationStr = [locationStr stringByAppendingString:[NSString stringWithFormat:@"%@ · ",self.transferSingleModel.city]];
        } else {
            locationStr = [locationStr stringByAppendingString:self.transferSingleModel.city];
        }
    }
    if (self.transferSingleModel.district.length){
        if (locationStr.length){
            locationStr = [locationStr stringByAppendingString:[NSString stringWithFormat:@"%@",self.transferSingleModel.district]];
        } else {
            locationStr = [locationStr stringByAppendingString:self.transferSingleModel.district];
        }
    }
    
    self.locationLabel.text = locationStr;
    self.locationLabel.textAlignment = NSTextAlignmentRight;
    
    // 4. 创建内容
    self.descLabel.text = transferSingleModel.desc;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(self.bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), self.bgView.size_width - 2 * LCFloat(11), descSize.height);
    
    // 5.
    if (self.imgBgView.subviews.count){
        [self.imgBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    CGFloat margin = LCFloat(11);
    CGFloat img_width = (self.bgView.size_width - 2 * LCFloat(11) - 4 * margin) / 3.;
    if (transferSingleModel.imgArr.count){
        for (int index = 0 ; index < transferSingleModel.imgArr.count;index++){
            // 1. origin_x
            CGFloat origin_x = margin + (index % 3) * img_width + (index % 3) * margin;
            // 2. origin_y
            CGFloat origin_y = margin + (index / 3) * (img_width + margin);
            // 3 .frame
            CGRect tempRect = CGRectMake(origin_x, origin_y, img_width,img_width);
            
            NSString *imgUrl = [transferSingleModel.imgArr objectAtIndex:index];
            
            GWImageView *imgView = [[GWImageView alloc]init];
            imgView.frame = tempRect;
            [imgView uploadImgWithNormal:imgUrl placehorder:nil back:NULL];
            imgView.backgroundColor = [UIColor clearColor];;
            [self.imgBgView addSubview:imgView];
        }
    }
    
    CGFloat imgBgViewHeight = 0;
    imgBgViewHeight += margin ;
    NSInteger excessNumber = (transferSingleModel.imgArr.count) % 3 == 0 ? 0 : 1;
    imgBgViewHeight += (transferSingleModel.imgArr.count / 3 + excessNumber) * (margin + img_width);
    
    
    self.imgBgView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.descLabel.frame) + LCFloat(11), self.bgView.size_width - 2 * LCFloat(11), imgBgViewHeight);
    
    // 重新计算
    self.timeLabel.orgin_y = self.transferCellHeight - LCFloat(11) - self.timeLabel.size_height;
    self.locationLabel.orgin_y = self.timeLabel.orgin_y - LCFloat(7) - self.locationLabel.size_height;
    
    self.locationButton.frame = self.locationLabel.frame;
    self.locationButton.backgroundColor = [UIColor clearColor];
    self.locationButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.locationButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(GWDiarySingleModel *transferSingleModel) = objc_getAssociatedObject(strongSelf, &locationButtonActionClickKey);
        if (block){
            block(transferSingleModel);
        }
    }];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

+(CGFloat)calculationCellHeightWithModel:(GWDiarySingleModel *)transferSingleModel{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(65);
    cellHeight += LCFloat(11);
    // 4. 创建内容
    CGSize descSize = [transferSingleModel.desc sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += descSize.height;
    cellHeight += LCFloat(11);

    CGFloat margin = LCFloat(11);
    CGFloat img_width = (kScreenBounds.size.width - 4 * margin) / 3.;
    CGFloat imgBgViewHeight = 0;
    imgBgViewHeight += margin * 2;
    NSInteger excessNumber = (transferSingleModel.imgArr.count + 1) % 3 == 0 ? 0 : 1;
    imgBgViewHeight += (transferSingleModel.imgArr.count / 3 + excessNumber) * (margin + img_width);
    cellHeight += imgBgViewHeight;
    cellHeight += LCFloat(11);
    return cellHeight;
}

-(void)locationButtonActionClick:(void(^)(GWDiarySingleModel *transferSingleModel))block{
    objc_setAssociatedObject(self, &locationButtonActionClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
