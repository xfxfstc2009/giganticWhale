//
//  GWDiaryImgCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/2.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWDiaryImgCell : UITableViewCell

@property (nonatomic,strong)NSArray *transferImgArr;                /**< 传入进入的图片数组*/
@property (nonatomic,assign)CGFloat transferCellHeight;


+(CGFloat)calculationCellHeight;
@end
