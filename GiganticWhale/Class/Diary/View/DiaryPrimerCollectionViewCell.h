//
//  DiaryPrimerCollectionViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/6.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWDiarySingleModel.h"

@interface DiaryPrimerCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)GWDiarySingleModel *transferSingleModel;
@property (nonatomic,assign)NSInteger transferIndex;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeightWithModel:(GWDiarySingleModel *)transferSingleModel;
-(void)locationButtonActionClick:(void(^)(GWDiarySingleModel *transferSingleModel))block;

@end
