//
//  GWDiarybackgroundView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWDiarybackgroundView : UIView
@property(nonatomic) CGFloat startAngle;                                //起点 角度
@property(nonatomic)CGFloat endInnerAngle;                              //终点 角度
@property(nonatomic)CGFloat lineWith;                                   //线宽
@property(nonatomic,strong)UIColor *unfillColor;                        //基准圆环颜色
@property(nonatomic,strong)UIColor *fillColor;                          //显示圆环颜色
@property (nonatomic ,strong)UILabel *centerLable;                      //中心数据显示标签
@property (nonatomic,assign)CGFloat transferPercentage;                 /**< 传递过来的百分比*/

-(void)setPercent:(CGFloat)percent setpCount:(NSInteger)setpCount distance:(CGFloat)distance animated:(BOOL)animated ;

-(void)startAnimation;
@end
