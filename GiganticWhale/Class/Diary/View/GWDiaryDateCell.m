//
//  GWDiaryDateCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/5.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDiaryDateCell.h"

@interface GWDiaryDateCell()
@property (nonatomic,strong)UILabel *dateLabel;

@end

@implementation GWDiaryDateCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.dateLabel = [[UILabel alloc]init];
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.dateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    self.dateLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, [NSString contentofHeight:self.dateLabel.font]);
    self.dateLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.dateLabel];
}

+(CGFloat)calculationCellHeight{
    return [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
}

@end
