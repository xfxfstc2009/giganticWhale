//
//  TuLingSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/6/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface TuLingSingleModel : FetchModel

@property (nonatomic,copy)NSString *text;

@end
