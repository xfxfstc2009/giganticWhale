//
//  GWAPICloudViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/6/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWAPICloudViewController.h"
#import "YinyuetaiSingleModel.h"
#import "TuLingSingleModel.h"

@interface GWAPICloudViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *apiTableView;
@property (nonatomic,strong)NSArray *apiArr;
@end

@implementation GWAPICloudViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma makr - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"API Cloud";
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.apiArr = @[@[@"音悦台"],@[@""]];
}

-(void)createTableView{
    if (!self.apiTableView){
        self.apiTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.apiTableView.dataSource = self;
        self.apiTableView.delegate = self;
        [self.view addSubview:self.apiTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.apiArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.apiArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cellWithRowOne.textLabel.text = [[self.apiArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){
        [self sendRequestToGetYinyuetaiInfo];
    } else if (indexPath.section == 1){                     // 图灵机器人
        [self sendRequestToGetTulingInfo];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)sendRequestToGetYinyuetaiInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"videoId":@"2844888"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:yinyuetai_info requestParams:params responseObjectClass:[YinyuetaiSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            YinyuetaiSingleModel *singleModel = (YinyuetaiSingleModel *)responseObject;
            NSLog(@"%@",singleModel);
        }
    }];
}

#pragma mark - 图灵
-(void)sendRequestToGetTulingInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"info":@"今天天气",@"key":tulingKey};
    [[NetworkAdapter sharedAdapter] fetchWithPath:tuling_info requestParams:params responseObjectClass:[TuLingSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            TuLingSingleModel *singleModel = (TuLingSingleModel *)responseObject;
            [[UIAlertView alertViewWithTitle:singleModel.text message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}
@end
