//
//  YinyuetaiSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/6/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface YinyuetaiSingleModel : FetchModel

@property (nonatomic,assign)NSInteger duration;
@property (nonatomic,assign)NSInteger error;
@property (nonatomic,copy)NSString *hcVideoUrl;
@property (nonatomic,copy)NSString *hdVideoUrl;
@property (nonatomic,copy)NSString *heVideoUrl;
@property (nonatomic,copy)NSString *logined;
@property (nonatomic,copy)NSString *message;

@end
