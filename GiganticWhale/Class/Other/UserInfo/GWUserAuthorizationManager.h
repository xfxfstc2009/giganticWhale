//
//  GWUserAuthorizationManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//
// 用户授权
#import <Foundation/Foundation.h>
#import "AppDelegate.h"
@class AppDelegate;
@interface GWUserAuthorizationManager : NSObject

#pragma mark 百度地图授权
+(void)authorizationWithBaiduMap;

@end
