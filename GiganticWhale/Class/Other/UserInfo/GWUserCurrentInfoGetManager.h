//
//  GWUserCurrentInfoGetManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWLocationMethod.h"
#import "GWWeatherSingleModel.h"

@interface GWUserCurrentInfoGetManager : NSObject
// 1. 地理信息
@property (nonatomic,assign)CGFloat locationLat;
@property (nonatomic,assign)CGFloat locationLng;
@property (nonatomic,strong)AMapAddressComponent *addressComponent;

// 2. 天气质量
@property (nonatomic,strong)GWWeatherCurrentModel *weatherCurrentModel;

// 4. 当前launch信息
@property (nonatomic,strong)UIImage *launchImg;

+ (GWUserCurrentInfoGetManager *)sharedHealthManager ;
-(void)getUserCurrentInfoManager;               // 获取当前信息
@end
