//
//  GWBannerTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWBannerTableViewCell.h"
#import "PDScrollView.h"
@interface GWBannerTableViewCell()
@property (nonatomic,strong)PDScrollView *headerScrollView;

@end

@implementation GWBannerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 2. 创建北京scrollView
    self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200))];
    self.headerScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    __weak typeof(self)weakSelf = self;
    [self.headerScrollView bannerImgTapManagerWithInfoblock:^(GWScrollViewSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSLog(@"123");
    }];
    [self addSubview:self.headerScrollView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight {
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
//    NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
//    for (int i = 0 ; i < self.transferImgArr.count;i++){
//        HomeRootBannerSingleModel *bannerSingleModel = [self.transferImgArr objectAtIndex:i];
//        GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
//        scrollViewSingleModel.img = bannerSingleModel.img_url;
//        scrollViewSingleModel.content = bannerSingleModel.title;
//        [tempScrollMutableArr addObject:scrollViewSingleModel];
//    }
//    self.headerScrollView.transferImArr = tempScrollMutableArr;
}


+(CGFloat) calculationCellHeight{
    return LCFloat(200);
}
@end