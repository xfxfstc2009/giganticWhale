//
//  GWAvatarTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAvatarTableViewCell.h"

@interface GWAvatarTableViewCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)GWImageView *avatar;
@property (nonatomic,strong)GWImageView *arrowImg;
@property (nonatomic,strong)UIView *convertView;


@end

@implementation GWAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.fixedLabel.text = @"上传图片";
    [self addSubview:self.fixedLabel];
    
    // convert
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    self.convertView.layer.cornerRadius = LCFloat(4);
    [self addSubview:self.convertView];
    
    
    self.avatar = [[GWImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.avatar];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferAvatar:(UIImage *)transferAvatar{
    _transferAvatar = transferAvatar;
    
    // 1. title
    CGSize titleSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, self.transferCellHeight);
    
    // 2. avatar;
    self.convertView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - (self.transferCellHeight - 2 * LCFloat(7)), LCFloat(7), self.transferCellHeight - 2 * LCFloat(7), self.transferCellHeight - 2 * LCFloat(7));
    
    // 3. img
    if (transferAvatar){
        self.avatar.image = transferAvatar;
        [GWTool setTransferSingleAsset:transferAvatar imgView:self.avatar convertView:self.convertView];
    } else {
        self.avatar.image = [UIImage imageNamed:@"diary_addImage"];
        self.avatar.frame = self.convertView.bounds;
    }
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.fixedLabel.text = transferTitle;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}


@end
