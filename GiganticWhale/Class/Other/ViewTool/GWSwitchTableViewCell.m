//
//  GWSwitchTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSwitchTableViewCell.h"

static char switchKey;
@interface GWSwitchTableViewCell()
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation GWSwitchTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 创建lft_label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.fixedLabel];
    
    // 2.
    // 是否默认
    self.normalSwitch = [[UISwitch alloc] init];
    [self.normalSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    self.normalSwitch.onTintColor = [UIColor colorWithCustomerName:@"蓝"];
    [self addSubview:self.normalSwitch];
    
    if (!IS_IOS7_LATER){
        [self.normalSwitch setFrame   :CGRectMake(305-CGRectGetWidth([self.normalSwitch frame]), 7, 31, 27)];
        [self.normalSwitch setOnImage :[[UIImage imageNamed:@"login_thirdLogin_weibo"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 13, 15, 13)]];
        [self.normalSwitch setOffImage:[[UIImage imageNamed:@"switch_off"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 13, 15, 13)]];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setIsOn:(BOOL)isOn{
    if (isOn){
        [self.normalSwitch setOn:YES];
    } else {
        [self.normalSwitch setOn:NO];
    }
}

-(void)setTransferInfo:(NSString *)transferInfo{
    _transferInfo = transferInfo;
    
    self.fixedLabel.text = transferInfo;
    CGSize fixedSize= [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.transferCellHeight);
    
    self.normalSwitch.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 51, (self.transferCellHeight - 31) / 2., 51, 31);
    
}

-(void)switchAction{
    void(^block)(BOOL status) = objc_getAssociatedObject(self, &switchKey);
    if (block){
        block(self.normalSwitch.on);
    }
}


-(void)actionClickWIthSwitch:(void(^)(BOOL switchStatus))block{
    objc_setAssociatedObject(self, &switchKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
   return LCFloat(44);
}

@end
