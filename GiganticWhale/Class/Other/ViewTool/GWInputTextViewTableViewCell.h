//
//  GWInputTextViewTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/3/18.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWInputTextViewTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferPlaceholder;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,assign)NSInteger limitCount;
@property (nonatomic,copy)NSString *transferInputText;
@property (nonatomic,strong)UITextView *inputTextView;

-(void)textViewTextDidChangedBlock:(void(^)(NSString *info))block;
+(CGFloat)calculationCellHeight;

@end
