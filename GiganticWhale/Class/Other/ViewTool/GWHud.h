//
//  GWHud.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWHud : NSObject

+(void)showInfo:(NSString *)info dalay:(NSInteger)dalay;

+(void)dismiss;

@end
