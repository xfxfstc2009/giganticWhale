//
//  GWAvatarTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWAvatarTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)UIImage *transferAvatar;
@property (nonatomic,copy)NSString *transferTitle;

+(CGFloat)calculationCellHeight;

@end
