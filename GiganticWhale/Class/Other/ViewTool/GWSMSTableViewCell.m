//
//  GWSMSTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSMSTableViewCell.h"
#import "PDCountDownButton.h"

static char smsButtonKey;
static char textFieldKey;
@interface GWSMSTableViewCell()
@property (nonatomic,strong)PDCountDownButton *countDownButton;
@end

@implementation GWSMSTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.
    self.textField = [[UITextField alloc]init];
    self.textField.backgroundColor = [UIColor clearColor];
    self.textField.placeholder = @"请输入验证码";
    self.textField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.textField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.textField];
    
    // 2. 创建按钮
    NSString*smsStr = @"获取验证码";
    CGSize smsSize = [smsStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX,    [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    __weak typeof(self)weakSelf = self;
    self.countDownButton = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(11) - smsSize.width, LCFloat(5), smsSize.width, LCFloat(44) - 2 * LCFloat(5)) daojishi:60 withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &smsButtonKey);
        if (block){
            block();
        }
    }];
    self.countDownButton.clipsToBounds = YES;
    self.countDownButton.layer.cornerRadius = LCFloat(3);
    [self addSubview:self.countDownButton];
    
    self.textField.frame = CGRectMake(LCFloat(11), 0, self.countDownButton.orgin_x - LCFloat(11) - LCFloat(11), LCFloat(44));
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
}

-(void)actionClickWithSms:(void(^)())block{
    objc_setAssociatedObject(self, &smsButtonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)textFieldDidChanged{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textFieldKey);
    if (block){
        block(self.textField.text);
    }
}

-(void)textFieldDidChangeBlock:(void (^)(NSString *))block{
    objc_setAssociatedObject(self, &textFieldKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)hasEnable:(BOOL)enable{
    if (enable){
        [self.countDownButton smsLabelTypeManager:YES];
    } else {
        [self.countDownButton smsLabelTypeManager:NO];
    }
}
@end
