//
//  GWSelectedImgTableViewCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 选择当前的图片

#import "GWSelectedImgTableViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define kMarinWidth         LCFloat(25)
#define kTagWidthWithProductRelease (kScreenBounds.size.width - 4 * kMarinWidth) / 3.
#define kTagHeightWithProductRelease (kScreenBounds.size.width - 4 * kMarinWidth) / 3.

@interface GWSelectedImgTableViewCell()
@property (nonatomic,strong)UIButton *addButton;            /**< 添加按钮*/


@end

@implementation GWSelectedImgTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 300);
    [self addSubview:self.bgView];
    
    //2. 创建
    [self createButton];
}

#pragma mark - CreateCustomerItem
-(UIView *)createCustomerItemWithIndex:(NSInteger)index callBlocak:(void (^)(UIView *imageBgView))imgTapCallBack{
    // 1. origin_x
    CGFloat origin_x = kMarinWidth + (index % 3) * kTagWidthWithProductRelease + (index % 3) * kMarinWidth;
    // 2. origin_y
    CGFloat origin_y = LCFloat(5) + ( index / 3 ) * (kTagHeightWithProductRelease + LCFloat(10));
    // 3 .frame
    CGRect tempRect = CGRectMake(origin_x, origin_y, kTagWidthWithProductRelease,kTagHeightWithProductRelease);
    
    
    UIImage *image = [self.transferSelectedImgArr objectAtIndex:index];
    //    image = [self getImageByCuttingImage:image Rect:CGRectMake(LCFloat(30), (image.size.height - image.size.width) / 2., image.size.width, image.size.height - image.size.width)];
    image = image;
    
    UIView *imageBgView = [[UIView alloc]init];
    imageBgView.frame = tempRect;
    imageBgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:imageBgView];
    
    // 2. 创建imageview
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image ;
    imageView.frame = imageBgView.bounds;
    imageView.stringTag = [NSString stringWithFormat:@"customButton%li",(long)index];
    [imageBgView addSubview:imageView];
    
    // 3. 创建按钮
    UIButton *imageTapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    imageTapButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [imageTapButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (imgTapCallBack){
            imgTapCallBack(imageBgView);
        }
    }];
    imageTapButton.frame = imageBgView.bounds;
    [imageBgView addSubview:imageTapButton];
    return imageBgView;
}

-(void)setTransferSelectedImgArr:(NSArray *)transferSelectedImgArr{
    _transferSelectedImgArr = transferSelectedImgArr;
    // 1. 删除所有内容
    [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i< transferSelectedImgArr.count;i++){
        [weakSelf createCustomerItemWithIndex:i callBlocak:^(UIView *imgView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.delegate imageSelectedDeleteClick:i imageView:imgView andSuperCell:strongSelf];
            
        }];
    }
    [self createButton];
}


#pragma mark - 添加按钮
-(void)createButton{
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addButton setBackgroundColor:[UIColor clearColor]];
    [self.addButton setBackgroundImage:[UIImage imageNamed:@"icon_img_add"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [weakSelf.addButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.delegate imageSelectedButtonClick:strongSelf];
        
    }];
    [self.bgView addSubview:self.addButton];
    
    // frame
    CGFloat origin_x = kMarinWidth + (self.transferSelectedImgArr.count % 3) * kTagWidthWithProductRelease + (self.transferSelectedImgArr.count % 3) * kMarinWidth;
    CGFloat origin_y = LCFloat(5) + ( self.transferSelectedImgArr.count / 3 ) * (kTagHeightWithProductRelease + LCFloat(10));
    CGRect tempRect = CGRectMake(origin_x, origin_y, kTagWidthWithProductRelease,kTagHeightWithProductRelease);
    self.addButton.frame = tempRect;
}

+(CGFloat)calculationCellHeightWithImgArr:(NSArray *)transferSelectedImgArr{
    CGFloat cellHeight = 0;
    
    NSInteger typeCount = transferSelectedImgArr.count + 1;
    NSInteger excessNumber = (transferSelectedImgArr.count + 1) % 3 == 0 ? 0 : 1;
    cellHeight += LCFloat(5);
    cellHeight += (typeCount / 3 + excessNumber) * (LCFloat(10) + kTagHeightWithProductRelease);
    cellHeight += LCFloat(5);
    return cellHeight;
}


@end
