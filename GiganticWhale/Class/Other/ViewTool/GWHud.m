//
//  GWHud.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWHud.h"

@implementation GWHud

+(void)showInfo:(NSString *)info dalay:(NSInteger)dalay{
    if (dalay == 0){
        [WSProgressHUD showProgress:.5f status:info];
        
    } else {
        [WSProgressHUD showShimmeringString:info];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(dalay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [WSProgressHUD dismiss];
        });
    }
}

+(void)dismiss{
   [WSProgressHUD dismiss]; 
}

@end
