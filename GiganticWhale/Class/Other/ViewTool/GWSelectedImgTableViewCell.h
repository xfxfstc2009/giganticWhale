//
//  GWSelectedImgTableViewCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWSelectedImgTableViewCell;

@protocol GWSelectedImgTableViewCellDelegate <NSObject>

-(void)imageSelectedButtonClick:(GWSelectedImgTableViewCell *)cell;
-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(GWSelectedImgTableViewCell *)cell;

@end

@interface GWSelectedImgTableViewCell : UITableViewCell

@property (nonatomic,strong)NSArray *transferSelectedImgArr;
@property (nonatomic,weak)id<GWSelectedImgTableViewCellDelegate> delegate;
@property (nonatomic,strong)UIView *bgView;

+(CGFloat)calculationCellHeightWithImgArr:(NSArray *)isSelectedImageArr;

@end
