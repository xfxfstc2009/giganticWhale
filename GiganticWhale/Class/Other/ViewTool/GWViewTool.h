//
//  GWViewTool.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWInputTextFieldTableViewCell.h"



@interface GWViewTool : NSObject

+(UITableView *)gwCreateTableViewRect:(CGRect)rect;

+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color;

+(UIView *)createBottomButtonViewframe:(CGRect)frame image:(UIImage *)image label:(NSString *)text buttonBlock:(void(^)())block;

@end
