//
//  GWViewTool.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWViewTool.h"

@implementation GWViewTool

+(UITableView *)gwCreateTableViewRect:(CGRect)rect{
    UITableView *tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.scrollEnabled = YES;
    return tableView;
}

+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithCustomerName:color];
    label.font = [UIFont fontWithCustomerSizeName:font];
    return label;
}

+(UIView *)createBottomButtonViewframe:(CGRect)frame image:(UIImage *)image label:(NSString *)text buttonBlock:(void(^)())block{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    view.frame = frame;
    
    // 2. 创建图片
    GWImageView *imageView = [[GWImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image;
    [view addSubview:imageView];
    
    // 3. 创建label
    UILabel *titleLabel = [GWViewTool createLabelFont:@"提示" textColor:@"灰"];
    titleLabel.text = text;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
    
    CGFloat margin = (frame.size.height - [NSString contentofHeight:titleLabel.font] - image.size.height) / 3.;
    
    [imageView sizeToFit];
    imageView.orgin_y = margin;
    imageView.center_x = frame.size.width / 2.;
    titleLabel.frame= CGRectMake(0, CGRectGetMaxY(imageView.frame) + margin, frame.size.width, [NSString contentofHeight:titleLabel.font]);
    
    // 4. 创建按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    __weak typeof(self)weakSelf = self;
    [button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    [view addSubview:button];
    
    return view;
}

@end
