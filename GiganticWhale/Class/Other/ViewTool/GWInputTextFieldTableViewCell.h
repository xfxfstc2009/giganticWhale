//
//  GWInputTextFieldTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWInputTextFieldTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transfrCellHeight;              /**< cell高度*/
@property (nonatomic,copy)NSString *transferTitle;                  /**< 标题*/
@property (nonatomic,copy)NSString *transferPlaceholeder;           /**< placeholeder*/
@property (nonatomic,copy)NSString *transferInputText;              /**< 输入的信息*/

@property (nonatomic,strong)UITextField *inputTextField;            /**< 输入框*/

+(CGFloat)calculationCellHeight;



-(void)textFieldDidChangeBlock:(void(^)(NSString *info))block;

@end
