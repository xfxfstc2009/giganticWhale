//
//  GWSMSTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWSMSTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

@property (nonatomic,strong)UITextField *textField;

-(void)actionClickWithSms:(void(^)())block;

-(void)hasEnable:(BOOL)enable;

-(void)textFieldDidChangeBlock:(void (^)(NSString *))block;
@end
