//
//  GWUploadFileManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/6.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AliOSSManager.h"
#import "GWDiarySingleModel.h"

@interface GWUploadFileManager : NSObject

+ (GWUploadFileManager *)sharedUploadManager;
#pragma mark - 提交日记信息
-(void)sendRequesTouploadDiaryInfoWithDiarySingleModel:(GWDiarySingleModel *)singleModel block:(void(^)(BOOL successed,GWDiarySingleModel *backSingleModel))block;

#pragma mark - 提交Laungch


-(void)uploadWithType:(OSSUploadType)type fileArr:(NSArray<OSSSingleFileModel> *)fileArr fileBlock:(void(^)(NSString *fileMainUrl))block;

@end
