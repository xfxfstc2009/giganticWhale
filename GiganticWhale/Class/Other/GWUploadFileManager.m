//
//  GWUploadFileManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/6.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWUploadFileManager.h"
#import "GWDiarySingleModel.h"

@interface GWUploadFileManager()
@property (nonatomic,copy)NSString *fileMainStr;

@end

@implementation GWUploadFileManager

+ (GWUploadFileManager *)sharedUploadManager{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - 提交日记信息
-(void)sendRequesTouploadDiaryInfoWithDiarySingleModel:(GWDiarySingleModel *)singleModel block:(void(^)(BOOL successed,GWDiarySingleModel *backSingleModel))block{
    NSMutableDictionary *dic =[NSMutableDictionary dictionary];
    if (singleModel.showLocation){
        // 1.精度
        if ([GWLocationMethod sharedLocationManager].lat){
            [dic setObject:@([GWLocationMethod sharedLocationManager].lat) forKey:@"lat"];
            singleModel.lat = [GWLocationMethod sharedLocationManager].lat;
        }
        // 2.维度
        if ([GWLocationMethod sharedLocationManager].lng){                              // 维度
            [dic setObject:@([GWLocationMethod sharedLocationManager].lng) forKey:@"lng"];
            singleModel.lat = [GWLocationMethod sharedLocationManager].lng;
        }
        // 3.省
        if ([GWLocationMethod sharedLocationManager].addressComponent.province.length){        // 省
            [dic setObject:[GWLocationMethod sharedLocationManager].addressComponent.province forKey:@"province"];
            singleModel.province = [GWLocationMethod sharedLocationManager].addressComponent.province;
        }
        // 4.市
        if ([GWLocationMethod sharedLocationManager].addressComponent.city.length){            // 市
            [dic setObject:[GWLocationMethod sharedLocationManager].addressComponent.city forKey:@"city"];
            singleModel.city = [GWLocationMethod sharedLocationManager].addressComponent.city;
        }
        // 5.区
        if ([GWLocationMethod sharedLocationManager].addressComponent.district.length){        // 区
            [dic setObject:[GWLocationMethod sharedLocationManager].addressComponent.district forKey:@"district"];
            singleModel.district = [GWLocationMethod sharedLocationManager].addressComponent.district;
        }
        // 6.街道
        if ([GWLocationMethod sharedLocationManager].addressComponent.township.length){        // 街道
            [dic setObject:[GWLocationMethod sharedLocationManager].addressComponent.township forKey:@"township"];
            singleModel.township = [GWLocationMethod sharedLocationManager].addressComponent.township;
        }
        // 7.小区
        if ([GWLocationMethod sharedLocationManager].addressComponent.neighborhood.length){    // 小区
            [dic setObject:[GWLocationMethod sharedLocationManager].addressComponent.neighborhood forKey:@"neighborhood"];
            singleModel.neighborhood = [GWLocationMethod sharedLocationManager].addressComponent.neighborhood;
        }
    }
    // 8.标题
    [dic setObject:[NSDate getCurrentTime] forKey:@"title"];                        // 标题
    singleModel.title = [NSDate getCurrentTime];
    // 9.内容
    if (singleModel.desc.length){
        [dic setObject:singleModel.desc forKey:@"desc"];                     // 内容
    }
    
    __weak typeof(self)weakSelf = self;
    [self uploadWithType:OSSUploadTypeDiary fileArr:singleModel.aliOSSArr fileBlock:^(NSString *fileMainUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [dic setObject:fileMainUrl forKey:@"img"];
        [strongSelf sendRequestToUploadDic:dic block:^(BOOL successed, GWDiarySingleModel *backSingleModel) {
            if (block){
                block(successed,backSingleModel);
            }
        }];
    }];
}

-(void)uploadWithType:(OSSUploadType)type fileArr:(NSArray<OSSSingleFileModel> *)fileArr fileBlock:(void(^)(NSString *fileMainUrl))block{
    self.fileMainStr = @"";
    dispatch_queue_t queue = dispatch_queue_create("Blur queue", NULL);
    __weak typeof(self)weakSelf = self;
    dispatch_async(queue, ^ {
        for (int i = 0 ; i < fileArr.count;i++){
            OSSSingleFileModel *fileObjc = [fileArr objectAtIndex:i];
            [AliOSSManager uploadFileWithObjctype:type name:fileObjc.objcName objImg:fileObjc.objcImg block:^(NSString *fileUrl) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (i == fileArr.count -1){
                    strongSelf.fileMainStr = [strongSelf.fileMainStr stringByAppendingString:fileUrl];
                } else {
                    strongSelf.fileMainStr = [strongSelf.fileMainStr stringByAppendingString:[NSString stringWithFormat:@"%@,",fileUrl]];
                }
                
            } progress:^(CGFloat progress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [StatusBarManager statusBarShowWithText:[NSString stringWithFormat:@"正在上传第%li张图片",(long)(i + 1)]];
                });
            }];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [StatusBarManager statusBarHidenWithText:@"正在提交信息"];
            if (block){
                block(self.fileMainStr);
            }
        });
    });
}
-(void)sendRequestToUploadDic:(NSDictionary *)dictionary block:(void(^)(BOOL successed,GWDiarySingleModel *backSingleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:diary_add requestParams:dictionary responseObjectClass:[GWDiarySingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            GWDiarySingleModel *singleModel = (GWDiarySingleModel *)responseObject;
            
            [StatusBarManager statusBarHidenWithText:@"日记提交成功"];
            if (block){
                block(YES,singleModel);
            }
        } else {
            [StatusBarManager statusBarHidenWithText:@"日记提交失败"];
            if (block){
                block(NO,nil);
            }
        }
    }];
}



@end
