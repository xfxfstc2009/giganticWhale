//
//  GWAssetsCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAssetsCollectionViewCell.h"

@interface GWAssetsCollectionViewCell()
@property (nonatomic,strong)UIButton *chooseImgButton;

@end


@implementation GWAssetsCollectionViewCell
#define kThumbnailLength    100

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self createView];
        self.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.containerView = [[UIView alloc]init];
    self.containerView.backgroundColor = [UIColor clearColor];
    self.containerView.frame = self.bounds;
    self.containerView.clipsToBounds = YES;
    [self addSubview:self.containerView];
    
    self.assetsImageView = [[UIImageView alloc]init];
    self.assetsImageView.backgroundColor = [UIColor clearColor];
    self.assetsImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.assetsImageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    [self.containerView addSubview:self.assetsImageView];
    
    // 2. 创建图片选择img
    self.chooseImgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chooseImgButton.backgroundColor = [UIColor clearColor];
    [self.chooseImgButton setImage:[UIImage imageNamed:@"asset_btn_select_nor"] forState:UIControlStateNormal];
    self.chooseImgButton.frame = CGRectMake(self.assetsImageView.bounds.size.width - LCFloat(40), 0, LCFloat(40), LCFloat(40));
    self.chooseImgButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [weakSelf.chooseImgButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if ([strongSelf.transferSelectedAssets containsObject:strongSelf.transferSingleAsset]){
            [strongSelf.transferSelectedAssets removeObject:strongSelf.transferSingleAsset];
            [GWTool animationClickShowWithView:strongSelf.chooseImgButton block:^{
                [self.chooseImgButton setImage:[UIImage imageNamed:@"asset_btn_select_nor"] forState:UIControlStateNormal];
            }];
        } else {
            if (self.transferSelectedAssets.count >= self.fixedTransferMaxSelected){
                NSString *errMsg = [NSString stringWithFormat:@"您最多只能选择%li张",(long)self.fixedTransferMaxSelected];
                [[UIAlertView alertViewWithTitle:nil message:errMsg buttonTitles:@[@"确定"] callBlock:NULL]show];
                return;
            }
            [strongSelf.transferSelectedAssets addObject:strongSelf.transferSingleAsset];
            [GWTool animationClickShowWithView:strongSelf.chooseImgButton block:^{
                [self.chooseImgButton setImage:[UIImage imageNamed:@"asset_btn_select_hlt"] forState:UIControlStateNormal];
            }];
        }
        if (strongSelf.assetSelectedBlock){
            strongSelf.assetSelectedBlock();
        }
    }];
    [self addSubview:self.chooseImgButton];
}




-(void)setTransferSingleAsset:(ALAsset *)transferSingleAsset{
    _transferSingleAsset = transferSingleAsset;
    
    CGImageRef posterImage = [transferSingleAsset aspectRatioThumbnail];
    // 1. 获取当前图片高度
    size_t posterImageHeight = CGImageGetHeight(posterImage);
    CGFloat scale = posterImageHeight / kThumbnailLength;
    self.assetsImageView.image = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
    
    // 基本尺寸参数
    CGSize boundsSize = self.bounds.size;
    CGFloat boundsWidth = boundsSize.width;
    CGFloat boundsHeight = boundsSize.height;
    
    CGSize imageSize = self.assetsImageView.image.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    CGRect imageFrame = CGRectMake(0, (boundsHeight - boundsWidth) / 2., boundsWidth, imageHeight * boundsWidth / imageWidth);
    
    if (imageHeight > imageWidth){          // 图片高度大于图片宽度
        if (imageFrame.size.height < boundsHeight) {
            imageFrame.origin.y = floorf((boundsHeight - imageFrame.size.height) / 2.0);
        } else {
            imageFrame.origin.y = 0;
        }
    } else {                                // 图片宽度大于高度
        if (imageFrame.size.width < boundsWidth){
            imageFrame.origin.x = 0;
            imageFrame.size.width = (boundsHeight * 1.00 / imageFrame.size.height) *imageFrame.size.width;
            imageFrame.size.height = boundsHeight;
        } else {
            imageFrame.size.width = (boundsHeight / imageFrame.size.height) * imageFrame.size.width;
            imageFrame.size.height = boundsHeight;
            imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
        }
    }
    self.assetsImageView.frame = imageFrame;
}

-(void)setTransferSelectedAssets:(NSMutableArray *)transferSelectedAssets{
    _transferSelectedAssets = transferSelectedAssets;
    if ([self.transferSelectedAssets containsObject:self.transferSingleAsset]){         // 如果包含
        [self.chooseImgButton setImage:[UIImage imageNamed:@"asset_btn_select_hlt"] forState:UIControlStateNormal];
    } else {
        [self.chooseImgButton setImage:[UIImage imageNamed:@"asset_btn_select_nor"] forState:UIControlStateNormal];
    }
}

@end
