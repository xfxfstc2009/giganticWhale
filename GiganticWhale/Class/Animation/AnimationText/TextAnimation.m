//
//  TextAnimation.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "TextAnimation.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import <objc/runtime.h>

static char animationFinishBlock;
@interface TextAnimation(){

}
@property (nonatomic,strong)CALayer *animationLayer;
@property (nonatomic,strong)CAShapeLayer *pathLayerG;
@property (nonatomic,strong)CAShapeLayer *pathLayerI;
@property (nonatomic,strong)CAShapeLayer *pathLayerG2;
@property (nonatomic,strong)CAShapeLayer *pathLayerA;
@property (nonatomic,strong)CAShapeLayer *pathLayerN;
@property (nonatomic,strong)CAShapeLayer *pathLayerT;
@property (nonatomic,strong)CAShapeLayer *pathLayerI2;
@property (nonatomic,strong)CAShapeLayer *pathLayerC;
@property (nonatomic,strong)CAShapeLayer *pathLayerW;
@property (nonatomic,strong)CAShapeLayer *pathLayerH;
@property (nonatomic,strong)CAShapeLayer *pathLayerA2;
@property (nonatomic,strong)CAShapeLayer *pathLayerL;
@property (nonatomic,strong)CAShapeLayer *pathLayerE;

@end

@implementation TextAnimation

-(instancetype)initWithFrame:(CGRect)frame  title:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self){
        self.size_height = [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
        [self createView];
        [self setupTextLayerWithTitle:title];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        self.size_height = [NSString contentofHeight:[UIFont fontWithName:@"HelveticaNeue-Bold" size:37]];
        [self createView];
        
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.animationLayer = [CALayer layer];
    self.animationLayer.frame = self.bounds;
    [self.layer addSublayer:self.animationLayer];
}

-(void)cleanPath{
    [self cleanPathWithLayer:self.pathLayerG];
    [self cleanPathWithLayer:self.pathLayerI];
    [self cleanPathWithLayer:self.pathLayerG2];
    [self cleanPathWithLayer:self.pathLayerA];
    [self cleanPathWithLayer:self.pathLayerN];
    [self cleanPathWithLayer:self.pathLayerT];
    [self cleanPathWithLayer:self.pathLayerI2];
    [self cleanPathWithLayer:self.pathLayerC];
    [self cleanPathWithLayer:self.pathLayerW];
    [self cleanPathWithLayer:self.pathLayerH];
    [self cleanPathWithLayer:self.pathLayerA2];
    [self cleanPathWithLayer:self.pathLayerL];
    [self cleanPathWithLayer:self.pathLayerE];
}

-(void)cleanPathWithLayer:(CAShapeLayer *)layer{
    if (layer){
        [layer removeFromSuperlayer];
        layer = nil;
    }
}

-(void)setupTextLayerWithTitle:(NSString *)title{
    //    [self cleanPath];
    
    CGMutablePathRef letters = CGPathCreateMutable();
    CTFontRef font = CTFontCreateWithName(CFSTR("HelveticaNeue-Bold"), 37.0f, NULL);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id _Nonnull)((CTFontRef)font), kCTFontAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:title attributes:attrs];
    CTLineRef line = CTLineCreateWithAttributedString((CFAttributedStringRef)attrString);
    CFArrayRef runArray = CTLineGetGlyphRuns(line);
    for (CFIndex runIndex = 0 ; runIndex < CFArrayGetCount(runArray);runIndex++){
        CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, runIndex);
        CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
        for (CFIndex runGlyphIndex = 0; runGlyphIndex < CTRunGetGlyphCount(run); runGlyphIndex++) {
            CFRange thisGlyphRange = CFRangeMake(runGlyphIndex, 1);
            CGGlyph glyph;
            CGPoint position;
            CTRunGetGlyphs(run, thisGlyphRange, &glyph);
            CTRunGetPositions(run, thisGlyphRange, &position);
            {
                CGMutablePathRef letters1 = CGPathCreateMutable();
                CGPathRef letter = CTFontCreatePathForGlyph(runFont, glyph, NULL);
                CGAffineTransform t = CGAffineTransformMakeTranslation(position.x, position.y);
                CGPathAddPath(letters1, &t, letter);
                CGPathAddPath(letters, &t, letter);
                
                UIBezierPath *path = [UIBezierPath bezierPath];
                [path moveToPoint:CGPointZero];
                [path appendPath:[UIBezierPath bezierPathWithCGPath:letters1]];
                
                [self autoPathWithPath:path andIndex:runGlyphIndex];
                
                CGPathRelease(letters1);
            }
        }
    }
    CFRelease(line);
    
    CGPathRelease(letters);
    CFRelease(font);
}


#pragma mark - setting Frame
-(void)autoPathWithPath:(UIBezierPath *)path andIndex:(NSInteger)index{
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    CGFloat fontHeight = [NSString contentofHeight:GFont];

    if (index == 0){                // G
        self.pathLayerG = pathLayer;
        CGSize contentOfSize = [@"G" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerG.frame = CGRectMake(0, 0, contentOfSize.width, fontHeight);
    } else if (index == 1){         // I
        self.pathLayerI = pathLayer;
        CGSize contentOfSize = [@"i" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerI.frame = CGRectMake(CGRectGetMaxX(self.pathLayerG.frame) + LCFloat(2), 0, contentOfSize.width, fontHeight);
    } else if (index == 2){         // G
        self.pathLayerG2 = pathLayer;
        CGSize contentOfSize = [@"g" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerG2.frame = CGRectMake(CGRectGetMaxX(self.pathLayerI.frame) + LCFloat(5), 7, contentOfSize.width, fontHeight);
    } else if (index == 3){         // A
        self.pathLayerA = pathLayer;
        CGSize contentOfSize = [@"a" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerA.frame = CGRectMake(CGRectGetMaxX(self.pathLayerG2.frame) + LCFloat(5), 3, contentOfSize.width, fontHeight);
    } else if (index == 4){         // N
        self.pathLayerN = pathLayer;
        CGSize contentOfSize = [@"n" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerN.frame = CGRectMake(CGRectGetMaxX(self.pathLayerA.frame) + LCFloat(4), 3, contentOfSize.width, fontHeight);
    } else if (index == 5){         // T
        self.pathLayerT = pathLayer;
        CGSize contentOfSize = [@"t" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerT.frame = CGRectMake(CGRectGetMaxX(self.pathLayerN.frame) + LCFloat(4), 1, contentOfSize.width, fontHeight);
    } else if (index == 6){         // I
        self.pathLayerI2 = pathLayer;
        CGSize contentOfSize = [@"i" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerI2.frame = CGRectMake(CGRectGetMaxX(self.pathLayerT.frame) + LCFloat(3), 0, contentOfSize.width, fontHeight);
    } else if (index == 7){         // C
        self.pathLayerC = pathLayer;
        CGSize contentOfSize = [@"c" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerC.frame = CGRectMake(CGRectGetMaxX(self.pathLayerI2.frame) + LCFloat(5), 4, contentOfSize.width, fontHeight);
    } else if (index == 8){         // W
        self.pathLayerW = pathLayer;
        CGSize contentOfSize = [@"W" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerW.frame = CGRectMake(CGRectGetMaxX(self.pathLayerC.frame) + LCFloat(6), 0, contentOfSize.width, fontHeight);
    } else if (index == 9){         // H
        self.pathLayerH = pathLayer;
        CGSize contentOfSize = [@"h" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerH.frame = CGRectMake(CGRectGetMaxX(self.pathLayerW.frame) + LCFloat(3), 0, contentOfSize.width, fontHeight);
    } else if (index == 10){        // A
        self.pathLayerA2 = pathLayer;
        CGSize contentOfSize = [@"a" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerA2.frame = CGRectMake(CGRectGetMaxX(self.pathLayerH.frame) + LCFloat(4), 4, contentOfSize.width, fontHeight);
    } else if (index == 11){        // L
        self.pathLayerL = pathLayer;
        CGSize contentOfSize = [@"l" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerL.frame = CGRectMake(CGRectGetMaxX(self.pathLayerA2.frame) + LCFloat(2), 0, contentOfSize.width, fontHeight);
    } else if (index == 12){        // E
        self.pathLayerE = pathLayer;
        CGSize contentOfSize = [@"e" sizeWithCalcFont:GFont constrainedToSize:CGSizeMake(CGFLOAT_MAX, fontHeight)];
        self.pathLayerE.frame = CGRectMake(CGRectGetMaxX(self.pathLayerL.frame) + LCFloat(6), 4, contentOfSize.width, fontHeight);
    }
    
    pathLayer.bounds = CGPathGetBoundingBox(path.CGPath);
    pathLayer.geometryFlipped = YES;
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor =    [[UIColor clearColor] CGColor];
    pathLayer.fillColor = nil;
    pathLayer.lineWidth = .5f;
    pathLayer.lineJoin = kCALineJoinBevel;
    
    self.size_width = CGRectGetMaxX(self.pathLayerE.frame) + LCFloat(3);
    
    [self.animationLayer addSublayer:pathLayer];
}

#pragma mark - Show Animation
-(void)showAnimation{
    [self.pathLayerG removeAllAnimations];
    [self.pathLayerI removeAllAnimations];
    [self.pathLayerG2 removeAllAnimations];
    [self.pathLayerA removeAllAnimations];
    [self.pathLayerN removeAllAnimations];
    [self.pathLayerT removeAllAnimations];
    [self.pathLayerI removeAllAnimations];
    [self.pathLayerC removeAllAnimations];
    [self.pathLayerW removeAllAnimations];
    [self.pathLayerH removeAllAnimations];
    [self.pathLayerA2 removeAllAnimations];
    [self.pathLayerL removeAllAnimations];
    [self.pathLayerE removeAllAnimations];
    
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.5;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:@"0" forKey:@"anim1"];
    
    [self.pathLayerG addAnimation:pathAnimation forKey:@"ani1"];
}

#pragma mark - AniamtionDelegate
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:@"anim1"] isEqualToString:@"0"]){
        [self tempAnimationLayer:self.pathLayerG];
        [self showAnimation2WithIndex:0];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"0"]){
        [self showAnimationManager1WithIndex:1];
    } else if ([[anim valueForKey:@"anim1"] isEqualToString:@"1"]){
        [self tempAnimationLayer:self.pathLayerI];
        [self showAnimation2WithIndex:1];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"1"]){
        [self showAnimationManager1WithIndex:2];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"2"]){
        [self tempAnimationLayer:self.pathLayerG2];
        [self showAnimation2WithIndex:2];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"2"]){
        [self showAnimationManager1WithIndex:3];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"3"]){
        [self tempAnimationLayer:self.pathLayerA];
        [self showAnimation2WithIndex:3];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"3"]){
        [self showAnimationManager1WithIndex:4];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"4"]){
        [self tempAnimationLayer:self.pathLayerN];
        [self showAnimation2WithIndex:4];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"4"]){
        [self showAnimationManager1WithIndex:5];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"5"]){
        [self tempAnimationLayer:self.pathLayerT];
        [self showAnimation2WithIndex:5];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"5"]){
        [self showAnimationManager1WithIndex:6];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"6"]){
        [self tempAnimationLayer:self.pathLayerI2];
        [self showAnimation2WithIndex:6];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"6"]){
        [self showAnimationManager1WithIndex:7];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"7"]){
        [self tempAnimationLayer:self.pathLayerC];
        [self showAnimation2WithIndex:7];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"7"]){
        [self showAnimationManager1WithIndex:8];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"8"]){
        [self tempAnimationLayer:self.pathLayerW];
        [self showAnimation2WithIndex:8];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"8"]){
        [self showAnimationManager1WithIndex:9];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"9"]){
        [self tempAnimationLayer:self.pathLayerH];
        [self showAnimation2WithIndex:9];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"9"]){
        [self showAnimationManager1WithIndex:10];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"10"]){
        [self tempAnimationLayer:self.pathLayerA2];
        [self showAnimation2WithIndex:10];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"10"]){
        [self showAnimationManager1WithIndex:11];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"11"]){
        [self tempAnimationLayer:self.pathLayerL];
        [self showAnimation2WithIndex:11];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"11"]){
        [self showAnimationManager1WithIndex:12];
    }else if ([[anim valueForKey:@"anim1"] isEqualToString:@"12"]){
        [self tempAnimationLayer:self.pathLayerE];
        [self showAnimation2WithIndex:12];
    } else if ([[anim valueForKey:@"anim2"] isEqualToString:@"12"]){
        // 结束
        void(^block)() = objc_getAssociatedObject(self, &animationFinishBlock);
        if (block){
            block();
        }
    }
}

#pragma mark - Animation 1
-(void)showAnimationManager1WithIndex:(NSInteger)index{
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.01f;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:[NSString stringWithFormat:@"%li",(long)index] forKey:@"anim1"];
    
    if (index == 0){
        [self.pathLayerG addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 1){
        [self.pathLayerI addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 2){
        [self.pathLayerG addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 3){
        [self.pathLayerA addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 4){
        [self.pathLayerN addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 5){
        [self.pathLayerT addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 6){
        [self.pathLayerI addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 7){
        [self.pathLayerC addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 8){
        [self.pathLayerW addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 9){
        [self.pathLayerH addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 10){
        [self.pathLayerA2 addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 11){
        [self.pathLayerL addAnimation:pathAnimation forKey:@"anim1"];
    } else if (index == 12){
        [self.pathLayerE addAnimation:pathAnimation forKey:@"anim1"];
    }
}

#pragma mark - 动画2
-(void)showAnimation2WithIndex:(NSInteger)index{
    // 【翻转】
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    animation.duration = .25f;                                              // 持续时间
    animation.repeatCount = 1;                                              // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: M_PI];                 // 起始角度
    animation.toValue = [NSNumber numberWithFloat:2 * M_PI];                // 终止角度
    animation.delegate = self;
    NSInteger indexValue = index ;
    [animation setValue:[NSString stringWithFormat:@"%li",(long)indexValue] forKey:@"anim2"];
    
    // 【渐变】
    CABasicAnimation *animation2=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation2.fromValue=[NSNumber numberWithFloat:.0f];
    animation2.toValue=[NSNumber numberWithFloat:1.0f];
    animation2.duration = .3f;
    animation2.repeatCount = 1;
    animation2.removedOnCompletion = NO;
    animation2.fillMode = kCAFillModeForwards;
    
    // 【组合】
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = .3f;
    group.repeatCount = 1;
    group.animations = [NSArray arrayWithObjects:animation, animation2, nil];
    group.delegate = self;
    [group setValue:[NSString stringWithFormat:@"%li",(long)indexValue] forKey:@"anim2"];
    
    if (indexValue == 0){
        [self.pathLayerG addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 1){
        [self.pathLayerI addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 2){
        [self.pathLayerG2 addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 3){
        [self.pathLayerA addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 4){
        [self.pathLayerN addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 5){
        [self.pathLayerT addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 6){
        [self.pathLayerI2 addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 7){
        [self.pathLayerC addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 8){
        [self.pathLayerW addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 9){
        [self.pathLayerH addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 10){
        [self.pathLayerA2 addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 11){
        [self.pathLayerL addAnimation:group forKey:@"anim2"];
    } else if (indexValue == 12){
        [self.pathLayerE addAnimation:group forKey:@"anim2"];
    }
}



#pragma mark - temp
-(void)tempAnimationLayer:(CAShapeLayer *)layer{
    layer.fillColor = [UIColor whiteColor].CGColor;
    layer.strokeColor = [UIColor clearColor].CGColor;
}

-(void)showAnimationWithFinishBlock:(void(^)())finish{
    objc_setAssociatedObject(self, &animationFinishBlock, finish, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self showAnimation];
}
@end
