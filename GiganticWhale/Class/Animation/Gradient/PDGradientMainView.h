//
//  PDGradientMainView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDGradientMainView : UIView

@property (nonatomic,strong)NSArray *transferImgArr;            /**< 传入的图片数组*/

@end
