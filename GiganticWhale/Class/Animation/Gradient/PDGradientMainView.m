//
//  PDGradientMainView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGradientMainView.h"
#import "PDGradientMaskView.h"
#import "GCD.h"

typedef enum : NSUInteger {
    kTypeOne,
    kTypeTwo,
} EType;

@interface PDGradientMainView()
//@property (nonatomic,strong)NSArray *imgArr;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,strong)PDGradientMaskView *tranformFadeViewOne;            /**< 蒙层1*/
@property (nonatomic,strong)PDGradientMaskView *tranformFadeViewTwo;            /**< 蒙*/
@property (nonatomic,strong)GCDTimer *timer;                                    /**< */
@property (nonatomic,assign)EType type;                                         /**< 切换类型*/

@end

@implementation PDGradientMainView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self setup];
    }
    return self;
}
-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
    
    _transferImgArr = @[@"about/meLighthouse.jpg",@"about/meTulips.jpg",@"about/meLighthouse.jpg"];
    
    __weak typeof(self) weakSelf = self;
    [self.timer event:^{
        [weakSelf timerEvent];
    } timeIntervalWithSecs:3 delaySecs:0.f];
    [self.timer start];
}

-(void)setup{
    self.tranformFadeViewOne               = [[PDGradientMaskView alloc] initWithFrame:self.bounds];
    self.tranformFadeViewOne.contentMode   = UIViewContentModeScaleAspectFill;
    self.tranformFadeViewOne.fadeDuradtion = 4.f;
    [self.tranformFadeViewOne.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {

    }];
    [self addSubview:self.tranformFadeViewOne];
    
    self.tranformFadeViewTwo               = [[PDGradientMaskView alloc] initWithFrame:self.bounds];
    self.tranformFadeViewTwo.contentMode   = UIViewContentModeScaleAspectFill;
    self.tranformFadeViewTwo.fadeDuradtion = 4.f;
    [self addSubview:self.tranformFadeViewTwo];
    [self.tranformFadeViewTwo fadeAnimated:NO];
    
    // timer
    self.timer = [[GCDTimer alloc] initInQueue:[GCDQueue mainQueue]];
}

- (void)timerEvent {
    if (self.type == kTypeOne) {
        self.type = kTypeTwo;
        [self sendSubviewToBack:self.tranformFadeViewTwo];
        [self.tranformFadeViewTwo.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {
            [self.tranformFadeViewTwo showAnimated:NO];
            [self.tranformFadeViewOne fadeAnimated:YES];
        }];
    } else {
        self.type = kTypeOne;
        [self sendSubviewToBack:self.tranformFadeViewOne];
        [self.tranformFadeViewOne.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {
            [self.tranformFadeViewOne showAnimated:NO];
            [self.tranformFadeViewTwo fadeAnimated:YES];
        }];
    }
}

- (NSString *)currentImage {
    self.count = ++ self.count % self.transferImgArr.count;
    return self.transferImgArr[self.count];
}
@end
