//
//  GWBarMainTitleView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/3.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWBarMainTitleView : UIView

@property (nonatomic,copy)NSString *titleText;
-(instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;
@end
