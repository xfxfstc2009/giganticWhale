//
//  GWAboutViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/1.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutViewController.h"
#import "GWAboutMeTableViewCell.h"              // 1. 头像cell
#import "GWAboutOtherCell.h"                    // 2. 其他cell
#import "GWAboutProductCell.h"                  // 3. 我的作品cell
#import "GWAboutCompaynCell.h"                  // 4. 我的公司cell
#import "GWAboutMeDetailCell.h"                 // 5. 关于我照片
#import "GWAboutSchoolCell.h"                   // 6. 关于我的学校
// Model
#import "GWAboutMeModel.h"
#import "GWAboutCompanyListModel.h"
#import "UIScrollView+QSPullToRefresh.h"
#import "GWAboutProductListModel.h"
#import "PDSchoolListModel.h"                   // 我的学校
#import "GWAboutProductTypeCell.h"

#import "ConnectionQQ.h"                        // 跳转QQ
#import "PopView.h"

@interface GWAboutViewController()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,GWAboutCompaynCellDelegate,GWAboutSchoolCellDelegate>{
    GWAboutMeModel *aboutMeModel;
}
@property (nonatomic,strong)UITableView *mainTableView;

@property (nonatomic,strong)NSMutableArray *myInfoArr;                      /**< 关于我数组*/
@property (nonatomic,strong)NSMutableArray *myProductArr;                   /**< 关于我的作品数组*/
@property (nonatomic,strong)NSMutableArray *myCompanyMutableArr;            /**< 我的公司数组*/
@property (nonatomic,strong)NSMutableArray *mySchoolMutableArr;             /**< 我的学校数组*/
@property (nonatomic,strong)NSMutableArray *mySkillMutableArr;              /**< 我的技能*/
@property (nonatomic,strong)NSMutableArray *mainMutableArray;               /**< 总数组*/

// 【关于我】
@property (nonatomic,strong)UITextField *emailTextFiled;
@property (nonatomic,strong)UIAlertView *emialAlert;
@end

@implementation GWAboutViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.view.backgroundColor = [UIColor clearColor];
}

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self interface];
}

-(void)interface{
    __weak typeof(self)weakSelf = self;
    if (self.aboutType == aboutTypeAboutMe){                // 【关于我】
        if (!self.myInfoArr){
            self.myInfoArr = [NSMutableArray array];
        }
        [weakSelf sendRequestRequestGetMe];
    } else if (self.aboutType == aboutTypeCompany){         // 【我的公司】
        if (!self.myCompanyMutableArr){
            self.myCompanyMutableArr = [NSMutableArray array];
        }
        [weakSelf sendRequestRequestGetCompany];
    } else if (self.aboutType == aboutTypeProduct){         // 【我的作品】
        if (!self.myProductArr){
            self.myProductArr = [NSMutableArray array];
        }
        [weakSelf sendRequestToGetMyProducts];
    } else if (self.aboutType == aboutTypeSchool){          // 【我的学校】
        if (!self.mySchoolMutableArr){
            self.mySchoolMutableArr = [NSMutableArray array];
        }
        [weakSelf sendRequestToGetMySchool];
    }
//    else if (self.aboutType == aboutTypeSkill){           // 【我的技能】
//        if (!self.mySkillMutableArr){
//            self.mySkillMutableArr = [NSMutableArray array];
//        }
//    }
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mainMutableArray = [NSMutableArray array];
    [self.mainMutableArray removeAllObjects];
    
    if (self.aboutType == aboutTypeAboutMe){                // 【关于我】
        self.myInfoArr = [NSMutableArray array];
    } else if (self.aboutType == aboutTypeCompany){         // 【关于我的公司】
        self.myCompanyMutableArr = [NSMutableArray array];
    } else if (self.aboutType == aboutTypeProduct){         // 【关于我的作品】
        self.myProductArr = [NSMutableArray array];
    } else if (self.aboutType == aboutTypeSchool){          // 【关于我的学校】
        self.mySchoolMutableArr = [NSMutableArray array];
    }
//    else if (self.aboutType == aboutTypeSkill){           // 【关于我的技能】
//        self.mySkillMutableArr = [NSMutableArray array];
//    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height - 64) style:UITableViewStylePlain];
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.mainTableView.showsVerticalScrollIndicator = YES;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mainTableView];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.mainTableView addQSPullToRefreshWithActionHandler:^{              // 下翻
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf loadLastNews];
    }];

    [self.mainTableView addQSInfiniteScrollingWithActionHandler:^{          // 上翻
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf loadNextNews];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mainMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.aboutType == aboutTypeCompany || (self.aboutType == aboutTypeSchool)){
        return 1;
    }
    NSArray *sectionOfArr = [self.mainMutableArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (self.aboutType == aboutTypeAboutMe){                    // 【关于我】
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像"]){
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"头像"]){
                static NSString *cellIdentifyWithAboutMeRowOne = @"cellIdentifyWithAboutMeRowOne";
                GWAboutMeTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithAboutMeRowOne];
                if (!cellWithRowOne){
                    cellWithRowOne = [[GWAboutMeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithAboutMeRowOne];
                    cellWithRowOne.backgroundColor = [UIColor clearColor];
                }
                cellWithRowOne.transferCellHeight = cellHeight;
                cellWithRowOne.transferAboutMeModel = aboutMeModel;
                return cellWithRowOne;
            } else {
                static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
                GWAboutOtherCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
                if (!cellWithRowTwo){
                    cellWithRowTwo = [[GWAboutOtherCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                }
                cellWithRowTwo.transferCellHeight = cellHeight;
                if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的主页"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的主页"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeMainPage;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的博客"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的博客"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeBoke;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"生日"] && indexPath.row == [self cellIndexPathRowWithcellData:@"生日"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeBirthDay;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Email"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Email"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeEmail;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"居住地"] && indexPath.row == [self cellIndexPathRowWithcellData:@"居住地"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeResidence;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"简介"] && indexPath.row == [self cellIndexPathRowWithcellData:@"简介"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeDesc;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的手机"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的手机"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypePhone;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的QQ"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的QQ"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeQQ;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的学历"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的学历"]){
                    cellWithRowTwo.transferAboutType = AboutMeOtherCellTypeEducation;
                }
                
                cellWithRowTwo.transferAboutMeModel = aboutMeModel;
                
                return cellWithRowTwo;
            }
        } else {
            NSLog(@"%li,%li",indexPath.section,indexPath.row);
            static NSString *cellIdentifyWithRowOthe = @"cellIdentifyWithRowOthe";
            GWAboutMeDetailCell *cellWithRowOthe = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOthe];
            if (!cellWithRowOthe){
                cellWithRowOthe = [[GWAboutMeDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOthe];
                cellWithRowOthe.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOthe.transferCellHeight = cellHeight;
            cellWithRowOthe.transferAboutMeDetailModel = [[self.mainMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithRowOthe;
        }

    } else if (self.aboutType == aboutTypeCompany){          // 【关于我的公司】
        static NSString *cellIdentifyWithCompanyRowThr = @"cellIdentifyWithCompanyRowThr";
        GWAboutCompaynCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithCompanyRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWAboutCompaynCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithCompanyRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.delegate = self;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferCompanySingleModel = [self.myCompanyMutableArr objectAtIndex:indexPath.section] ;
        
        return cellWithRowThr;
    } else if (self.aboutType == aboutTypeProduct){         // 【我的作品】
        static NSString *cellIdentifyWithProductRowFour = @"cellIdentifyWithProductRowFour";
        GWAboutProductCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithProductRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWAboutProductCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithProductRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferProductSingleModel = [[self.myProductArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithRowFour;

    } else if (self.aboutType == aboutTypeSchool){          // 【我学校】
        static NSString *cellIdentifyWithSchoolRowFiv = @"cellIdentifyWithSchoolRowFiv";
        GWAboutSchoolCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSchoolRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[GWAboutSchoolCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSchoolRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowFiv.delegate = self;
        }
        PDSchoolSingleModel *schoolSingleModel = [self.mySchoolMutableArr objectAtIndex:indexPath.section];
        cellWithRowFiv.transferSchoolSingleModel = schoolSingleModel;
        cellWithRowFiv.tramsferCellHeight = cellHeight;
        
        return cellWithRowFiv;
    }
//    else if (self.aboutType == aboutTypeSkill){           // 【我的技能】
//        static NSString *cellIdentifyWithSkillRowSex= @"cellIdentifyWithSkillRowOne";
//        UITableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSkillRowSex];
//        if (!cellWithRowSex){
//            cellWithRowSex = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSkillRowSex];
//            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
//
//        }
//        cellWithRowSex.textLabel.text = @"技能";
//        return cellWithRowSex;
//    }
    else {
        static NSString *cellIdentifyWithOtherRowEight = @"cellIdentifyWithOtherRowEight";
        UITableViewCell *cellWithRowEight = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithOtherRowEight];
        if (!cellWithRowEight){
            cellWithRowEight = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithOtherRowEight];
            cellWithRowEight.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowEight.textLabel.text = @"其他";
        return cellWithRowEight;
    }
    return nil;
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.aboutType == aboutTypeAboutMe){               // 【关于我】
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像"]){
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"头像"]){
                return [GWAboutMeTableViewCell calculationCellHeight];
            } else {
                if (indexPath.row == [self cellIndexPathRowWithcellData:@"简介"]){
                    return [GWAboutOtherCell calculationCellWithDescWithModel:aboutMeModel];
                } else {
                    return [GWAboutOtherCell calculationCellHeight];
                }
            }
        } else {
            return [GWAboutMeDetailCell calculationCellHeight];
        }
    } else if (self.aboutType == aboutTypeCompany){          // 【关于我的公司】
        GWAboutCompanySingleModel *companyModel = [self.myCompanyMutableArr objectAtIndex:indexPath.section];
        return [GWAboutCompaynCell caculationCellHeightWithModel:companyModel];
    } else if (self.aboutType == aboutTypeProduct){         //  【我的作品】
        GWAboutProductSingleModel *productSingleModel = [[self.myProductArr objectAtIndex:indexPath.section ]objectAtIndex:indexPath.row];
        return [GWAboutProductCell calculationCellHeightWithModel:productSingleModel];
    } else if (self.aboutType == aboutTypeSchool){          // 【我学校】
        PDSchoolSingleModel *schoolSingleModel = [self.mySchoolMutableArr objectAtIndex:indexPath.section];
        return [GWAboutSchoolCell calculationCellHeightWithSchoolSingleModel:schoolSingleModel];
    }
//    else if (self.aboutType == aboutTypeSkill){           // 【我的技能】
//        return 144;
//    }
    else {
        return 144;
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.aboutType == aboutTypeCompany){
        return LCFloat(10);
    } else if (self.aboutType == aboutTypeProduct){
        return LCFloat(10);
    } else if (self.aboutType == aboutTypeSchool){
        return LCFloat(10);
    }
    else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.aboutType == aboutTypeAboutMe){                        // 【关于我】
        [self clickManageWithIndexPath:indexPath];
    } else if (self.aboutType == aboutTypeCompany){                 // 【我的公司】
        GWAboutCompaynCell *cell = (GWAboutCompaynCell *)[self.mainTableView cellForRowAtIndexPath:indexPath];
        [cell animationWithShrinkManager];
    } else if (self.aboutType == aboutTypeProduct){                 // 【我的作品】
        GWAboutProductSingleModel *transferProductSingleModel = [[self.myProductArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [self openUrl:transferProductSingleModel.open_url appUrl:transferProductSingleModel.appId];
    } else if (self.aboutType == aboutTypeSchool){                  // 【我的学校】

    }
//    else if (self.aboutType == aboutTypeSkill){                   // 【我的技能】
    
//    }
}

#pragma mark - 【关于我】点击方法
-(void)clickManageWithIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的主页"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的主页"]){
        WebViewController *webViewController = [[WebViewController alloc]init];
        [webViewController webViewControllerWithAddress:aboutMeModel.home_url];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的博客"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的博客"]){
        WebViewController *webViewController = [[WebViewController alloc]init];
        [webViewController webViewControllerWithAddress:aboutMeModel.boke_url];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"生日"] && indexPath.row == [self cellIndexPathRowWithcellData:@"生日"]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Email"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Email"]){
        [self directionEmailManager];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"居住地"] && indexPath.row == [self cellIndexPathRowWithcellData:@"居住地"]){

        CGFloat xWidth = self.view.bounds.size.width - 70.0f;
        CGFloat yHeight = 272.0f;
        CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
        PopView *poplistview = [[PopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
        poplistview.type = PopViewTypeLocation;
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = aboutMeModel.lat;
        coordinate.longitude = aboutMeModel.lng;
        poplistview.transferCoordinate = coordinate;
        
        [poplistview viewShow];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"简介"] && indexPath.row == [self cellIndexPathRowWithcellData:@"简介"]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的手机"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的手机"]){
        [self directPhoneManager];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的QQ"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的QQ"]){
        [self directQQManager];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的学历"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的学历"]){
       
    }

}

#pragma mark - directManager
// 【跳转到QQ】
-(void)directQQManager{
    if (aboutMeModel.qq.length){
        __weak typeof(self)weakSelf = self;
        [[UIAlertView alertViewWithTitle:@"跳转QQ进行对话" message:aboutMeModel.qq buttonTitles:@[@"取消",@"跳转"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (buttonIndex == 1){
                [ConnectionQQ callQQWithNumber:strongSelf->aboutMeModel.qq];
            }
        }]show];
    }
}

// 【跳转到手机】
-(void)directPhoneManager{
    if (aboutMeModel.phone.length){
        __weak typeof(self)weakSelf = self;
        [[UIAlertView alertViewWithTitle:@"拨打电话" message:aboutMeModel.phone buttonTitles:@[@"取消",@"拨打"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (buttonIndex == 1){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",strongSelf ->aboutMeModel.phone]]];
            }
        }]show];
    }
}

// 【简历】
-(void)directionEmailManager{
    NSString *msgString = [NSString stringWithFormat:@"%@会将简历发送给你，请注意查收",aboutMeModel.email];
    self.emialAlert = [[UIAlertView alloc]initWithTitle:@"请填写你的Email" message:msgString delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    self.emialAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.emailTextFiled = [self.emialAlert textFieldAtIndex:0];
    self.emailTextFiled.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextFiled.placeholder = @"请输入你的email";
    [self.emialAlert show];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.aboutType == aboutTypeProduct){            // 【我的作品】
        static CGFloat initialDelay = 0.2f;
        static CGFloat stutter = 0.06f;
        
        
        GWAboutProductSingleModel *productSingleModel = [[self.myProductArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.section != [self cellIndexPathSectionWithcellData:@"作品类别"]){
            GWAboutProductCell *productCell = (GWAboutProductCell *)cell;
            if (productSingleModel.isAnimation == NO){
                [productCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
                productSingleModel.isAnimation = YES;
            }
        }
    }
}


#pragma mark 打开app
-(void)openUrl:(NSString *)openURL appUrl:(NSString *)appUrl{
    if ([openURL hasPrefix:@"http"]){
        WebViewController *webViewController = [[WebViewController alloc]init];
        [webViewController webViewControllerWithAddress:openURL];
        [self.navigationController pushViewController:webViewController animated:YES];
        return;
    }
    
    BOOL ishasUrl = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:openURL]];
    if (ishasUrl){
        NSURL *url = [NSURL URLWithString:[openURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:url];
    } else {
        [[UIAlertView alertViewWithTitle:nil message:@"您还没有安装该App，是否跳转安装" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:appUrl]];
            }
        }]show];
    }
}





#pragma mark - Loading Page
- (void)loadNextNews{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollToNextViewWithType:)]) {
        self.aboutType = self.aboutType + 1;
        if (self.aboutType == aboutTypeSchool + 1){
            self.aboutType = aboutTypeAboutMe;
        }
        [self.delegate scrollToNextViewWithType:(self.aboutType)];
    }
}
//加载上一条新闻
- (void)loadLastNews{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollToLastViewWithType:)]) {
        self.aboutType = self.aboutType - 1;
        if (self.aboutType == aboutTypeAboutMe - 1){
            self.aboutType = aboutTypeSchool;
        }
        [self.delegate scrollToLastViewWithType:(self.aboutType)];
    }
}

-(void)loadToPage:(aboutType)aboutType{
    if ([self.delegate respondsToSelector:@selector(scrollToPageViewWithType:)]) {
        [self.delegate scrollToPageViewWithType:aboutType];
    }
}

#pragma mark - Other Manager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.mainMutableArray.count ; i++){
        NSArray *dataTempArr = [self.mainMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = [dataTempArr objectAtIndex:j];
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.mainMutableArray.count ; i++){
        NSArray *dataTempArr = [self.mainMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = [dataTempArr objectAtIndex:j];
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - Interface
// 关于我
-(void)sendRequestRequestGetMe{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:about_Me requestParams:nil responseObjectClass:[GWAboutMeModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->aboutMeModel = (GWAboutMeModel *)responseObject;
            
            if (strongSelf.mainMutableArray.count){
                [strongSelf.mainMutableArray removeAllObjects];
            }
            if (strongSelf.myInfoArr.count){
                [strongSelf.myInfoArr removeAllObjects];
            }
            NSArray *aboutMeArr = @[@"头像",@"我的手机",@"我的QQ",@"我的学历",@"我的主页",@"我的博客",@"生日",@"Email",@"居住地",@"简介"];
            [strongSelf.myInfoArr addObject:aboutMeArr];
            [strongSelf.mainMutableArray addObjectsFromArray:strongSelf.myInfoArr];
            [strongSelf.mainMutableArray addObject:strongSelf->aboutMeModel.detail];
            [strongSelf.mainTableView reloadData];
        }
    }];
    
}
 
// 【获取我的公司】
-(void)sendRequestRequestGetCompany {
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:about_Company requestParams:nil responseObjectClass:[GWAboutCompanyListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.myCompanyMutableArr.count){
                [strongSelf.myCompanyMutableArr removeAllObjects];
            }
            if (strongSelf.mainMutableArray.count){
                [strongSelf.mainMutableArray removeAllObjects];
            }
            
            GWAboutCompanyListModel *companyListModel = (GWAboutCompanyListModel *)responseObject;
            [strongSelf.myCompanyMutableArr addObjectsFromArray:companyListModel.companyList];
            [strongSelf.mainMutableArray addObjectsFromArray:strongSelf.myCompanyMutableArr];
            [strongSelf.mainTableView reloadData];
        }
    }];
}

// 【获取我的作品】
-(void)sendRequestToGetMyProducts{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:about_Products requestParams:nil responseObjectClass:[GWAboutProductListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWAboutProductListModel *productListModel = (GWAboutProductListModel *)responseObject;
            if (strongSelf.mainMutableArray.count){
                [strongSelf.mainMutableArray removeAllObjects];
            }
            if (strongSelf.myProductArr.count){
                [strongSelf.myProductArr removeAllObjects];
            }
            [strongSelf.myProductArr addObject:productListModel.productList];
            [strongSelf.mainMutableArray addObjectsFromArray:strongSelf.myProductArr];
            [strongSelf.mainTableView reloadData];
        }
    }];
}

// 【获取我的学校】
-(void)sendRequestToGetMySchool{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:about_School requestParams:nil responseObjectClass:[PDSchoolListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.mySchoolMutableArr.count){
                [strongSelf.mySchoolMutableArr removeAllObjects];
            }
            if (strongSelf.mainMutableArray.count){
                [strongSelf.mainMutableArray removeAllObjects];
            }
            PDSchoolListModel *schoolListModel = (PDSchoolListModel *)responseObject;
            [strongSelf.mySchoolMutableArr addObjectsFromArray:schoolListModel.schoolList];
            [strongSelf.mainMutableArray addObjectsFromArray:strongSelf.mySchoolMutableArr];
            [strongSelf.mainTableView reloadData];
        } else {
            
        }
    }];
}


#pragma mark - 发送简历
-(void)sendRequestToJianliWithEmail:(NSString *)email{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"email":email};
    [[NetworkAdapter sharedAdapter] fetchWithPath:about_MeMail requestParams:dic responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"邮件发送成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        } else {
            
        }
    }];
}


#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == self.emialAlert){
        if (self.emailTextFiled.text.length){
            [self sendRequestToJianliWithEmail:self.emailTextFiled.text];
        } else {
            [self directionEmailManager];
        }
    }
}

#pragma mark - Company Delegate
-(void)aboutCompanyAnimationWithShrinkManagerWithModel:(GWAboutCompanySingleModel *)singleModel{
    NSInteger index = [self.myCompanyMutableArr indexOfObject:singleModel];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    [self.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)aboutCompanyWithLocation:(GWAboutCompanySingleModel *)singleModel{
    CGFloat xWidth = self.view.bounds.size.width - 70.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PopView *poplistview = [[PopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.type = PopViewTypeLocation;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = singleModel.lng;
    coordinate.longitude = singleModel.lat;
    poplistview.transferCoordinate = coordinate;
    
    [poplistview viewShow];
}

-(void)aboutCompanyWithHomePage:(GWAboutCompanySingleModel *)singleModel{
    WebViewController *webViewController = [[WebViewController alloc]init];
    [webViewController webViewControllerWithAddress:singleModel.url];
    [self.navigationController pushViewController:webViewController animated:YES];
}

#pragma mark - 我的学校代理
-(void)aboutSchoolWithLocation:(PDSchoolSingleModel *)singleModel{
    CGFloat xWidth = self.view.bounds.size.width - 70.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PopView *poplistview = [[PopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.type = PopViewTypeLocation;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = singleModel.lng;
    coordinate.longitude = singleModel.lat;
    poplistview.transferCoordinate = coordinate;
    
    [poplistview viewShow];
}

-(void)aboutSchoolUrlInfo:(PDSchoolSingleModel *)singleModel{
    WebViewController *webViewController = [[WebViewController alloc]init];
    [webViewController webViewControllerWithAddress:singleModel.url];
    [self.navigationController pushViewController:webViewController animated:YES];
}
@end
