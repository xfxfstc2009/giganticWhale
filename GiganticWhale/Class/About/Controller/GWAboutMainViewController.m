//
//  GWAboutMainViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/6.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutMainViewController.h"
#import "GWAboutViewController.h"
#import "REBackgroundView.h"
#import "ShareSDKManager.h"

@interface GWAboutMainViewController()<GWAboutViewControllerDelegate>
@property (nonatomic,strong)UIScrollView *mainScrollView;                       /**< ScrollView*/
@property (nonatomic,strong)GWAboutViewController *detailViewController;        /**< 详情页面*/
@end

@implementation GWAboutMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createBackGroundView];
    [self createMainScrollView];
    self.detailViewController = [self setContainerController];
    [self.mainScrollView addSubview:self.detailViewController.view];
    [self addChildViewController:self.detailViewController];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.barMainTitle = @"我的简介";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (strongSelf.detailViewController.aboutType == aboutTypeCompany){            // 公司
//            NSLog(@"1");
//        } else if (strongSelf.detailViewController.aboutType == aboutTypeAboutMe){      // 关于我
//            NSLog(@"2");
//        } else if (strongSelf.detailViewController.aboutType == aboutTypeProduct){      // 我的作品
//            NSLog(@"3");
//        } else if (strongSelf.detailViewController.aboutType == aboutTypeSchool){       // 我的学校
//            NSLog(@"4");
//        }
    }];
}

-(void)arrayWithInit{
    
}

#pragma mark - createBackGroundView
-(void)createBackGroundView{
    REBackgroundView *imageView = [[REBackgroundView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.frame = self.view.bounds;
    [self.view addSubview:imageView];
}


#pragma mark -CreateView
-(void)createMainScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, 3 * kScreenBounds.size.height);
        self.mainScrollView.contentOffset = CGPointMake(0, kScreenBounds.size.height);
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bounces = NO;
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mainScrollView];
    }
}

- (GWAboutViewController *)setContainerController{
    GWAboutViewController *detail = [[GWAboutViewController alloc] init];
    detail.aboutType = aboutTypeAboutMe;
    detail.view.frame = CGRectMake(0, self.mainScrollView.size_height, kScreenBounds.size.width, self.mainScrollView.size_height);
    detail.delegate = self;
    return detail;
}

#pragma mark - GWAboutViewCOntrollerDelegate
// 跳转到上一页
-(void)scrollToLastViewWithType:(aboutType)aboutType{
    [self setContentOffset:0. aboutType:aboutType];
}

// 跳转到下一页
-(void)scrollToNextViewWithType:(aboutType)aboutType{
    [self setContentOffset:2. aboutType:aboutType];
}

// 跳转到指定页
-(void)scrollToPageViewWithType:(aboutType)aboutType{
    [self setContentOffset:2. aboutType:aboutType];
}

//设置滚动偏移值
- (void)setContentOffset:(CGFloat)number aboutType:(aboutType)aboutType{
    GWAboutViewController *dvc = [self setContainerController];
    dvc.aboutType = aboutType;
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:.5f animations:^{
        weakSelf.mainScrollView.contentOffset = CGPointMake(0, number * kScreenBounds.size.height);
    } completion:^(BOOL finished) {
        [weakSelf.detailViewController removeFromParentViewController];
        [weakSelf.detailViewController.view removeFromSuperview];
        weakSelf.detailViewController = nil;
        weakSelf.mainScrollView.contentOffset = CGPointMake(0, kScreenBounds.size.height);
        weakSelf.detailViewController = dvc;
        [weakSelf.mainScrollView addSubview:self.detailViewController.view];
        [weakSelf addChildViewController:self.detailViewController];
    }];
}



@end
