//
//  GWAboutViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/1.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//
// 关于我
#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,aboutType) {
    aboutTypeAboutMe = 1,                           /**< 关于我*/
    aboutTypeProduct = 2,                           /**< 我的作品*/
    aboutTypeCompany = 3,                           /**< 关于我的公司*/
    aboutTypeSchool = 4,                            /**< 我的学校*/
//    aboutTypeSkill = 5,                             /**< 我的技能*/
};


@protocol GWAboutViewControllerDelegate <NSObject>

@optional
- (void)scrollToNextViewWithType:(aboutType)storyId;             // 滚动到下一个页面
- (void)scrollToLastViewWithType:(aboutType)storyId;             // 滚动到上一个页面
- (void)scrollToPageViewWithType:(aboutType)aboutType;           // 滚动到某一页

@end

@interface GWAboutViewController : AbstractViewController

@property (nonatomic,assign)aboutType aboutType;                // 关于我的类型
@property (nonatomic,weak)id<GWAboutViewControllerDelegate>delegate;            // 代理

@end
