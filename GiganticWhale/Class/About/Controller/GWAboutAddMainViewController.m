//
//  GWAboutAddMainViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAboutAddMainViewController.h"

@interface GWAboutAddMainViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSMutableArray *aboutAddMutableArr;

@end

@implementation GWAboutAddMainViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加信息";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.aboutAddMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [[UITableView alloc] initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.mainTableView.showsVerticalScrollIndicator = YES;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mainTableView];
    }
}




@end
