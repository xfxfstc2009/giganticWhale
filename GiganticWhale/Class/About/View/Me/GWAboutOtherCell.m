//
//  GWAboutMeBokeCell.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutOtherCell.h"

@interface GWAboutOtherCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)GWImageView *iconImageView;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation GWAboutOtherCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createView];
    }
    return self;
}

-(void)createView{
    // icon
    self.iconImageView = [[GWImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.fixedLabel];
    
    // dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.dymicLabel];
}

-(void)setTransferAboutType:(AboutMeOtherCellType)transferAboutType{
    _transferAboutType = transferAboutType;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, 100, transferCellHeight);
    self.dymicLabel.frame = CGRectMake(LCFloat(11) + LCFloat(100) + LCFloat(5), 0, kScreenBounds.size.width - LCFloat(11) - (LCFloat(11) + LCFloat(100) + LCFloat(5)), transferCellHeight);
}

-(void)setTransferAboutMeModel:(GWAboutMeModel *)transferAboutMeModel{
    if (self.transferAboutType == AboutMeOtherCellTypeBirthDay){            // 我的生日
        self.fixedLabel.text = @"我的生日:";
        self.dymicLabel.text = [NSDate getTimeBirthday:transferAboutMeModel.birthday];
    } else if (self.transferAboutType == AboutMeOtherCellTypeBoke){         // 我的博客
        self.fixedLabel.text = @"我的博客:";
        self.dymicLabel.text = transferAboutMeModel.boke_url;
    } else if (self.transferAboutType == AboutMeOtherCellTypeDesc){         // 简介
        self.fixedLabel.text = @"我的简介:";
        self.dymicLabel.numberOfLines = 0;
        
        NSArray *descArr = [transferAboutMeModel.desc componentsSeparatedByString:@"【"];
        NSString *tempStr = @"";
        for (int i = 0 ; i < descArr.count; i++){
            NSString *descTemp = [descArr objectAtIndex:i];
            if (descTemp.length){
                if (i == 0){
                    descTemp = [NSString stringWithFormat:@"【%@",descTemp];
                } else {
                    descTemp = [NSString stringWithFormat:@"\n【%@",descTemp];
                }
                tempStr = [tempStr stringByAppendingString:descTemp];
            }
        }
        self.dymicLabel.text = tempStr;
        
        
    } else if (self.transferAboutType == AboutMeOtherCellTypeEmail){        // Email
        self.fixedLabel.text = @"我的Email:";
        self.dymicLabel.text = transferAboutMeModel.email;
    } else if (self.transferAboutType == AboutMeOtherCellTypeMainPage){     // 我的主页
        self.fixedLabel.text = @"我的主页:";
        self.dymicLabel.text = transferAboutMeModel.home_url;
    } else if (self.transferAboutType == AboutMeOtherCellTypeResidence){    // 居住地
        self.fixedLabel.text = @"我的居住地:";
        self.dymicLabel.text = transferAboutMeModel.location;
    } else if (self.transferAboutType == AboutMeOtherCellTypePhone){        // 手机
        self.fixedLabel.text = @"我的号码:";
        self.dymicLabel.text = transferAboutMeModel.phone;
    } else if (self.transferAboutType == AboutMeOtherCellTypeEducation){    // 学历
        self.fixedLabel.text = @"我的学历:";
        self.dymicLabel.text = transferAboutMeModel.education;
    } else if (self.transferAboutType == AboutMeOtherCellTypeQQ){           // QQ
        self.fixedLabel.text = @"我的QQ:";
        self.dymicLabel.text = transferAboutMeModel.qq;
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(7);
    return cellHeight;
}

+(CGFloat)calculationCellWithDescWithModel:(GWAboutMeModel *)model{
    CGFloat cellHeight = LCFloat(7);
    if (model){
        NSArray *descArr = [model.desc componentsSeparatedByString:@"【"];
        NSString *tempStr = @"";
        for (int i = 0 ; i < descArr.count; i++){
            NSString *descTemp = [descArr objectAtIndex:i];
            if (descTemp.length){
                if (i == 0){
                    descTemp = [NSString stringWithFormat:@"【%@",descTemp];
                } else {
                    descTemp = [NSString stringWithFormat:@"\n【%@",descTemp];
                }
                tempStr = [tempStr stringByAppendingString:descTemp];
            }
        }
        
        CGSize contentOfSize = [tempStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(100) - LCFloat(5) - LCFloat(11), CGFLOAT_MAX)];
        cellHeight += contentOfSize.height;
    }
    cellHeight += LCFloat(7);
    return cellHeight;
}

@end
