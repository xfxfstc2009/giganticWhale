//
//  GWAboutMeDetailCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAboutMeDetailCell.h"
#import "PDScrollView.h"

@interface GWAboutMeDetailCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)PDScrollView *scrollView;

@end

@implementation GWAboutMeDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建标题
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.fixedLabel.numberOfLines = 0;
    [self addSubview:self.fixedLabel];
    
    // 2. 创建scrollView
    
    self.scrollView = [[PDScrollView alloc]initWithFrame:CGRectMake(LCFloat(11), [NSString contentofHeight:self.fixedLabel.font] + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200))];
    self.scrollView.isPageControl = YES;
    self.scrollView.clipsToBounds = YES;
    self.scrollView.layer.cornerRadius = LCFloat(9);
    [self addSubview:self.scrollView];
    [self.scrollView startTimerWithTime:5];

}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferAboutMeDetailModel:(GWAboutMeDetailModel *)transferAboutMeDetailModel{
    _transferAboutMeDetailModel = transferAboutMeDetailModel;
    // 1. 标题
    self.fixedLabel.text = transferAboutMeDetailModel.content;
    CGSize contentOfSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(6), kScreenBounds.size.width - 2 * LCFloat(11), contentOfSize.height);
    
    // 2. 创建内容
    NSMutableArray *scrollArr = [NSMutableArray array];
    for (int i = 0 ; i < transferAboutMeDetailModel.imgList.count;i++){
        GWAboutMeDetailSingleModel *singleModel = [transferAboutMeDetailModel.imgList objectAtIndex:i];
        
        GWScrollViewSingleModel *scrollViewSingleMode = [[GWScrollViewSingleModel alloc]init];
        scrollViewSingleMode.img = singleModel.img;
        scrollViewSingleMode.content = singleModel.contents;
        [scrollArr addObject:scrollViewSingleMode];
    }
    self.scrollView.transferImArr = scrollArr;
    self.scrollView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    [self.scrollView startTimerWithTime:2];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(250);
}

@end
