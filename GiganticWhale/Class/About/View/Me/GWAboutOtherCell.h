//
//  GWAboutMeBokeCell.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//
// 关于我博客
#import <UIKit/UIKit.h>
#import "GWAboutMeModel.h"

typedef NS_ENUM(NSInteger,AboutMeOtherCellType) {
    AboutMeOtherCellTypeMainPage,                   /**< 我的主页*/
    AboutMeOtherCellTypeBoke,                       /**< 我的博客*/
    AboutMeOtherCellTypeBirthDay,                   /**< 我的生日*/
    AboutMeOtherCellTypeEmail,                      /**< Email*/
    AboutMeOtherCellTypeResidence,                  /**< 居住地*/
    AboutMeOtherCellTypeDesc,                       /**< 简介*/
    AboutMeOtherCellTypePhone,                      /**< 手机*/
    AboutMeOtherCellTypeEducation,                  /**< 学历*/
    AboutMeOtherCellTypeQQ,                         /**< QQ*/
};

@interface GWAboutOtherCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;             /**< 传递cell的高度*/
@property (nonatomic,strong)GWAboutMeModel *transferAboutMeModel;
@property (nonatomic,assign)AboutMeOtherCellType transferAboutType; /**< 传递过去类型*/
+(CGFloat)calculationCellHeight;
+(CGFloat)calculationCellWithDescWithModel:(GWAboutMeModel *)model;

@end
