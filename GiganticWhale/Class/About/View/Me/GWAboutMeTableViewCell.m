//
//  GWAboutMeTableViewCell.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutMeTableViewCell.h"
#import "WhaleView.h"

@interface GWAboutMeTableViewCell()
@property (nonatomic,strong)UILabel *nameFixedLabel;        /**< 姓名固定*/
@property (nonatomic,strong)UILabel *nameLabel;             /**< 姓名*/

@property (nonatomic,strong)UILabel *workTimeFixedLabel;    /**< 工作固定*/
@property (nonatomic,strong)UILabel *workTimeLabel;         /**< 工作年限*/

@property (nonatomic,strong)UILabel *moneyFixedLabel;       /**< 薪资固定*/
@property (nonatomic,strong)UILabel *moneyLabel;            /**< 目前薪资*/

@property (nonatomic,strong)GWImageView *avatarView;        /**< 头像*/
@property (nonatomic,strong)WhaleView *whaleView;           /**< 波浪头像*/
@end

@implementation GWAboutMeTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createView];
    }
    return self;
}

#pragma mark - CreateView
-(void)createView{
    // 1. 创建波浪头像
    self.whaleView = [[WhaleView alloc] initWithFrame:CGRectMake(LCFloat(11),LCFloat(11), LCFloat(100), LCFloat(100))];
    self.whaleView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.whaleView];
    
    // 1. 头像
    self.avatarView = [[GWImageView alloc]initWithFrame:CGRectMake(LCFloat(11), LCFloat(11), LCFloat(100), LCFloat(100))];
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = self.avatarView.size_height / 2.;
    [self addSubview:self.avatarView];
    
    // 2. 固定label
    self.nameFixedLabel = [[UILabel alloc]init];
    self.nameFixedLabel.backgroundColor = [UIColor clearColor];
    self.nameFixedLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.nameFixedLabel.text = @"姓名:";
    [self addSubview:self.nameFixedLabel];
    
    // 2. 姓名
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self addSubview:self.nameLabel];
    
    self.workTimeFixedLabel = [[UILabel alloc]init];
    self.workTimeFixedLabel.backgroundColor = [UIColor clearColor];
    self.workTimeFixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.workTimeFixedLabel.text = @"工作年限:";
    [self addSubview:self.workTimeFixedLabel];
    
    // 3. 创建工作年限
    self.workTimeLabel = [[UILabel alloc]init];
    self.workTimeLabel.backgroundColor = [UIColor clearColor];
    self.workTimeLabel.font = [UIFont systemFontOfCustomeSize:15.];
    [self addSubview:self.workTimeLabel];
    
    self.moneyFixedLabel = [[UILabel alloc]init];
    self.moneyFixedLabel.backgroundColor = [UIColor clearColor];
    self.moneyFixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.moneyFixedLabel.text = @"当前薪资:";
    [self addSubview:self.moneyFixedLabel];
    
    // 4. 目前薪资
    self.moneyLabel = [[UILabel alloc]init];
    self.moneyLabel.backgroundColor = [UIColor clearColor];
    self.moneyLabel.font = [UIFont systemFontOfCustomeSize:14.];
    [self addSubview:self.moneyLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferAboutMeModel:(GWAboutMeModel *)transferAboutMeModel{
    _transferAboutMeModel = transferAboutMeModel;
    
    CGFloat margin = (self.whaleView.size_height - 3 * [NSString contentofHeight:self.nameLabel.font]) / 2.;
    
    // 【1. 姓名】
    CGSize nameOfSize = [self.nameFixedLabel.text sizeWithCalcFont:self.nameFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.nameFixedLabel.font])];
    self.nameFixedLabel.frame = CGRectMake(CGRectGetMaxX(self.whaleView.frame) + LCFloat(11), self.whaleView.orgin_y, nameOfSize.width, [NSString contentofHeight:self.nameFixedLabel.font]);
    
    self.nameLabel.text = transferAboutMeModel.name;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.nameFixedLabel.frame) + LCFloat(5), self.nameFixedLabel.orgin_y, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.nameFixedLabel.frame) - LCFloat(5), [NSString contentofHeight:self.nameLabel.font]);
    
    // 【2. 工作年限】
    CGSize workOfSize = [self.workTimeFixedLabel.text sizeWithCalcFont:self.workTimeFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.workTimeFixedLabel.font])];
    self.workTimeFixedLabel.frame = CGRectMake(self.nameFixedLabel.orgin_x, CGRectGetMaxY(self.nameFixedLabel.frame) + margin, workOfSize.width, [NSString contentofHeight:self.workTimeFixedLabel.font]);
    
    self.workTimeLabel.text = transferAboutMeModel.work_time;
    self.workTimeLabel.frame = CGRectMake(CGRectGetMaxX(self.workTimeFixedLabel.frame) + LCFloat(5), self.workTimeFixedLabel.orgin_y, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.workTimeFixedLabel.frame) - LCFloat(5), [NSString contentofHeight:self.workTimeLabel.font]);
    
    // 【3. 薪资】
    CGSize moneySize = [self.moneyFixedLabel.text sizeWithCalcFont:self.moneyFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.moneyFixedLabel.font])];
    self.moneyFixedLabel.frame = CGRectMake(self.nameFixedLabel.orgin_x, CGRectGetMaxY(self.workTimeFixedLabel.frame) + margin, moneySize.width, [NSString contentofHeight:self.moneyFixedLabel.font]);
    
    self.moneyLabel.text = transferAboutMeModel.money;
    self.moneyLabel.frame = CGRectMake(CGRectGetMaxX(self.moneyFixedLabel.frame) + LCFloat(5), self.moneyFixedLabel.orgin_y, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.moneyFixedLabel.frame) - LCFloat(5), [NSString contentofHeight:self.moneyLabel.font]);
    
    // 【4.头像】
    if (transferAboutMeModel.type == 0){
        self.avatarView.hidden = NO;
        self.whaleView.hidden = YES;
        [self.avatarView uploadImageWithURL:transferAboutMeModel.avatar placeholder:nil callback:NULL];
    } else {
        self.avatarView.hidden = YES;
        if (transferAboutMeModel.type == 1){        // 鲸鱼
            self.whaleView.hidden = NO;
        }
    }
}

#pragma mark cellHeight
+(CGFloat)calculationCellHeight{
    return LCFloat(100) + 2 * LCFloat(11);
}

@end
