//
//  GWAboutMeDetailCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 简介详情
#import <UIKit/UIKit.h>
#import "GWAboutMeDetailModel.h"
@interface GWAboutMeDetailCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;                                 /**外围传入的高度*/
@property (nonatomic,strong)GWAboutMeDetailModel *transferAboutMeDetailModel;           /**< 传入详情*/

+(CGFloat)calculationCellHeight;
@end
