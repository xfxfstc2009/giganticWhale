//
//  GWAboutMeTableViewCell.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWAboutMeModel.h"
@interface GWAboutMeTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;             /**< 传递cell的高度*/
@property (nonatomic,strong)GWAboutMeModel *transferAboutMeModel;
+(CGFloat)calculationCellHeight;
@end
