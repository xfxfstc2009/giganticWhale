//
//  GWAboutSchoolCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAboutSchoolCell.h"
#import "PDScrollView.h"

@interface GWAboutSchoolCell()

//@property (nonatomic,strong)UILabel *schoolNameFixedLabel;              /**< 学校名称*/
//@property (nonatomic,strong)UILabel *schoolNameDymicLabel;              /**< 学校详情*/
//@property (nonatomic,strong)UILabel *schoolTypeFixedLabel;              /**< 学校类型*/
//@property (nonatomic,strong)UILabel *schoolTypeDymicLabel;              /**< 学校类型dymic*/
//@property (nonatomic,strong)UILabel *schoolLocationFixedLabel;          /**< 学校地址*/
//@property (nonatomic,strong)UILabel *schoolLocationDymicLabel;          /**< 学校动态地址*/
//@property (nonatomic,strong)UILabel *schoolImgLabel;                    /**< 学校图片*/
//@property (nonatomic,strong)UILabel *schoolImgDymicLabel;               /**< 学校动态label*/
//@property (nonatomic,strong)UILabel *schoolDescFixedlabel;              /**< 学校详情*/
//@property (nonatomic,strong)UILabel *schoolDescDymicLabel;              /**< 学校详情动态*/
//@property (nonatomic,strong)GWImageView *locationImgView;               /**< 学校地址*/
////
@property (nonatomic,strong)UILabel *schoolNamelabel ;              /**< 学校名字*/
@property (nonatomic,strong)UIButton *schoolNameButton;             /**< 学校名称按钮*/

@property (nonatomic,strong)UILabel *typeLabel;                     /**< 学校类型*/
@property (nonatomic,strong)PDScrollView *mainScrollView;           /**< 学校图片*/
@property (nonatomic,copy)NSString *desc;                           /**< 详细*/
@property (nonatomic,strong)GWImageView *locationImgView;           /**< 学校location*/
@property (nonatomic,strong)UILabel  *locationLabel;                /**< 地址*/
@property (nonatomic,strong)UIButton *locationButton;               /**< 学校地址按钮*/
@property (nonatomic,strong)UILabel *descLabel;                     /**< 详情label*/
@property (nonatomic,strong)UIButton *descButton;                   /**< 详情按钮*/

@end

@implementation GWAboutSchoolCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [GWTool stretchImageWithName:@"common_card_background"];
    bgView.alpha = .3f;
    self.backgroundView = bgView;
    
    // 1. 创建1. 学校名称
    self.schoolNamelabel = [[UILabel alloc]init];
    self.schoolNamelabel.backgroundColor = [UIColor clearColor];
    self.schoolNamelabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.schoolNamelabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [self addSubview:self.schoolNamelabel];
    
    // 2. 创建类型
    self.typeLabel = [[UILabel alloc]init];
    self.typeLabel.backgroundColor = [UIColor clearColor];
    self.typeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.typeLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [self addSubview:self.typeLabel];
    
    // 3. 创建
    self.locationImgView = [[GWImageView alloc]init];
    self.locationImgView.backgroundColor = [UIColor clearColor];
    self.locationImgView.image = [UIImage imageNamed:@"icon_school_locate"];
    [self addSubview:self.locationImgView];
    
    // 4. 创建地址信息
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.locationLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.locationLabel.numberOfLines = 0;
    [self addSubview:self.locationLabel];
    
    // 5. 创建
    self.mainScrollView = [[PDScrollView alloc]initWithFrame:CGRectMake(LCFloat(11), CGRectGetMaxY(self.schoolNamelabel.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200))];
    self.mainScrollView.isPageControl = YES;
    self.mainScrollView.layer.cornerRadius = LCFloat(9);
    self.mainScrollView.clipsToBounds = YES;
    [self.mainScrollView startTimerWithTime:3];
    [self addSubview:self.mainScrollView];
    
    // 6. 创建desc
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    
    self.locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.locationButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.locationButton];
    __weak typeof(self)weakSelf = self;
    [self.locationButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(aboutSchoolWithLocation:)]){
            [strongSelf.delegate aboutSchoolWithLocation:strongSelf.transferSchoolSingleModel];
        }
    }];
    
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.text = @"查看详情>";
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.descLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.descLabel];
    
    // 详情
    self.descButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.descButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(aboutSchoolUrlInfo:)]){
            [strongSelf.delegate aboutSchoolUrlInfo:strongSelf.transferSchoolSingleModel];
        }
    }];
    [self addSubview:self.descButton];
}

-(void)setTramsferCellHeight:(CGFloat)tramsferCellHeight{
    _tramsferCellHeight = tramsferCellHeight;
}

-(void)setTransferSchoolSingleModel:(PDSchoolSingleModel *)transferSchoolSingleModel{
    _transferSchoolSingleModel = transferSchoolSingleModel;
    
    // 1.创建类型
    self.typeLabel.text = transferSchoolSingleModel.type;
    CGSize contentOfSize = [self.typeLabel.text sizeWithCalcFont:self.typeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.typeLabel.font])];
    self.typeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - contentOfSize.width - LCFloat(5), LCFloat(5), contentOfSize.width, [NSString contentofHeight:self.typeLabel.font]);
    
    // 1. 创建名字
    self.schoolNamelabel.text = transferSchoolSingleModel.name;
    self.schoolNamelabel.frame = CGRectMake(LCFloat(11), self.typeLabel.orgin_y, self.typeLabel.orgin_x - LCFloat(11) - LCFloat(11), [NSString contentofHeight:self.schoolNamelabel.font]);
    self.schoolNamelabel.adjustsFontSizeToFitWidth = YES;
    
    // 3. 创建图片墙
    NSMutableArray *imgTempArr = [NSMutableArray array];
    for (int i = 0 ; i < transferSchoolSingleModel.imgList.count;i++){
        GWScrollViewSingleModel *scrollViewSingleMode = [[GWScrollViewSingleModel alloc]init];
        scrollViewSingleMode.img = [transferSchoolSingleModel.imgList objectAtIndex:i];
        [imgTempArr addObject:scrollViewSingleMode];
    }
    self.mainScrollView.transferImArr = imgTempArr;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.schoolNamelabel.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    
    // 4. 创建地址
    self.locationImgView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.mainScrollView.frame) + LCFloat(5), LCFloat(14), LCFloat(18));
    
    // 4. 创建地址label
    self.locationLabel.text = transferSchoolSingleModel.location;
    CGSize locationSize = [self.locationLabel.text sizeWithCalcFont:self.locationLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) * 2 - CGRectGetMaxX(self.locationImgView.frame), CGFLOAT_MAX)];
    self.locationLabel.frame = CGRectMake(CGRectGetMaxX(self.locationImgView.frame) + LCFloat(5), self.locationImgView.orgin_y, kScreenBounds.size.width - LCFloat(11) * 2 - CGRectGetMaxX(self.locationImgView.frame),locationSize.height);
    
    // 4.2 创建地址按钮
    self.locationButton.frame = CGRectMake(self.locationImgView.orgin_x, self.locationImgView.orgin_y, kScreenBounds.size.width, self.locationLabel.size_height);
    
    // 5. 创建详情
    self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.locationLabel.frame), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeight:self.descLabel.font]);
    self.descButton.frame = self.descLabel.frame;
}

+(CGFloat)calculationCellHeightWithSchoolSingleModel:(PDSchoolSingleModel *)transferSchoolSingleModel{
    CGFloat cellHeight = 0;
    // 1. 标题
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(5);
    // 2. 图片
    cellHeight += LCFloat(200);
    cellHeight += LCFloat(5);
    // 地址
    CGSize locationSize = [transferSchoolSingleModel.location sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) * 2 - LCFloat(14) - LCFloat(11), CGFLOAT_MAX)];
    if (locationSize.height > LCFloat(18)){
        cellHeight += locationSize.height;
    } else {
        cellHeight += LCFloat(18);
    }
    // 详情
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    
    return cellHeight;
}
@end
