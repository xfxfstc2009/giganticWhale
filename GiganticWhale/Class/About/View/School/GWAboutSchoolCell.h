//
//  GWAboutSchoolCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSchoolSingleModel.h"

@protocol GWAboutSchoolCellDelegate <NSObject>

-(void)aboutSchoolWithLocation:(PDSchoolSingleModel *)singleModel;
-(void)aboutSchoolUrlInfo:(PDSchoolSingleModel *)singleModel;
@end

@interface GWAboutSchoolCell : UITableViewCell

@property (nonatomic,strong)PDSchoolSingleModel *transferSchoolSingleModel;        /**< 传递过来的图片数组*/
@property (nonatomic,assign)CGFloat tramsferCellHeight;     /**< 传递过来的高度*/
@property (nonatomic,weak)id<GWAboutSchoolCellDelegate>delegate;

+(CGFloat)calculationCellHeightWithSchoolSingleModel:(PDSchoolSingleModel *)transferSchoolSingleModel;
@end
