//
//  PopView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/28.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PopView.h"
@interface PopView()
@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)UIView *tableBgView;
@property (nonatomic,strong)PDMapView *mapView;
@end

@implementation PopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
        [self createDismissBackgroundView];
        [self createView];
    }
    return self;
}

#pragma mark - createView

#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}

- (void)viewShow {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}

-(void)createView{
    self.tableBgView = [[UIView alloc]init];
    self.tableBgView.backgroundColor = [UIColor whiteColor];
    self.tableBgView.frame = self.bounds;
    self.tableBgView.userInteractionEnabled = YES;
    [self addSubview:self.tableBgView];
    
    if (self.type == PopViewTypeLocation){              // 显示地址
        [self createMapView];
    }
}


#pragma mark - 创建地图
-(void)createMapView{
    if (!self.mapView){
        self.mapView = [[PDMapView alloc]initWithFrame:self.tableBgView.bounds];
        self.mapView.transferCoordinate = self.transferCoordinate;
        [self.tableBgView addSubview:self.mapView];
    }
}

-(void) setTransferCoordinate:(CLLocationCoordinate2D)transferCoordinate{
     self.mapView.transferCoordinate = transferCoordinate;
}
@end
