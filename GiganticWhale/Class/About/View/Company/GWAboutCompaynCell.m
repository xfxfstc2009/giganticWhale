//
//  GWAboutCompaynCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutCompaynCell.h"

@interface GWAboutCompaynCell()
@property (nonatomic,strong)UILabel *achievementLabel;          /**< 我的成绩Label*/
@property (nonatomic,strong)UILabel *companyLabel;              /**< 公司label*/
@property (nonatomic,strong)GWImageView *iconImageView;         /**< 图片imageView*/
@property (nonatomic,strong)UIButton *arrowImageView;           /**< 右上角的按钮*/
@property (nonatomic,strong)UILabel *locationFixedLabel;        /**< 公司固定地址*/
@property (nonatomic,strong)UILabel *locationLabel;             /**< 公司地址*/
@property (nonatomic,strong)GWImageView *locationImageView;     /**< 定位地址图片*/
@property (nonatomic,strong)UIButton *locationButton;           /**< 公司地址*/
@property (nonatomic,strong)UILabel *companyHomeFixedLabel;     /**< 公司主页固定*/
@property (nonatomic,strong)UILabel *companyHomeLabel;          /**< 公司主页*/
@property (nonatomic,strong)UIButton *companyHomeButton;        /**< 公司主页*/
@property (nonatomic,strong)UIView *achiveView;

@property (nonatomic,strong)UILabel *descLabel;                 /**< descLabel*/
@property (nonatomic,strong)UILabel *descFixedLabel;            /**< 详情*/
@end

@implementation GWAboutCompaynCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [GWTool stretchImageWithName:@"common_card_background"];
    bgView.alpha = .3f;
    self.backgroundView = bgView;
    
    // 1. 公司icon
    self.iconImageView = [[GWImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 1. 公司名字
    self.companyLabel = [[UILabel alloc]init];
    self.companyLabel.backgroundColor = [UIColor clearColor];
    self.companyLabel.textColor = [UIColor colorWithCustomerName:@"白高亮"];
    self.companyLabel.font = [[UIFont fontWithCustomerSizeName:@"副标题"]boldFont];
    [self addSubview:self.companyLabel];
    
    // 1.1 创建arrowImageView
    self.arrowImageView = [[UIButton alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    [self.arrowImageView setImage:[UIImage imageNamed:@"icon_arrow_down"] forState:UIControlStateNormal];
    [self addSubview:self.arrowImageView];
    
    // 1.2 我的成就
    self.achievementLabel = [[UILabel alloc]init];
    self.achievementLabel.backgroundColor = [UIColor clearColor];
    self.achievementLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.achievementLabel.text = @"我的成绩:";
    [self addSubview:self.achievementLabel];
    
    self.achiveView = [[UIView alloc]init];
    self.achiveView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.achiveView];
    
    // 地址
    self.locationFixedLabel = [[UILabel alloc]init];
    self.locationFixedLabel.backgroundColor = [UIColor clearColor];
    self.locationFixedLabel.text = @"公司地址:";
    self.locationFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.locationFixedLabel];
    
    // 地址详情
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.locationLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.locationLabel.numberOfLines = 0;
    [self addSubview:self.locationLabel];
    
    // 4. 创建公司地址按钮
    self.locationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.locationButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.locationButton];
    
    // 6. 公司url
    self.companyHomeFixedLabel = [[UILabel alloc]init];
    self.companyHomeFixedLabel.backgroundColor = [UIColor clearColor];
    self.companyHomeFixedLabel.text = @"公司主页:";
    self.companyHomeFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.companyHomeFixedLabel];
    
    self.companyHomeLabel = [[UILabel alloc]init];
    self.companyHomeLabel.backgroundColor = [UIColor clearColor];
    self.companyHomeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.companyHomeLabel.numberOfLines = 0;
    self.companyHomeLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [self addSubview:self.companyHomeLabel];
    
    self.companyHomeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.companyHomeButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.companyHomeButton];
    
    // 2. 创建详情
    self.descFixedLabel = [[UILabel alloc]init];
    self.descFixedLabel.backgroundColor = [UIColor clearColor];
    self.descFixedLabel.text = @"公司简介:";
    self.descFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.descFixedLabel];
    
    
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.numberOfLines = 0;
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [self addSubview:self.descLabel];
    
    self.locationImageView = [[GWImageView alloc]init];
    self.locationImageView.backgroundColor = [UIColor clearColor];
    self.locationImageView.image = [UIImage imageNamed:@"icon_company_location"];
    [self addSubview:self.locationImageView];
}


#pragma mark - Transfer
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCompanySingleModel:(GWAboutCompanySingleModel *)transferCompanySingleModel{
    _transferCompanySingleModel = transferCompanySingleModel;
    
    // 1. 公司头像
    self.iconImageView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(80), LCFloat(80));
    [self.iconImageView uploadAboutCompanyURL:transferCompanySingleModel.icon callback:NULL];
    
    // 2. 创建公司名称
    self.companyLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(11), self.iconImageView.orgin_y, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), [NSString contentofHeight:self.companyLabel.font]);
    self.companyLabel.text = transferCompanySingleModel.name;
    
    // 3. 箭头符号
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(11) - LCFloat(14), LCFloat(11), LCFloat(14), LCFloat(14));
    __weak typeof(self)weakSelf = self;
    [self.arrowImageView buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationWithShrinkManager];                // 收缩方法
    }];
    
    // 4. 我的成就
    self.achievementLabel.text = @"我的成绩:";
    CGSize achievementSize = [self.achievementLabel.text sizeWithCalcFont:self.achievementLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), CGFLOAT_MAX)];
    self.achievementLabel.frame = CGRectMake(self.companyLabel.orgin_x, CGRectGetMaxY(self.companyLabel.frame) + LCFloat(5),kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11) , achievementSize.height);
    
    if (self.achiveView.subviews.count){
        [self.achiveView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    if (self.transferCompanySingleModel.achiList.count){
        CGFloat achiveHeight = 0;
        for (int i = 0 ; i < self.transferCompanySingleModel.achiList.count;i++){
            CGFloat origin_x = LCFloat(11);
            CGFloat origin_y = achiveHeight;
            CGFloat size_width = kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11);
            UILabel *achiveLabel = [[UILabel alloc]init];
            achiveLabel.backgroundColor = [UIColor clearColor];
            achiveLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            achiveLabel.textColor = [UIColor colorWithCustomerName:@"白"];
            achiveLabel.numberOfLines = 0;
            NSString *achiveSingleStr = [self.transferCompanySingleModel.achiList objectAtIndex:i];
            achiveLabel.text = achiveSingleStr;
            CGSize contentOfSize = [achiveSingleStr sizeWithCalcFont:achiveLabel.font constrainedToSize:CGSizeMake(size_width, CGFLOAT_MAX)];
            achiveLabel.frame = CGRectMake(origin_x, origin_y, size_width, contentOfSize.height);

            [self.achiveView addSubview:achiveLabel];
            achiveHeight += contentOfSize.height;
        }
        self.achiveView.frame = CGRectMake(self.companyLabel.orgin_x, CGRectGetMaxY(self.achievementLabel.frame), kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), achiveHeight);
    }
    
    // 5. 公司位置
    CGSize locationFixedSize = [self.locationFixedLabel.text sizeWithCalcFont:self.locationFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.locationFixedLabel.font])];
    self.locationFixedLabel.frame = CGRectMake(self.companyLabel.orgin_x, CGRectGetMaxY(self.achiveView.frame), locationFixedSize.width, [NSString contentofHeight:self.locationFixedLabel.font]);
    

    self.locationImageView.frame = CGRectMake(self.companyLabel.orgin_x, CGRectGetMaxY(self.locationFixedLabel.frame), LCFloat(12), LCFloat(17));
    
    self.locationLabel.text = self.transferCompanySingleModel.location;
    CGSize locationOfSize = [self.locationLabel.text sizeWithCalcFont:self.locationLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(11) - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), CGFLOAT_MAX)];
    self.locationLabel.frame = CGRectMake(CGRectGetMaxX(self.locationImageView.frame) + LCFloat(5), self.locationImageView.orgin_y, kScreenBounds.size.width - LCFloat(11) - LCFloat(11) - LCFloat(5) - CGRectGetMaxX(self.locationImageView.frame) - LCFloat(11), locationOfSize.height);
    
    // 6. 位置按钮
    self.locationButton.frame = CGRectMake(self.locationFixedLabel.orgin_x, self.locationFixedLabel.orgin_y, self.companyLabel.size_width, (CGRectGetMaxY(self.locationLabel.frame) - self.locationFixedLabel.orgin_y));
    [self.locationButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [_delegate respondsToSelector:@selector(aboutCompanyWithLocation:)]){
            [strongSelf.delegate aboutCompanyWithLocation:strongSelf.transferCompanySingleModel];
        }
    }];
    
    // 创建url
    self.companyHomeFixedLabel.frame = CGRectMake(self.companyLabel.orgin_x, CGRectGetMaxY(self.locationLabel.frame) + LCFloat(5), locationFixedSize.width, [NSString contentofHeight:self.companyHomeFixedLabel.font]);
    
    self.companyHomeLabel.text = self.transferCompanySingleModel.url;
    CGSize companyHomeSize = [self.companyHomeLabel.text sizeWithCalcFont:self.companyHomeLabel.font constrainedToSize:CGSizeMake(self.locationLabel.size_width, CGFLOAT_MAX)];
    self.companyHomeLabel.frame = CGRectMake(self.companyHomeFixedLabel.orgin_x, CGRectGetMaxY(self.companyHomeFixedLabel.frame), self.locationLabel.size_width, companyHomeSize.height);

    // url 按钮
    self.companyHomeButton.frame = CGRectMake(self.companyHomeFixedLabel.orgin_x, self.companyHomeFixedLabel.orgin_y, self.locationLabel.size_width, (CGRectGetMaxY(self.companyHomeLabel.frame) - self.companyHomeFixedLabel.orgin_y));
    [self.companyHomeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [_delegate respondsToSelector:@selector(aboutCompanyWithHomePage:)]){
            [strongSelf.delegate aboutCompanyWithHomePage:strongSelf.transferCompanySingleModel];
        }
    }];
    
    
    // 3. 创建公司简介
    self.descFixedLabel.frame = CGRectMake(self.locationFixedLabel.orgin_x, CGRectGetMaxY(self.companyHomeLabel.frame), self.locationFixedLabel.size_width, [NSString contentofHeight:self.descFixedLabel.font]);
    
    self.descLabel.text = transferCompanySingleModel.desc;
    CGSize contentOfSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(11), CGRectGetMaxY(self.descFixedLabel.frame) + LCFloat(5), kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), contentOfSize.height);
    
    if (self.transferCompanySingleModel.isOpen){        // 张开状态
        // 1. 文字
        self.descLabel.numberOfLines = 0;
        CGSize contentOfSize = [self.transferCompanySingleModel.desc sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), CGFLOAT_MAX)];
        self.descLabel.size_height = contentOfSize.height;
    } else {                                            // 收拢状态
        self.descLabel.numberOfLines = 5;
        self.descLabel.size_height = 5 * [NSString contentofHeight:self.descLabel.font];
    }
}

-(void)animationWithShrinkManager{
    if (self.transferCompanySingleModel.isOpen){        // 张开状态
        // 1. 文字
        self.descLabel.numberOfLines = 0;
        CGSize contentOfSize = [self.transferCompanySingleModel.desc sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), CGFLOAT_MAX)];
        self.descLabel.size_height = contentOfSize.height;
    } else {                                            // 收拢状态
        self.descLabel.numberOfLines = 5;
        self.descLabel.size_height = 5 * [NSString contentofHeight:self.descLabel.font];
    }
    self.transferCompanySingleModel.isOpen = !self.transferCompanySingleModel.isOpen;
    if (self.delegate && [_delegate respondsToSelector:@selector(aboutCompanyAnimationWithShrinkManagerWithModel:)]){
        [self.delegate aboutCompanyAnimationWithShrinkManagerWithModel:self.transferCompanySingleModel];
    }
}

// 显示公司地址方法
-(void)loacationShowManager{

}

+(CGFloat)caculationCellHeightWithModel:(GWAboutCompanySingleModel *)transferCompanySingleModel{
    CGFloat cellHeight = 0;
    
    // 1. 上边距
    cellHeight += LCFloat(11);
    // 2. 公司名字
    cellHeight += [NSString contentofHeight:[[UIFont fontWithCustomerSizeName:@"副标题"]boldFont]];
    // 3. 公司名字边距
    cellHeight += LCFloat(5);
    // 4. 我的成绩
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    CGFloat margin_width = kScreenBounds.size.width - 2 * LCFloat(11) - LCFloat(80) - LCFloat(11);
    CGFloat sizeHeight = 0;
    for ( int i = 0 ; i < transferCompanySingleModel.achiList.count;i++){
        NSString *achi = [transferCompanySingleModel.achiList objectAtIndex:i];
        CGSize contentOfSize = [achi sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(margin_width, CGFLOAT_MAX)];
        sizeHeight += contentOfSize.height;
    }
    cellHeight += sizeHeight;
    // 5.公司地址
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    
    NSString *locationString = transferCompanySingleModel.location;
    
CGSize locationOfSize = [locationString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]  constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(11) - LCFloat(11) - LCFloat(91) - LCFloat(11), CGFLOAT_MAX)];
    
    cellHeight += locationOfSize.height;
    cellHeight += LCFloat(5);
    
    // 6.公司主页
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    
    NSString *companyUrlString = transferCompanySingleModel.url;
    CGSize companyUrlSize = [companyUrlString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(margin_width, CGFLOAT_MAX)];
    cellHeight += companyUrlSize.height;
    
    // 7.公司简介
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];

    if (transferCompanySingleModel.isOpen){
        CGSize contentOfSize = [transferCompanySingleModel.desc sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(margin_width, CGFLOAT_MAX)];
        cellHeight += contentOfSize.height;
    } else {
        cellHeight += 5 * [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
    }
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
