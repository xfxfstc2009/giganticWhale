//
//  GWAboutCompaynCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWAboutCompanySingleModel.h"


@protocol GWAboutCompaynCellDelegate <NSObject>

-(void)aboutCompanyAnimationWithShrinkManagerWithModel:(GWAboutCompanySingleModel *)singleModel;
-(void)aboutCompanyWithLocation:(GWAboutCompanySingleModel *)singleModel;
-(void)aboutCompanyWithHomePage:(GWAboutCompanySingleModel *)singleModel;
@end

@interface GWAboutCompaynCell : UITableViewCell

@property (nonatomic,strong)UIButton *achieveButton;            /**< 个人成就按钮*/

@property (nonatomic,assign)CGFloat transferCellHeight;                                 /**< 传入的cell高度*/
@property (nonatomic,strong)GWAboutCompanySingleModel *transferCompanySingleModel;      /**< 传入的公司model*/

@property (nonatomic,retain)id<GWAboutCompaynCellDelegate> delegate;

+(CGFloat)caculationCellHeightWithModel:(GWAboutCompanySingleModel *)transferCompanySingleModel;

// 收缩方法
-(void)animationWithShrinkManager;

@end
