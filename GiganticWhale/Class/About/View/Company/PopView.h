//
//  PopView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/28.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMapView.h"

typedef NS_ENUM(NSInteger, PopViewType) {
    PopViewTypeLocation,            /**< 用户显示地址*/
};

@interface PopView : UIView

@property (nonatomic,assign)PopViewType type;

@property (nonatomic,assign)CLLocationCoordinate2D transferCoordinate;
-(void)viewShow;
-(void)viewDismiss;

@end
