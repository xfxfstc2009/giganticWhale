//
//  GWAboutProductTypeCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAboutProductTypeCell.h"
#import "HTHorizontalSelectionList.h"

@interface GWAboutProductTypeCell()<HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource>
@property (nonatomic,strong) HTHorizontalSelectionList *segmentList;            /**< segment */
@property (nonatomic,strong) NSArray *segmentArr;

@end

@implementation GWAboutProductTypeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, 44);
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    [self addSubview:self.segmentList];
    self.segmentArr = @[@"全部",@"iOS",@".NET"];
    

}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    
}





-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);
    [self.segmentList reloadData];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}
@end
