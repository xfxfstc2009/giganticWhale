//
//  GWAboutProductCell.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutProductCell.h"

@interface GWAboutProductCell()
@property (nonatomic,strong)GWImageView *iConImageView;         /**< App图片*/
@property (nonatomic,strong)UILabel *appNameFixedLabel;         /**< app名字*/
@property (nonatomic,strong)UILabel *appNameLabel;              /**< App名字label*/
@property (nonatomic,strong)UILabel *appTypeLabel;              /**< app类型*/
@property (nonatomic,strong)UILabel *appDymicLabel;             /**< App */
@property (nonatomic,strong)UILabel *timeFixedLabel;            /**< 固定文字*/
@property (nonatomic,strong)UILabel *timeDymicLabel;
@property (nonatomic,strong)UILabel *dymicFixedLaebl;           /**< 作品简介*/

@end

@implementation GWAboutProductCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView;
-(void)createView{
    // 1. 背景
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [GWTool stretchImageWithName:@"about_product_bg_nor"];
    bgView.alpha = .3f;
    self.backgroundView = bgView;
    
    // 2. 图片
    self.iConImageView = [[GWImageView alloc]init];
    self.iConImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iConImageView];
    
    // 2. 创建App名字
    self.appNameFixedLabel = [[UILabel alloc]init];
    self.appNameFixedLabel.backgroundColor = [UIColor clearColor];
    self.appNameFixedLabel.text = @"作品名称:";
    self.appNameFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.appNameFixedLabel];
    
    // 2.1 app名字
    self.appNameLabel = [[UILabel alloc]init];
    self.appNameLabel.backgroundColor = [UIColor clearColor];
    self.appNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.appNameLabel];
    
    
    // 3.app 类型
    self.appTypeLabel = [[UILabel alloc]init];
    self.appTypeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.appTypeLabel.textColor =[UIColor grayColor];
    self.appTypeLabel.textAlignment = NSTextAlignmentCenter;
    self.appTypeLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.appTypeLabel];
    
    // 3. 时间
    self.timeFixedLabel = [[UILabel alloc]init];
    self.timeFixedLabel.backgroundColor = [UIColor clearColor];
    self.timeFixedLabel.text = @"制作时间:";
    self.timeFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.timeFixedLabel];
    
    self.timeDymicLabel = [[UILabel alloc]init];
    self.timeDymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.timeDymicLabel.textColor =[UIColor grayColor];
    self.timeDymicLabel.numberOfLines = 1;
    self.timeDymicLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.timeDymicLabel];
    
    
    // 4. 创建App介绍
    self.appDymicLabel = [[UILabel alloc]init];
    self.appDymicLabel.backgroundColor = [UIColor clearColor];
    self.appDymicLabel.numberOfLines = 0;
    self.appDymicLabel.font = [UIFont systemFontOfCustomeSize:12.];
    [self addSubview:self.appDymicLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferProductSingleModel:(GWAboutProductSingleModel *)transferProductSingleModel{
    _transferProductSingleModel = transferProductSingleModel;
    // 1. iCon
    self.iConImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), 64, 64);
    [self.iConImageView uploadAboutProductURL:transferProductSingleModel.icon callback:NULL];

    // 3. app类型
    self.appTypeLabel.text = transferProductSingleModel.type;
//    CGSize appTypeSize = [self.appTypeLabel.text sizeWithCalcFont:self.appTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.appTypeLabel.font])];
    self.appTypeLabel.frame = CGRectMake(self.iConImageView.orgin_x, CGRectGetMaxY(self.iConImageView.frame),self.iConImageView.size_width , [NSString contentofHeight:self.appTypeLabel.font]);
    
    // 2. appName
    CGSize appNameFixedSize = [self.appNameFixedLabel.text sizeWithCalcFont:self.appNameFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.appNameFixedLabel.font])];
    self.appNameFixedLabel.frame = CGRectMake(CGRectGetMaxX(self.iConImageView.frame) + LCFloat(11), self.iConImageView.orgin_y, appNameFixedSize.width, [NSString contentofHeight:self.appNameFixedLabel.font]);
    
    
    self.appNameLabel.text = transferProductSingleModel.name;
    self.appNameLabel.frame = CGRectMake(CGRectGetMaxX(self.appNameFixedLabel.frame), self.appNameFixedLabel.orgin_y,kScreenBounds.size.width - CGRectGetMaxX(self.appNameFixedLabel.frame) - LCFloat(11) * 2, [NSString contentofHeight:self.appNameLabel.font]);

    
    // 3. 制作时间
    self.timeFixedLabel.frame = CGRectMake(self.appNameFixedLabel.orgin_x, CGRectGetMaxY(self.appNameFixedLabel.frame) + LCFloat(3), self.appNameFixedLabel.size_width, [NSString contentofHeight:self.timeFixedLabel.font]);
    
    NSString *beginTime = [NSDate getTimeBirthday:transferProductSingleModel.beginTime];
    NSString *endTime = [NSDate getTimeBirthday:transferProductSingleModel.endTime];
    NSString *timeString = [NSString stringWithFormat:@"%@~%@",beginTime,endTime];
    self.timeDymicLabel.text = timeString;
    CGSize timeDymicSize = [self.timeDymicLabel.text sizeWithCalcFont:self.timeDymicLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.timeFixedLabel.frame), CGFLOAT_MAX)];
    self.timeDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.timeFixedLabel.frame), self.timeFixedLabel.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(self.timeFixedLabel.frame), timeDymicSize.height);
    
    
    
    // 3. app 介绍
    NSString *descInfo = [NSString stringWithFormat:@"作品介绍：%@",transferProductSingleModel.desc];
    
    self.appDymicLabel.attributedText = [GWTool rangeLabelWithContent:descInfo hltContentArr:@[@"作品介绍："] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:[UIColor colorWithCustomerName:@"灰"]];

    CGSize contentOfSize = [descInfo sizeWithCalcFont:self.appDymicLabel.font  constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.iConImageView.frame) - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.appDymicLabel.frame = CGRectMake(self.appNameFixedLabel.orgin_x, CGRectGetMaxY(self.timeFixedLabel.frame) + LCFloat(5), kScreenBounds.size.width - CGRectGetMaxX(self.iConImageView.frame) - 2 * LCFloat(11), contentOfSize.height);
}

+(CGFloat)calculationCellHeightWithModel:(GWAboutProductSingleModel *)transferProductSingleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(3);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    // 介绍
    NSString *descInfo = [NSString stringWithFormat:@"作品介绍：%@",transferProductSingleModel.desc];
    CGSize contentOfSize = [descInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]  constrainedToSize:CGSizeMake(kScreenBounds.size.width - (LCFloat(11) + 64) - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(5);
    cellHeight += LCFloat(5);
    
    if (cellHeight < 64 + LCFloat(5) + [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]] + LCFloat(5)){
        return 64 + LCFloat(5) + [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]] + LCFloat(5);
    }
    return cellHeight;
}


#pragma mark 飘动过去Animation
- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.transform =  CGAffineTransformMakeTranslation(kScreenWidth, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
}
@end
