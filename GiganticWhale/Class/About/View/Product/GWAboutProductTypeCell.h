//
//  GWAboutProductTypeCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWAboutProductTypeCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;         /**< 传递cell 高度*/


+(CGFloat)calculationCellHeight;
@end

