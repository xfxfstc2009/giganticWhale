//
//  GWAboutProductCell.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWAboutProductSingleModel.h"
@interface GWAboutProductCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)GWAboutProductSingleModel *transferProductSingleModel;      /**< 传递的商品model*/

+(CGFloat)calculationCellHeightWithModel:(GWAboutProductSingleModel *)transferProductSingleModel;

- (void)startAnimationWithDelay:(CGFloat)delayTime ;
@end
