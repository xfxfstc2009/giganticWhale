//
//  GWAboutMeDetailModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWAboutMeDetailSingleModel.h"

@protocol GWAboutMeDetailModel <NSObject>

@end

@interface GWAboutMeDetailModel : FetchModel


@property (nonatomic,copy)NSString *content;            
@property (nonatomic,strong)NSArray <GWAboutMeDetailSingleModel> *imgList;

@end
