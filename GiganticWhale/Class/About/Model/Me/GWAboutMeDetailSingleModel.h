//
//  GWAboutMeDetailSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWAboutMeDetailSingleModel <NSObject>

@end

@interface GWAboutMeDetailSingleModel : FetchModel

@property (nonatomic,copy)NSString * contents;
@property (nonatomic,copy)NSString *img;


@end
