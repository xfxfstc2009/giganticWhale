//
//  GWAboutMeModel.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWAboutMeDetailModel.h"

typedef NS_ENUM(NSInteger,showType) {
    showTypeImg = 1,
    showTypeWhale = 2,                  /**< 鲸鱼*/
};

@interface GWAboutMeModel : FetchModel

@property (nonatomic,copy)NSString *avatar;             /**< 头像*/
@property (nonatomic,copy)NSString *home_url;           /**< 主页*/
@property (nonatomic,copy)NSString *boke_url;           /**< 博客*/
@property (nonatomic,assign)NSTimeInterval birthday;    /**< 生日*/
@property (nonatomic,copy)NSString *email;              /**< email*/
@property (nonatomic,copy)NSString *location;           /**< live*/
@property (nonatomic,copy)NSString *desc;               /**< 简介*/
@property (nonatomic,copy)NSString *name;               /**< 名字*/
@property (nonatomic,copy)NSString *work_time;           /**< 工作时间*/
@property (nonatomic,copy)NSString *money;              /**< 工资*/
@property (nonatomic,assign)NSInteger type;             /**< 类型*/
@property (nonatomic,copy)NSString *education;          /**< 学历*/
@property (nonatomic,copy)NSString *qq;                 /**< qq*/
@property (nonatomic,copy)NSString *phone;              /**< 号码*/
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)CGFloat lng;    
@property (nonatomic,strong)NSArray<GWAboutMeDetailModel> *detail;   /**< 详情*/
@end
