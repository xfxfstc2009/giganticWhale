//
//  GWAboutCompanyImgModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GWAboutCompanyImgModel <NSObject>

@end

@interface GWAboutCompanyImgModel : FetchModel

@property (nonatomic,copy)NSString *sontents;
@property (nonatomic,copy)NSString *img;

@end
