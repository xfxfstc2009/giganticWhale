//
//  GWAboutCompanyAchiModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol  GWAboutCompanyAchiModel<NSObject>

@end

@interface GWAboutCompanyAchiModel : FetchModel

@property (nonatomic,copy)NSString *achi;           /**< 成就*/

@end
