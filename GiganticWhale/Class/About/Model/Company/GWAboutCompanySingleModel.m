//
//  GWAboutCompanySingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/4.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutCompanySingleModel.h"

@implementation GWAboutCompanySingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc": @"description",@"companyId":@"id"};
}

@end
