//
//  GWAboutCompanySingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/4.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWAboutCompanyImgModel.h"
#import "GWAboutCompanyAchiModel.h"
@protocol GWAboutCompanySingleModel <NSObject>

@end

@interface GWAboutCompanySingleModel : FetchModel

@property (nonatomic,strong)NSArray *achiList;                                  /**< 成就地址*/
@property (nonatomic,copy)NSString *desc;                                       /**< 详情*/
@property (nonatomic,copy)NSString *icon;                                       /**< icon*/
@property (nonatomic,strong)NSArray<GWAboutCompanyImgModel> *imgList;           /**< 公司图片地址*/
@property (nonatomic,assign)CGFloat lat;                                        /**< 经纬度*/
@property (nonatomic,assign)CGFloat lng;                                        /**< 经纬度*/
@property (nonatomic,copy)NSString *location;                                   /**< 公司地址*/
@property (nonatomic,copy)NSString *name;                                       /**< 公司名字*/
@property (nonatomic,copy)NSString *url;                                        /**< url*/

@property (nonatomic,copy)NSString *companyId;                                  /**< 公司编号*/

// 其他
@property (nonatomic,assign)BOOL isOpen;                                        /**< 判断是否打开*/

@end
