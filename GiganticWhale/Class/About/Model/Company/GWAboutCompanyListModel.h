//
//  GWAboutCompanyListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/4.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWAboutCompanySingleModel.h"
@interface GWAboutCompanyListModel : FetchModel

@property (nonatomic,strong)NSArray<GWAboutCompanySingleModel>*companyList;

@end
