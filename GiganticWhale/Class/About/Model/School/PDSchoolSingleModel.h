//
//  PDSchoolSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSchoolSingleModel <NSObject>

@end

@interface PDSchoolSingleModel : FetchModel

@property (nonatomic,copy)NSString *desc;               /**< 详情*/
@property (nonatomic,copy)NSString *schoolId;           /**< 学校编号*/
@property (nonatomic,strong)NSArray *imgList;           /**< 学校图片*/
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,copy)NSString *location;
@property (nonatomic,copy)NSString *name;               /**< 学校名称*/
@property (nonatomic,copy)NSString *type;               /**< 学校类别*/
@property (nonatomic,copy)NSString *url;                /**< 学校地址*/

@property (nonatomic,strong)NSArray *schoolRewardArr;   /**< 学校奖励*/

@end
