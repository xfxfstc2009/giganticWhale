//
//  PDSchoolSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PDSchoolSingleModel.h"

@implementation PDSchoolSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc": @"description",@"schoolId":@"id"};
}
@end
