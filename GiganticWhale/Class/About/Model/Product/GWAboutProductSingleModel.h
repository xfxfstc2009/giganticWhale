//
//  GWAboutProductSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/6.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol GWAboutProductSingleModel <NSObject>


@end

@interface GWAboutProductSingleModel : FetchModel

@property (nonatomic,copy)NSString *appId;                      /**<  APP url*/
@property (nonatomic,assign)NSTimeInterval beginTime;           /**< 开始时间*/
@property (nonatomic,copy)NSString *company_id;                 /**< 公司id*/
@property (nonatomic,copy)NSString *desc;                       /**< 详情*/
@property (nonatomic,assign)NSTimeInterval endTime;             /**< 结束时间*/
@property (nonatomic,copy)NSString *icon;                       /**< 编号*/
@property (nonatomic,copy)NSString *productId;                  /**< 商品编号*/
@property (nonatomic,copy)NSString *name;                       /**< 产品名称*/
@property (nonatomic,copy)NSString *open_url;                    /**< 打开编号*/
@property (nonatomic,assign)BOOL sign;                          /**< 标记*/
@property (nonatomic,copy)NSString *type;                       /**< 产品类型*/




@property (nonatomic,assign)BOOL isAnimation;

@end
