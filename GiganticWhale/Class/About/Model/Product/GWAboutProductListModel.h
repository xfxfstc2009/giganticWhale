//
//  GWAboutProductListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/6.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWAboutProductSingleModel.h"
@interface GWAboutProductListModel : FetchModel

@property (nonatomic,strong)NSArray<GWAboutProductSingleModel> *productList;

@end
