//
//  GWAboutProductSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/5/6.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAboutProductSingleModel.h"

@implementation GWAboutProductSingleModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc": @"description",@"productId":@"id"};
}
@end
