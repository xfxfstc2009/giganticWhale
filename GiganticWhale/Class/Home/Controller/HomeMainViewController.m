//
//  HomeMainViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "HomeMainViewController.h"
#import "GWSearchBar.h"                         // 搜索栏
#import "GWMapSliderView.h"                     // 菜单
#import <pop/POP.h>
#import "GWPopImgView.h"
#import "GWMusicPlayerViewController.h"
#import "GWMusicTopView.h"
#import "GWNewFeatureViewController.h"          // 新特性界面
#import "GWHealthManager.h"

// test
#import "GWStudyInfoListModel.h"

typedef struct {
    CGFloat progress;
    CGFloat toValue;
    CGFloat currentValue;
} AnimationInfo;


@interface HomeMainViewController()<GWSearchBarDelegate>
@property (nonatomic,strong)UIButton *openDrawerButton;                     /**< 开关*/
@property (nonatomic,strong)GWSearchBar *searchBar;                         /**< 搜索*/
@property (nonatomic,strong)GWMapSliderView *mapSliderView;                 /**< 创建slider*/
@property (nonatomic,strong)UIButton *sliderButton;                         /**< 创建slider Button*/
@property (nonatomic,strong)GWMusicTopView *musicTopView;                   /**< */
@end

@implementation HomeMainViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
//    [self createSearchBar];
    [self addlocation];
    [self createSlider];
    [[GWUserCurrentInfoGetManager sharedHealthManager] getUserCurrentInfoManager];          // 获取当前信息
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushNotificationWithLocation:) name:GWPushNotificationWithLocation object:nil];
    
  

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showTopView];
}


-(void)pushNotificationWithLocation:(NSNotification *)note{
    id object = note.object;
    if ([object isKindOfClass:[AccountModel class]]){
        
    }
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark - PageSetting
-(void)pageSetting{
    self.barMainTitle = @"首页";
    
    // 2. 创建按钮
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
}

#pragma mark - UISearchBar
-(void)createSearchBar{
    self.searchBar = [[GWSearchBar alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(25) - 30,self.openDrawerButton.orgin_y,40,40)];
    self.searchBar.searchField.returnKeyType = UIReturnKeySearch;
    self.searchBar.delegate = self;
    [self.view addSubview:self.searchBar];
}

#pragma mark - UISearchBarDelegate
- (CGRect)destinationFrameForSearchBar:(GWSearchBar *)searchBar {           // 点开searchBar 事件
    return CGRectMake(LCFloat(11), 20 + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), 35);
}

// searchBar 将要使用
- (void)searchBar:(GWSearchBar *)searchBar willStartTransitioningToState:(GWSearchBarState)destinationState {
   
}

// searchBar 已经使用
- (void)searchBar:(GWSearchBar *)searchBar didEndTransitioningFromState:(GWSearchBarState)previousState {
    if (previousState == GWSearchBarStateSearchBarHasContent){
        
    } else if (previousState == GWSearchBarStateSearchBarVisible){      // 消失
        
    } else if (previousState == GWSearchBarStateTransitioning){         // 过度
        
    } else if (previousState == GWSearchBarStateNormal){                // 出现
      
    }
}

// searchBar 回车
- (void)searchBarDidTapReturn:(GWSearchBar *)searchBar {
    if ([searchBar.searchField isFirstResponder]){
        [searchBar.searchField resignFirstResponder];
    }
    
//    // 1. 进行搜索
    NSString *key = searchBar.searchField.text;
    [self searchPoiWithType:AMapPOISearchTypeKeywords keyword:key];
}

// searchBar 文字修改
- (void)searchBarTextDidChange:(GWSearchBar *)searchBar {
//    [self searchTipsWithKey:self.searchBar.searchField.text];
//    if (self.searchResultView){
//        [self.searchResultView showInView:self.navigationController.view animated:YES];
//    }
}

-(void)statusAnimationWithNormal{
//    // 隐藏输入信息
//    [self.searchBar hideSearchBar:nil];                          // searchBar 隐藏
//    [self.searchResultView hideAnimated:YES];               // searchView 隐藏
//    [UIView animateWithDuration:.5f animations:^{
//        self.openDrawerButton.orgin_y = LCFloat(30);
//    }];
}


#pragma mark - 菜单
-(void)createSlider{
    
    [self createView];
    
//    self.sliderButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.sliderButton.backgroundColor = [UIColor clearColor];
//    self.sliderButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(30), kScreenBounds.size.height - LCFloat(20) - LCFloat(10) - LCFloat(30), LCFloat(30), LCFloat(30));
//    [self.view addSubview:self.sliderButton];
    __weak typeof(self)weakSelf = self;
//    [self.sliderButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (strongSelf.mapSliderView.isOpen){
//            [strongSelf.mapSliderView close];
//        } else {
//            [strongSelf.mapSliderView open];
//        }
//    }];
    
    // 1. 音乐
    GWMapSliderCustomerView *musicView = [[GWMapSliderCustomerView alloc]initWithImg:[UIImage imageNamed:@"icon_home_slider"] block:^{
        if (!weakSelf){
            return ;
        }
//        GWHealthManager *healthManager = [[GWHealthManager alloc]init];
//        //    [healthManager gethealthMainManagerWithBlock:^(GWHealthSingleModel *healthModel) {
//        //        NSLog(@"%@",healthModel);
//        //    }];
//        [healthManager requestAuthorizationWithBlock:^(BOOL isSuccess) {
//            NSLog(@"%li",isSuccess);
//        }];
    }];
    
    // 3. 离线地图
    GWMapSliderCustomerView *offLineView = [[GWMapSliderCustomerView alloc]initWithImg:[UIImage imageNamed:@"icon_home_slider"] block:^{
        if (!weakSelf){
            return ;
        }
    }];
    
    self.mapSliderView = [[GWMapSliderView alloc] initWithItems:@[musicView,offLineView]];
    [self.mapSliderView setItemSpacing:5.0f];
    [self.view addSubview:self.mapSliderView];
}


#pragma mark test
-(void)addlocation{
    GiganticWhaleAnnotation *annotation = [[GiganticWhaleAnnotation alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = 30.188449;
    location.longitude = 120.289899;
    annotation.coordinate = location;
    annotation.title    = @"AutoNavi";
    annotation.subtitle = @"CustomAnnotationView";
    
    [self.mapView addAnnotation:annotation];
}


#pragma mark - 创建可移动的按钮
-(void)createView{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    
    CGRect fr = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(30), kScreenBounds.size.height - LCFloat(20) - LCFloat(10) - LCFloat(30), LCFloat(30), LCFloat(30));
    GWPopImgView *imageView = [[GWPopImgView alloc] initWithFrame:fr];
    [imageView setImage:[UIImage imageNamed:@"icon_home_slider"]];
    [imageView addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
    [imageView addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addGestureRecognizer:recognizer];
    
    [self.view addSubview:imageView];
    [self scaleDownView:imageView];
}

- (void)touchDown:(UIControl *)sender {
    [self pauseAllAnimations:YES forLayer:sender.layer];
}

- (void)touchUpInside:(UIControl *)sender {
//    AnimationInfo animationInfo = [self animationInfoForLayer:sender.layer];
//    BOOL hasAnimations = sender.layer.pop_animationKeys;
//    
//    if (hasAnimations && animationInfo.progress < 0.98) {
//        [self pauseAllAnimations:NO forLayer:sender.layer];
//        return;
//    }
//    
//    [sender.layer pop_removeAllAnimations];
//    if (animationInfo.toValue == 1 || sender.layer.affineTransform.a == 1) {
//        [self scaleDownView:sender];
//        return;
//    }
//    [self scaleUpView:sender];

    if (self.mapSliderView.isOpen){
        [self.mapSliderView close];
    } else {
        [self.mapSliderView open];
    }
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    [self scaleDownView:recognizer.view];
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
    if(recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint velocity = [recognizer velocityInView:self.view];
        
        POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
        positionAnimation.velocity = [NSValue valueWithCGPoint:velocity];
        positionAnimation.dynamicsTension = 10.f;
        positionAnimation.dynamicsFriction = 1.0f;
        positionAnimation.springBounciness = 12.0f;
        [recognizer.view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];
    }
}

-(void)scaleUpView:(UIView *)view
{
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    positionAnimation.toValue = [NSValue valueWithCGPoint:self.view.center];
    [view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)scaleDownView:(UIView *)view
{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)pauseAllAnimations:(BOOL)pause forLayer:(CALayer *)layer
{
    for (NSString *key in layer.pop_animationKeys) {
        POPAnimation *animation = [layer pop_animationForKey:key];
        [animation setPaused:pause];
    }
}

- (AnimationInfo)animationInfoForLayer:(CALayer *)layer
{
    POPSpringAnimation *animation = [layer pop_animationForKey:@"scaleAnimation"];
    CGPoint toValue = [animation.toValue CGPointValue];
    CGPoint currentValue = [[animation valueForKey:@"currentValue"] CGPointValue];
    
    CGFloat min = MIN(toValue.x, currentValue.x);
    CGFloat max = MAX(toValue.x, currentValue.x);
    
    AnimationInfo info;
    info.toValue = toValue.x;
    info.currentValue = currentValue.x;
    info.progress = min / max;
    return info;
}



#pragma mark - showMusicView
-(void)showTopView{
    if ([GWMusicPlayerMethod sharedLocationManager].streamer){
        [self createTopView];
    } else {
    
    }
}

-(void)createTopView{
    if (!self.musicTopView){
        self.musicTopView = [[GWMusicTopView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(10) - LCFloat(70), LCFloat(100), LCFloat(70),LCFloat(70))];
        __weak typeof(self)weakSelf = self;
        [weakSelf.musicTopView actionClickBlock:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            CGRect cellFrameInSuperview = [strongSelf.musicTopView convertRect:strongSelf.musicTopView.flagImageView.frame toView:strongSelf.view];
            GWMusicPlayerViewController *musicPlayerViewController = [[GWMusicPlayerViewController alloc]init];
            musicPlayerViewController.transferRect = cellFrameInSuperview;
            [strongSelf.navigationController pushViewController:musicPlayerViewController animated:NO];
        }];
        [self.view addSubview:self.musicTopView];
        [self.musicTopView imageUploadWithBlock:NULL];
        [self.musicTopView showWithAnimation];
        [self.musicTopView animationWithRound];
    } else {
        [self.musicTopView animationWithRound];
    }
}


@end
