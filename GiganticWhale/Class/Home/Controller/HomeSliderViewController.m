//
//  HomeSliderViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "HomeSliderViewController.h"

// cell
#import "PDSliderTableViewCell.h"

#import "GWSettingRootViewController.h"             // 【设置页面】
#import "HomeMainViewController.h"                  // 【首页】
#import "GWPlayerRootViewController.h"              // 【播放】
#import "GWMusicListViewController.h"               // 【音乐】
#import "GWAboutMainViewController.h"               // 【我】
#import "GWMyWorkListViewController.h"              // 【我的工作】
#import "GWLoginViewController.h"                   // 【登录】
#import "ConnectionWechat.h"                        // 【微信登录】
#import "GWMusicPlayerViewController.h"             // 【音乐】
#import "GWMyStudyViewController.h"                 // 【学习】
#import "GWHealthRootViewController.h"              // 【健康】
#import "GWDiaryRootViewController.h"               // 【日记】
#import "GWShopRootViewController.h"                // 【商店】
#import "GWWeatherViewController.h"                 // 【天气】
#import "GWWeatherMainViewController.h"
#import "GWCurrentIMViewController.h"               // 【聊天】


// view
#import "GWGradientNavBar.h"

@interface HomeSliderViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *sliderArr;


@property (nonatomic,strong)UIView *bottomView;             /**< 创建底部的view*/
@property (nonatomic,strong)GWWeatherView *weatherView;     /**< 天气*/

@end

@implementation HomeSliderViewController

-(void)dealloc{
    NSLog(@"123");
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createBottomView];
    [self arrayWithInit];
    [self createTableView];
    [self autoLogin];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - 
-(void)pageSetting{
    self.view.backgroundColor = [UIColor clearColor];
    
}

-(void)autoLogin{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"登录"] inSection: [self cellIndexPathSectionWithcellData:@"登录"]];
        GWSliderLeftTableViewCell *cell = (GWSliderLeftTableViewCell *)[strongSelf.sliderTableView cellForRowAtIndexPath:indexPath];
        cell.transferLoginModel = [AccountModel sharedAccountModel];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.sliderArr = [NSMutableArray array];
    NSArray *sliderArr = @[@[@"登录"],@[@"HomePage",@"Player",@"Music"],@[@"AboutMe",@"Mywork",@"Setting"],@[@"Health",@"Mywork"]];
    [self.sliderArr addObjectsFromArray:sliderArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.sliderTableView){
        self.sliderTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.sliderTableView.size_height = kScreenBounds.size.height - self.bottomView.size_height;
        self.sliderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.sliderTableView.delegate = self;
        self.sliderTableView.dataSource = self;
        self.sliderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.sliderTableView.backgroundColor = [UIColor clearColor];
        self.sliderTableView.backgroundView = nil;
        self.sliderTableView.bounces = YES;
        self.sliderTableView.opaque = NO;
        [self.view addSubview:self.sliderTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sliderArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.sliderArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        GWSliderLeftTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[GWSliderLeftTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowZero.transferCellHeiht = cellHeight;
        cellWithRowZero.transferLoginModel = [AccountModel sharedAccountModel];
        
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDSliderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDSliderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferTitle = [[self.sliderArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOne.transferCellHeight = cellHeight;
        return cellWithRowOne;
    }
}

#pragma mark - UITableDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        return [GWSliderLeftTableViewCell calculationCellHeight];
    } else {
        return [PDSliderTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 64;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GWNavigationController *navigationController = [[GWNavigationController alloc] initWithNavigationBarClass:[GWGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    NSArray *navBarArr;
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        if (![[AccountModel sharedAccountModel] hasLoggedIn]){
            HomeMainViewController *homeMainViewController = [[HomeMainViewController alloc]init];
            [navigationController setViewControllers:@[homeMainViewController]];
            __weak typeof(self)weakSelf = self;
            [[AccountModel sharedAccountModel] wechatLoginManagerWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.headerImageView = [[GWImageView alloc]init];
                strongSelf.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
                [strongSelf loginSuccessManager];
            }];
        } else {
            [self.sideMenuViewController hideMenuViewController];
            return;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"HomePage"] && indexPath.row == [self cellIndexPathRowWithcellData:@"HomePage"]){           // 首页
        HomeMainViewController *homeMainViewController = [[HomeMainViewController alloc]init];
        [navigationController setViewControllers:@[homeMainViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Player"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Player"]){             // 视频播放
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
        
        GWPlayerRootViewController *playerViewController = [[GWPlayerRootViewController alloc]init];
        [navigationController setViewControllers:@[playerViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Music"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Music"]){              // 音乐播放
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
        GWMusicListViewController *musicPlayerViewController = [[GWMusicListViewController alloc]init];
        [navigationController setViewControllers:@[musicPlayerViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Mywork"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Mywork"]){             // 我的工作
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        GWMyWorkListViewController *myWorkListViewController = [[GWMyWorkListViewController alloc]init];
        [navigationController setViewControllers:@[myWorkListViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Setting"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Setting"]){            // 设置
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWSettingRootViewController *settingRootViewController = [[GWSettingRootViewController alloc]init];
        [navigationController setViewControllers:@[settingRootViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"AboutMe"] && indexPath.row == [self cellIndexPathRowWithcellData:@"AboutMe"]){            // 关于我
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWAboutMainViewController *aboutViewController = [[GWAboutMainViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Study"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Study"]){
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWMyStudyViewController *aboutViewController = [[GWMyStudyViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Health"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Health"]){             // 【健康】
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWHealthRootViewController *aboutViewController = [[GWHealthRootViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Diary"] && indexPath.row == [self  cellIndexPathRowWithcellData:@"Diary"]){
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWDiaryRootViewController *diaryRootViewController = [[GWDiaryRootViewController alloc]init];
        [navigationController setViewControllers:@[diaryRootViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Shop"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Shop"]){
        UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
        UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
        navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
        
        GWShopRootViewController *diaryRootViewController = [[GWShopRootViewController alloc]init];
        [navigationController setViewControllers:@[diaryRootViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Link"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Link"]){
        if (![[AccountModel sharedAccountModel] hasLoggedIn]){
            [self wechatLogin];
        } else {
            UIColor *color1 = [UIColor hexChangeFloat:@"4CB8C4"];
            UIColor *color6 = [UIColor hexChangeFloat:@"3CD3AD"];
            navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
            
            YWPerson *person = [[YWPerson alloc]initWithPersonId:@"1"];
            [[AliChatManager sharedInstance] openConversationWithPerson:person callBack:^(GWCurrentIMViewController *conversationController) {
                [navigationController setViewControllers:@[conversationController]];
                
                return ;
            }];
        }
    } else {
        GWLoginViewController *aboutViewController = [[GWLoginViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    }
    [[GWGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    [self.sideMenuViewController setContentViewController:navigationController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)wechatLogin{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] wechatLoginManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!strongSelf.headerImageView){
            strongSelf.headerImageView = [[GWImageView alloc]init];
        }
        strongSelf.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
        [strongSelf loginSuccessManager];
        
        YWPerson *person = [[YWPerson alloc]initWithPersonId:@"1"];
        GWNavigationController *navigationController = [[GWNavigationController alloc] initWithNavigationBarClass:[GWGradientNavBar class] toolbarClass:nil];
        [[AliChatManager sharedInstance] openConversationWithPerson:person callBack:^(GWCurrentIMViewController *conversationController) {
            [navigationController setViewControllers:@[conversationController]];
            
            return ;
        }];
        [strongSelf.sideMenuViewController setContentViewController:navigationController animated:YES];
        [strongSelf.sideMenuViewController hideMenuViewController];
    }];

}


#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.sliderArr.count ; i++){
        NSArray *dataTempArr = [self.sliderArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.sliderArr.count ; i++){
        NSArray *dataTempArr = [self.sliderArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}


-(void)loginSuccess{
    HomeMainViewController *homeViewController = [[HomeMainViewController alloc]init];
    [self.sideMenuViewController setContentViewController:[[GWNavigationController alloc]initWithRootViewController:homeViewController] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)logoutSuccess{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"登录"] inSection: [self cellIndexPathSectionWithcellData:@"登录"]];
    GWSliderLeftTableViewCell *cell = (GWSliderLeftTableViewCell *)[self.sliderTableView cellForRowAtIndexPath:indexPath];
    cell.transferLoginModel = [AccountModel sharedAccountModel];
}


-(void)updateList:(NSArray *)sliderList{
    if (self.sliderArr.count){
        [self.sliderArr removeAllObjects];
    }
    NSMutableArray *sliderTempArr = [NSMutableArray array];
    for (int i = 0 ; i < sliderList.count;i++){
        GWSettingSliderSingleModel *singleModel = [sliderList objectAtIndex:i];
        [sliderTempArr addObject:singleModel.sign];
    }
    [self.sliderArr addObject:sliderTempArr];
    
    [self.sliderTableView reloadData];
}

#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(70), kScreenBounds.size.width, LCFloat(70));
    [self.view addSubview:self.bottomView];
    
    CGFloat main_width = kScreenBounds.size.width / 2. + kScreenBounds.size.width / 15.;
    
    // 2. 创建天气
    CGRect weatherFrame = CGRectMake(0, 0, main_width / 3 * 2, self.bottomView.size_height);
    __weak typeof(self)weakSelf = self;
    self.weatherView = [[GWWeatherView alloc]initWithFrame:weatherFrame withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWWeatherMainViewController *weatherViewController = [[GWWeatherMainViewController alloc]init];
        [strongSelf.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:weatherViewController] animated:YES];
        [strongSelf.sideMenuViewController hideMenuViewController];
    }];
    self.weatherView.backgroundColor = [UIColor clearColor];
    [self.bottomView addSubview:self.weatherView];
}



#pragma mark - 更新天气
-(void)reloadWeatherManager{
    [self.weatherView setDataWithTemp:[GWUserCurrentInfoGetManager sharedHealthManager].weatherCurrentModel.temperature city:[GWLocationMethod sharedLocationManager].addressComponent.district];
}

#pragma mark - 创建动画
-(void)createLeftAnimationMovie{

}

@end
