//
//  HomeSliderViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWSliderLeftTableViewCell.h"               // 登录的cell
#import "GWSettingSliderListModel.h"
#import "GWBaseSettingModel.h"

@class AbstractViewController;
@interface HomeSliderViewController : AbstractViewController

@property (nonatomic,strong)UITableView *sliderTableView;

-(void)updateList:(NSArray *)sliderList;

-(void)loginSuccess;
-(void)logoutSuccess;

#pragma mark - 更新天气
-(void)reloadWeatherManager;

@end
