//
//  GWSideMenu.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSideMenu.h"
#import "HomeSliderViewController.h"
#import "HomeMainViewController.h"

@interface GWSideMenu()<RESideMenuDelegate>

@end

@implementation GWSideMenu

+(RESideMenu *)setupSlider{
    GWNavigationController *navigationController = [[GWNavigationController alloc] initWithRootViewController:[[HomeMainViewController alloc] init]];
    HomeSliderViewController *leftMenuViewController = [[HomeSliderViewController alloc] init];
    
    
    RESideMenu *sideMenuViewController = [[RESideMenu shareInstance] initWithContentViewController:navigationController leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
    sideMenuViewController.backgroundImage = [UIImage imageNamed:@"giganticwhale"];
    sideMenuViewController.menuPreferredStatusBarStyle = 1;     // UIStatusBarStyleLightContent
    sideMenuViewController.animationDuration = .55f;
    sideMenuViewController.contentViewShadowColor = [UIColor colorWithCustomerName:@"灰"];
    sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
    sideMenuViewController.contentViewShadowOpacity = 10;
    sideMenuViewController.contentViewShadowRadius = 10;
    sideMenuViewController.contentViewScaleValue = .88f;
    sideMenuViewController.contentViewInPortraitOffsetCenterX = + kScreenBounds.size.width / 15.;
    sideMenuViewController.contentViewShadowEnabled = YES;
    sideMenuViewController.parallaxEnabled = YES;
    sideMenuViewController.bouncesHorizontally = YES;
    sideMenuViewController.fadeMenuView = YES;
    sideMenuViewController.contentViewFadeOutAlpha = 1.f;
    
    return sideMenuViewController;
}

@end
