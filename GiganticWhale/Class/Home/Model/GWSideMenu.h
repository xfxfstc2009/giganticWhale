//
//  GWSideMenu.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWSideMenu : NSObject

+(RESideMenu *)setupSlider;

@end
