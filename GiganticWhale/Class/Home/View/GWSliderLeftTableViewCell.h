//
//  GWSliderLeftTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/15.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWSliderLeftTableViewCell : UITableViewCell
@property (nonatomic,strong)GWImageView *iconImageView;
@property (nonatomic,strong)UIButton *scanningButton;

@property (nonatomic,assign)CGFloat transferCellHeiht;
@property (nonatomic,strong)AccountModel *transferLoginModel;

-(void)loginManager;

+(CGFloat)calculationCellHeight;

@end
