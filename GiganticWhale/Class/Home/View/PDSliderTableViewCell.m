//
//  PDSliderTableViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderTableViewCell.h"
#import "PDLabel.h"

@interface PDSliderTableViewCell()
@property (nonatomic,strong)GWImageView *iconImageView;             /**< icon*/
@property (nonatomic,strong)PDLabel *fixedLabel;                    /**< fixedLaberl*/

@end

@implementation PDSliderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[GWImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 2. 创建label
    self.fixedLabel = [[PDLabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.fixedLabel.glowInColor = [UIColor colorWithCustomerName:@"白"];
    self.fixedLabel.glowInSize = 5.;
    self.fixedLabel.glowOutColor = [UIColor colorWithCustomerName:@"白"];
    self.fixedLabel.glowOutSize = 10;
    self.fixedLabel.glowShow = YES;
    [self addSubview:self.fixedLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    // 1. 当前的icon
    self.iconImageView.frame = CGRectMake(LCFloat(13), (_transferCellHeight - LCFloat(15)) / 2. , LCFloat(15) ,LCFloat(15));
    NSString *imgName = @"";
    if ([self.transferTitle isEqualToString:@"盼达资讯"]){
        imgName = @"icon_slider_information";
    } else if ([self.transferTitle isEqualToString:@"我的战场"]){
        imgName = @"icon_slider_challenge";
    } else if ([self.transferTitle isEqualToString:@"绑定召唤师"]){
        imgName = @"icon_slider_lolSetting";
    } else if ([self.transferTitle isEqualToString:@"盼达商城"]){
        imgName = @"icon_slider_shop";
    } else if ([self.transferTitle isEqualToString:@"我的撸友"]){
        imgName = @"icon_slider_firend";
    } else if ([self.transferTitle isEqualToString:@"设置"]){
        imgName = @"icon_slider_setting";
    }
    self.iconImageView.image = [UIImage imageNamed:imgName];
    
    // 2. 创建fixedLabel
    self.fixedLabel.text = self.transferTitle;
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(15), 0, self.size_width - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), self.transferCellHeight);
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}



@end
