
//
//  GWSliderLeftTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/15.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWSliderLeftTableViewCell.h"
#import "PDLabel.h"
@interface GWSliderLeftTableViewCell()

@property (nonatomic,strong) PDLabel*nicknameLabel;

@end

@implementation GWSliderLeftTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[GWImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    self.iconImageView.clipsToBounds = YES;
    
    [self addSubview:self.iconImageView];
    
    // 2. 创建nickLabel
    self.nicknameLabel = [[PDLabel alloc]init];
    self.nicknameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
    self.nicknameLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.nicknameLabel.glowInColor = [UIColor colorWithCustomerName:@"白"];
    self.nicknameLabel.glowInSize = 5.;
    self.nicknameLabel.glowOutColor = [UIColor colorWithCustomerName:@"白"];
    self.nicknameLabel.glowOutSize = 10;
    self.nicknameLabel.glowShow = YES;
    [self addSubview:self.nicknameLabel];
    
//    // 3. 创建二维码
//    self.scanningButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.scanningButton setBackgroundImage:[UIImage imageNamed:@"icon_erweima"] forState:UIControlStateNormal];
//    [self addSubview:self.scanningButton];
}

-(void)setTransferCellHeiht:(CGFloat)transferCellHeiht{
    _transferCellHeiht = transferCellHeiht;
}

-(void)setTransferLoginModel:(AccountModel *)transferLoginModel{
    // 1. 头像
    self.iconImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), (self.transferCellHeiht - 2 * LCFloat(13)), (self.transferCellHeiht - 2 * LCFloat(13)));
    self.iconImageView.layer.cornerRadius = self.iconImageView.size_height / 2.;
    // 2. 昵称
    self.nicknameLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(15), CGRectGetMaxY(self.iconImageView.frame) - LCFloat(40), LCFloat(200), LCFloat(40));
    
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){        // 已经登录
        [self.iconImageView uploadImageWithURL:[AccountModel sharedAccountModel].avatar  placeholder:nil callback:NULL];
        self.nicknameLabel.text = transferLoginModel.nick;
    } else {    // 没有登录
        self.iconImageView.image = [GWTool imageByComposingImage: [UIImage imageNamed:@"icon_login_avatar"] withMaskImage:[UIImage imageNamed:@"round"]];
        
        self.nicknameLabel.text = @"Login";
    }
}


#pragma mark - 登录方法
-(void)loginManager{
    // 1. 头像
    [self.iconImageView uploadImageWithURL:[AccountModel sharedAccountModel].avatar  placeholder:nil callback:NULL];
    // 2. 名字
    self.nicknameLabel.text = [AccountModel sharedAccountModel].nick;
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}
@end
