//
//  GWPlayerAblumListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/31.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWPlayerAblumSingleModel.h"

@interface GWPlayerAblumListModel : FetchModel

@property (nonatomic,strong)NSArray<GWPlayerAblumSingleModel> *ablumList;

@end
