//
//  GWPlayerAblumSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/31.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol  GWPlayerAblumSingleModel<NSObject>

@end

@interface GWPlayerAblumSingleModel : FetchModel

@property (nonatomic,copy)NSString *ablumId;            /**< 专辑编号*/
@property (nonatomic,assign)NSInteger count;            /**< 数量*/
@property (nonatomic,copy)NSString *desc;               /**< 详情*/
@property (nonatomic,copy)NSString *img;                /**< 图片*/
@property (nonatomic,copy)NSString *name;               /**< 名称*/
@property (nonatomic,strong)NSArray *tags;              /**< tags*/
@property (nonatomic,assign)NSTimeInterval update_time;  /**< 修改时间*/

@end
