//
//  GWPlayerAblumSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/31.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWPlayerAblumSingleModel.h"

@implementation GWPlayerAblumSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc":@"description",@"ablumId":@"id"};
}

@end
