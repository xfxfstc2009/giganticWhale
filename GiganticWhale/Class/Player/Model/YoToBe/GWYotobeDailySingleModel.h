//
//  GWYotobeDailySingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWYotobeVideoListModel.h"

@protocol GWYotobeDailySingleModel <NSObject>

@end

@interface GWYotobeDailySingleModel : FetchModel

@property (nonatomic,assign)NSInteger total;                    /**< 总数*/
@property (nonatomic,assign)NSTimeInterval date;                /**< 时间*/
@property (nonatomic,strong)NSArray<GWYotobeVideoSigleModel> *videoList;       /**< 视频列表*/

@end
