//
//  GWYotobeDailyListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/15.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWYotobeDailySingleModel.h"

@interface GWYotobeDailyListModel : FetchModel

@property (nonatomic,strong)NSArray<GWYotobeDailySingleModel> *dailyList;           /**< 数组*/
@property (nonatomic,copy)NSString *newestIssueType;
@property (nonatomic,assign)NSTimeInterval nextPublishTime;
@property (nonatomic,copy)NSString *nextPageUrl;

@end
