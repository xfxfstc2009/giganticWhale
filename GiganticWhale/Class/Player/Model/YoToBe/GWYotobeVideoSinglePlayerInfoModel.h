//
//  GWYotobeVideoSinglePlayerInfoModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWYotobeVideoSinglePlayerInfoModel <NSObject>

@end

@interface GWYotobeVideoSinglePlayerInfoModel : FetchModel

@property (nonatomic,assign)CGFloat height;             /**< 高度*/
@property (nonatomic,copy)NSString *name;               /**< 名称*/
@property (nonatomic,copy)NSString *type;               /**< 视频清晰度*/
@property (nonatomic,copy)NSString *url;                /**< 视频url*/
@property (nonatomic,assign)CGFloat width;              /**< 宽度*/


@end
