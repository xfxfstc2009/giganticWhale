//
//  GWYotobeVideoSigleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWYotobeVideoSinglePlayerInfoModel.h"
#import "GWYotobeVideoProviderModel.h"                          // 供应商的信息

@protocol GWYotobeVideoSigleModel <NSObject>


@end

@interface GWYotobeVideoSigleModel : FetchModel

@property (nonatomic,copy)NSString *adTrack;                    /**< 广告跟踪*/
@property (nonatomic,copy)NSString *author;                     /**< 作者*/
@property (nonatomic,copy)NSString *campaign;                   /**< 运动*/
@property (nonatomic,copy)NSString *category;                   /**< 分类*/
@property (nonatomic,copy)NSString *coverBlurred;               /**< 模糊图*/
@property (nonatomic,copy)NSString *coverForDetail;             /**< 细节图*/
@property (nonatomic,copy)NSString *coverForFeed;               /**< 图 */
@property (nonatomic,copy)NSString *coverForSharing;            /**< 分享*/

@property (nonatomic,copy)NSString *dataType;                   /**< 数据分类*/
@property (nonatomic,assign)NSTimeInterval date;               /**< 时间*/
@property (nonatomic,copy)NSString *desc;                       /**< 详情*/
@property (nonatomic,assign)NSInteger duration;                 /**< 播放时间*/
@property (nonatomic,strong)NSArray<GWYotobeVideoSinglePlayerInfoModel> *playInfo;      /**< 播放内容*/
@property (nonatomic,copy)NSString *playUrl;                    /**< 播放url*/
@property (nonatomic,copy)NSString *promotion;                  /**< 推广*/
@property (nonatomic,strong)GWYotobeVideoProviderModel *model;  /**< 推广详情*/
@property (nonatomic,copy)NSString *rawWebUrl;                  /**< 原网页的url*/
@property (nonatomic,assign)NSTimeInterval releaseTime;        /**< 真是使用的url*/
@property (nonatomic,copy)NSString *title;                      /**< 标题*/
@property (nonatomic,copy)NSString *type;                       /**< 类型*/
@property (nonatomic,copy)NSString *webUrlForWeibo;             /**< 微博跳转的url*/


@property (nonatomic,assign)BOOL isAnimation;                   /**< 是否动画*/
@end
