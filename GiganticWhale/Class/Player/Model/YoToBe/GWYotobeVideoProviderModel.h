//
//  GWYotobeVideoProviderModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface GWYotobeVideoProviderModel : FetchModel

@property (nonatomic,copy)NSString *alias;                  /**< 别名*/
@property (nonatomic,copy)NSString *icon;                   /**< 图标*/
@property (nonatomic,copy)NSString *name;                   /**< 名称*/


@end
