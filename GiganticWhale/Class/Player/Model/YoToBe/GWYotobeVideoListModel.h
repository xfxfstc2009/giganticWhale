//
//  GWYotobeVideoListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWYotobeVideoSigleModel.h"


@protocol GWYotobeVideoListModel <NSObject>

@end

@interface GWYotobeVideoListModel : FetchModel

@property (nonatomic,strong)NSArray<GWYotobeVideoSigleModel> *videoList;            /**< 视频列表*/

@end
