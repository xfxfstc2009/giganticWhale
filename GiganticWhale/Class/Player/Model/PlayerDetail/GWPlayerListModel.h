//
//  GWPlayerListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWPlayerSingleModel.h"

@interface GWPlayerListModel : FetchModel

@property (nonatomic,strong)NSArray<GWPlayerSingleModel> *infoList;

@end
