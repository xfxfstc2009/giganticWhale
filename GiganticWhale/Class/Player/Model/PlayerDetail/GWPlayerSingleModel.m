//
//  GWPlayerSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWPlayerSingleModel.h"

@implementation GWPlayerSingleModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc": @"description",
             @"playerId":@"id"};
}
@end
