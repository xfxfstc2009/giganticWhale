//
//  GWPlayerSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol GWPlayerSingleModel <NSObject>

@end


@interface GWPlayerSingleModel : FetchModel
@property (nonatomic,assign)NSTimeInterval create_time;                 /**< 创建时间*/
@property (nonatomic,copy)NSString *desc;                               /**< 信息*/
@property (nonatomic,copy)NSString *playerId;                           /**< 编号*/
@property (nonatomic,copy)NSString *name;                               /**< 视频名称*/
@property (nonatomic,copy)NSString *player_bgImg;                       /**< 图片信息*/
@property (nonatomic,copy)NSString *player_url;                         /**< 视频路径*/
@property (nonatomic,assign)NSTimeInterval update_time;                 /**< 创建时间*/

@property (nonatomic,assign)BOOL sign;                                  /**< 是否标记*/

// 是否动画
@property (nonatomic,assign)BOOL isAnimation;
@end

