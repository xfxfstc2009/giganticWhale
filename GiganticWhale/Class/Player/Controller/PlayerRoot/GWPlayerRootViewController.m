//
//  GWPlayerRootViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerRootViewController.h"

#import "GWPlayerLayout.h"                               // layout
#import "GWPlayerCollectionViewCell.h"                   // cell

#import "GWPlayerAblumListModel.h"
#import "GWPlayerDetailViewController.h"                 // 视频详情
#import "GWPlayerRootAddViewController.h"               // 添加视频

#import "GWPlayerDetailYotobeViewController.h"

static NSString *playerSlidingCellIdentifier = @"playerSlidingCellIdentifier";

@interface GWPlayerRootViewController()<UICollectionViewDataSource,UICollectionViewDelegate,GWPlayerLayoutDelegate>
@property (nonatomic,strong)UICollectionView *playerRootCollectionView;     /**< 播放器列表首页*/
@property (nonatomic,strong)NSMutableArray *mainMutableArr;
@end

@implementation GWPlayerRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
//    [self createCustomerNavView];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的视频";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWPlayerDetailViewController *playerDetailViewController = [[GWPlayerDetailViewController alloc]init];
//        playerDetailViewController.transferPlayerSingleModel = [self.mainMutableArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:playerDetailViewController animated:YES];
    }];
}


#pragma mark - 
-(void)arrayWithInit{
    self.mainMutableArr = [NSMutableArray array];
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if (!self.playerRootCollectionView){
        GWPlayerLayout *flowLayout= [[GWPlayerLayout alloc]initWithDelegate:self];
        self.playerRootCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.playerRootCollectionView.backgroundColor = [UIColor clearColor];
        self.playerRootCollectionView.showsVerticalScrollIndicator = NO;
        self.playerRootCollectionView.showsHorizontalScrollIndicator = NO;
        self.playerRootCollectionView.delegate = self;
        self.playerRootCollectionView.dataSource = self;
        self.playerRootCollectionView.scrollsToTop = YES;
        self.playerRootCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.playerRootCollectionView];
        
        [self.playerRootCollectionView registerClass:[GWPlayerCollectionViewCell class] forCellWithReuseIdentifier:playerSlidingCellIdentifier];
        
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
}

#pragma mark - LayoutDelegate
-(CGFloat)heightForCellWithHlt{
    return 240.;
}

-(CGFloat)heightForCellWithNor{
    return 180.;
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.mainMutableArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GWPlayerCollectionViewCell *playerCell = [collectionView dequeueReusableCellWithReuseIdentifier:playerSlidingCellIdentifier forIndexPath:indexPath];
    
    GWPlayerAblumSingleModel *playerAblumSingleModel = [self.mainMutableArr objectAtIndex:indexPath.row];
    playerCell.transferPlayerAblumSingleModel = playerAblumSingleModel;
    
    return playerCell;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GWPlayerAblumSingleModel *playerAblumSingleModel = [self.mainMutableArr objectAtIndex:indexPath.row];
    if ([playerAblumSingleModel.desc hasPrefix:@"Yotobe"]){
        GWPlayerDetailYotobeViewController *playerDetailYotobeViewController = [[GWPlayerDetailYotobeViewController alloc]init];
        [self.navigationController pushViewController:playerDetailYotobeViewController animated:YES];
    } else {
        GWPlayerDetailViewController *playerDetailViewController = [[GWPlayerDetailViewController alloc]init];
        playerDetailViewController.transferPlayerSingleModel = [self.mainMutableArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:playerDetailViewController animated:YES];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:player_ablum requestParams:@{@"page":@"1",@"pageSize":@"5"} responseObjectClass:[GWPlayerAblumListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWPlayerAblumListModel *ablumListModel = (GWPlayerAblumListModel *)responseObject;
            [strongSelf.mainMutableArr addObjectsFromArray:ablumListModel.ablumList];
            [strongSelf.playerRootCollectionView reloadData];
        }
    }];
}

@end
