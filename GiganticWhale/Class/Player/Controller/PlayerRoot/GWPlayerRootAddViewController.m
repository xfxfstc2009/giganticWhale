//
//  GWPlayerRootAddViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerRootAddViewController.h"

@interface GWPlayerRootAddViewController()<UITableViewDelegate,UITableViewDataSource,GWSelectedImgTableViewCellDelegate>
@property (nonatomic,strong)UITableView *playerRootTableView;
@property (nonatomic,strong)NSMutableArray *playerRootMutableArr;
@property (nonatomic,strong)NSMutableArray *selectedImgMutableArr;
@property (nonatomic,strong)NSMutableArray *tagsMutableArr;

@end

@implementation GWPlayerRootAddViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加视频专辑";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 添加
    }];
}

-(void)arrayWithInit{
    self.playerRootMutableArr = [NSMutableArray array];
    [self.playerRootMutableArr addObjectsFromArray:@[@[@"专辑名称",@"专辑简介"],@[@"专辑图片"]]];
    self.selectedImgMutableArr = [NSMutableArray array];
    self.tagsMutableArr = [NSMutableArray array];
    [self.playerRootMutableArr addObject:self.tagsMutableArr];
}

-(void)createTableView{
    if (!self.playerRootTableView){
        self.playerRootTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.playerRootTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.playerRootTableView.delegate = self;
        self.playerRootTableView.dataSource = self;
        self.playerRootTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.playerRootTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.playerRootTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.playerRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.playerRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"专辑名称"] && indexPath.row == [self cellIndexPathRowWithcellData:@"专辑名称"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowOne.transfrCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"专辑名称";
        cellWithRowOne.transferPlaceholeder = @"请输入专辑名称";
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"专辑简介"] && indexPath.row == [self cellIndexPathRowWithcellData:@"专辑简介"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transfrCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"专辑简介";
        cellWithRowTwo.transferPlaceholeder = @"请输入专辑简介";
        return cellWithRowTwo;
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"专辑图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"专辑图片"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWSelectedImgTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWSelectedImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.delegate = self;
        cellWithRowFour.transferSelectedImgArr = self.selectedImgMutableArr;
        return cellWithRowFour;
    } else {            // 专辑标签
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        UITableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(15);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    // 创建label
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - LCFloat(11) * 2, LCFloat(15));
    [headerView addSubview:fixedLabel];

    if (section == [self cellIndexPathSectionWithcellData:@"专辑名称"]){
        fixedLabel.text = @"专辑信息";
    } else if (section == [self cellIndexPathSectionWithcellData:@"专辑标签"]){
        fixedLabel.text = @"标签信息";
    } else if (section == [self cellIndexPathSectionWithcellData:@"专辑图片"]){
        fixedLabel.text= @"专辑图片";
    }
    
    return headerView;
}


#pragma mark - IMGSelect Delegate
-(void)imageSelectedButtonClick:(GWSelectedImgTableViewCell *)cell{ // 添加图片
    GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 - self.selectedImgMutableArr.count andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.selectedImgMutableArr addObjectsFromArray:selectedImgArr];
        // 刷新行
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"专辑图片"] inSection:[self cellIndexPathSectionWithcellData:@"专辑图片"]];
        [strongSelf.playerRootTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
    [self.navigationController pushViewController:assetLibraryViewController animated:YES];
}

-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(GWSelectedImgTableViewCell *)cell{
    __weak typeof(self)weakSelf = self;
    [[UIActionSheet actionSheetWithTitle:@"图片操作" buttonTitles:@[@"删除图片"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.selectedImgMutableArr removeObjectAtIndex:imgIndex];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"专辑图片"] inSection:[self cellIndexPathSectionWithcellData:@"专辑图片"]];
        [strongSelf.playerRootTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }]showInView:self.view];;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"专辑图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"专辑图片"]){
        return [GWSelectedImgTableViewCell calculationCellHeightWithImgArr:self.selectedImgMutableArr];
    } else {
        return 44;
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.playerRootTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.playerRootMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.playerRootMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.playerRootMutableArr.count ; i++){
        NSArray *dataTempArr = [self.playerRootMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.playerRootMutableArr.count ; i++){
        NSArray *dataTempArr = [self.playerRootMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}


@end
