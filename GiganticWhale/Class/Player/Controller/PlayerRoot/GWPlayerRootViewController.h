//
//  GWPlayerRootViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 【播放器首页】
#import "AbstractViewController.h"

@interface GWPlayerRootViewController : AbstractViewController

@end
