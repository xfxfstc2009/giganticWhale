//
//  GWPlayerDetailAddViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerDetailAddViewController.h"

@interface GWPlayerDetailAddViewController()<UITableViewDataSource,UITableViewDelegate>{
    NSString *playerName;           /**<视频名称*/
    NSString *playerDesc;           /**<视频详情*/
    NSString *playerSinceUrl;       /**<视频源*/
    NSString *playerUrl;            /**<视频播放地址*/
    NSString *playerImg;            /**<视频图片*/
}
@property (nonatomic,strong)UITableView *playerDetailTableView;
@property (nonatomic,strong)NSMutableArray *playerDetailArr;

@end

@implementation GWPlayerDetailAddViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加视频";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    NSArray *tempArr = @[@[@"视频名称",@"视频详情"],@[@"视频源地址",@"视频播放地址"],@[@"添加按钮"]];
    self.playerDetailArr = [NSMutableArray arrayWithArray:tempArr];
}

-(void)createTableView{
    if (!self.playerDetailTableView){
        self.playerDetailTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.playerDetailTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.playerDetailTableView.delegate = self;
        self.playerDetailTableView.dataSource = self;
        self.playerDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.playerDetailTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.playerDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.playerDetailArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.playerDetailArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"视频名称"] && indexPath.row == [self cellIndexPathRowWithcellData:@"视频名称"]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"视频详情"] && indexPath.row == [self cellIndexPathRowWithcellData:@"视频详情"])){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        __weak typeof(self)weakSelf = self;
        cellWithRowOne.transfrCellHeight = cellHeight;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"视频名称"]){
            cellWithRowOne.transferTitle = @"视频名称";
            cellWithRowOne.transferPlaceholeder = @"请输入视频名称";
            cellWithRowOne.transferInputText = playerName;
            [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf ->playerName = info;
            }];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"视频详情"]){
            cellWithRowOne.transferTitle = @"视频详情";
            cellWithRowOne.transferPlaceholeder = @"请输入视频详情";
            cellWithRowOne.inputTextField.text = playerDesc;
            [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf ->playerDesc = info;
            }];
        }
       return cellWithRowOne;

    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"视频源地址"] && indexPath.row == [self cellIndexPathRowWithcellData:@"视频源地址"]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"视频播放地址"] && indexPath.row == [self cellIndexPathRowWithcellData:@"视频播放地址"])){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowTwo.transfrCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        if(indexPath.row == [self cellIndexPathRowWithcellData:@"视频源地址"]){
            cellWithRowTwo.transferTitle = @"视频源地址";
            cellWithRowTwo.transferPlaceholeder = @"请输入视频源地址";
            cellWithRowTwo.inputTextField.text = playerSinceUrl;
            [cellWithRowTwo textFieldDidChangeBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf ->playerSinceUrl = info;
            }];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"视频播放地址"]){
            cellWithRowTwo.transferTitle = @"视频播放地址";
            cellWithRowTwo.transferPlaceholeder = @"请输入视频播放地址";
            cellWithRowTwo.inputTextField.text = playerUrl;
            [cellWithRowTwo textFieldDidChangeBlock:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf ->playerUrl = info;
            }];
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"添加按钮"] && indexPath.row == [self cellIndexPathRowWithcellData:@"添加按钮"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"添加视频";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf verificationWithUploadInfoManager];
 
        }];
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(15);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.playerDetailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.playerDetailArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.playerDetailArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.playerDetailArr.count ; i++){
        NSArray *dataTempArr = [self.playerDetailArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.playerDetailArr.count ; i++){
        NSArray *dataTempArr = [self.playerDetailArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark 接口
-(void)verificationWithUploadInfoManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToUploaderPlayerImgWithBlock:^(NSArray *fileUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToInfoAddImgUrl:[fileUrlArr firstObject]];
    }];
}

-(void)sendRequestToInfoAddImgUrl:(NSString *)imgUrl{
    NSDictionary *params = @{@"name":playerName,@"desc":playerDesc,@"playerSinceUrl":playerSinceUrl,@"playerUrl":playerUrl,@"img":imgUrl,@"ablumId":self.transferAblumId};
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:player_detail_add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"上传成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


#pragma mark - 获取当前视频图片
-(void)sendRequestToUploaderPlayerImgWithBlock:(void(^)(NSArray *fileUrlArr))block{
    // 2. 上传
    OSSSingleFileModel *fileModel = [[OSSSingleFileModel alloc]init];
    fileModel.objcImg = [GWTool getPlayerThumbnailImageWithURL:playerUrl];
    NSMutableArray *fileMutableArr = [NSMutableArray array];
    [fileMutableArr addObject:fileModel];
    NSArray<OSSSingleFileModel> *imgArr = [fileMutableArr copy];
    __weak typeof(self)weakSelf = self;
    [AliOSSManager uploadFileWithImgArr:imgArr uploadType:OSSUploadTypePlayerInfo compressionRatio:1. block:^(NSArray *fileUrlArr) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(fileUrlArr);
        }
    }];
}

@end
