//
//  GWPlayerDetailYotobeViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerDetailYotobeViewController.h"
#import "ZFPlayerView.h"
#import "GWPlayerListTableViewCell.h"
#import "GWYotobeDailyListModel.h"

@interface GWPlayerDetailYotobeViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *playerListMutableArr;       /**< 数组对象*/
@property (nonatomic,strong)UITableView *playerTableView;
@property (nonatomic,strong) ZFPlayerView *playerView;
@property (nonatomic,assign)NSInteger page;

@end

@implementation GWPlayerDetailYotobeViewController

-(void)dealloc{
    NSLog(@"释放");
}

// 页面消失时候
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.playerView resetPlayer];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        self.view.backgroundColor = [UIColor whiteColor];
    }else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.view.backgroundColor = [UIColor blackColor];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToGetYotobePlayListInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"Yotobe 精选";
}

-(void)arrayWithInit{
    self.playerListMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.playerTableView){
        self.playerTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.playerTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.playerTableView.delegate = self;
        self.playerTableView.dataSource = self;
        self.playerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.playerTableView.backgroundColor = [UIColor clearColor];
        self.playerTableView.backgroundView = nil;
        self.playerTableView.bounces = YES;
        self.playerTableView.opaque = NO;
        [self.view addSubview:self.playerTableView];
    }
//    // 1. 增加下啦刷新
//    __weak typeof(self)weakSelf = self;
//    [self.playerTableView addQSPullToRefreshWithActionHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendReqeustToGetYotobePlayListInfo];
//    }];
//    
//    [self.playerTableView addQSInfiniteScrollingWithActionHandler:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendReqeustToGetYotobePlayListInfo];
//    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.playerListMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:section];
    return yotobeSingleModel.total;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWPlayerListTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWPlayerListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.section];
    GWYotobeVideoSigleModel *yotobeVideoSingleModel = [yotobeSingleModel.videoList objectAtIndex:indexPath.row];
    
    cellWithRowOne.transferIndexPath = indexPath;
    cellWithRowOne.transferYotobeSingleModel = yotobeVideoSingleModel;
    
    __block NSIndexPath *weakIndexPath = indexPath;
    __block GWPlayerListTableViewCell *weakCell = cellWithRowOne;
    __weak typeof(self) weakSelf = self;
    [cellWithRowOne yotobePlayerViewClickManagerWithBlock:^(GWYotobeVideoSigleModel *transferPlayerSingleModel) {
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        weakSelf.playerView = [ZFPlayerView sharedPlayerView];
        NSURL *videoURL = [NSURL URLWithString:yotobeVideoSingleModel.playUrl];
        [weakSelf.playerView setVideoURL:videoURL withTableView:strongSelf.playerTableView AtIndexPath:weakIndexPath withImageViewTag:102];
        [weakSelf.playerView addPlayerToCellImageView:weakCell.playerImageView];
        
        weakSelf.playerView.playerLayerGravity = ZFPlayerLayerGravityResizeAspectFill;
    }];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.section];
    GWYotobeVideoSigleModel *yotobeVideoSingleModel = [yotobeSingleModel.videoList objectAtIndex:indexPath.row];
    
    return [GWPlayerListTableViewCell calculationCellHeightWithYotobePlayerSingleModel:yotobeVideoSingleModel];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.section];
    GWYotobeVideoSigleModel *yotobeVideoSingleModel = [yotobeSingleModel.videoList objectAtIndex:indexPath.row];
    WebViewController *webViewController = [[WebViewController alloc]init];
    [webViewController webViewControllerWithAddress:yotobeVideoSingleModel.rawWebUrl];
    [self.navigationController pushViewController:webViewController animated:YES];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;
    
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.section];
    GWYotobeVideoSigleModel *yotobeVideoSingleModel = [yotobeSingleModel.videoList objectAtIndex:indexPath.row];
    
    GWPlayerListTableViewCell *cardCell = (GWPlayerListTableViewCell *)cell;
    if (yotobeVideoSingleModel.isAnimation == NO){
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
        yotobeVideoSingleModel.isAnimation = YES;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    GWYotobeDailySingleModel *yotobeSingleModel = [self.playerListMutableArr objectAtIndex:section];
    
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, LCFloat(20));
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"蓝"];
    fixedLabel.text = [NSDate getTimeGap:(yotobeSingleModel.date / 1000)];
    [headerView addSubview:fixedLabel];
    return headerView;
}

#pragma mark - 接口
-(void)sendReqeustToGetYotobePlayListInfo{
    __weak typeof(self)weakSelf = self;
    NSString *dateTime = [NSDate getCurrentTimeWithYotobe];
    [[NetworkAdapter sharedAdapter] fetchWithPath:player_yotobe_detail requestParams:@{@"num":@"5",@"date":dateTime} responseObjectClass:[GWYotobeDailyListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWYotobeDailyListModel *yotobeDetailListModel = (GWYotobeDailyListModel *)responseObject;
            [strongSelf.playerListMutableArr addObjectsFromArray:yotobeDetailListModel.dailyList];
            [strongSelf.playerTableView reloadData];
        } else {
            
        }
    }];
}

@end
