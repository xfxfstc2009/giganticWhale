//
//  GWPlayerDetailAddViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"

@interface GWPlayerDetailAddViewController : AbstractViewController
@property (nonatomic,copy)NSString *transferAblumId;
@end
