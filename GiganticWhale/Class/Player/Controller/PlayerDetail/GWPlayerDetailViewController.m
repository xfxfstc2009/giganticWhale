//
//  GWPlayerDetailViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/13.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerDetailViewController.h"
#import "ZFPlayerView.h"
#import "GWPlayerListModel.h"                   // model
#import "GWPlayerListTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import "GWPlayerDetailAddViewController.h"         // 播放详情

@interface GWPlayerDetailViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *playerListMutableArr;       /**< 数组对象*/
@property (nonatomic,strong)UITableView *playerTableView;
@property (nonatomic,strong) ZFPlayerView *playerView;
@property (nonatomic,assign)NSInteger page;

@end

@implementation GWPlayerDetailViewController


-(void)dealloc{
    NSLog(@"释放");
}

// 页面消失时候
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.playerView resetPlayer];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        self.view.backgroundColor = [UIColor whiteColor];
    }else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.view.backgroundColor = [UIColor blackColor];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
//    [weakSelf sendReqeustToGetPlayListInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    // 创建title
    self.barMainTitle = self.transferPlayerSingleModel.name.length?self.transferPlayerSingleModel.name:@"";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWPlayerDetailAddViewController *playerDetailAddVC = [[GWPlayerDetailAddViewController alloc]init];
        playerDetailAddVC.transferAblumId = self.transferPlayerSingleModel.ablumId;
        [strongSelf.navigationController pushViewController:playerDetailAddVC animated:YES];
    }];
}

-(void)arrayWithInit{
    self.playerListMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < 10 ; i++){
        GWPlayerSingleModel *singleModel = [[GWPlayerSingleModel alloc]init];
        singleModel.create_time = 123123123;
        singleModel.desc = @"123";
        singleModel.playerId = @"123";
        singleModel.name = @"123123";
        singleModel.player_bgImg = @"123";
        [self.playerListMutableArr addObject:singleModel];
//        @property (nonatomic,assign)NSTimeInterval create_time;                 /**< 创建时间*/
//        @property (nonatomic,copy)NSString *desc;                               /**< 信息*/
//        @property (nonatomic,copy)NSString *playerId;                           /**< 编号*/
//        @property (nonatomic,copy)NSString *name;                               /**< 视频名称*/
//        @property (nonatomic,copy)NSString *player_bgImg;                       /**< 图片信息*/
//        @property (nonatomic,copy)NSString *player_url;                         /**< 视频路径*/
//        @property (nonatomic,assign)NSTimeInterval update_time;                 /**< 创建时间*/
//
//        @property (nonatomic,assign)BOOL sign;                                  /**< 是否标记*/
//        
//        // 是否动画
//        @property (nonatomic,assign)BOOL isAnimation;
//        @end
    }

}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.playerTableView){
        self.playerTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.playerTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.playerTableView.delegate = self;
        self.playerTableView.dataSource = self;
        self.playerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.playerTableView.backgroundColor = [UIColor clearColor];
        self.playerTableView.backgroundView = nil;
        self.playerTableView.bounces = YES;
        self.playerTableView.opaque = NO;
        [self.view addSubview:self.playerTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.playerListMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWPlayerListTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWPlayerListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    GWPlayerSingleModel *playerSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferIndexPath = indexPath;
    cellWithRowOne.transferPlayerSingleModel = playerSingleModel;
    
    __block NSIndexPath *weakIndexPath = indexPath;
    __block GWPlayerListTableViewCell *weakCell = cellWithRowOne;
    __weak typeof(self) weakSelf = self;
    [cellWithRowOne playerViewClickManagerWithBlock:^(GWPlayerSingleModel *transferPlayerSingleModel) {
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        weakSelf.playerView = [ZFPlayerView sharedPlayerView];
        NSURL *videoURL = [NSURL URLWithString:@"http://xiongda.imwork.net/upload/2018/8/e8549773-35c2-4e0b-b7cf-5da934be0cd8.wma"];
//        [weakSelf.playerView setVideoURL: withTableView:strongSelf.playerTableView AtIndexPath:weakIndexPath withImageViewTag:102];
        [weakSelf.playerView addPlayerToCellImageView:weakCell.playerImageView];
        weakSelf.playerView.playerLayerGravity = ZFPlayerLayerGravityResizeAspectFill;
    }];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    GWPlayerSingleModel *playerSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.row];
    return [GWPlayerListTableViewCell calculationCellHeightWithPlayerSingleModel:playerSingleModel];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;

    GWPlayerSingleModel *playerSingleModel = [self.playerListMutableArr objectAtIndex:indexPath.row];
    GWPlayerListTableViewCell *cardCell = (GWPlayerListTableViewCell *)cell;
    if (playerSingleModel.isAnimation == NO){
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
        playerSingleModel.isAnimation = YES;
    }
}

#pragma mark - 接口
// 获取当前视频数据
-(void)sendReqeustToGetPlayListInfo{
    self.page = 1;
   NSDictionary *requestParams = @{@"size":@"5",@"page":@(self.page),@"ablumId":self.transferPlayerSingleModel.ablumId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:player_detail_list requestParams:requestParams responseObjectClass:[GWPlayerListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWPlayerListModel *playerListModel = (GWPlayerListModel *)responseObject;
            [strongSelf.playerListMutableArr addObjectsFromArray:playerListModel.infoList];
            [strongSelf.playerTableView reloadData];
        }
    }];
}




@end
