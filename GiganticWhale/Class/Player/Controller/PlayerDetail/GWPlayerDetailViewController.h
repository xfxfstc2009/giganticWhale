//
//  GWPlayerDetailViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/13.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWPlayerAblumSingleModel.h"            // 上个页面传递的

@interface GWPlayerDetailViewController : AbstractViewController

@property (nonatomic,strong)GWPlayerAblumSingleModel *transferPlayerSingleModel;     /**< 上个页面传递过来的model*/

@end
