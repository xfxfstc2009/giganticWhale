//
//  GWPlayerCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerCollectionViewCell.h"

const CGFloat RPSlidingCellDetailTextPadding = 20.0f;
const CGFloat RPSlidingMenuNormalImageCoverAlpha = 0.5f;
const CGFloat RPSlidingMenuFeaturedImageCoverAlpha = 0.2f;
@interface GWPlayerCollectionViewCell()

@property (nonatomic,strong)GWImageView *backingView;       /**< 背景图片*/
@property (strong, nonatomic) UIView *imageCover;           /**< 遮罩层*/
@property (nonatomic,strong)UILabel *titleLabel;            /**< 标题label*/
@property (nonatomic,strong)UILabel *descLabel;             /**< 详情label*/
@property (nonatomic,strong)UILabel *numberOfLabel;         /**< 数字label*/
@property (nonatomic,strong)UILabel *updateLabel;           /**< 创建时间*/
@property (nonatomic,strong)UILabel *tagLabel;

@end


@implementation GWPlayerCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        [self createView];
    }
    
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 背景
    self.backingView = [[GWImageView alloc]init];
    self.backingView.backgroundColor = [UIColor clearColor];
    self.backingView.center = self.contentView.center;
    self.backingView.clipsToBounds = YES;
    self.backingView.frame = CGRectMake(0.0f, 0.0f, kScreenBounds.size.width, playerCellHeight_Hlt);
    self.backingView.contentMode = UIViewContentModeScaleAspectFill;
    self.backingView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.contentView addSubview:self.backingView];
    
    // 2. 遮罩
    self.imageCover = [[UIView alloc] initWithFrame:self.backingView.frame];
    self.imageCover.backgroundColor = [UIColor blackColor];
    self.imageCover.alpha = 0.6f;
    self.imageCover.autoresizingMask = self.backingView.autoresizingMask;
    [self.backingView addSubview:self.imageCover];
    [self.contentView insertSubview:self.backingView atIndex:0];
    [self.contentView insertSubview:self.imageCover atIndex:1];
    
    // 3.创建文字
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.center = self.contentView.center;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:32.0];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.titleLabel];
    
    // 4. 其他
//    CGFloat startY = self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height - 40.0f;
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.descLabel.numberOfLines = 0;
    self.descLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    self.descLabel.textColor = [UIColor whiteColor];
    self.descLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.descLabel];
    
    // 5. 创建数量
    self.numberOfLabel = [[UILabel alloc]init];
    self.numberOfLabel.backgroundColor = [UIColor clearColor];
    self.numberOfLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.numberOfLabel.textColor = [UIColor whiteColor];
    self.numberOfLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.numberOfLabel];
    
    // 6. 创建创建时间
    self.updateLabel = [[UILabel alloc]init];
    self.updateLabel.backgroundColor = [UIColor clearColor];
    self.updateLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.updateLabel.textColor = [UIColor whiteColor];
    self.updateLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.updateLabel];
    
    // 7.创建标签
    self.tagLabel = [[UILabel alloc]init];
    self.tagLabel.backgroundColor = [UIColor clearColor];
    self.tagLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.tagLabel.textColor = [UIColor whiteColor];
    self.tagLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.tagLabel];
}

-(void)setTransferPlayerAblumSingleModel:(GWPlayerAblumSingleModel *)transferPlayerAblumSingleModel{
    // 图片
    [self.backingView uploadPlayerListImageWithURL:transferPlayerAblumSingleModel.img placeholder:nil callback:NULL];
   
    // 播放类别
    self.titleLabel.text = transferPlayerAblumSingleModel.name;
    self.titleLabel.frame = CGRectMake(0, (self.contentView.size_height - [NSString contentofHeight:self.titleLabel.font]) / 2. - LCFloat(20), kScreenWidth, [NSString contentofHeight:self.titleLabel.font]);

    // 数量
    self.numberOfLabel.text = [NSString stringWithFormat:@"%li",(long)transferPlayerAblumSingleModel.count];        // 数量
    self.numberOfLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), self.titleLabel.size_width, [NSString contentofHeight:self.numberOfLabel.font]);
    
    // desc
    self.descLabel.text = transferPlayerAblumSingleModel.desc;
    CGSize contentOfDesc = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - (RPSlidingCellDetailTextPadding * 2), CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(RPSlidingCellDetailTextPadding, CGRectGetMaxY(self.numberOfLabel.frame) + LCFloat(10), kScreenWidth - 2 * RPSlidingCellDetailTextPadding, contentOfDesc.height);
    
    // 创建时间
    self.updateLabel.text = [NSDate getTimeGap:transferPlayerAblumSingleModel.update_time];
    CGSize updateSize = [self.updateLabel.text sizeWithCalcFont:self.updateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.updateLabel.font])];
    self.updateLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - updateSize.width, self.backingView.size_height - LCFloat(11) - [NSString contentofHeight:self.updateLabel.font], updateSize.width, [NSString contentofHeight:self.updateLabel.font]);
    
    // 创建标签
    NSString *tagsString = @"";
    if (transferPlayerAblumSingleModel.tags.count){
        tagsString = [tagsString stringByAppendingString:@"【"];
        for (int i = 0 ; i < transferPlayerAblumSingleModel.tags.count;i++){
            NSString *tag = [transferPlayerAblumSingleModel.tags objectAtIndex:i];
            tagsString = [tagsString stringByAppendingString:tag];
            if (i != transferPlayerAblumSingleModel.tags.count - 1){
                tagsString = [tagsString stringByAppendingString:@","];
            } else {
                tagsString = [tagsString stringByAppendingString:@"】"];
            }
        }
    }
    self.tagLabel.text = tagsString;
    
    CGSize tagsSize = [self.tagLabel.text sizeWithCalcFont:self.tagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.tagLabel.font])];
    self.tagLabel.frame = CGRectMake(LCFloat(11), self.backingView.size_height - LCFloat(11) - [NSString contentofHeight:self.tagLabel.font], tagsSize.width, [NSString contentofHeight:self.tagLabel.font]);
}


- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    
    [super applyLayoutAttributes:layoutAttributes];
    
    CGFloat featureNormaHeightDifference = playerCellHeight_Hlt - playerCellHeight_Nor;
    
    CGFloat amountGrown = playerCellHeight_Hlt - self.frame.size.height;
    
    CGFloat percentOfGrowth = 1 - (amountGrown / featureNormaHeightDifference);
    
    percentOfGrowth = sin(percentOfGrowth * M_PI_2);
    
    CGFloat scaleAndAlpha = MAX(percentOfGrowth, 0.5f);
    
    self.titleLabel.transform = CGAffineTransformMakeScale(scaleAndAlpha, scaleAndAlpha);
    self.titleLabel.center = CGPointMake(self.contentView.center_x, self.contentView.center_y - LCFloat(20));
    
    self.numberOfLabel.center_y = self.titleLabel.center_y + [NSString contentofHeight:self.titleLabel.font] / 2. + LCFloat(15);
    
    self.descLabel.orgin_y = CGRectGetMaxY(self.numberOfLabel.frame) + LCFloat(10);
    
    self.descLabel.alpha = percentOfGrowth;
    
    self.imageCover.alpha = RPSlidingMenuNormalImageCoverAlpha - (percentOfGrowth * (RPSlidingMenuNormalImageCoverAlpha - RPSlidingMenuFeaturedImageCoverAlpha));
}

@end
