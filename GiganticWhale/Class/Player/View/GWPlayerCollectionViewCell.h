//
//  GWPlayerCollectionViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWPlayerLayout.h"
#import "GWPlayerAblumSingleModel.h"

@class GWPlayerLayout;
@interface GWPlayerCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)GWPlayerAblumSingleModel *transferPlayerAblumSingleModel;

@end
