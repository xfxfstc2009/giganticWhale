//
//  GWPlayerLayout.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPlayerLayout.h"

const CGFloat RPSlidingCellDragInterval = 180.0f;

@interface GWPlayerLayout()
@property (strong, nonatomic) NSDictionary *layoutAttributes;
@end

@implementation GWPlayerLayout

-(instancetype)initWithDelegate:(id<GWPlayerLayoutDelegate>)delegate{
    self = [super init];
    if (self){
        _delegate = delegate;
    }
    
    return self;
}

- (void)prepareLayout {
    
    [super prepareLayout];
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    NSInteger topFeatureIndex = [self currentCellIndex];
    
    CGFloat topCellsInterpolation =  [self currentCellIndex] - topFeatureIndex;
    
    NSMutableDictionary *layoutAttributes = [NSMutableDictionary dictionary];
    NSIndexPath *indexPath;
    

    CGRect lastRect = CGRectMake(0.0f, 0.0f, screenWidth, playerCellHeight_Nor);
    NSInteger numItems = [self.collectionView numberOfItemsInSection:0];
    
    CGFloat featureHeight = [self featureHeight];
    CGFloat normalHeight = [self collapsedHeight];
    
    for (NSInteger itemIndex = 0; itemIndex < numItems; itemIndex++) {
        indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:0];
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        attributes.zIndex = itemIndex;
        NSInteger yValue = 0.0f;
        
        if (indexPath.row == topFeatureIndex) {
        
            CGFloat yOffset = normalHeight  *topCellsInterpolation;
            yValue = self.collectionView.contentOffset.y - yOffset;
            attributes.frame = CGRectMake(0.0f, yValue , screenWidth, featureHeight);
        } else if (indexPath.row == (topFeatureIndex + 1) && indexPath.row != numItems) {
        
            yValue = lastRect.origin.y + lastRect.size.height;
            CGFloat bottomYValue = yValue + normalHeight;
            CGFloat amountToGrow = MAX((featureHeight - normalHeight) *topCellsInterpolation, 0);
            NSInteger newHeight = normalHeight + amountToGrow;
            attributes.frame = CGRectMake(0.0f, bottomYValue - newHeight, screenWidth, newHeight);
        } else {
            
            yValue = lastRect.origin.y + lastRect.size.height;
            attributes.frame = CGRectMake(0.0f, yValue, screenWidth, normalHeight);
        }
        
        lastRect = attributes.frame;
        [layoutAttributes setObject:attributes forKey:indexPath];
    }
    
    self.layoutAttributes = layoutAttributes;
}

- (CGFloat)currentCellIndex {
    return (self.collectionView.contentOffset.y / RPSlidingCellDragInterval);
}

- (CGFloat)featureHeight{
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightForCellWithHlt)]){
        return [self.delegate heightForCellWithHlt];
    }else{
        return playerCellHeight_Hlt;
    }
}

- (CGFloat)collapsedHeight{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(heightForCellWithNor)]){
        return [self.delegate heightForCellWithNor];
    }else{
        return playerCellHeight_Nor;
    }
}


- (CGSize)collectionViewContentSize {
    
    NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:0];
    CGFloat height = (numberOfItems * RPSlidingCellDragInterval) + (self.collectionView.frame.size.height - RPSlidingCellDragInterval);
    return CGSizeMake(self.collectionView.frame.size.width, height);
    
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *attributesInRect =  [NSMutableArray array];
    for (UICollectionViewLayoutAttributes *attributes in [self.layoutAttributes allValues]) {
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [attributesInRect addObject:attributes];
        }
    }
    
    return attributesInRect;
}


- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    
    CGFloat proposedPageIndex = roundf(proposedContentOffset.y / RPSlidingCellDragInterval);
    CGFloat nearestPageOffset = proposedPageIndex * RPSlidingCellDragInterval;
    
    return CGPointMake(0.0f, nearestPageOffset);
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.layoutAttributes[indexPath];
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}


@end
