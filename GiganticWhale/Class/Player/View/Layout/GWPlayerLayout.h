//
//  GWPlayerLayout.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat playerCellHeight_Hlt = 240.0f;
static const CGFloat playerCellHeight_Nor = 88.0f;

@class GWPlayerLayout;
extern const CGFloat RPSlidingCellDragInterval;

@protocol GWPlayerLayoutDelegate <NSObject>

- (CGFloat)heightForCellWithHlt;
- (CGFloat)heightForCellWithNor;

@end

@interface GWPlayerLayout : UICollectionViewLayout

- (instancetype)initWithDelegate:(id<GWPlayerLayoutDelegate>)delegate;
@property (nonatomic, assign) id <GWPlayerLayoutDelegate> delegate;


@end
