//
//  GWPlayerDetailLayout.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/13.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWPlayerDetailLayout : UICollectionViewLayout

@property (nonatomic, assign) CGFloat firstItemTransform;

@end
