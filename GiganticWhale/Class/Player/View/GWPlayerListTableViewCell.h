//
//  GWPlayerListTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/4/4.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWPlayerSingleModel.h"
#import "GWYotobeVideoSigleModel.h"
typedef void(^playerButtonClick)(GWPlayerSingleModel *transferPlayerSingleModel);
typedef void(^yotobePlayerButtonClick)(GWYotobeVideoSigleModel *transferPlayerSingleModel);
@interface GWPlayerListTableViewCell : UITableViewCell


@property (nonatomic,strong)GWPlayerSingleModel *transferPlayerSingleModel;     /**< 传递过去的model*/
@property (nonatomic,strong)GWYotobeVideoSigleModel *transferYotobeSingleModel; /**< 传递过去的Yotobe model*/

@property (nonatomic,strong)GWImageView *playerImageView;
@property (nonatomic,strong)NSIndexPath *transferIndexPath;
// Normal
+(CGFloat)calculationCellHeightWithPlayerSingleModel:(GWPlayerSingleModel *)transferPlayerSingleModel;
-(void)playerViewClickManagerWithBlock:(playerButtonClick)block;
- (void)startAnimationWithDelay:(CGFloat)delayTime ;

-(void)showBlunesManager;

// Yotobe
+(CGFloat)calculationCellHeightWithYotobePlayerSingleModel:(GWYotobeVideoSigleModel *)transferPlayerSingleModel;
-(void)yotobePlayerViewClickManagerWithBlock:(yotobePlayerButtonClick)block;

@end
