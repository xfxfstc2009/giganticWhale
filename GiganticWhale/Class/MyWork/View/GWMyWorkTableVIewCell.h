//
//  GWMyWorkTableVIewCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWMyWorkTableVIewCell : UITableViewCell

@property (nonatomic,copy)NSString *dymicTitle;

+(CGFloat)calculationCellHeight;

-(void)loadContentWithIndex:(NSInteger)index;
@end
