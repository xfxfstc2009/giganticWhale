//
//  GWMyWorkTableVIewCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMyWorkTableVIewCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"
@interface GWMyWorkTableVIewCell()
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *createLabel;
@property (nonatomic,strong)GWImageView *arrowImageView;


@end

@implementation GWMyWorkTableVIewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"副标题"];
    self.nameLabel.text = @"哈哈哈哈";
    self.nameLabel.frame = CGRectMake(LCFloat(11), LCFloat(5), kScreenBounds.size.width, [NSString contentofHeight:self.nameLabel.font]);
    [self addSubview:self.nameLabel];
    
    // 创建时间label
    self.createLabel = [[UILabel alloc]init];
    self.createLabel.text = @"2016-6-6";
    self.createLabel.backgroundColor = [UIColor clearColor];
    self.createLabel.font = [UIFont AvenirLightWithFontSize:8.f];
        self.createLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(5), kScreenBounds.size.width, [NSString contentofHeight:self.nameLabel.font]);
    [self addSubview:self.createLabel];
    
    self.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)setDymicTitle:(NSString *)dymicTitle{
    _dymicTitle = dymicTitle;
}

-(void)loadContentWithIndex:(NSInteger)index{
    if (index % 2) {
        self.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.05f];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
    NSString *fullStirng = [NSString stringWithFormat:@"%02ld. %@", (long)index, self.dymicTitle];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont HeitiSCWithFontSize:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.nameLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.createLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"副标题"]];
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeight:[UIFont AvenirLightWithFontSize:8.f]];
    cellHeight += LCFloat(5);
    return cellHeight;
}

@end
