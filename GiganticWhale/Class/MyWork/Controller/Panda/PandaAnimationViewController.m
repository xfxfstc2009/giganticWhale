//
//  PandaAnimationViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/10.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PandaAnimationViewController.h"
#import "HTHorizontalSelectionList.h"
#import "PandaAnmiationView.h"
#import "PandaAnimationView1.h"
#import "PandaAnimationView2.h"
#import "PDPandaHUDView.h"
#import "PandaAnimationLaungchViewController.h"

@interface PandaAnimationViewController()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;             /**< */
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;

// animationView
@property (nonatomic,strong)PandaAnimationView1 *pandaAnimationView1;
@property (nonatomic,strong)PandaAnimationView2 *pandaAnimationView2;

@property (nonatomic,strong)PDPandaHUDView *pandaHUD;


@end
@implementation PandaAnimationViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    [self createView1];
    [self createView2];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"熊猫动画";
}

#pragma mark - createView;
-(void)createView{
    if (!self.segmentList){
        self.segmentList = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0, 0,kScreenBounds.size.width ,LCFloat(50))];
        self.segmentList.backgroundColor = [UIColor clearColor];
        self.segmentList.bottomTrimHidden = YES;
        self.segmentList.dataSource = self;
        self.segmentList.delegate = self;
//        self.segmentList.isNotScroll = YES;
        [self.segmentList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.view addSubview:self.segmentList];
    }
    self.segmentArr = @[@"熊猫动画1",@"熊猫动画2",@"熊猫动画3"];
    
    // 创建scrollView
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.segmentList.frame));
    self.mainScrollView.contentSize = CGSizeMake(self.segmentArr.count * kScreenBounds.size.width, self.mainScrollView.size_height);
    self.mainScrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mainScrollView];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    if (index == 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.pandaAnimationView1 startAnimationWithBlock:NULL];
            [self.pandaAnimationView2 startAnimation];
            [self.pandaHUD startAnimationWithBlock:NULL];
        });
    } else if (index == 1){
        [self createView2];
    }
}


#pragma mark - createView1
-(void)createView1{
    CGRect pandaAnimationViewRect = CGRectMake((kScreenBounds.size.width - 140) / 2., 10, 140, 140);
    
    self.pandaAnimationView1 = [[PandaAnimationView1 alloc]init];
    self.pandaAnimationView1.frame = pandaAnimationViewRect;
    self.pandaAnimationView1.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.pandaAnimationView1];
    
    self.pandaAnimationView2 = [[PandaAnimationView2 alloc]init];
    self.pandaAnimationView2.frame = CGRectMake(self.pandaAnimationView1.orgin_x, CGRectGetMaxY(self.pandaAnimationView1.frame), self.pandaAnimationView1.size_width, self.pandaAnimationView1.size_height);
    self.pandaAnimationView2.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.pandaAnimationView2];
    
    self.pandaHUD = [[PDPandaHUDView alloc]init];
    self.pandaHUD.frame = CGRectMake(self.pandaAnimationView1.orgin_x, CGRectGetMaxY(self.pandaAnimationView2.frame), self.pandaAnimationView1.size_width, self.pandaAnimationView1.size_height);
    self.pandaHUD.backgroundColor = [UIColor clearColor];
    [self.mainScrollView addSubview:self.pandaHUD];
}

-(void)createView2{
    PandaAnimationLaungchViewController *launchVC = [[PandaAnimationLaungchViewController alloc] init];
   
    [[[[UIApplication sharedApplication] delegate] window] addSubview:launchVC.view];
}
@end
