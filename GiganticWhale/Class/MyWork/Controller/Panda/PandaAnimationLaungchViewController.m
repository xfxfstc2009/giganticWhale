//
//  PandaAnimationLaungchViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PandaAnimationLaungchViewController.h"
#import "PandaAnimationView1.h"                         /**< 动画模块*/

@interface PandaAnimationLaungchViewController()
@property (nonatomic,strong)PandaAnimationView1 *pandaAnimationView1;
@end

@implementation PandaAnimationLaungchViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createAnimationView];
}

#pragma mark - createAnimationView
-(void)createAnimationView{
    self.pandaAnimationView1 = [[PandaAnimationView1 alloc]init];
    self.pandaAnimationView1.frame = CGRectMake((kScreenBounds.size.width - 140) / 2., (kScreenBounds.size.height - 140) / 2. - 64, 140, 140);
    self.pandaAnimationView1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.pandaAnimationView1];
    
    [self.pandaAnimationView1 startAnimationWithBlock:^{
        [self performSelector:@selector(dismissAniamtionManager1) withObject:nil afterDelay:2];
    }];
}



#pragma mark - 消失动画1
-(void)dismissAniamtionManager1{
    [UIView animateWithDuration:1 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(10, 10);
        self.pandaAnimationView1.alpha = 0;
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画2
-(void)dismissAniamtionManager2{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager21];
    }];
}

-(void)dismissAnimationManager21{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.1f, .1f);
        //        self.pandaAnimationView1.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画3
-(void)dismissAniamtionManager3{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager31];
    }];
}

-(void)dismissAnimationManager31{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.7f, .7f);
        
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self dismissAnimationManager32];
    }];
}

-(void)dismissAnimationManager32{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        //        [self.view removeFromSuperview];
    }];
}


#pragma mark - 消失动画4
-(void)dismissAniamtionManager4{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager41];
    }];
}

-(void)dismissAnimationManager41{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.8f, .8f);
        
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor clearColor];
        [self dismissAnimationManager42];
    }];
}

-(void)dismissAnimationManager42{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.window originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画5
-(void)dismissAniamtionManager5{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager51];
    }];
}

-(void)dismissAnimationManager51{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.8f, .8f);
        
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self dismissAnimationManager52];
    }];
}

-(void)dismissAnimationManager52{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdInflateTransitionFromView:self.view toView:keywindow.window originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        [self.view removeFromSuperview];
    }];
}


@end
