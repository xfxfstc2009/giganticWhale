//
//  ArcToCircleLayer.m
//  MyAnimation
//
//  Created by 裴烨烽 on 16/5/19.
//  Copyright © 2016年 Animation. All rights reserved.
//

#import "ArcToCircleLayer.h"

@implementation ArcToCircleLayer

@dynamic progress;
@dynamic color;
@dynamic lineWidth;


+ (BOOL)needsDisplayForKey:(NSString *)key {
    if ([key isEqualToString:@"progress"]) {
        return YES;
    } else if ([key isEqualToString:@"color"]) {
        return YES;
    } else if ([key isEqualToString:@"lineWidth"]) {
        return YES;
    }
    
    return [super needsDisplayForKey:key];
}


-(void)drawInContext:(CGContextRef)ctx{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    CGFloat radius = MIN(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds)) / 2 - self.lineWidth / 2;
    CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    
    // 0.
    CGFloat originStart = M_PI * 7 / 2.;
    CGFloat originEnd = M_PI * 2;
    CGFloat currentOrigin = originStart - (originStart - originEnd) * self.progress;
    
    // 1.
    CGFloat destStart = M_PI * 3;
    CGFloat destEnd = 0;
    CGFloat currentDesr = destStart - (destStart - destEnd) * self.progress;
    
    [path addArcWithCenter:center radius:radius startAngle:currentOrigin endAngle:currentDesr clockwise:NO];
    CGContextAddPath(ctx, path.CGPath);
    CGContextSetLineWidth(ctx, self.lineWidth);
    CGContextSetStrokeColorWithColor(ctx, self.color.CGColor);
    CGContextStrokePath(ctx);
}


@end
