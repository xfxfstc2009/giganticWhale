//
//  PandaAnmiationView.m
//  MyAnimation
//
//  Created by 裴烨烽 on 16/5/19.
//  Copyright © 2016年 Animation. All rights reserved.
//

#import "PandaAnmiationView.h"
#import "ArcToCircleLayer.h"
#import <objc/runtime.h>
#import <pop/POP.h>

CGFloat PDBiliFloat (CGFloat floatValue) {
    return PDBiliFloatWithPadding(floatValue, 0.0f);
}


CGFloat PDBiliFloatWithPadding(CGFloat floatValue, CGFloat padding) {
    return floorl((floatValue * 1.f /300) * animationWidth);
}


static NSString *const kName = @"name";
static char animationFinishBlock;

//static CGFloat const kRadius = 150;                         /**< 圆脸的半径*/
//static CGFloat const kLineWidth = PDBiliFloat(6);                        /**< 圆脸的宽度*/
static CGFloat const kStep1Duration = 0.2;                  /**< 动画1执行时间*/
static CGFloat const kStep2Duration = .5f;                  /**< 动画2执行时间*/
static CGFloat const kStep3Duration = .15f;                 /**< 动画3执行时间*/
static CGFloat const kStep4Duration = .25f;                 /**< 动画4执行时间*/
static CGFloat const kStep5Duration = 0.25;                 /**< 动画5执行时间*/
//static CGFloat const kStep6Duration = 0.25;                 /**< 动画6执行时间*/

static CGFloat const kVerticalThinLayerWidth = 4;           /**< 跳出来的小点的宽度*/
static CGFloat const kVerticalMoveLayerHeight = 15;         /**< 跳出来小点的高度*/

// 第4阶段 头像变扁的比例
static CGFloat const kYScale = 0.8;                             //


@interface PandaAnmiationView()<POPAnimationDelegate>{
    CGFloat kRadius;
    CGFloat kLineWidth;
}

@property (nonatomic,strong) CAShapeLayer *arcToCircleLayer;
@property (nonatomic,strong) CAShapeLayer *faceShapeLayer;              /**< 脸layer*/
@property (nonatomic, strong) CAShapeLayer *faceMaskLayer;              /**< 脸的mask*/

@property (nonatomic,strong) CAShapeLayer *moveArcLayer;

@property (nonatomic,strong) CALayer *verticalMoveLayer;

@property (nonatomic) CAShapeLayer *verticalDisappearLayer;             /**< 消失的竖线*/
@property (nonatomic) CAShapeLayer *verticalAppearLayer;

// 脸
@property (nonatomic,strong)UIView *faceView;
@property (nonatomic,strong)CAShapeLayer *faceLayer1;
@property (nonatomic,strong)CABasicAnimation *faceAnimation;
@property (nonatomic,strong)CAShapeLayer *trackLayer;

// 眼睛
@property (nonatomic,strong)CAShapeLayer *leftEyeLayer;
@property (nonatomic,strong)CAShapeLayer *rightEyeLayer;

// 耳朵
@property (nonatomic,strong)UIView *earView;
@property (nonatomic,strong)CAShapeLayer *earLayer;

@end

@implementation PandaAnmiationView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        kRadius =  self.bounds.size.width / 2.;
        kLineWidth = PDBiliFloat(10);
    }
    return self;
}

#pragma mark - public
- (void)startAnimation {
    [self reset];
    [self doStep1];
}
-(void)startAnimationWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &animationFinishBlock, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self startAnimation];
}

#pragma mark - animation
- (void)reset {
    [self.arcToCircleLayer removeFromSuperlayer];
    [self.moveArcLayer removeFromSuperlayer];
    [self.verticalMoveLayer removeFromSuperlayer];
    [self.leftEyeLayer removeFromSuperlayer];
    [self.rightEyeLayer removeFromSuperlayer];
    [self.earLayer removeFromSuperlayer];
}

// 第一阶段 画一个圆
-(void)doStep1{
    self.arcToCircleLayer = [CAShapeLayer layer];
    self.arcToCircleLayer.path = [self createFacePath].CGPath;
    self.arcToCircleLayer.backgroundColor = [UIColor clearColor].CGColor;
    self.arcToCircleLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    [self.layer addSublayer:self.arcToCircleLayer];
    
    self.arcToCircleLayer.bounds = CGRectMake(PDBiliFloat(19) , PDBiliFloat(20), (self.bounds.size.width - PDBiliFloat(20) * 2), (self.bounds.size.height - PDBiliFloat(20) * 2));
    self.arcToCircleLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    
    // animation
//    self.arcToCircleLayer.progress = 1; // end status
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.duration = kStep1Duration;
    animation.fromValue = @0.0;
    animation.toValue = @1.;
    animation.delegate = self;
    [animation setValue:@"step1" forKey:kName];
    [self.arcToCircleLayer addAnimation:animation forKey:nil];

}

-(void)doStep11{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation.springBounciness    = 20.f;
    [scaleAnimation setValue:@"step11" forKey:kName];
    scaleAnimation.delegate = self;
    [self.arcToCircleLayer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

// 第二阶段，水跳出
-(void)doStep2{
    self.moveArcLayer = [CAShapeLayer layer];
    [self.layer addSublayer:self.moveArcLayer];
    self.moveArcLayer.frame = self.layer.bounds;
    // 弧的path
    UIBezierPath *moveArcPath = [UIBezierPath bezierPath];
    // 小圆圆心
    CGPoint center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    // d（x轴上弧圆心与小圆左边缘的距离）
    CGFloat d = kRadius / 2;
    // 弧圆心
    CGPoint arcCenter = CGPointMake(center.x - kRadius - d, center.y);
    // 弧半径
    CGFloat arcRadius = kRadius * 2 + d;
    // O(origin)
    CGFloat origin = M_PI * 2;
    // D(dest)
    CGFloat dest = M_PI * 2 - asin(kRadius * 2 / arcRadius);
    [moveArcPath addArcWithCenter:arcCenter radius:arcRadius startAngle:origin endAngle:dest clockwise:NO];
    self.moveArcLayer.path = moveArcPath.CGPath;
    self.moveArcLayer.lineWidth = kLineWidth;
    self.moveArcLayer.strokeColor = [UIColor blackColor].CGColor;
    self.moveArcLayer.fillColor = nil;
    
    // SS(strokeStart)
    CGFloat SSFrom = 0;
    CGFloat SSTo = 0.9;
    
    // SE(strokeEnd)
    CGFloat SEFrom = 0.1;
    CGFloat SETo = 1;
    
    // end status
    self.moveArcLayer.strokeStart = SSTo;
    self.moveArcLayer.strokeEnd = SETo;
    
    // animation
    CABasicAnimation *startAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    startAnimation.fromValue = @(SSFrom);
    startAnimation.toValue = @(SSTo);
    
    CABasicAnimation *endAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    endAnimation.fromValue = @(SEFrom);
    endAnimation.toValue = @(SETo);
    
    CAAnimationGroup *step2 = [CAAnimationGroup animation];
    step2.animations = @[startAnimation, endAnimation];
    step2.duration = kStep2Duration;
    step2.delegate = self;
    [step2 setValue:@"step2" forKey:kName];
    step2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self.moveArcLayer addAnimation:step2 forKey:nil];
}

// 第三阶段，融入
-(void)doStep3{
    [self.moveArcLayer removeFromSuperlayer];           // 移除上一个moveLayer
    
    // step3 layer
    self.verticalMoveLayer = [CALayer layer];
    self.verticalMoveLayer.contentsScale = [UIScreen mainScreen].scale;
    [self.layer addSublayer:self.verticalMoveLayer];
    
    CGFloat height = kVerticalMoveLayerHeight;
    self.verticalMoveLayer.bounds = CGRectMake(0, 0, kLineWidth, height);
    self.verticalMoveLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds) - kRadius * 2 + height / 2.);
    self.verticalMoveLayer.backgroundColor = [UIColor blackColor].CGColor;
    
    CGPoint originPosition = self.verticalMoveLayer.position;
    CGPoint destPosition = CGPointMake(originPosition.x, CGRectGetMidY(self.bounds) - kRadius - height / 2.);
    
    // end status
    self.verticalMoveLayer.position = destPosition;
    
    // animation
    CABasicAnimation *step3 = [CABasicAnimation animationWithKeyPath:@"position.y"];
    step3.fromValue = @(originPosition.y);
    step3.toValue = @(destPosition.y);
    step3.duration = kStep3Duration;
    step3.delegate = self;
    [step3 setValue:@"step3" forKey:kName];
    [self.verticalMoveLayer addAnimation:step3 forKey:nil];
}

// 第4阶段，头变扁，然后点消失
- (void)doStep4 {
    [self doStep4a];
    [self doStep4b];
}

// 4阶段a：小圆变形
- (void)doStep4a {
    CGRect frame = self.arcToCircleLayer.frame;
    self.arcToCircleLayer.anchorPoint = CGPointMake(0.5, 1);
    self.arcToCircleLayer.frame = frame;
//    self.arcToCircleLayer.color = [UIColor blackColor];
    
    // y scale
    CGFloat yFromScale = 1.0;
    CGFloat yToScale = kYScale;
    
    // x scale
    CGFloat xFromScale = 1.0;
    CGFloat xToScale = 1.1;
    
    // end status
    self.arcToCircleLayer.transform = CATransform3DMakeScale(xToScale, yToScale, 1);
    
    // animation
    CABasicAnimation *yAnima = [CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    yAnima.fromValue = @(yFromScale);
    yAnima.toValue = @(yToScale);
    
    CABasicAnimation *xAnima = [CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
    xAnima.fromValue = @(xFromScale);
    xAnima.toValue = @(xToScale);
    
    CAAnimationGroup *anima = [CAAnimationGroup animation];
    anima.animations = @[yAnima, xAnima];
    anima.duration = kStep4Duration;
    anima.delegate = self;
    [anima setValue:@"step4" forKey:kName];
    
    [self.arcToCircleLayer addAnimation:anima forKey:nil];
}

// 4阶段b：逐渐消失的竖线
- (void)doStep4b {
    // remove
    [self.verticalMoveLayer removeFromSuperlayer];
    
    // new
    self.verticalDisappearLayer = [CAShapeLayer layer];
    self.verticalDisappearLayer.frame = self.bounds;
    [self.layer addSublayer:self.verticalDisappearLayer];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat height = kLineWidth;
    CGFloat originY = CGRectGetMidY(self.bounds) - kRadius - height;
    CGFloat pathHeight = height + kRadius * 2 * (1 - kYScale);
    CGFloat destY = originY + pathHeight;
    [path moveToPoint:CGPointMake(CGRectGetMidX(self.bounds), originY)];
    [path addLineToPoint:CGPointMake(CGRectGetMidX(self.bounds), destY)];
    self.verticalDisappearLayer.path = path.CGPath;
    self.verticalDisappearLayer.lineWidth = kVerticalThinLayerWidth;
    self.verticalDisappearLayer.strokeColor = [UIColor blackColor].CGColor;
    self.verticalDisappearLayer.fillColor = nil;
    
    // SS(strokeStart)
    CGFloat SSFrom = 0;
    CGFloat SSTo = 1.0;
    
    // SE(strokeEnd)
    CGFloat SEFrom = height / pathHeight;
    CGFloat SETo = 1.0;
    
    // end status
    self.verticalDisappearLayer.strokeStart = SSTo;
    self.verticalDisappearLayer.strokeEnd = SETo;
    
    // animation
    CABasicAnimation *startAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    startAnimation.fromValue = @(SSFrom);
    startAnimation.toValue = @(SSTo);
    
    CABasicAnimation *endAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    endAnimation.fromValue = @(SEFrom);
    endAnimation.toValue = @(SETo);
    
    CAAnimationGroup *anima = [CAAnimationGroup animation];
    anima.animations = @[startAnimation, endAnimation];
    anima.duration = kStep4Duration;
    [self.verticalDisappearLayer addAnimation:anima forKey:nil];
}

// 第5步 ,头像恢复
-(void)doStep5{
    [self doStep5a];
}

-(void)doStep5a{
    [self.verticalDisappearLayer removeFromSuperlayer];
    // end status
    self.arcToCircleLayer.transform = CATransform3DIdentity;
    
    // animation
    CABasicAnimation *anima = [CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    anima.duration = kStep5Duration;
    anima.fromValue = @(kYScale);
    anima.toValue = @1;
    anima.delegate = self;
    [anima setValue:@"step5" forKey:kName];
    [self.arcToCircleLayer addAnimation:anima forKey:nil];
}

// 脸出来
-(void)doStep6{
    [self doStep6a];
    [self doStep6b];
    [self doStep6c];
}

-(void)doStep6a{
    self.faceShapeLayer = [CAShapeLayer layer];
    UIBezierPath* oval2Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(30, 30, 79.7, 79.7)];
    self.faceShapeLayer.path = oval2Path.CGPath;
    self.faceShapeLayer.fillColor = [UIColor whiteColor].CGColor;
    self.faceShapeLayer.opacity = 0;
    self.faceShapeLayer.transform = CATransform3DMakeScale(0, 0, 0);
    [self.layer insertSublayer:self.faceShapeLayer atIndex:0];
}


-(void)doStep6b{
    self.faceMaskLayer = [CAShapeLayer layer];
    self.faceMaskLayer.path = self.faceShapeLayer.path;
    self.faceMaskLayer.frame = self.bounds;
    self.faceMaskLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    self.faceMaskLayer.fillColor = [UIColor blackColor].CGColor;
    self.faceMaskLayer.opacity = 0;
    self.faceMaskLayer.transform = CATransform3DMakeScale(0, 0, 0);
    [self.layer insertSublayer:self.faceMaskLayer atIndex:0];
}

-(void)doStep6c{
    self.faceShapeLayer.opacity = 1;
    self.faceShapeLayer.transform = CATransform3DMakeScale(1, 1, 0);
    
    self.faceMaskLayer.transform = CATransform3DMakeScale(1, 1, 0);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.fromValue = [NSNumber numberWithDouble:0.0];
    animation.toValue = [NSNumber numberWithDouble:1.0];
    animation.duration = 0.3f;
//    animation.beginTime = CACurrentMediaTime() + 0.2f;
    animation.fillMode = kCAFillModeBackwards;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    animation.delegate = self;
    [animation setValue:@"step6" forKey:kName];
    [self.faceMaskLayer addAnimation:animation forKey:@"growMask"];
}

// 第六步，眼睛张开
-(void)doStep7{
    [self doStep7a];
    [self doStep7b];
    [self doStep7c];
}

-(void)doStep7a{
    self.leftEyeLayer = [CAShapeLayer layer];
    self.leftEyeLayer.path = [self createLeftEyePath].CGPath;
    self.leftEyeLayer.fillColor = [UIColor blackColor].CGColor;
    self.leftEyeLayer.strokeColor = [UIColor blackColor].CGColor;
    self.leftEyeLayer.lineWidth = PDBiliFloat(3);
    self.leftEyeLayer.bounds = CGRectMake(33.24, 57.22, 32.6, 21.82);
    self.leftEyeLayer.position = CGPointMake(33.24 + 32.6 / 2., 57.22 + 21.82 / 2.);
    [self.layer addSublayer:self.leftEyeLayer];
}

-(void)doStep7b{
    self.rightEyeLayer = [CAShapeLayer layer];
    self.rightEyeLayer.path = [self createRightEyePath].CGPath;
    self.rightEyeLayer.fillColor = [UIColor blackColor].CGColor;
    self.rightEyeLayer.strokeColor = [UIColor blackColor].CGColor;
    self.rightEyeLayer.lineWidth = PDBiliFloat(3);
    self.rightEyeLayer.bounds = CGRectMake(74.39, 57.22, 32.6, 21.82);
    self.rightEyeLayer.position = CGPointMake(74.39 + 32.6 / 2., 57.22 + 21.82 / 2.);
    [self.layer addSublayer:self.rightEyeLayer];

}


-(void)doStep7c{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.duration = kStep1Duration;
    animation.fromValue = @0.0;
    animation.toValue = @1.0;
    animation.delegate = self;
    [animation setValue:@"step7" forKey:kName];
    [self.leftEyeLayer addAnimation:animation forKey:nil];
    
    CABasicAnimation *animation1 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation1.duration = kStep1Duration;
    animation1.fromValue = @0.0;
    animation1.toValue = @1.0;
    animation1.delegate = self;
    [self.rightEyeLayer addAnimation:animation1 forKey:nil];
}

-(void)doStep7d{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation.springBounciness    = 20.f;
    [self.leftEyeLayer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *scaleAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation1.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation1.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation1.springBounciness    = 20.f;
    scaleAnimation1.delegate = self;
    [scaleAnimation1 setValue:@"step77" forKey:kName];
    [self.rightEyeLayer pop_addAnimation:scaleAnimation1 forKey:@"scaleAnimation"];
}

// 创建左眼轨迹
-(UIBezierPath *)createLeftEyePath{
    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(51.96, 60.29)];
    [bezier4Path addCurveToPoint: CGPointMake(65.83, 65.09) controlPoint1: CGPointMake(56.12, 63.03) controlPoint2: CGPointMake(60.75, 64.63)];
    [bezier4Path addCurveToPoint: CGPointMake(42.48, 79.03) controlPoint1: CGPointMake(61.44, 73.55) controlPoint2: CGPointMake(52.65, 79.03)];
    [bezier4Path addCurveToPoint: CGPointMake(33.24, 69.89) controlPoint1: CGPointMake(37.4, 79.03) controlPoint2: CGPointMake(33.24, 74.92)];
    [bezier4Path addCurveToPoint: CGPointMake(40.86, 58) controlPoint1: CGPointMake(33.24, 64.86) controlPoint2: CGPointMake(36.24, 60.29)];
    [bezier4Path addCurveToPoint: CGPointMake(52.19, 60.29) controlPoint1: CGPointMake(45.03, 56.17) controlPoint2: CGPointMake(48.72, 57.77)];
    [bezier4Path addLineToPoint: CGPointMake(51.96, 60.29)];
    [bezier4Path closePath];
    bezier4Path.miterLimit = 4;
    
    return bezier4Path;
}

// 创建右眼轨迹
-(UIBezierPath *)createRightEyePath{
    //// Bezier 3 Drawing
    UIBezierPath* bezier3Path = [UIBezierPath bezierPath];
    [bezier3Path moveToPoint: CGPointMake(92.19, 58)];
    [bezier3Path addCurveToPoint: CGPointMake(88.26, 60.29) controlPoint1: CGPointMake(90.8, 58.69) controlPoint2: CGPointMake(89.41, 59.37)];
    [bezier3Path addCurveToPoint: CGPointMake(74.39, 65.09) controlPoint1: CGPointMake(84.1, 63.03) controlPoint2: CGPointMake(79.47, 64.63)];
    [bezier3Path addCurveToPoint: CGPointMake(97.73, 79.03) controlPoint1: CGPointMake(78.78, 73.55) controlPoint2: CGPointMake(87.56, 79.03)];
    [bezier3Path addCurveToPoint: CGPointMake(106.98, 69.89) controlPoint1: CGPointMake(102.82, 79.03) controlPoint2: CGPointMake(106.98, 74.92)];
    [bezier3Path addCurveToPoint: CGPointMake(99.35, 58) controlPoint1: CGPointMake(106.98, 64.86) controlPoint2: CGPointMake(103.98, 60.29)];
    [bezier3Path addCurveToPoint: CGPointMake(92.19, 58) controlPoint1: CGPointMake(97.04, 56.86) controlPoint2: CGPointMake(94.5, 56.86)];
    [bezier3Path closePath];
    bezier3Path.miterLimit = 4;
    
    return bezier3Path;
}

// 脸轨迹
-(UIBezierPath *)createFacePath{
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(69.85, 30)];
    [bezier5Path addCurveToPoint: CGPointMake(48.52, 36.18) controlPoint1: CGPointMake(62.01, 30) controlPoint2: CGPointMake(54.69, 32.27)];
    [bezier5Path addCurveToPoint: CGPointMake(30, 69.85) controlPoint1: CGPointMake(37.39, 43.25) controlPoint2: CGPointMake(30, 55.69)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 109.7) controlPoint1: CGPointMake(30, 91.86) controlPoint2: CGPointMake(47.84, 109.7)];
    [bezier5Path addCurveToPoint: CGPointMake(109.7, 69.85) controlPoint1: CGPointMake(91.86, 109.7) controlPoint2: CGPointMake(109.7, 91.86)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 30) controlPoint1: CGPointMake(109.7, 47.84) controlPoint2: CGPointMake(91.86, 30)];
    [bezier5Path closePath];
    [bezier5Path moveToPoint: CGPointMake(114.7, 69.85)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 114.7) controlPoint1: CGPointMake(114.7, 94.62) controlPoint2: CGPointMake(94.62, 114.7)];
    [bezier5Path addCurveToPoint: CGPointMake(25, 69.85) controlPoint1: CGPointMake(45.08, 114.7) controlPoint2: CGPointMake(25, 94.62)];
    [bezier5Path addCurveToPoint: CGPointMake(44.21, 33.04) controlPoint1: CGPointMake(25, 54.61) controlPoint2: CGPointMake(32.6, 41.15)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 25) controlPoint1: CGPointMake(51.48, 27.97) controlPoint2: CGPointMake(60.32, 25)];
    [bezier5Path addCurveToPoint: CGPointMake(114.7, 69.85) controlPoint1: CGPointMake(94.62, 25) controlPoint2: CGPointMake(114.7, 45.08)];
    [bezier5Path closePath];

    return bezier5Path;
}


// 创建耳朵
-(void)doSetp8{
    [self doStep8a];
    [self doStep8b];
    [self doStep8c];
}

-(void)doStep8a{
    self.earView = [[UIView alloc]init];
    self.earView.frame = self.bounds;
    self.earView.backgroundColor = [UIColor blackColor];
    [self insertSubview:self.earView atIndex:0];
}

-(void)doStep8b{
    self.earLayer = [CAShapeLayer layer];
    self.earLayer.path = [self createEarsPath].CGPath;
    self.earLayer.frame = self.earView.bounds;
    self.earLayer.fillRule = kCAFillRuleEvenOdd;
    self.earLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    self.earLayer.anchorPoint = CGPointMake(.5f,.5f);
    self.earLayer.fillColor = [UIColor purpleColor].CGColor;
    self.earLayer.transform = CATransform3DMakeScale(0, 0, 0);
    self.earView.layer.mask = self.earLayer;
    
    self.earView.layer.masksToBounds = YES;
}

-(void)doStep8c{
    self.earLayer.transform = CATransform3DMakeScale(1, 1, 0);
    
    CABasicAnimation *maskAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    maskAnimation.fromValue = [NSNumber numberWithDouble:0.0];
    maskAnimation.toValue = [NSNumber numberWithDouble:1.0];
    maskAnimation.duration = .5f;
//    maskAnimation.beginTime = CACurrentMediaTime() + 0.2f;
    maskAnimation.fillMode = kCAFillModeBackwards;
    maskAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    maskAnimation.delegate = self;
    [maskAnimation setValue:@"step8" forKey:kName];
    [self.earLayer addAnimation:maskAnimation forKey:@"growMask"];
}

-(void)doStep8d{
    POPSpringAnimation *scaleAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation1.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation1.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation1.springBounciness    = 20.f;
    scaleAnimation1.delegate = self;
    [scaleAnimation1 setValue:@"step88" forKey:kName];
    [self.earView.layer pop_addAnimation:scaleAnimation1 forKey:@"scaleAnimation"];
}

// 耳朵旋转半圈
-(void)doStep9{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = .3f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: 0];               // 起始角度
    animation.toValue = [NSNumber numberWithFloat: M_PI / 2.];  // 终止角度
    animation.delegate = self;
    [animation setValue:@"step9" forKey:kName];
    [self.earLayer addAnimation:animation forKey:@"smart"];
}



-(UIBezierPath *)createEarsPath{
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(46.64, 22.57)];
    [bezier5Path addCurveToPoint: CGPointMake(70.34, 46) controlPoint1: CGPointMake(54.56, 30.4) controlPoint2: CGPointMake(62.46, 38.21)];
    [bezier5Path addLineToPoint: CGPointMake(74.64, 41.75)];
    [bezier5Path addCurveToPoint: CGPointMake(93.8, 22.8) controlPoint1: CGPointMake(81.02, 35.44) controlPoint2: CGPointMake(87.4, 29.13)];
    [bezier5Path addCurveToPoint: CGPointMake(106.29, 22.8) controlPoint1: CGPointMake(97.27, 19.37) controlPoint2: CGPointMake(102.82, 19.37)];
    [bezier5Path addLineToPoint: CGPointMake(117.85, 34.23)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 46.57) controlPoint1: CGPointMake(121.32, 37.66) controlPoint2: CGPointMake(121.32, 43.14)];
    [bezier5Path addCurveToPoint: CGPointMake(94.38, 69.78) controlPoint1: CGPointMake(110.01, 54.33) controlPoint2: CGPointMake(102.19, 62.06)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 92.98) controlPoint1: CGPointMake(102.19, 77.5) controlPoint2: CGPointMake(110.01, 85.22)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 105.32) controlPoint1: CGPointMake(121.32, 96.41) controlPoint2: CGPointMake(121.32, 101.89)];
    [bezier5Path addLineToPoint: CGPointMake(106.29, 116.75)];
    [bezier5Path addCurveToPoint: CGPointMake(93.8, 116.75) controlPoint1: CGPointMake(102.82, 120.18) controlPoint2: CGPointMake(97.27, 120.18)];
    [bezier5Path addCurveToPoint: CGPointMake(70.34, 93.55) controlPoint1: CGPointMake(85.96, 109) controlPoint2: CGPointMake(78.15, 101.27)];
    [bezier5Path addCurveToPoint: CGPointMake(46.64, 116.98) controlPoint1: CGPointMake(62.46, 101.35) controlPoint2: CGPointMake(54.56, 109.15)];
    [bezier5Path addCurveToPoint: CGPointMake(34.16, 116.98) controlPoint1: CGPointMake(43.18, 120.41) controlPoint2: CGPointMake(37.63, 120.41)];
    [bezier5Path addLineToPoint: CGPointMake(22.6, 105.55)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 93.21) controlPoint1: CGPointMake(19.13, 102.12) controlPoint2: CGPointMake(19.13, 96.64)];
    [bezier5Path addCurveToPoint: CGPointMake(44.87, 71.19) controlPoint1: CGPointMake(30.04, 85.85) controlPoint2: CGPointMake(37.46, 78.51)];
    [bezier5Path addLineToPoint: CGPointMake(46.3, 69.78)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 46.35) controlPoint1: CGPointMake(38.41, 61.98) controlPoint2: CGPointMake(30.52, 54.18)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 34) controlPoint1: CGPointMake(19.13, 42.92) controlPoint2: CGPointMake(19.13, 37.43)];
    [bezier5Path addCurveToPoint: CGPointMake(32.66, 24.06) controlPoint1: CGPointMake(22.6, 34) controlPoint2: CGPointMake(29.53, 27.15)];
    [bezier5Path addCurveToPoint: CGPointMake(34.16, 22.57) controlPoint1: CGPointMake(33.57, 23.15) controlPoint2: CGPointMake(34.16, 22.57)];
    [bezier5Path addCurveToPoint: CGPointMake(46.64, 22.57) controlPoint1: CGPointMake(37.63, 19.14) controlPoint2: CGPointMake(43.18, 19.14)];
    [bezier5Path closePath];

    return bezier5Path;
}



#pragma mark - Animation Delegate
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:kName] isEqualToString:@"step1"]){           // 【动画1：画圆】
//        [self doStep2];
//    } else if ([[anim valueForKey:kName] isEqualToString:@"step2"]){    // 【动画2：水跳出】
//        [self doStep3];
//    } else if ([[anim valueForKey:kName] isEqualToString:@"step3"]){    // 【动画3：融入】
//        [self doStep4];
//    } else if ([[anim valueForKey:kName] isEqualToString:@"step4"]){    // 【动画4：头变扁，消失】
//        [self doStep5];
//    } else if ([[anim valueForKey:kName] isEqualToString:@"step5"]){    // 【动画5：头恢复】
        [self doStep11];
        
    }  else if ([[anim valueForKey:kName] isEqualToString:@"step11"]){
        [self doStep6];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step6"]){    // 【动画6：脸出现】
        [self doStep7];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step7"]){
        [self doStep7d];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step77"]){
        [self doSetp8];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step8"]){    // 【眼睛出现】
        [self doStep8d];
        
//        [self doStep9];
    } else if([[anim valueForKey:kName] isEqualToString:@"step9"]){
        void(^block)() = objc_getAssociatedObject(self, &animationFinishBlock);
        if (block){
            block();
        }
    }
//         [self doSetp8];
}

- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished{
    if ([[anim valueForKey:kName] isEqualToString:@"step11"]){
        [self doStep6];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step77"]){
        [self doSetp8];
    } else if ([[anim valueForKey:kName] isEqualToString:@"step88"]){
        [self doStep9];
    }
}

static const CGFloat t1 = .7f;
static const CGFloat t2 = .49f;

-(CGFloat)getInterpolation:(CGFloat )input{
    if (input < t1){
        return (1.f / (t2)) * (input * input);
    } else {
        return (input * input - input)/ t2 - input / t1 + 1 / t1 + 1;
    }
}

@end
