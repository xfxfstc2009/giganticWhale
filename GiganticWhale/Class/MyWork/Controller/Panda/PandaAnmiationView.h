//
//  PandaAnmiationView.h
//  MyAnimation
//
//  Created by 裴烨烽 on 16/5/19.
//  Copyright © 2016年 Animation. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat PDBiliFloat(CGFloat floatValue);
extern CGFloat PDBiliFloatWithPadding(CGFloat floatValue, CGFloat padding);
static CGFloat animationWidth = 100;

@interface PandaAnmiationView : UIView

-(void)startAnimationWithBlock:(void(^)())block;

@end
