//
//  GWWhaleViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWWhaleViewController.h"
#import "WhaleView.h"
@interface GWWhaleViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *whaleTableView;
@property (nonatomic,strong)NSArray *whaleArr;

@end

@implementation GWWhaleViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"鲸鱼";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.whaleArr = @[@"原型图",@"效果图"];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.whaleTableView){
        self.whaleTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.whaleTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.whaleTableView.delegate = self;
        self.whaleTableView.dataSource = self;
        self.whaleTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.whaleTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.whaleTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.whaleArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    
        // 1. 创建图片
        GWImageView *imageView = [[GWImageView  alloc]init];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.stringTag = @"imageView";
        imageView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(150), LCFloat(150));
        [cellWithRowOne addSubview:imageView];
        
        // 2. 创建view
        WhaleView *whaleView = [[WhaleView alloc] initWithFrame:CGRectMake(LCFloat(11),LCFloat(11), LCFloat(150), LCFloat(150))];
        whaleView.backgroundColor = [UIColor clearColor];
        whaleView.stringTag = @"whaleView";
        [cellWithRowOne addSubview:whaleView];
    }
    GWImageView *imageView = (GWImageView *)[cellWithRowOne viewWithStringTag:@"imageView"];
    WhaleView *whaleView = (WhaleView *)[cellWithRowOne viewWithStringTag:@"whaleView"];
    
    [imageView uploadImageWithURL:@"myWork/IMG_0943.JPG" placeholder:nil callback:NULL];
    if(indexPath.section == 0){
        imageView.hidden = NO;
        whaleView.hidden = YES;
    } else if (indexPath.section == 1){
        imageView.hidden = YES;
        whaleView.hidden = NO;
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(172);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];

    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, LCFloat(20));
    if (section == 0){
        fixedLabel.text = @"原型图";
    } else {
        fixedLabel.text = @"效果";
    }
    [headerView addSubview:fixedLabel];
    
    return headerView;
}

@end
