//
//  GWPopImgView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWPopImgView : UIControl
@property (nonatomic,strong)UIImage *image;
@end
