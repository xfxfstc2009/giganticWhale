//
//  GWPopImgView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPopImgView.h"

@interface GWPopImgView()
@property(nonatomic) UIImageView *imageView;

@end

@implementation GWPopImgView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5.f;
        self.layer.masksToBounds = YES;
        
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_imageView];
    }
    return self;
}

#pragma mark - Property Setters

- (void)setImage:(UIImage *)image {
    [self.imageView setImage:image];
    _image = image;
}


@end
