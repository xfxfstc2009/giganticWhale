//
//  GWAnimationLabelViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWAnimationLabelViewController.h"
#import "GiganticWhale-Swift.h"

@interface GWAnimationLabelViewController()
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation GWAnimationLabelViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createLabel];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"测试";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"确定" barNorImage:NULL barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        NSInteger x = arc4random() % 7;
        
        strongSelf.dymicLabel.text = [NSString stringWithFormat:@"GiganticWhale%li",x];
    }];
}


#pragma mark - createLabel
-(void)createLabel{
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:18.];
    self.dymicLabel.textColor = [UIColor grayColor];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.frame = CGRectMake(0, 200, kScreenBounds.size.width, 44);
    self.dymicLabel.text = @"";
    [self.view addSubview:self.dymicLabel];

}


@end
