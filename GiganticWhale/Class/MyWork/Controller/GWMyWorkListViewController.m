//
//  GWMyWorkListViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMyWorkListViewController.h"
#import "GWMyWorkTableVIewCell.h"

#import "GWPopImgVIewController.h"                  /**< 图片pop */
#import "PandaAnimationViewController.h"            /**< 熊猫*/
#import "GWWhaleViewController.h"                   /**< 鲸鱼*/
#import "GWScaningViewController.h"                 /**< 扫描二维码*/
#import "GWAssetsLibraryViewController.h"           /**< 相册*/
#import "MyWorkHealthViewController.h"              /**< 健康环*/
#import "GWAnimationLabelViewController.h"          /**< 动画label*/

#import "GWPlayerDetailYotobeViewController.h"
@interface GWMyWorkListViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *myworkTableView;
@property (nonatomic,strong)NSMutableArray *myWorkMutableArr;

@end

@implementation GWMyWorkListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的工作内容";
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    NSArray *workArr = @[@[@"Pop浮层",@"熊猫动画",@"鲸鱼动画",@"扫描二维码",@"相册",@"健康环",@"动态label"]];
    self.myWorkMutableArr = [NSMutableArray array];
    [self.myWorkMutableArr addObjectsFromArray:workArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.myworkTableView){
        self.myworkTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.myworkTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.myworkTableView.delegate = self;
        self.myworkTableView.dataSource = self;
        self.myworkTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.myworkTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.myworkTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.myWorkMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.myWorkMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWMyWorkTableVIewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWMyWorkTableVIewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSString *dymic = [[self.myWorkMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cellWithRowOne.dymicTitle = dymic;
    [cellWithRowOne loadContentWithIndex:indexPath.row];

    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 0){                  // 图片pop
        GWPopImgVIewController *popImgViewController = [[GWPopImgVIewController alloc]init];
        [self.navigationController pushViewController:popImgViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 1){           // 熊猫动画
        PandaAnimationViewController *animationViewController = [[PandaAnimationViewController alloc]init];
        [self.navigationController pushViewController:animationViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 2){           // 鲸鱼动画
        GWWhaleViewController *whaleViewController = [[GWWhaleViewController alloc]init];
        [self.navigationController pushViewController:whaleViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 3){           // 扫描二维码
        GWScaningViewController *scaningViewController = [[GWScaningViewController alloc]init];
        [self.navigationController pushViewController:scaningViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 4){           // 相册
        GWAssetsLibraryViewController *assetsLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        assetsLibraryViewController.isNotShowCamera = NO;
        __weak typeof(self)weakSelf = self;
        [assetsLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            
        }];
        [self.navigationController pushViewController:assetsLibraryViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 5){
        MyWorkHealthViewController *workHealthViewController = [[MyWorkHealthViewController alloc]init];
        [self.navigationController pushViewController:workHealthViewController animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 6){
        GWAnimationLabelViewController *animationLabelVC = [[GWAnimationLabelViewController alloc]init];
        [self.navigationController pushViewController:animationLabelVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWMyWorkTableVIewCell calculationCellHeight];
}


@end
