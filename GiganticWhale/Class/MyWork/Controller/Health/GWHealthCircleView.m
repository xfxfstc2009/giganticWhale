//
//  GWHealthCircleView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/21.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWHealthCircleView.h"
#import "Masonry.h"
#import <POP/POP.h>

#define   DEGREES_TO_RADIANS(degrees)  ((M_PI * degrees)/ 180)
@interface GWHealthCircleView(){
    CAShapeLayer *shapLayer;
    CAShapeLayer *progressLayer;
}
@property(nonatomic) CGPoint CGPoinCerter;
@property(nonatomic) CGFloat endAngle;
@property (nonatomic)CGFloat radius;
@property (nonatomic,strong)UILabel *labelOne;          /**< 步数*/
@property (nonatomic,strong)UILabel *labelTwo;          /**< 走路公里*/
@property (nonatomic,strong)UILabel *labelTwoFixed;     /**< 走路公里*/
@end

@implementation GWHealthCircleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
    }
    return self;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.lineWith = 10.0;
    self.startAngle = 150;
    self.endAngle = 390;
    self.backgroundColor = [UIColor clearColor];
}

-(void)drawRect:(CGRect)rect{
    [self dataWithInit];
    [self drawMiddlecircle];
    [self drawOutCCircle];
    [self setupLabel];
}

#pragma mark - 
-(void)dataWithInit{
    CGFloat center = MIN(self.bounds.size.height / 2, self.bounds.size.width / 2);
    self.CGPoinCerter = CGPointMake(center, center);
    
    if(self.transferPercentage>1){
        self.transferPercentage = 1.0;
    }
    else if(self.transferPercentage < 0){
        self.transferPercentage = 0;
    }
    //半径计算
    self.radius = MIN(self.bounds.size.height/2-self.lineWith/2, self.bounds.size.width/2-self.lineWith/2);
    //起点与终点坐标
    self.endInnerAngle = DEGREES_TO_RADIANS(self.endInnerAngle);
    self.startAngle = DEGREES_TO_RADIANS(self.startAngle);
    self.endAngle = self.transferPercentage * (self.endInnerAngle - self.startAngle) + self.startAngle;
}

/**
 *  显示圆环
 */
-(void )drawOutCCircle{
    UIBezierPath *bPath = [UIBezierPath bezierPathWithArcCenter:self.CGPoinCerter radius:self.radius startAngle:self.startAngle endAngle:self.endAngle clockwise:YES];
    bPath.lineWidth = self.lineWith;
    bPath.lineCapStyle = kCGLineCapRound;
    bPath.lineJoinStyle = kCGLineJoinRound;
    
    [bPath stroke];
    
    
    // 生产出一个圆形路径的Layer
    
    progressLayer = [CAShapeLayer layer];
    progressLayer.path = bPath.CGPath;
    progressLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    progressLayer.fillColor = [[UIColor clearColor] CGColor];
    progressLayer.lineWidth = self.lineWith;
    progressLayer.lineCap = kCALineCapRound;
    
    // 可以设置出圆的完整性
    progressLayer.strokeStart = 0;
    progressLayer.strokeEnd = 0;
    
    CALayer *gradientLayer = [CALayer layer];
    
    CAGradientLayer *gradientLayer1 =  [CAGradientLayer layer];
    gradientLayer1.frame = self.bounds;
    // 创建颜色数组
    NSMutableArray *colors = [NSMutableArray array];
    [colors addObject:(id)[UIColor colorWithRed:87 / 255. green:198/255. blue:196/255. alpha:1].CGColor];
    [colors addObject:(id)[UIColor colorWithRed:136 / 255. green:132/255. blue:246/255. alpha:1].CGColor];
    [gradientLayer1 setColors:colors];
    [gradientLayer1 setLocations:@[@0.5,@0.9,@1 ]];
    [gradientLayer1 setStartPoint:CGPointMake(0.5, 1)];
    [gradientLayer1 setEndPoint:CGPointMake(0.5, 0)];
    [gradientLayer addSublayer:gradientLayer1];
    gradientLayer.mask = progressLayer;
    [self.layer addSublayer:gradientLayer];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = 5;
    animation.repeatCount = MAXFLOAT;
    animation.fromValue = [NSNumber numberWithDouble:0];
    animation.toValue = [NSNumber numberWithDouble:M_PI*2];
    [gradientLayer1 addAnimation:animation forKey:@"transform"];
}


-(void)drawMiddlecircle{
    UIBezierPath *cPath = [UIBezierPath bezierPathWithArcCenter:self.CGPoinCerter radius:self.radius startAngle:self.startAngle endAngle:self.endInnerAngle clockwise:YES];
    cPath.lineWidth=self.lineWith;
    cPath.lineCapStyle = kCGLineCapRound;
    cPath.lineJoinStyle = kCGLineJoinRound;
    // 填充颜色
    UIColor *color = [UIColor lightGrayColor];
    [color setStroke];
    
    [cPath stroke];
    
    shapLayer = [CAShapeLayer layer];
    shapLayer.frame = self.bounds;
    [self.layer addSublayer:shapLayer];
    shapLayer.fillColor = [[UIColor whiteColor] CGColor];
    shapLayer.strokeColor = [[UIColor whiteColor] CGColor];//指定path的渲染颜色
    shapLayer.opacity = 0;
    shapLayer.lineCap = kCALineCapRound;//指定线的边缘是圆的
    shapLayer.lineWidth = self.lineWith;//线的宽度
    
    shapLayer.path = [cPath CGPath];
    
}

-(void)setPercent:(CGFloat)percent setpCount:(NSInteger)setpCount distance:(CGFloat)distance animated:(BOOL)animated {
    [CATransaction begin];
    [CATransaction setDisableActions:!animated];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [CATransaction setAnimationDuration:1];
    progressLayer.strokeEnd = percent;
    [CATransaction commit];
    
    // animation
    [self performAnimationWithStepCount:setpCount distance:distance];
}


#pragma mark - label
-(void)setupLabel{
    // 1. 步数
    self.labelOne = [[UILabel alloc]init];
    self.labelOne.backgroundColor = [UIColor clearColor];
    self.labelOne.font = [[UIFont fontWithName:@"HelveticaNeue-Thin" size:30] boldFont];
    self.labelOne.frame = CGRectMake(0, self.bounds.size.height / 4., self.bounds.size.width, [NSString contentofHeight:self.labelOne.font]);
    self.labelOne.textAlignment = NSTextAlignmentCenter;
    self.labelOne.textColor = [UIColor whiteColor];
    [self addSubview:self.labelOne];
    // 2. 公里数
    self.labelTwo = [[UILabel alloc]init];
    self.labelTwo.backgroundColor = [UIColor clearColor];
    self.labelTwo.textAlignment = NSTextAlignmentRight;
    self.labelTwo.font = [[UIFont fontWithName:@"HelveticaNeue-Thin" size:21] boldFont];
    self.labelTwo.textColor = [UIColor whiteColor];
    [self addSubview:self.labelTwo];
    // 2.1 公里
    
    self.labelTwoFixed = [[UILabel alloc]init];
    self.labelTwoFixed.backgroundColor = [UIColor clearColor];
    self.labelTwoFixed.textAlignment = NSTextAlignmentCenter;
    self.labelTwoFixed.text = @"公里";
    self.labelTwoFixed.font = [UIFont systemFontOfCustomeSize:14];
    self.labelTwoFixed.textColor = [UIColor whiteColor];
    [self addSubview:self.labelTwoFixed];
    
    // 计算
    CGSize labelTwoFixedSize = [self.labelTwoFixed.text sizeWithCalcFont:self.labelTwoFixed.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.labelTwoFixed.font])];
    
    self.labelTwoFixed.frame = CGRectMake(self.bounds.size.width - LCFloat(30) - labelTwoFixedSize.width, CGRectGetMaxY(self.labelOne.frame) + LCFloat(10), labelTwoFixedSize.width, [NSString contentofHeight:self.labelTwoFixed.font]);

    self.labelTwo.frame = CGRectMake(0, self.labelTwoFixed.orgin_y - ABS(([NSString contentofHeight:self.labelTwo.font] - [NSString contentofHeight:self.labelTwoFixed.font]) / 2.) , self.labelTwoFixed.orgin_x, [NSString contentofHeight:self.labelTwo.font]);

}

-(void)performAnimationWithStepCount:(NSInteger)stepCount distance:(CGFloat)distance{
    //  步数
    [self animationManagerWithLabel:self.labelOne toValue:stepCount];
    //
    [self animationManagerWithLabel:self.labelTwo toValue:distance];
}

-(void)animationManagerWithLabel:(UILabel *)label toValue:(CGFloat)toValue{
    [label pop_removeAllAnimations];
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            if (label == self.labelTwo){
                [obj setText:[NSString stringWithFormat:@"%.2f",(float)values[0]]];
            } else {
                [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
            }
            
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @(toValue);
    
    [label pop_addAnimation:anim forKey:@"counting"];
}
@end
