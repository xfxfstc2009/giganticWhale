//
//  MyWorkHealthViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "MyWorkHealthViewController.h"
#import "GWHealthCircleView.h"

@interface MyWorkHealthViewController()
@property (nonatomic,strong)GWHealthCircleView *healthCircleView;

@end

@implementation MyWorkHealthViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"健康环";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        CGFloat precent = arc4random() % 100 / 100.;                /**< 运动环*/
        NSInteger setpCount = arc4random() % 100;
        CGFloat distance = arc4random() % 100 / 100.;
        
        [strongSelf.healthCircleView setPercent:precent setpCount:setpCount distance:distance animated:YES];
    }];
}

-(void)createView{
    // 5. 创建健康数据
    self.healthCircleView = [[GWHealthCircleView alloc]initWithFrame:CGRectMake(LCFloat(100),(kScreenBounds.size.width - LCFloat(150)) / 2., LCFloat(150), LCFloat(150))];
    self.healthCircleView.transferPercentage = 1;
    self.healthCircleView.lineWith = 10;           // 线宽
    self.healthCircleView.startAngle = 150;        // 百分百的圆环起点
    self.healthCircleView.endInnerAngle = 390;     // 百分百的圆环终点
    [self.view addSubview:self.healthCircleView];
}

@end
