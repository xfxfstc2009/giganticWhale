//
//  GWLoginViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWLoginViewController.h"
#import "ConnectionWechat.h"
@implementation GWLoginViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"123";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf loginManager];
    }];
}

-(void)loginManager{
    ConnectionWechat *connect = [[ConnectionWechat alloc]init];
    __weak typeof(self)weakSelf = self;
    [connect loginWithWechatWithBlock:^(NSDictionary *dic) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [AccountModel sharedAccountModel].nick = [dic objectForKey:@"nickname"];
        [AccountModel sharedAccountModel].avatar = [dic objectForKey:@"headimgurl"];
        strongSelf.headerImageView = [[GWImageView alloc]init];
        strongSelf.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
        [strongSelf loginSuccessManager];
    }];
}

@end
