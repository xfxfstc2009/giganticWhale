//
//  AccountModel.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AccountModel.h"
#import "ConnectionWechat.h"
#import "HomeSliderViewController.h"
#import "GWLocationMethod.h"
#import "ShareSDKManager.h"
#import "AliChatManager.h"

@implementation AccountModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"userId": @"id"};
}

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}

- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].userId.length ? YES : NO);
}


#pragma mark - 微信登录方法
-(void)wechatLoginManagerWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [ShareSDKManager thirdLoginWithType:thirdLoginTypeQQ successBlock:^(SSDKUser *user) {
        if (!weakSelf){
            return ;
        }
        // 进行登录
        NSString *userName = user.uid;
        NSString *password = @"";
        NSString *nick = user.nickname;
        NSString *icon = user.icon;
        [[AccountModel sharedAccountModel] loginManager:userName password:password loginType:loginTypeThird nick:nick icon:icon successBlock:^{
            if (block){
                block();
            }
        }];
    } failBlock:^{
        
    }];
}


#pragma mark - 登录方法
-(void)loginManager:(NSString *)userName password:(NSString *)password loginType:(loginType)loginType nick:(NSString *)nick icon:(NSString *)icon successBlock:(void(^)())block{
    
    NSMutableDictionary *paramsDic = [NSMutableDictionary dictionary];
    if (userName.length){
        [paramsDic setObject:userName forKey:@"userName"];
    }
    if (password.length){
        [paramsDic setObject:password forKey:@"password"];
    }
    if (loginType != 0){
        [paramsDic setObject:@(loginType) forKey:@"loginType"];
    }
    if (nick.length){
        [paramsDic setObject:nick forKey:@"nick"];
    }
    if (icon.length){
        [paramsDic setObject:icon forKey:@"icon"];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:user_login requestParams:paramsDic responseObjectClass:[AccountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            AccountModel *accountModel = (AccountModel *)responseObject;
            
            [AccountModel sharedAccountModel].nick = accountModel.nick;
            [AccountModel sharedAccountModel].avatar = accountModel.icon;
            [AccountModel sharedAccountModel].userId = accountModel.userId;
            [AccountModel sharedAccountModel].lat = accountModel.lat;
            [AccountModel sharedAccountModel].lng = accountModel.lng;
            [AccountModel sharedAccountModel].loginType = accountModel.loginType;
            
            // 1. 保存账号
            [GWTool userDefaulteWithKey:AUTO_Login_User Obj:userName];
            // 2. 保存登录方式
            [GWTool userDefaulteWithKey:AUTO_Login_Type Obj:[NSString stringWithFormat:@"%li",loginType]];
            // 3. 提交location
            [strongSelf getLocation];
            
            // 4.登录IM
            [[AliChatManager sharedInstance] loginWithUserName:accountModel.userId password:@"giganticwhale" preloginedBlock:^{
                [StatusBarManager statusBarHidenWithText:@"IM 预登录成功"];
            } successBlock:^{
                [StatusBarManager statusBarHidenWithText:@"IM 登录成功"];
            } failedBlock:^(NSError *err) {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"IM 预登录失败 %@",err.localizedDescription]];
            }];
            
            
            if (block){
                block();
            }
        } else {
            [StatusBarManager statusBarHidenWithText:@"登录失败"];
        }
    }];
}


-(void)logoutManagerBlock:(void(^)())block{
    // 1.删除账号
    [AccountModel sharedAccountModel].userId = @"";
    [AccountModel sharedAccountModel].nick = @"";
    [AccountModel sharedAccountModel].avatar = @"";
    
    // 2. 删除保存信息
    [GWTool userDefaultDelegtaeWithKey:AUTO_Login_User];
    [GWTool userDefaultDelegtaeWithKey:AUTO_Login_Type];
    // 3. 修改UI
    HomeSliderViewController *slider = (HomeSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
    [slider logoutSuccess];
    
    // block
    if (block){
        block();
    }
}


#pragma mark - 提交当前定位信息
-(void)getLocation{
//    __weak typeof(self)weakSelf = self;
//    [[GWLocationMethod sharedLocationManager] getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf sendRequestToUploadLocation:lat lng:lng];
//    }];
}

-(void)sendRequestToUploadLocation:(CGFloat)lat lng:(CGFloat)lng{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:user_location requestParams:@{@"lat":@(lat),@"lng":@(lng)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        }
    }];
}

@end
