//
//  AccountModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

typedef NS_ENUM(NSInteger,loginType) {
    loginTypeNormal = 1,                        /**< 普通登录*/
    loginTypeThird = 2,                         /**< 三方登录*/
};

@interface AccountModel : FetchModel


#pragma mark 三方登录
@property (nonatomic, copy) NSString *wx_accessToken;
@property (nonatomic, copy) NSString *wx_refreshToken;
@property (nonatomic, copy) NSString *wx_openId;

//
@property (nonatomic,copy)NSString *icon;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lng;
@property (nonatomic,assign)loginType loginType;
@property (nonatomic,copy)NSString *nick;

@property (nonatomic,copy)NSString *avatar;

@property (nonatomic,copy)NSString *slider;
@property (nonatomic,copy)NSString *laungch;

+(instancetype)sharedAccountModel;                                                               /**< 单例*/


#pragma mark - 判断是否登录
- (BOOL)hasLoggedIn;

#pragma mark - 微信登录方法
-(void)wechatLoginManagerWithBlock:(void(^)())block;

#pragma mark - 登录方法
-(void)loginManager:(NSString *)userName password:(NSString *)password loginType:(loginType)loginType nick:(NSString *)nick icon:(NSString *)icon successBlock:(void(^)())block;

#pragma mark - 退出登录方法
-(void)logoutManagerBlock:(void(^)())block;
@end
