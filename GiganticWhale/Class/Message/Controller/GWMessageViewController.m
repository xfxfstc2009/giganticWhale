//
//  GWMessageViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMessageViewController.h"
#import "GWStupSQLiteManager.h"
#import "GWMessageSingleModel.h"
#import "GWMessageTableViewCell.h"
@interface GWMessageViewController()<UITableViewDataSource,UITableViewDelegate,GWMessageTableViewCellDelegate>
@property (nonatomic,strong)UITableView *messageTableView;
@property (nonatomic,strong)NSMutableArray *messageMutableArr;
@property (nonatomic,strong)UIButton *openDrawerButton;

@end

@implementation GWMessageViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"消息";
    
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.messageMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < 10;i++){
        [self.messageMutableArr addObjectsFromArray:[GWStupSQLiteManager getListWithMessageTableName]];
    }
}

-(void)createTableView{
    if (!self.messageTableView){
        self.messageTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.messageTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.messageTableView.delegate = self;
        self.messageTableView.dataSource = self;
        self.messageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.messageTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.messageTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageMutableArr.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWMessageTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWMessageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.delegate = self;
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    GWMessageSingleModel *singleModel = [self.messageMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferMessageSingleModel = singleModel;

    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    GWMessageSingleModel *singleModel = [self.messageMutableArr objectAtIndex:indexPath.row];
    return [GWMessageTableViewCell calculationCellHeightWithModel:singleModel];
}

-(void)slideToDeleteCell:(GWMessageTableViewCell *)slideDeleteCell{
    NSIndexPath *indexPath = [self.messageTableView indexPathForCell:slideDeleteCell];
    [self.messageMutableArr removeObjectAtIndex:indexPath.row];
    [self.messageTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

@end
