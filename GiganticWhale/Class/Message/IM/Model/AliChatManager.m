//
//  AliChatManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/25.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "AliChatManager.h"
#import "SoundsManager.h"
#import "StatusBarManager.h"

@interface AliChatManager()<YWMessageLifeDelegate>

@end

@implementation AliChatManager


#pragma mark - 单例
+ (instancetype)sharedInstance {
    static AliChatManager *sExample = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sExample = [[AliChatManager alloc] init];
    });
    
    return sExample;
}

- (id)init {
    self = [super init];
    if (self) {             /// 初始化
        [self setLastConnectionStatus:YWIMConnectionStatusDisconnected];
    }
    return self;
}


#pragma mark - properties
-(id<UIApplicationDelegate>)appDelegate{
    return [UIApplication sharedApplication].delegate;
}

- (UIWindow *)rootWindow {
    UIWindow *result = nil;
    do {
        if ([self.appDelegate respondsToSelector:@selector(window)]) {
            result = [self.appDelegate window];
        }
        
        if (result) {
            break;
        }
    } while (NO);
    
    NSAssert(result, @"如果在您的App中出现这个断言失败，您需要检查- [SPKitExample rootWindow]中的实现，是否符合您的App结构");
    
    return result;
}

- (UINavigationController *)rootNavigationController {
    UINavigationController *result = [self.rootWindow.rootViewController isKindOfClass:[UINavigationController class]] ? (UINavigationController *)self.rootWindow.rootViewController : nil;
    
    NSAssert(result, @"如果在您的App中出现这个断言失败，您需要检查- [SPKitExample rootNavigationController]中的实现，是否符合您的App结构");
    
    return result;
}


#pragma mark - 1.1初始化
#pragma mark Launch初始化页面进行调用

- (void)callThisInDidFinishLaunching {
    if ([self exampleInit]) {                   /// 初始化成功 // 在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
        [self exampleHandleAPNSPush];
        [self exampleListenMyMessageLife];      /// 监听消息生命周期回调
    } else {                                    /// 初始化失败，需要提示用户
        [PDHUD showHUDProgress:@"SDK 初始化失败，请检查网络后重试" diary:2];
    }
}


- (BOOL)exampleInit {
    [[YWAPI sharedInstance] setEnvironment:YWEnvironmentRelease];                           /// 1. 设置环境
    [[YWAPI sharedInstance] setLogEnabled:YES];                                             /// 2. 开启日志
    NSError *error = nil;                                                                   /// 3. 开启error
    [[YWAPI sharedInstance] syncInitWithOwnAppKey:AliChattingIdentify getError:&error];
    
    if (error.code != 0 && error.code != YWSdkInitErrorCodeAlreadyInited) {                 /// 初始化失败
        [PDHUD showHUDProgress:@"初始化失败了，请联系管理员" diary:2];
        return NO;
    } else {
        if (error.code == 0) {                                                              /// 首次初始化成功 ///获取一个IMKit并持有
            self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
            [[self.ywIMKit.IMCore getContactService] setEnableContactOnlineStatus:YES];
        } else {
            /// 已经初始化
        }
        return YES;
    }
}

// 1.2
/**
 *  您需要在-[AppDelegate application:didFinishLaunchingWithOptions:]中第一时间设置此回调
 *  在IMSDK截获到Push通知并需要您处理Push时，IMSDK会自动调用此回调
 */

- (void)exampleHandleAPNSPush {
    __weak typeof(self) weakSelf = self;
    [[[YWAPI sharedInstance] getGlobalPushService] addHandlePushBlockV4:^(NSDictionary *aResult, BOOL *aShouldStop) {
        BOOL isLaunching = [aResult[YWPushHandleResultKeyIsLaunching] boolValue];
        UIApplicationState state = [aResult[YWPushHandleResultKeyApplicationState] integerValue];
        NSString *conversationId = aResult[YWPushHandleResultKeyConversationId];
        Class conversationClass = aResult[YWPushHandleResultKeyConversationClass];
        
        if (!conversationId.length){           // 会话标识
            return ;
        }
        if (conversationClass == NULL){
            return;
        }
        if (isLaunching){                      // 用户进入App
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,(int16_t)(1.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf exampleHandleAPNSPushControllerManagerWithClass:conversationClass aConversationId:conversationId];
            });
        } else {                // App已经启动状态
            if (state != UIApplicationStateActive) {
                [weakSelf exampleHandleAPNSPushControllerManagerWithClass:conversationClass aConversationId:conversationId];
            } else {
                /// 应用处于前台
                /// 建议不做处理，等待IM连接建立后，收取离线消息。
            }
        }
    } forKey:self.description ofPriority:YWBlockPriorityDeveloper];

}

-(void)exampleHandleAPNSPushControllerManagerWithClass:(__unsafe_unretained Class)aConversationClass aConversationId:(NSString *)aConversationId{
    if ([self exampleIsPreLogined]){                                        // 预登录成功 ,创建会话
        YWConversation *conversation = nil;
        if (aConversationClass == [YWP2PConversation class]){               // 单聊对话
            conversation = [YWP2PConversation fetchConversationByConversationId:aConversationId creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
        } else if (aConversationClass == [YWTribeConversation class]){      // 群聊会话
            conversation = [YWTribeConversation fetchConversationByConversationId:aConversationId creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
        }
        if (conversation){          // 如果有会话，就打开某个会话
//#warning 打开某个会话
        }
    }

}

#pragma mark 1.3 监听消息生命周期回调
- (void)exampleListenMyMessageLife {
    [[self.ywIMKit.IMCore getConversationService] addMessageLifeDelegate:self forPriority:YWBlockPriorityDeveloper];
}



#pragma mark - 2.1 登录注册
-(void)loginWithUserName:(NSString *)userName password:(NSString *)password preloginedBlock:(void(^)())aPreloginedBlock successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock {
    [self exampleListenConnectionStatus];                       // 1. 监听链接状态
    [self exampleSetAudioCategory];                             // 2.1.0 设置声音播放模式
    [self exampleSetAvatarStyle];                               // 2. 设置头像和昵称
    [self exampleSetProfile];                                   // 3. 设置昵称
    [self exampleSetMaxBubbleWidth];                            // 4. 设置气泡最大宽度
    [self exampleListenNewMessage];                             // 5. 监听新消息
    [self exampleSetNotificationBlock];                         // 6. 设置提示
    [self exampleListenOnClickAvatar];                          // 7. 监听头像点击事件
    [self exampleListenOnClickUrl];                             // 8. 监听链接点击事件
    [self exampleListenOnPreviewImage];                         // 9. 监听预览大图
    [self exampleEnableReadFlag];                               /// 开启单聊已读未读状态显示
    [self exampleLoginWithLoginName:userName password:password andPreloginedBlock:aPreloginedBlock andSuccessBlock:aSuccessBlock failedBlock:aFailedBlock];     // 10 . 登录
}

#pragma mark  2.1.1 监听链接状态
- (void)exampleListenConnectionStatus {
    __weak typeof(self) weakSelf = self;
    [[self.ywIMKit.IMCore getLoginService] addConnectionStatusChangedBlock:^(YWIMConnectionStatus aStatus, NSError *aError) {
        if (aStatus == YWIMConnectionStatusForceLogout || aStatus == YWIMConnectionStatusMannualLogout || aStatus == YWIMConnectionStatusAutoConnectFailed) {                       /// 手动登出、被踢、自动连接失败，都退出到登录页面
            if (aStatus != YWIMConnectionStatusMannualLogout) {
                [[UIAlertView alertViewWithTitle:@"退出登录" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
            [[weakSelf rootNavigationController] popToRootViewControllerAnimated:YES];
        }
    } forKey:[self description] ofPriority:YWBlockPriorityDeveloper];
}

#pragma mark 2.1.0 设置声音播放模式
- (void)exampleSetAudioCategory {           /// 设置为扬声器模式，这样可以支持靠近耳朵时自动切换到听筒
    [self.ywIMKit setAudioSessionCategory:AVAudioSessionCategoryPlayback];
}

#pragma mark  2.2 设置头像和昵称
- (void)exampleSetAvatarStyle {
    [self.ywIMKit setAvatarImageViewCornerRadius:4.f];
    [self.ywIMKit setAvatarImageViewContentMode:UIViewContentModeScaleAspectFill];
}

#pragma mark  2.2.1 获取好友列表
//#warning 这里执行自己的服务器好友关系列表
-(void)exampleSetProfile{
    [self.ywIMKit setFetchProfileForPersonBlock:^(YWPerson *aPerson, YWTribe *aTribe, YWProfileProgressBlock aProgressBlock, YWProfileCompletionBlock aCompletionBlock) {
        if (!aPerson.personId){
            return ;
        }
    }];
}

#pragma mark  2.3 设置气泡最大宽度
- (void)exampleSetMaxBubbleWidth {
    [YWBaseBubbleChatView setMaxWidthUsedForLayout:LCFloat(280.f)];
}

#pragma mark  2.4 监听新消息
-(void)exampleListenNewMessage{
    __weak typeof(self)weakSelf = self;
    [[weakSelf.ywIMKit.IMCore getConversationService] addOnNewMessageBlockV2:^(NSArray *aMessages, BOOL aIsOffline) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [SoundsManager aliMessageSounds];                                                                   // 1. 添加声音
        [aMessages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {                   // 2. 获取消息
            id<IYWMessage> message = [obj conformsToProtocol:@protocol(IYWMessage)] ? obj : nil;
            if (message){
                [strongSelf.messageDelegate listenNewMessageManagerWithMessage:message];
            }
        }];
    } forKey:self.description ofPriority:YWBlockPriorityDeveloper];
}

#pragma mark  2.5 监听头像点击事件
-(void)exampleListenOnClickAvatar{
    __weak typeof(self)weakSelf = self;
    [self.ywIMKit setOpenProfileBlock:^(YWPerson *aPerson, UIViewController *aParentController) {
        BOOL isMe = [aPerson isEqualToPerson:[[weakSelf.ywIMKit.IMCore getLoginService] currentLoginedUser]];
        if (isMe){
            NSLog(@"点击我");
        } else {
            NSLog(@"点击别人");
        }
    }];
}

#pragma mark  2.6 监听链接点击事件
- (void)exampleListenOnClickUrl {
    [self.ywIMKit setOpenURLBlock:^(NSString *aURLString, UIViewController *aParentController) {
        YWWebViewController *controller = [YWWebViewController makeControllerWithUrlString:aURLString];
        [aParentController.navigationController pushViewController:controller animated:YES];
    }];
}

#pragma mark  2.7 监听点开大图事件
//#warning 监听大图
- (void)exampleListenOnPreviewImage {
//    __weak typeof(self)weakSelf = self;
    [self.ywIMKit setPreviewImageMessageBlockV2:^(id<IYWMessage> aMessage, YWConversation *aOfConversation, UIViewController *aFromController) {
        
    }];
}

#pragma mark  2.7.1 监听消息
-(void)exampleSetNotificationBlock{
    [self.ywIMKit setShowNotificationBlock:^(UIViewController *aViewController, NSString *aTitle, NSString *aSubtitle, YWMessageNotificationType aType) {
        NSString *string = [NSString stringWithFormat:@"%@ %@",aTitle,aSubtitle];
        [StatusBarManager statusBarHidenWithText:string];
    }];
}
- (void)exampleEnableReadFlag {         // 开启单聊已读未读显示开关，如果应用场景不需要，可以关闭
    [[self.ywIMKit.IMCore getConversationService] setEnableMessageReadFlag:YES];
}


#pragma mark  2.8 登录
-(void)exampleLoginWithLoginName:(NSString *)userName password:(NSString *)password andPreloginedBlock:(void (^)())aPreloginedBlock andSuccessBlock:(void(^)())successBlock failedBlock:(void (^)(NSError *))failBlock {
    if (userName.length > 0 && password.length > 0){
         [self examplePreLoginWithLoginId:userName successBlock:aPreloginedBlock];                                              // 1. 预登录
        [self exampleLoginWithUserID:userName password:password successBlock:successBlock failedBlock:failBlock];               // 2. 真正登录
    } else {
        if (failBlock){
            failBlock([NSError errorWithDomain:YWLoginServiceDomain code:YWLoginErrorCodePasswordError userInfo:nil]);
        }
    }
}

#pragma mark  2.8.0 判断是否已经预登录
- (BOOL)exampleIsPreLogined {
    return YES;
}

#pragma mark  2.8.1 预登录
-(void)examplePreLoginWithLoginId:(NSString *)loginId successBlock:(void (^)())aPreligindBlock{
    if ([[self.ywIMKit.IMCore getLoginService] preLoginWithPerson:[[YWPerson alloc] initWithPersonId:loginId]]){
        if (aPreligindBlock){
            aPreligindBlock();
        }
    }
}

#pragma mark  2.8.2 真正登录
- (void)exampleLoginWithUserID:(NSString *)aUserID password:(NSString *)aPassword successBlock:(void(^)())aSuccessBlock failedBlock:(void (^)(NSError *))aFailedBlock {
    __weak typeof(self) weakSelf = self;
    aSuccessBlock = [aSuccessBlock copy];
    aFailedBlock = [aFailedBlock copy];
    
    /// 当IM向服务器发起登录请求之前，会调用这个block，来获取用户名和密码信息。
    [[self.ywIMKit.IMCore getLoginService] setFetchLoginInfoBlock:^(YWFetchLoginInfoCompletionBlock aCompletionBlock) {
        aCompletionBlock(YES, aUserID, aPassword, nil, nil);
    }];
    
    [[self.ywIMKit.IMCore getLoginService] asyncLoginWithCompletionBlock:^(NSError *aError, NSDictionary *aResult) {                /// 发起登录
        if (aError.code == 0 || [[weakSelf.ywIMKit.IMCore getLoginService] isCurrentLogined]) {
            if (aSuccessBlock) {
                aSuccessBlock();
            }
        } else {
            if (aFailedBlock) {
                aFailedBlock(aError);
            }
        }
    }];
}


#pragma mark - 3.0 登出
-(void)aliLogoutWithBlock:(void (^)())logoutBlock{
    [[self.ywIMKit.IMCore getLoginService] asyncLogoutWithCompletionBlock:NULL];
    if (logoutBlock){
        logoutBlock();
    }
}

#pragma mark  3.1断开与服务器链接，但是可以收到push
-(void)aliLogOutWithDisConnect{
    [[self.ywIMKit.IMCore getLoginService] mannualDisconnect];
}

#pragma mark - 4.0 打开会话

#pragma mark 打开单聊页面
- (void)exampleOpenConversationViewControllerWithPerson:(YWPerson *)aPerson fromNavigationController:(UINavigationController *)aNavigationController callBcak:(void (^)(GWCurrentIMViewController *conversationController))block{
    YWConversation *conversation = [YWP2PConversation fetchConversationByPerson:aPerson creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    __weak typeof(self)weakSelf = self;
    [self exampleOpenConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController block:^(GWCurrentIMViewController *controller) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(controller);
        }
    }];
}

// 打开单聊页面
-(void)openConversationWithPerson:(YWPerson *)person callBack:(void(^)(GWCurrentIMViewController *conversationController))block{
    YWConversation *conversation = [YWP2PConversation fetchConversationByPerson:person creatIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    __weak typeof(self)weakSelf = self;
    [self openConversation:conversation block:^(GWCurrentIMViewController *controller) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(controller);
        }
    }];
}

// 打开某个会话
-(void)openConversation:(YWConversation *)conversation block:(void(^)(GWCurrentIMViewController *controller))block{
    __block GWCurrentIMViewController *conversationViewController = nil;
    if (!conversationViewController) {
        conversationViewController = [self exampleMakeConversationViewControllerWithConversation:conversation];
        if (block){
            block(conversationViewController);
        }
    }
}


#pragma mark  打开某个会话
- (void)exampleOpenConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController block:(void(^)(GWCurrentIMViewController *controller))block{
    
    UINavigationController *conversationNavigationController = nil;
    conversationNavigationController = aNavigationController;
    __block GWCurrentIMViewController *conversationViewController = nil;
    [aNavigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[YWConversationViewController class]]) {
            GWCurrentIMViewController *c = obj;
            if (aConversation.conversationId && [c.conversation.conversationId isEqualToString:aConversation.conversationId]) {
                conversationViewController = c;
                if (block){
                    block(conversationViewController);
                }
                *stop = YES;
            }
        }
    }];
    
    if (!conversationViewController) {
        conversationViewController = [self exampleMakeConversationViewControllerWithConversation:aConversation];
        if (block){
            block(conversationViewController);
        }
    }
    
    NSArray *viewControllers = nil;
    if (conversationNavigationController.viewControllers.firstObject == conversationViewController) {
        viewControllers = @[conversationNavigationController.viewControllers.firstObject];
    }
    else {
        viewControllers = @[conversationNavigationController.viewControllers.firstObject, conversationViewController];
    }
    
    [conversationNavigationController setViewControllers:viewControllers animated:YES];
}


#pragma mark 创建某个会话Controller，在这个Demo中仅用于iPad SplitController中显示会话
- (GWCurrentIMViewController *)exampleMakeConversationViewControllerWithConversation:(YWConversation *)conversation {
    GWCurrentIMViewController *conversationController = nil;
#if __has_include("SPTribeConversationViewController.h")
    /// Demo中使用了继承方式，实现群聊聊天页面。
    if ([conversation isKindOfClass:[YWTribeConversation class]]) {
        conversationController = [SPTribeConversationViewController makeControllerWithIMKit:self.ywIMKit conversation:conversation];
        [self.ywIMKit addDefaultInputViewPluginsToMessagesListController:conversationController];
    }
    else
#endif
    {
        conversationController = [GWCurrentIMViewController makeControllerWithIMKit:self.ywIMKit conversation:conversation];
        [self.ywIMKit addDefaultInputViewPluginsToMessagesListController:conversationController];
    }
#if  __has_include("SPContactProfileController.h")
    if ([conversation isKindOfClass:[YWP2PConversation class]]) {
        __weak typeof(self) weakSelf = self;
        __weak YWConversationViewController *weakController = conversationController;
        conversationController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"设置" style:UIBarButtonItemStylePlain andBlock:^{
            SPContactProfileController *profileController = [[SPContactProfileController alloc] initWithContact:((YWP2PConversation *)conversation).person IMKit:weakSelf.ywIMKit];
            [weakController presentViewController:profileController animated:YES completion:nil];
        }];
    }
#endif
    /// 如果需要客服跟踪用户操作轨迹的功能，你可以取消以下行的注释，引入YWExtensionForCustomerServiceFMWK.framework，并并且修改相应的属性
    //            conversationController.ywcsTrackTitle = @"聊天页面";
    
    //如果需要自定义聊天页面标题，可以取消以下行的注释，注意，这将不再显示在线状态、输入状态和文字双击放大
    //        if ([aConversation isKindOfClass:[YWP2PConversation class]] && [((YWP2PConversation *)aConversation).person.personId isEqualToString:@"云大旺"]) {
    //            conversationController.disableTitleAutoConfig = YES;
    //            conversationController.title = @"自定义标题";
    //            conversationController.disableTextShowInFullScreen = YES;
    //        }
    
//    /// 添加自定义插件
//    [self exampleAddInputViewPluginToConversationController:conversationController];
//    
//    /// 添加自定义表情
//    [self exampleShowCustomEmotionWithConversationController:conversationController];
//    
//    /// 设置显示自定义消息
//    [self exampleShowCustomMessageWithConversationController:conversationController];
//    
//    /// 设置消息长按菜单
//    [self exampleSetMessageMenuToConversationController:conversationController];
    
    conversationController.hidesBottomBarWhenPushed = YES;
    
    return conversationController;
}










#pragma mark  4.1 添加自定义表情
- (void)addExpressionWithConversationController:(YWConversationViewController *)aConversationController {
//    for (id item in aConversationController.messageInputView.allPluginList){
//        if ([item isKindOfClass:[YWInputViewPluginEmoticonPicker class]]){
//            YWInputViewPluginEmoticonPicker *emotionPicker = (YWInputViewPluginEmoticonPicker *)item;
//            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"YW_TGZ_Emoitons" ofType:@"emo"];
//            NSArray *groups = [YWEmoticonGroupLoader emoticonGroupsWithEMOFilePath:filePath];
//            
//            for (YWEmoticonGroup *group in groups) {
//                [emotionPicker addEmoticonGroup:group];
//            }
//        }
//    }
}

#pragma mark  4.2 添加自定义消息
-(void)addCustomMessageWithConversationController:(YWConversationViewController *)aConversationController{
    [aConversationController setHook4BubbleViewModel:^YWBaseBubbleViewModel *(id<IYWMessage> message) {
        if ([[message messageBody] isKindOfClass:[YWMessageBodyCustomize class]]){      // 判断是否是自定义消息
            NSLog(@"自定义消息");
        } else {
            NSLog(@"普通消息");
        }
        return nil;
    }];
    
    // 设置自定义消息的chatView
    [aConversationController setHook4BubbleView:^YWBaseBubbleChatView *(YWBaseBubbleViewModel *message) {
        return nil;
    }];
}

- (void)messageLifeDidSend:(NSString *)aMessageId conversationId:(NSString *)aConversationId result:(NSError *)aResult {
    /// 你可以在消息发送完成后，做一些事情，例如播放一个提示音等等
}

#pragma mark - EService 获取EService对象
- (YWPerson *)exampleFetchEServicePersonWithPersonId:(NSString *)aPersonId groupId:(NSString *)aGroupId {
    return [[YWPerson alloc] initWithPersonId:aPersonId EServiceGroupId:aGroupId baseContext:self.ywIMKit.IMCore];
}



@end
