//
//  GWCurrentIMViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWCurrentIMViewController.h"
#import "GWBarMainTitleView.h"

@interface GWCurrentIMViewController()<UIGestureRecognizerDelegate>
@property (nonatomic,strong)UIButton *openDrawerButton;

@end

@implementation GWCurrentIMViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createCustomerView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - 修改tableView 
-(void)createCustomerView{
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor colorWithRed:236/255. green:236/255. blue:236/255. alpha:1];
    backgroundView.frame = CGRectMake(0, 0, kScreenBounds.size.width, (LCFloat(30) + 30 + LCFloat(20)));
    self.customTopView = backgroundView;
    
    // 2. 创建左侧按钮
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
    
    // 3. 增加拖动
    UIPanGestureRecognizer *panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizedWithMap:)];
    panGest.delegate = self;
    [self.view addGestureRecognizer:panGest];
    
    // 创建title
    GWBarMainTitleView *barMainTitle = [[GWBarMainTitleView alloc]initWithFrame:backgroundView.bounds title:@"Messages"];
    [backgroundView addSubview:barMainTitle];
    
}

#pragma mark - 创建拖动手势
- (void)panGestureRecognizedWithMap:(UIPanGestureRecognizer *)recognizer{
    [[RESideMenu shareInstance] panGestureRecognized:recognizer];
}

-(void)dealloc{
    NSLog(@"释放");
}

@end
