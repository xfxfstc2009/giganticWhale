//
//  GWMessageTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWMessageSingleModel.h"

@class GWMessageTableViewCell;

@protocol  GWMessageTableViewCellDelegate<NSObject>
-(void)slideToDeleteCell:(GWMessageTableViewCell *)slideDeleteCell;

@end

@interface GWMessageTableViewCell : UITableViewCell<UIGestureRecognizerDelegate>

@property (nonatomic,strong)GWMessageSingleModel *transferMessageSingleModel;
@property(assign, nonatomic)id<GWMessageTableViewCellDelegate>delegate;

+(CGFloat)calculationCellHeightWithModel:(GWMessageSingleModel *)singleModel;

@end
