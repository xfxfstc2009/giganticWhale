//
//  GWMessageTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMessageTableViewCell.h"
#define kRotationRadian  90.0/360.0
#define kVelocity        100

@interface GWMessageTableViewCell()
@property (nonatomic,strong)GWImageView *bgImageView;
@property (nonatomic,strong)GWImageView *headerImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIImageView *isSelected;

@property(assign, nonatomic) CGPoint currentPoint;
@property(assign, nonatomic) CGPoint previousPoint;
@property(strong, nonatomic) UIPanGestureRecognizer *panGestureRecognizer;
@property(assign, nonatomic) float offsetRate;

@end

@implementation GWMessageTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.backgroundColor = [UIColor clearColor];
    // 1. 创建view
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.frame = CGRectMake(LCFloat(20), 0, kScreenBounds.size.width - 2 *LCFloat(20), LCFloat(210));
    self.headerImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.headerImageView];
//
//    // 2. 创建label
//    self.titleLabel = [[UILabel alloc]init];
//    self.titleLabel.backgroundColor = [UIColor clearColor];
//    self.titleLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.headerImageView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeight:self.titleLabel.font]);
//    self.titleLabel.font = [UIFont systemFontOfCustomeSize:14.];
//    [self.titleLabel.font boldFont];
//    [self addSubview:self.titleLabel];
//    
//    // 3. 创建timeLabel
//    self.timeLabel = [[UILabel alloc]init];
//    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeight:self.timeLabel.font]);
//    self.timeLabel.font = [UIFont systemFontOfCustomeSize:12.];
//    self.timeLabel.textColor = [UIColor lightGrayColor];
//    [self addSubview:self.timeLabel];
//    
//    // 4. 创建是否查看
//    self.isSelected = [[UIImageView alloc]init];
//    self.isSelected.backgroundColor = [UIColor clearColor];
//    self.isSelected.image = [UIImage imageNamed:@"test.jpg"];
//    self.isSelected.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(44), LCFloat(11), LCFloat(44), LCFloat(20));
//    [self addSubview:self.isSelected];
    
    // 添加手势
    self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(slideToDeleteCell:)];
    self.panGestureRecognizer.delegate = self;
    [self addGestureRecognizer:self.panGestureRecognizer];
}

-(void)setTransferMessageSingleModel:(GWMessageSingleModel *)transferMessageSingleModel{
    _transferMessageSingleModel = transferMessageSingleModel;
    [self.headerImageView uploadImageWithMessageURL:transferMessageSingleModel.msgImg callback:^(UIImage *image) {
        self.headerImageView.image = image;
    }];


    
    self.titleLabel.text = transferMessageSingleModel.title;
    
    self.timeLabel.text = [NSDate getTimeGap:transferMessageSingleModel.time];
}


+(CGFloat)calculationCellHeightWithModel:(GWMessageSingleModel *)singleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(200);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeight:[UIFont systemFontOfCustomeSize:14.]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeight:[UIFont systemFontOfCustomeSize:11.]];
    cellHeight += LCFloat(5);
    
    return cellHeight;
}

#pragma mark - animation
#pragma mark UIGestureRecognizerDelegate------------------------------------------------
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        CGPoint velocityPoint = [gestureRecognizer velocityInView:self];
        if (fabs(velocityPoint.x) > kVelocity) {
            return YES;
        }else
            return NO;
    }else
        return NO;
    
}

-(void)slideToDeleteCell:(UIPanGestureRecognizer *)panGestureRecognizer{
    
    _previousPoint = [panGestureRecognizer locationInView:self.superview];
    
    static CGPoint originalCenter;
    UIGestureRecognizerState state = panGestureRecognizer.state;
    if (state == UIGestureRecognizerStateBegan) {
        
        originalCenter = self.center;
        [self.superview bringSubviewToFront:self];
    }else if (state == UIGestureRecognizerStateChanged){
        CGPoint diff = CGPointMake(_previousPoint.x - _currentPoint.x, _previousPoint.y - _currentPoint.y);
        [self handleOffset:diff];
    }else if (state == UIGestureRecognizerStateEnded){
        if (_offsetRate < 0.5) {
            [UIView animateWithDuration:0.2 animations:^{
                
                self.transform = CGAffineTransformIdentity;
                self.center = originalCenter;
                self.alpha = 1.0;
                
            }];
        }else{
            [UIView animateWithDuration:0.2 animations:^{
                self.center = CGPointMake(self.center.x * 2, self.center.y);
                self.alpha = 0.0;
                
            } completion:^(BOOL finsh){
                if ([self.delegate respondsToSelector:@selector(slideToDeleteCell:)]) {
                    [self.delegate slideToDeleteCell:self];
                }
            }];
        }
    }
    _currentPoint = _previousPoint;
    
}

-(void)handleOffset:(CGPoint)offset{
    
    self.center = CGPointMake(self.center.x + offset.x, self.center.y);
    float distance = self.frame.size.width/2 - self.center.x;
    float distanceAbs = fabsf(distance);
    float distanceRate = (self.frame.size.width - distanceAbs) / self.frame.size.width;
    self.alpha = distanceRate;
    
    _offsetRate = 1 -distanceRate;
    
    if (distance >= 0) {
        self.transform = CGAffineTransformMakeRotation(-_offsetRate * kRotationRadian);
    }else
        self.transform = CGAffineTransformMakeRotation(_offsetRate * kRotationRadian);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}



@end
