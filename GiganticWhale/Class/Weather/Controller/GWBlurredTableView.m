//
//  GWBlurredTableView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWBlurredTableView.h"

@implementation GWBlurredTableView

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) {
        // Initialization code
        [self createView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        // Initialization code
        [self createView];
    }
    return self;
}


-(void)createView{
    if (!self.framesCount){
        self.framesCount = 20;
    }
    if (!self.blurTintColor){
        self.blurTintColor = [UIColor clearColor];
    }
    if (!self.roundingValue){
        self.roundingValue = 60;
    }
    
    if (!self.compressionQuality){
        self.compressionQuality = 0.001;
    }
    
    if (!_animationDuration){
        self.animationDuration = 0.25;
    }
    
    if (self.animateTintAlpha){
        if (self.startTintAlpha < 0.0 || self.startTintAlpha > 1.0){
            self.startTintAlpha = 0.0;
        } else if (self.endTintAlpha < 0.0 || self.endTintAlpha > 1.0){
            self.endTintAlpha = 1.0;
        }
    }
    self.animateOnLoad = YES;
}


-(void)setBackgroundImage:(UIImage *)backgroundImage
{
    _backgroundImage = backgroundImage;
    
    // Prepare layout sets the correct backgroundImageView and prepares the view to be manipulated.
    [self prepareLayout];
}


#pragma mark -
#pragma mark Helper Methods

-(UIImage*)downsampleImage:(UIImage*)image
{
    // Downsample our image.
    NSData *imageData = UIImageJPEGRepresentation(image, _compressionQuality);
    UIImage *downSampledImage = [UIImage imageWithData:imageData];
    return downSampledImage;
}


-(void)prepareLayout
{
    // Make sure the background color of the tableView itself is clear so that the image is visible.
    [self setBackgroundColor:[UIColor clearColor]];
    
    // Set the tableView's backgroundView as a UIImageView that is the size of the frame.
    _backgroundImageView = [[UIImageView alloc]initWithFrame:self.frame];
    _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.backgroundView = _backgroundImageView;
    
    // set the background color to the tableView's default background color.
    // We do this instead of setting the backgroundColor for the user by default to allow for flexibility if the user decides to layer transparent views, etc.
    [_backgroundImageView setBackgroundColor:self.backgroundColor];
    
    // Set up our defaults.
    // Clear our our array.
    _frames = [[NSMutableArray alloc]init];
    
    // Generate blur frames if a background is available.
    if (_backgroundImage)
    {
        UIColor *processedTint = _blurTintColor;
        // Start with startTint instead of the tintColor's alpha if animateTint is active.
        if(_animateTintAlpha)
        {processedTint = [_blurTintColor colorWithAlphaComponent:_startTintAlpha];}
        
        // Generate our first frame.
        UIImage *firstFrame = [_backgroundImage drn_boxblurImageWithBlur:0.0 withTintColor:processedTint];
        // Add our first frame to the array.
        [_frames addObject:firstFrame];
        [_backgroundImageView setImage:firstFrame];
        
        // Disable scroll blurring while rendering frames.
        _rendering = YES;
        [self renderBlurFramesWithCompletion:^{
            
            // Animate in on completion.
            if (_animateOnLoad){
                CGFloat currentOffset = self.contentOffset.y;
                for (int i = 0; i < (int)currentOffset; i++)
                {
                    // This slows down our loop.
                    [NSThread sleepForTimeInterval:_animationDuration/currentOffset];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        {[self updateBackgroundBlurWithVerticalContentOffset:i];
                        }
                    });
                }
            }
            // Done rendering. Re-enable scroll bluring.
            _rendering = NO;
        }];
    }
}

#pragma mark -
#pragma mark Generate Animations
-(void)renderBlurFramesWithCompletion:(void(^)())completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Generate our frames in the background-- since our frames aren't generated immediately you'll want to use this to customize how the background comes in.
        [self renderBlurFrames];
        completion();
    });
}

//  You probably don't want to call this by itself.

-(void)renderBlurFrames
{
    
    // Generate a downsampled image to process.
    UIImage *downsampledImage = [self downsampleImage:_backgroundImage];
    
    // Used to step our alpha up if animating tintAlpha.
    CGFloat i = _startTintAlpha;
    CGFloat difference = _endTintAlpha - _startTintAlpha;
    
    // If animating tintAlpha, step up alpha by the difference over the framesCount.
    CGFloat incrementBy = difference / (CGFloat)_framesCount;
    
    // Generate our frames based on the number of frames allowed.
    // We set our first frame earlier to preserve image fidelity. This is 1 and up.
    for(int frame = 1; frame < _framesCount; frame++)
    {
        // The color we'll use to tint with, changing if animate is on.
        UIColor *processColor = _blurTintColor;
        
        // If animating our tintAlpha, set the processColor's alpha to the startAlpha.
        if (_animateTintAlpha){
            // Increment by our incrementBy.
            i = i + incrementBy;
            processColor = [_blurTintColor colorWithAlphaComponent:i];
        }
        
        // Blur with specified tint color.
        UIImage *image = [downsampledImage drn_boxblurImageWithBlur:((CGFloat)frame/(_framesCount-1)) withTintColor:processColor];
        
        // add to our frames array.
        [_frames addObject:image];
    }
}

#pragma mark -
#pragma mark Observers

-(void)setContentOffset:(CGPoint)contentOffset
{
    // Watch the contentOffset to determine the image to show in the background.
    [super setContentOffset:contentOffset];
    // Update our backgroundBlur on scroll.
    if (!_rendering)
    {[self updateBackgroundBlurWithVerticalContentOffset:contentOffset.y];}
}

#pragma mark -
#pragma mark UI Code

-(void)updateBackgroundBlurWithVerticalContentOffset:(CGFloat)contentOffset
{
    
    // Our current frame is based on our offset being lowered by a rounding value.
    NSInteger frame = (int)(contentOffset/_roundingValue);
    
    // If that value is lower than 0 or over the maximum, ignore it.
    if (frame < 0)
    {frame = 0;}
    else if (frame >= _framesCount)
    {frame = _framesCount - 1;}
    
    // Calculate blur based on the contentOffset.y. The more frames we have and the larger the rounding value, the further the blur lasts.
    //UIImage *image = [_frames objectAtIndex:frame];
    [_backgroundImageView setImage:[_frames objectAtIndex:frame]];
}


@end
