//
//  GWWeatherViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherViewController.h"
#import "GWBlurredTableView.h"
#import "GWWeatherSingleModel.h"
#import "GWWeatherCurrentHeaderTableViewCell.h"
#import "GWWeatherCurrentModel.h"
#import "GWMovieView.h"
#import "GWWeatherFeatureTableViewCell.h"
#import "GWWeatherDetailCurrentView.h"

@interface GWWeatherViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *weatherTableView;
@property (nonatomic,strong)NSMutableArray *weatherMutableArr;

@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)GWWeatherCurrentModel *weatherSingleModel;
@property (nonatomic,strong)GWMovieView *movieView;
@property (nonatomic,strong)GWWeatherDetailCurrentView *weatherHeaderDetailView;            // 头部的view
@end

@implementation GWWeatherViewController

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createMovieView];
    [self createTableViewView];
    [self createSlider];
    __weak typeof(self)weakSelf = self;
    [weakSelf fetchModelWithWeather];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIView animateWithDuration:0 animations:^{
        [self.movieView setAlpha:1];
    } completion:^(BOOL finished) {
        [self.movieView startAnimation];
    }];

}

#pragma mark - ArraryWithInit
-(void)arrayWithInit{
    self.weatherMutableArr = [NSMutableArray array];
}

-(void)createMovieView{
    self.movieView = [[GWMovieView alloc]initWithFrame:self.view.frame];
    self.movieView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.movieView.alpha = 0;
    [self.view addSubview:self.movieView];
    
//    UIToolbar *tooBar = [[UIToolbar alloc]init];
//    tooBar.backgroundColor = [UIColor clearColor];
//    tooBar.barStyle = UIBarStyleBlackOpaque;
//    tooBar.frame = self.movieView.bounds;
//    [self.movieView addSubview:tooBar];
}

-(void)createSlider{
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
}


















#pragma mark - UITableView
-(void)createTableViewView{
    if (!self.weatherTableView){
        self.weatherTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height / 2., kScreenBounds.size.width, kScreenBounds.size.height / 2.) style:UITableViewStylePlain];
        self.weatherTableView.delegate = self;
        self.weatherTableView.dataSource = self;
        self.weatherTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.weatherTableView.showsVerticalScrollIndicator = NO;
        self.weatherTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.weatherTableView.backgroundColor = [UIColor clearColor];
        [self.movieView addSubview:self.weatherTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.weatherMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowTow = @"cellIdengifyWithRowTow";
    GWWeatherFeatureTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTow];
    if (!cellWithRowTwo){
        cellWithRowTwo = [[GWWeatherFeatureTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTow];
        cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowTwo.cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowTwo.transferFutureModel = [self.weatherMutableArr objectAtIndex:indexPath.row];
    return cellWithRowTwo;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWWeatherFeatureTableViewCell calculationCellHeight];
}


#pragma mark - Interface
-(void)fetchModelWithWeather{
   GWWeatherSingleModel *fetchModel = [[GWWeatherSingleModel alloc]init];
    fetchModel.requestParams = @{@"city":@"杭州",@"province":@"浙江"};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithGetShareAPIPath:@"http://apicloud.mob.com/v1/weather/query" completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){                       // 1.保存数据
            GWWeatherCurrentModel *weatherCurrentModel =(GWWeatherCurrentModel *)[fetchModel.data lastObject];
            
            strongSelf.weatherSingleModel = [fetchModel.data lastObject];
            
            
            strongSelf.weatherHeaderDetailView = [[GWWeatherDetailCurrentView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height / 2.) withModel:strongSelf.weatherSingleModel];
            [self.movieView addSubview:self.weatherHeaderDetailView];
            
            
            // 移动到上层
            [strongSelf.weatherHeaderDetailView bringSubviewToFront:strongSelf.openDrawerButton];
            
            [strongSelf.weatherMutableArr addObjectsFromArray:weatherCurrentModel.future];
            
            //            //1. 空气质量
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].airCondition = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] airCondition];
            //            // 2. 当前城市
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].city = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] city];
            //            // 3. 感冒指数
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].coldIndex = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] coldIndex];
            //            // 4. 当前时间
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].date = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] date];
            //            // 5. 当前时间戳
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].dateWithInterval = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] dateWithInterval];
            //            // 6. 区县
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].distrct = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] distrct];
            //            // 7. 穿衣指数
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].dressingIndex = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] dressingIndex];
            //            // 8. 运动指数
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].exerciseIndex = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] exerciseIndex];
            //            // 9. 湿度
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].humidity = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] humidity];
            //            // 10.空气质量指数
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].pollutionIndex = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] pollutionIndex];
            //            // 11.省份
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].province = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] province];
            //            // 12.sunrise日出时间
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].sunrise = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] sunrise];
            //            // 13. 日落时间
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].sunset = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] sunset];
            //            // 14. 温度
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].temperature = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] temperature];
            //            // 15.测试时间
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].time = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] time];
            //            // 16.修改时间
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].updateTime = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] updateTime];
            //            // 17. 洗衣指数
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].washIndex = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] washIndex];
            //            // 18. 天气
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].weather = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] weather];
            //            // 19. 星期
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].week = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] week];
            //            // 20. 风向
            //            [GWWeatherCurrentModel sharedWeatherCurrentModel].wind = [(GWWeatherCurrentModel *)[fetchModel.data lastObject] wind];
            [strongSelf.weatherTableView reloadData];
        }
    }];
}



@end
