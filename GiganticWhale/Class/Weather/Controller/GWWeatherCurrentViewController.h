//
//  GWWeatherCurrentViewController.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,weatherType) {
    weatherTypeCurrent = 1,                           /**< 今天天气*/
    weatherTypeNext = 2,                              /**< 未来天气*/
};

@protocol GWWeatherCurrentViewControllerDelegate <NSObject>

@optional
- (void)scrollToNextViewWithType:(weatherType)weatherType;             // 滚动到下一个页面
- (void)scrollToLastViewWithType:(weatherType)weatherType;             // 滚动到上一个页面

@end

@interface GWWeatherCurrentViewController : AbstractViewController
@property (nonatomic,assign)weatherType aboutType;                // 关于我的类型
@property (nonatomic,weak)id<GWWeatherCurrentViewControllerDelegate>delegate;            // 代理
@end
