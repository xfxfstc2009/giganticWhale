//
//  GWWeatherHistoryViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"

@interface GWWeatherHistoryViewController : AbstractViewController
@property (nonatomic,strong)UITableView *historyTableView;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;

@end
