//
//  GWWeatherCurrentViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherCurrentViewController.h"

@interface GWWeatherCurrentViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *mainTableView;

@end

@implementation GWWeatherCurrentViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createTableView];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - LCFloat(80)) style:UITableViewStylePlain];
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.contentInset = UIEdgeInsetsMake(LCFloat(80), 0, 0, 0);
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.mainTableView.showsVerticalScrollIndicator = YES;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mainTableView];
    }
//    __weak typeof(self)weakSelf = self;
//    [self.mainTableView addXQPullToRefreshWithActionHandler:^{
//        [weakSelf loadLastNews];
//    }];
//    [self.mainTableView addInfiniteScrollingWithActionHandler:^{
//        [weakSelf loadNextNews];
//    }];
}

#pragma mark- UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
        cellWithRowOne.textLabel.text = @"123";
    }
    return cellWithRowOne;
}





#pragma mark - Loading Page
- (void)loadNextNews{
    if ([self.delegate respondsToSelector:@selector(scrollToNextViewWithType:)]) {
        [self.delegate scrollToNextViewWithType:(self.aboutType + 1)];
    }
    
}
//加载上一条新闻
- (void)loadLastNews{
    if ([self.delegate respondsToSelector:@selector(scrollToLastViewWithType:)]) {
        [self.delegate scrollToLastViewWithType:(self.aboutType - 1)];
    }
}


@end
