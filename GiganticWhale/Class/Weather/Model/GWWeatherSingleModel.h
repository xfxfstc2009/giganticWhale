//
//  GWWeatherSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWWeatherCurrentModel.h"

@interface GWWeatherSingleModel : FetchModel


@property (nonatomic,strong)NSArray<GWWeatherCurrentModel>*data;


@end
