//
//  GWWeatherTodayOneCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWWeatherCurrentModel.h"

@interface GWWeatherTodayOneCell : UITableViewCell

@property (nonatomic,strong)GWWeatherCurrentModel *transferCurrentModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellheight:(GWWeatherCurrentModel *)singleModel;
@end
