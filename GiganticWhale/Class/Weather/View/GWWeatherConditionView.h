//
//  GWWeatherConditionView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWWeatherConditionView : UIView
-(id)initWithFrame:(CGRect)frame withFixed:(NSString *)fixedString dymicString:(NSString *)dymicString;
@property (nonatomic,copy)NSString *transferDymicStr;
@end
