//
//  GWWeatherView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherView.h"
#import <objc/runtime.h>
#import "GWUserCurrentInfoGetManager.h"

@interface GWWeatherView()
@property (nonatomic,strong)UILabel *weatherLabel;
@property (nonatomic,strong)UILabel *locationLabel;
@property (nonatomic,strong)UIButton *handerButton;
@end

static char weatherHanderKey;
@implementation GWWeatherView

-(instancetype)initWithFrame:(CGRect)frame withBlock:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &weatherHanderKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 2. 创建locationLabel
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.locationLabel.textColor = [UIColor whiteColor];
    self.locationLabel.textAlignment = NSTextAlignmentCenter;
    self.locationLabel.frame = CGRectMake(0,self.bounds.size.height - LCFloat(11) - [NSString contentofHeight:self.locationLabel.font], self.bounds.size.width, [NSString contentofHeight:self.locationLabel.font]);
    self.locationLabel.text = [GWUserCurrentInfoGetManager sharedHealthManager].weatherCurrentModel.city;
    [self addSubview:self.locationLabel];
    
    
    self.weatherLabel = [[UILabel alloc]init];
    self.weatherLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:33];
    
    self.weatherLabel.textColor = [UIColor whiteColor];
    self.weatherLabel.textAlignment = NSTextAlignmentCenter;
    self.weatherLabel.frame = CGRectMake(0, self.locationLabel.orgin_y - [NSString contentofHeight:self.weatherLabel.font], self.bounds.size.width, [NSString contentofHeight:self.weatherLabel.font]);
    self.weatherLabel.text = [GWUserCurrentInfoGetManager sharedHealthManager].weatherCurrentModel.temperature;
    [self addSubview:self.weatherLabel];
    

    
    // 3. 创建按钮
    self.handerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.handerButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.handerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
       void (^block)() = objc_getAssociatedObject(self , &weatherHanderKey);
        if (block){
            block();
        }
    }];
    self.handerButton.frame = self.bounds;
    [self addSubview:self.handerButton];
}

-(void)setDataWithTemp:(NSString *)temp city:(NSString *)city{
    self.locationLabel.text = city;
    self.weatherLabel.text = temp;
}
@end
