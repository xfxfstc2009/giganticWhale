//
//  GWWeatherDetailCurrentView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherDetailCurrentView.h"

@interface GWWeatherDetailCurrentView()
@property (nonatomic,strong)UILabel *cityLabel;
@property (nonatomic,strong)UILabel *airConditionLabel;      /**< 空气质量*/
@property (nonatomic,strong)UILabel *temperatureLabel;            /**< 温度*/
@property (nonatomic,strong)UILabel *humidityLabel;               /**< 湿度*/
@property (nonatomic,strong)UILabel *weatherLabel;              /**< 天气label*/
@property (nonatomic,strong)UILabel *pollutionIndexLabel;       /**< 污染指数*/

//@property (nonatomic,strong)GWWeatherConditionView *pollutionIndex;         /**< 污染指数*/
//@property (nonatomic,strong)GWWeatherConditionView *coldIndex;              /**< 感冒指数*/
//@property (nonatomic,strong)GWWeatherConditionView *dressingIndex;          /**< 穿衣指数*/
//@property (nonatomic,strong)GWWeatherConditionView *exerciseIndex;          /**< 运动指数*/
//@property (nonatomic,strong)GWWeatherConditionView *washIndex;              /**< 洗衣指数*/


@end

@implementation GWWeatherDetailCurrentView

-(instancetype)initWithFrame:(CGRect)frame withModel:(GWWeatherCurrentModel *)model{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithModel:model];
    }
    return self;
}

#pragma mark - createViee
-(void)createViewWithModel:(GWWeatherCurrentModel *)weatherModel{
    // 3. 创建city
    self.cityLabel = [[UILabel alloc]init];
    self.cityLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:25];
    self.cityLabel.backgroundColor = [UIColor clearColor];
    self.cityLabel.textAlignment = NSTextAlignmentCenter;
    self.cityLabel.textColor = [UIColor whiteColor];
    self.cityLabel.text = weatherModel.city;
    self.cityLabel.frame = CGRectMake((kScreenBounds.size.width - LCFloat(200)) / 2., LCFloat(30), LCFloat(200), [NSString contentofHeight:self.cityLabel.font]);
    [self addSubview:self.cityLabel];
    
    // 1. 创建天气
    self.weatherLabel = [[UILabel alloc]init];
    self.weatherLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.weatherLabel.backgroundColor = [UIColor clearColor];
    self.weatherLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(100), LCFloat(100), LCFloat(100), [NSString contentofHeight:self.weatherLabel.font]);
    self.weatherLabel.textAlignment = NSTextAlignmentRight;
    self.weatherLabel.text = weatherModel.weather;
    [self addSubview:self.weatherLabel];
    
    self.temperatureLabel = [[UILabel alloc]init];
    self.temperatureLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:50];
    self.temperatureLabel.backgroundColor = [UIColor clearColor];
    self.temperatureLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(100), CGRectGetMaxY(self.weatherLabel.frame), LCFloat(100), [NSString contentofHeight:self.temperatureLabel.font]);
    self.temperatureLabel.textAlignment = NSTextAlignmentRight;
    self.temperatureLabel.textColor = [UIColor whiteColor];
    self.temperatureLabel.text = weatherModel.temperature;
    [self addSubview:self.temperatureLabel];
    
    // 2. 湿度
    self.humidityLabel = [[UILabel alloc]init];
    self.humidityLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:14];
    self.humidityLabel.backgroundColor = [UIColor clearColor];
    self.humidityLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(200), CGRectGetMaxY(self.temperatureLabel.frame), LCFloat(200), [NSString contentofHeight:self.humidityLabel.font]);
    self.humidityLabel.textAlignment = NSTextAlignmentRight;
    self.humidityLabel.textColor = [UIColor whiteColor];
    self.humidityLabel.text = weatherModel.humidity;
    [self addSubview:self.humidityLabel];
    
    // 4. 创建airCondition 空气质量
    self.airConditionLabel = [[UILabel alloc]init];
    self.airConditionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15];
    self.airConditionLabel.backgroundColor = [UIColor clearColor];
    self.airConditionLabel.textAlignment = NSTextAlignmentRight;
    self.airConditionLabel.frame = CGRectMake(self.humidityLabel.orgin_x, CGRectGetMaxY(self.humidityLabel.frame) + LCFloat(10), self.humidityLabel.size_width, [NSString contentofHeight:self.airConditionLabel.font]);
    self.airConditionLabel.textColor = [UIColor whiteColor];
    self.airConditionLabel.text = [NSString stringWithFormat:@"空气质量：%@",weatherModel.airCondition];
    [self addSubview:self.airConditionLabel];
    
    // 5. 创建污染指数
    self.pollutionIndexLabel = [[UILabel alloc]init];
    self.pollutionIndexLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15];
    self.pollutionIndexLabel.backgroundColor = [UIColor clearColor];
    self.pollutionIndexLabel.textAlignment = NSTextAlignmentRight;
    self.pollutionIndexLabel.frame = CGRectMake(self.airConditionLabel.orgin_x, CGRectGetMaxY(self.airConditionLabel.frame) + LCFloat(10), self.airConditionLabel.size_width, [NSString contentofHeight:self.pollutionIndexLabel.font]);
    self.pollutionIndexLabel.textColor = [UIColor whiteColor];
    self.pollutionIndexLabel.text = [NSString stringWithFormat:@"污染指数：%@",weatherModel.pollutionIndex];
    [self addSubview:self.pollutionIndexLabel];
    
//    // 2. 创建感冒指数
//    self.coldIndex = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - kScreenBounds.size.width / 4.,kScreenBounds.size.width / 4. , kScreenBounds.size.width / 4.) withFixed:@"感冒指数" dymicString:weatherModel.coldIndex];
//    [self addSubview:self.coldIndex];
//    
//    // 3. 穿衣指数
//    self.dressingIndex = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 4., self.bounds.size.height - kScreenBounds.size.width / 4.,kScreenBounds.size.width / 4. , kScreenBounds.size.width / 4.) withFixed:@"穿衣指数" dymicString:weatherModel.dressingIndex];
//    [self addSubview:self.dressingIndex];
//    
//    // 4. 运动指数
//    self.exerciseIndex = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 4. * 2, self.bounds.size.height - kScreenBounds.size.width / 4.,kScreenBounds.size.width / 4. , kScreenBounds.size.width / 4.) withFixed:@"运动指数" dymicString:weatherModel.exerciseIndex];
//    [self addSubview:self.exerciseIndex];
//    
//    // 5. 创建洗衣指数
//    self.washIndex = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 4. * 3, self.bounds.size.height - kScreenBounds.size.width / 4.,kScreenBounds.size.width / 4. , kScreenBounds.size.width / 4.) withFixed:@"洗衣指数" dymicString:weatherModel.washIndex];
//    [self addSubview:self.washIndex];
}

@end
