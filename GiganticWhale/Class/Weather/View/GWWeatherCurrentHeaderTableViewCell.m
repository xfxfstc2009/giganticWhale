//
//  GWWeatherCurrentHeaderTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherCurrentHeaderTableViewCell.h"

@interface GWWeatherCurrentHeaderTableViewCell()
@property (nonatomic,strong)UILabel *temperatureLabel;            /**< 温度*/
@property (nonatomic,strong)UILabel *humidityLabel;               /**< 湿度*/
@property (nonatomic,strong)UILabel *weatherLabel;                /**< 天气*/
@property (nonatomic,strong)UILabel *windLabel;                   /**< 风向*/
@property (nonatomic,strong)UILabel *airConditionLabel;           /**< 空气质量*/

@property (nonatomic,strong)UILabel *coldIndexLabel;                  /**< 感冒指数*/
@property (nonatomic,strong)UILabel *dressingIndexLabel;              /**< 穿衣指数*/
@property (nonatomic,strong)UILabel *exerciseIndexLabel;              /**< 运动指数*/
@property (nonatomic,strong)UILabel *washIndexLabel;                  /**< 洗衣指数*/
@property (nonatomic,strong)UILabel *pollutionIndexLabel;             /**< 空气质量指数*/

@property (nonatomic,strong)UILabel *provinceLabel;               /**< 省份*/
@property (nonatomic,strong)UILabel *cityLabel;                   /**< 所在城市*/
//@property (nonatomic,strong)UILabel *distrctLabel;                /**< 区县*/

@property (nonatomic,strong)UILabel *dateLabel;                   /**< 时间*/
@property (nonatomic,assign)NSTimeInterval dateWithInterval;     /**< 时间戳*/



@property (nonatomic,copy)NSString *sunrise;                /**< 日出时间*/
@property (nonatomic,copy)NSString *sunset;                 /**< 日落时间*/

@property (nonatomic,copy)NSString *time;                   /**< 检测时间*/
@property (nonatomic,copy)NSString *updateTime;             /**< 更新时间*/


@property (nonatomic,strong)UILabel *weekLabel;                   /**< 星期*/

//@property (nonatomic,strong)NSArray<GWWeatherFutureModel> *future;/**< 未来*/

@end

@implementation GWWeatherCurrentHeaderTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self createView];
        self.backgroundColor = [UIColor yellowColor];
    }
    return self;
}

-(void)createView{
    // 1. 创建温度label
    self.temperatureLabel = [self customLabel];
    self.temperatureLabel.font = [UIFont systemFontOfCustomeSize:50];
    // 2. 创建湿度label
    self.humidityLabel = [self customLabel];
    // 3. 创建天气label
    self.weatherLabel = [self customLabel];
    // 4. 风向
    self.windLabel = [self customLabel];
    // 5. 空气质量
    self.airConditionLabel = [self customLabel];
    // 6.感冒指数
    self.coldIndexLabel = [self customLabel];
    // 7.穿衣指数
    self.dressingIndexLabel = [self customLabel];
    // 8.运动指数
    self.exerciseIndexLabel = [self customLabel];
    // 9. 洗衣指数
    self.washIndexLabel = [self customLabel];
    // 10.空气质量指数
    self.pollutionIndexLabel = [self customLabel];
    // 11.省
    self.provinceLabel = [self customLabel];
    // 12.市
    self.cityLabel = [self customLabel];
    // 13. 星期
    self.weekLabel = [self customLabel];
    
}

-(UILabel *)customLabel{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfCustomeSize:15.];
    [self addSubview:label];
    return label;
}

-(void)setTransferCurrentModel:(GWWeatherCurrentModel *)transferCurrentModel{
    _transferCurrentModel = transferCurrentModel;
    // 温度
    self.temperatureLabel.text = transferCurrentModel.temperature;
    CGSize temperatureSize = [transferCurrentModel.temperature sizeWithCalcFont:self.temperatureLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.temperatureLabel.font])];
    self.temperatureLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - temperatureSize.width, LCFloat(50), temperatureSize.width, [NSString contentofHeight:self.temperatureLabel.font]);
    
    // 2. 湿度
    self.humidityLabel.text = transferCurrentModel.humidity;
    CGSize humiditySize = [transferCurrentModel.humidity sizeWithCalcFont:self.humidityLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.humidityLabel.font])];
    self.humidityLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - humiditySize.width, CGRectGetMaxY(self.temperatureLabel.frame) , humiditySize.width, humiditySize.height);
    
    // 3. 天气
    self.weatherLabel.text = transferCurrentModel.weather;
    CGSize weatherSize = [transferCurrentModel.weather sizeWithCalcFont:self.weatherLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.weatherLabel.font])];
    self.weatherLabel.frame = CGRectMake(kScreenBounds.size.width - weatherSize.width - LCFloat(11), CGRectGetMaxY(self.humidityLabel.frame), weatherSize.width, [NSString contentofHeight:self.weatherLabel.font]);
    
    // 5. 空气质量
    self.airConditionLabel.text = transferCurrentModel.airCondition;
    CGSize airConditionSize = [transferCurrentModel.airCondition sizeWithCalcFont:self.airConditionLabel.font  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.airConditionLabel.font])];
    self.airConditionLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - airConditionSize.width, CGRectGetMaxY(self.weatherLabel.frame), airConditionSize.width, [NSString contentofHeight:self.airConditionLabel.font]);
    
    // 4. 风向
    self.windLabel.text = transferCurrentModel.wind;
    CGSize windSize = [transferCurrentModel.wind sizeWithCalcFont:self.weatherLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.windLabel.font])];
    self.windLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.weatherLabel.frame) + LCFloat(11), windSize.width, [NSString contentofHeight:self.windLabel.font]);
    
    // 6. 感冒指数
    self.coldIndexLabel.text = transferCurrentModel.coldIndex;
    CGSize coldIndexSize = [transferCurrentModel.coldIndex sizeWithCalcFont:self.coldIndexLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.coldIndexLabel.font])];
    self.coldIndexLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.airConditionLabel.frame) + LCFloat(11), coldIndexSize.width, [NSString contentofHeight:self.coldIndexLabel.font]);
    
    // 7 穿衣指数
    self.dressingIndexLabel.text = transferCurrentModel.dressingIndex;
    CGSize dressingSize = [self makeSizeWithLabel:self.dressingIndexLabel];
    self.dressingIndexLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.coldIndexLabel.frame) + LCFloat(11), dressingSize.width, dressingSize.height);
    
    // 8.运动指数
    self.exerciseIndexLabel.text = transferCurrentModel.exerciseIndex;
    CGSize exerciseIndexSize = [self makeSizeWithLabel:self.exerciseIndexLabel];
    self.dressingIndexLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.dressingIndexLabel.frame) + LCFloat(11), exerciseIndexSize.width, exerciseIndexSize.height);
    // 9. 洗衣指数
    self.washIndexLabel.text = transferCurrentModel.washIndex;
    CGSize washIndexSize = [self makeSizeWithLabel:self.exerciseIndexLabel];
    self.washIndexLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.exerciseIndexLabel.frame) + LCFloat(11), washIndexSize.width, washIndexSize.height);
    
    // 10.空气质量指数
    self.pollutionIndexLabel.text = transferCurrentModel.pollutionIndex;
    CGSize pollutionIndexSize = [self makeSizeWithLabel:self.pollutionIndexLabel];
    self.pollutionIndexLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.washIndexLabel.frame) + LCFloat(11), pollutionIndexSize.width, pollutionIndexSize.height);
    
    // 11.省
    self.provinceLabel.text = transferCurrentModel.province;
    CGSize provinceSize = [self makeSizeWithLabel:self.provinceLabel];
    self.provinceLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.pollutionIndexLabel.frame) + LCFloat(11), provinceSize.width, provinceSize.height);
    
    // 12.市
    self.cityLabel.text = transferCurrentModel.province;
    CGSize citySize = [self makeSizeWithLabel:self.cityLabel];
    self.cityLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.provinceLabel.frame) + LCFloat(11), citySize.width, citySize.height);
    
    // 13. 星期
    self.weekLabel.text = transferCurrentModel.week;
    CGSize weekSize = [self makeSizeWithLabel:self.weekLabel];
    self.weekLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.cityLabel.frame) + LCFloat(11), weekSize.width, weekSize.height);
}


-(CGSize)makeSizeWithLabel:(UILabel *)label{
    CGSize contentOfSize = [label.text sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:label.font])];
    return CGSizeMake(contentOfSize.width, [NSString contentofHeight:label.font]);
}



+(CGFloat)calculationCellHeight{
    return LCFloat(400);
}

@end
