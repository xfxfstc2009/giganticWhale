//
//  GWWeatherConditionView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherConditionView.h"

@interface GWWeatherConditionView()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation GWWeatherConditionView

-(id)initWithFrame:(CGRect)frame withFixed:(NSString *)fixedString dymicString:(NSString *)dymicString{
    self = [super initWithFrame:frame];
    if(self){
        [self createViewWithFixedString:fixedString dymicString:dymicString];
    }
    return self;
}

-(void)createViewWithFixedString:(NSString *)fixed dymicString:(NSString *)dymic{
    self.fixedLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.fixedLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.dymicLabel];
    
    self.fixedLabel.text = fixed;
    self.dymicLabel.text = dymic;
    
    CGFloat margin = (self.size_height - [NSString contentofHeight:self.fixedLabel.font] - [NSString contentofHeight:self.dymicLabel.font]) / 3.;
    self.fixedLabel.frame = CGRectMake(0, margin, self.size_width, [NSString contentofHeight:self.fixedLabel.font]);
    
    self.dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.fixedLabel.frame) + margin, self.size_width, [NSString contentofHeight:self.dymicLabel.font]);
}

-(void)setTransferDymicStr:(NSString *)transferDymicStr{
    _transferDymicStr = transferDymicStr;
    self.dymicLabel.text = transferDymicStr;
}

@end
