//
//  GWWeatherView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWWeatherView : UIView

-(instancetype)initWithFrame:(CGRect)frame withBlock:(void(^)())block;

-(void)setDataWithTemp:(NSString *)temp city:(NSString *)city;
@end
