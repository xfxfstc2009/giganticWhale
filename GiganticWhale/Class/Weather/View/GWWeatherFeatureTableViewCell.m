//
//  GWWeatherFeatureTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherFeatureTableViewCell.h"

@interface GWWeatherFeatureTableViewCell()
@property (nonatomic,strong)UILabel *dateLabel;
@property (nonatomic,strong)UILabel *dayTimeLabel;
@property (nonatomic,strong)UILabel *nightLabel;
@property (nonatomic,strong)UILabel *temperatureLabel;
@property (nonatomic,strong)UILabel *weekLabel;
@property (nonatomic,strong)UILabel *windLabel;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIVisualEffectView *effectview;

@end

@implementation GWWeatherFeatureTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{\
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建view
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    self.effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    self.effectview.alpha = .8f;
    [self addSubview:self.effectview];
    
    // 1. 创建日期
    self.dateLabel = [self customLabel];
    // 2. 创建dayTime
    self.dayTimeLabel = [self customLabel];
    // 3. 创建night
    self.nightLabel = [self customLabel];
    // 4. 创建温度
    self.temperatureLabel = [self customLabel];
}

-(void)setCellheight:(CGFloat)cellheight{
    _cellheight = cellheight ;
    self.effectview.frame = CGRectMake(0, 0, kScreenBounds.size.width, cellheight);
}


-(void)setTransferFutureModel:(GWWeatherFutureModel *)transferFutureModel{
    _transferFutureModel = transferFutureModel;
    // 1. 日期
    self.dateLabel.text = transferFutureModel.date;
    CGSize dateSize = [GWTool makeSizeWithLabel:self.dateLabel];
    self.dateLabel.frame = CGRectMake(LCFloat(11), LCFloat(3), dateSize.width, self.cellheight);
    
    // 4. 创建温度
    self.temperatureLabel.text = transferFutureModel.temperature;
    CGSize temperatureSize = [GWTool makeSizeWithLabel:self.temperatureLabel];
    self.temperatureLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.dayTimeLabel.frame) , temperatureSize.width, temperatureSize.height);
    self.temperatureLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - temperatureSize.width, LCFloat(3), temperatureSize.width, self.cellheight);
    
    // 2. 创建dayTime
    self.dayTimeLabel.text = [NSString stringWithFormat:@"%@  %@",transferFutureModel.dayTime,transferFutureModel.night];
//    CGSize dayTimeSie = [GWTool makeSizeWithLabel:self.dayTimeLabel];
    self.dayTimeLabel.frame = CGRectMake(CGRectGetMaxY(self.dateLabel.frame) + LCFloat(11), LCFloat(3), self.temperatureLabel.orgin_x - CGRectGetMaxY(self.dateLabel.frame), self.cellheight);
    self.dayTimeLabel.textAlignment = NSTextAlignmentCenter;
}

-(UILabel *)customLabel{
    UILabel *label = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    [self.effectview addSubview:label];
    return label;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end