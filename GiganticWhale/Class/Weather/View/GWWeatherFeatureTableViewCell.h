//
//  GWWeatherFeatureTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWWeatherFutureModel.h"
@interface GWWeatherFeatureTableViewCell : UITableViewCell

@property (nonatomic,strong)GWWeatherFutureModel *transferFutureModel;
@property (nonatomic,assign)CGFloat cellheight;

+(CGFloat)calculationCellHeight;
@end
