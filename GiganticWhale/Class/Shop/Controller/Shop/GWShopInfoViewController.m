//
//  GWShopInfoViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 【商店详情】
#import "GWShopInfoViewController.h"

@interface GWShopInfoViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *shopInfoTableView;
@property (nonatomic,strong)NSMutableArray *shopInfoArr;

@end

@implementation GWShopInfoViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"GiganticWhale的店";
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.shopInfoArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.shopInfoTableView){
        self.shopInfoTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.shopInfoTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.shopInfoTableView.delegate = self;
        self.shopInfoTableView.dataSource = self;
        self.shopInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.shopInfoTableView.backgroundColor = [UIColor clearColor];
        self.shopInfoTableView.backgroundView = nil;
        self.shopInfoTableView.bounces = YES;
        self.shopInfoTableView.opaque = NO;
        [self.view addSubview:self.shopInfoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.shopInfoArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.shopInfoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@""] && indexPath.row == [self cellIndexPathRowWithcellData:@""]){
    
    } else {
        
    }
    return nil;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.shopInfoArr.count ; i++){
        NSArray *dataTempArr = [self.shopInfoArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.shopInfoArr.count ; i++){
        NSArray *dataTempArr = [self.shopInfoArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}
@end
