//
//  GWShopDetailViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 商品详情
#import "GWShopDetailViewController.h"
#import "GWShopProductCollectionCell.h"
#import "GWShopProductCollectionHeaderView.h"
#import "GWShopProductCollectionHeaderView.h"



@interface GWShopDetailViewController()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *shopCollectionView;
@property (nonatomic,strong)NSMutableArray *shopCollectionArr;

@end

static NSString *GWProductsCollectionHeaderViewIdentifier = @"GWProductsCollectionHeaderViewIdentifier";

@implementation GWShopDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"店";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.shopCollectionArr = [NSMutableArray array];
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    
    self.shopCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.shopCollectionView.showsVerticalScrollIndicator = NO;
    self.shopCollectionView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.shopCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
    self.shopCollectionView.delegate = self;
    self.shopCollectionView.dataSource = self;
    self.shopCollectionView.scrollsToTop = YES;
    self.shopCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    // 注册 Cell - collectionIdentify
    [self.shopCollectionView registerClass:[GWShopProductCollectionCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
//    // header
    [self.shopCollectionView registerClass:[GWShopProductCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:GWProductsCollectionHeaderViewIdentifier];
    
    [self.view addSubview:self.shopCollectionView];

}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 100;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GWShopProductCollectionCell *cell = (GWShopProductCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, LCFloat(10), 5, LCFloat(10));
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize itemSize = CGSizeMake((kScreenBounds.size.width - LCFloat(35)) / 2.,(kScreenBounds.size.width - LCFloat(35)) / 2.);
    return itemSize;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,200};
    return size;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    GWShopProductCollectionHeaderView *headView;
    
    if([kind isEqual:UICollectionElementKindSectionHeader]) {
        headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:GWProductsCollectionHeaderViewIdentifier forIndexPath:indexPath];
    }
    return headView;
}

// didselect
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
}
@end
