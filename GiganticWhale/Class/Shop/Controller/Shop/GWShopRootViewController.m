//
//  GWShopRootViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShopRootViewController.h"
#import "GWShopShopSingleCell.h"
#import "GWShopInfoAddViewController.h"                     // 添加
#import "GWShopDetailViewController.h"


@interface GWShopRootViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *shopRootTableView;                 /**< 商店列表*/
@property (nonatomic,strong)NSMutableArray *shopMutableArr;                 /**< 商店数组*/

@end

@implementation GWShopRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self test];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"GiganticWhale 朋友们的店";
    [self rightBarButtonWithTitle:@"23" barNorImage:nil barHltImage:nil action:^{
        GWShopInfoAddViewController *vc = [[GWShopInfoAddViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.shopMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.shopRootTableView){
        self.shopRootTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.shopRootTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.shopRootTableView.delegate = self;
        self.shopRootTableView.dataSource = self;
        self.shopRootTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.shopRootTableView.backgroundColor = [UIColor clearColor];
        self.shopRootTableView.backgroundView = nil;
        self.shopRootTableView.bounces = YES;
        self.shopRootTableView.opaque = NO;
        [self.view addSubview:self.shopRootTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.shopRootTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
    }];
    
    [self.shopRootTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.shopMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.shopMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWShopShopSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWShopShopSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
        
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferShopSingleModel = [[self.shopMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    GWShopSingleModel *singleModel = [[self.shopMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    return [GWShopShopSingleCell calculationCellHeightWithModel:singleModel];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(15);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GWShopDetailViewController *shopInfoViewController = [[GWShopDetailViewController alloc]init];
    [self.navigationController pushViewController:shopInfoViewController animated:YES];
}

-(void)test{
    NSMutableArray *tempArr = [NSMutableArray array];
    for (int i = 0 ;i < 5; i++){
        GWShopSingleModel *shopSingleModel = [[GWShopSingleModel alloc]init];
        shopSingleModel.shopName = @"商店名字";
        shopSingleModel.customerName = @"裴烨风";
        shopSingleModel.location = @"浙江省杭州市萧山区";
        shopSingleModel.shopLat = @"13213.";
        shopSingleModel.shopLng = @"123";
        if (i % 2 == 0){
            shopSingleModel.shopBanner = @[@"http://img9.5sing.kgimg.com/force/T19YWTBQYT1RXrhCrK.jpg"];
        } else {
            shopSingleModel.shopBanner = @[@"http://img9.5sing.kgimg.com/force/T19YWTBQYT1RXrhCrK.jpg",@"http://img9.5sing.kgimg.com/force/T19YWTBQYT1RXrhCrK.jpg"];
        }
        [tempArr addObject:shopSingleModel];
    }
    [self.shopMutableArr addObject:tempArr];
    [self.shopRootTableView reloadData];
}
@end
