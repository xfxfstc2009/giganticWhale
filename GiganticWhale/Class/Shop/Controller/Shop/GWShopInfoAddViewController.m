//
//  GWShopInfoAddViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShopInfoAddViewController.h"

@interface GWShopInfoAddViewController()<UITableViewDelegate,UITableViewDataSource,GWSelectedImgTableViewCellDelegate>{
    UITextField *nameTextField;
    UITextField *descTextField;
    
}
@property (nonatomic,strong)UITableView *shopTableView;
@property (nonatomic,strong)NSMutableArray *shopMutableArr;
@property (nonatomic,strong)NSMutableArray *selectedShopImgArr;             /**< 选中的图片*/
@property (nonatomic,strong)NSMutableArray *selectedAvatarImgArr;           /**< 选中的图片*/
@property (nonatomic,strong)NSMutableArray *selectedWeChatImgArr;           /**< 选中的微信图片*/
@end

@implementation GWShopInfoAddViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加商店";
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        [self sendRequestToUploadInfo];
    }];
}

-(void)arrayWithInit{
    self.shopMutableArr = [NSMutableArray array];
    NSArray *tempArr = @[@[@"商店名称",@"商店简介"],@[@"上传商店头像"],@[@"上传商店图片"],@[@"商店地址"],@[@"电话",@"QQ",@"微信",@"微信名片"],@[@"提交"]];
    [self.shopMutableArr addObjectsFromArray:tempArr];
    
    // 商店选中数组
    self.selectedShopImgArr = [NSMutableArray array];
    self.selectedAvatarImgArr = [NSMutableArray array];
    self.selectedWeChatImgArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.shopTableView){
        self.shopTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.shopTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.shopTableView.delegate = self;
        self.shopTableView.dataSource = self;
        self.shopTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.shopTableView.backgroundColor = [UIColor clearColor];
        self.shopTableView.backgroundView = nil;
        self.shopTableView.bounces = YES;
        self.shopTableView.opaque = NO;
        [self.view addSubview:self.shopTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.shopMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.shopMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店名称"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店名称"]){
        static NSString *cellIdengifyWithRowOne = @"cellIdengifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transfrCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"商店名称";
        cellWithRowOne.transferPlaceholeder = @"请输入商店名称";
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店简介"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店简介"]){
        static NSString *cellIdengifyWithRowTwo = @"cellIdengifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transfrCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"商店简介";
        cellWithRowTwo.transferPlaceholeder = @"请输入商店简介";
        
        return cellWithRowTwo;
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传商店图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"上传商店图片"]){
        static NSString *cellIdengifyWithRowThr = @"cellIdengifyWithRowThr";
        GWSelectedImgTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWSelectedImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowThr];
            cellWithRowThr.delegate = self;
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferSelectedImgArr = self.selectedShopImgArr;
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店地址"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店地址"]){
        static NSString *cellIdengifyWithRowFour = @"cellIdengifyWithRowFour";
        GWInputTextFieldTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transfrCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"商店地址";
        cellWithRowFour.transferPlaceholeder = @"请输入商店地址";
        
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交"] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交"]){
        static NSString *cellIdengifyWithRowFiv = @"cellIdengifyWithRowFiv";
        GWButtonTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        cellWithRowFiv.transferTitle = @"提交";
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传商店头像"] && indexPath.row == [self cellIndexPathRowWithcellData:@"上传商店头像"]){
        static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
        GWSelectedImgTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
        if (!cellWithRowSex){
            cellWithRowSex = [[GWSelectedImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
            cellWithRowSex.delegate = self;
            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSex.transferSelectedImgArr = self.selectedAvatarImgArr;
        return cellWithRowSex;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"QQ"] && indexPath.row == [self cellIndexPathRowWithcellData:@"QQ"]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信"] && indexPath.row == [self cellIndexPathRowWithcellData:@"微信"]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"电话"] && indexPath.row == [self cellIndexPathRowWithcellData:@"电话"])){
        static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
        GWInputTextFieldTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
        if (!cellWithRowSev){
            cellWithRowSev = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
            cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSev.transfrCellHeight = cellHeight;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"QQ"]){
            cellWithRowSev.transferTitle = @"QQ";
            cellWithRowSev.transferPlaceholeder = @"请输入QQ号";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"微信"]){
            cellWithRowSev.transferTitle = @"微信";
            cellWithRowSev.transferPlaceholeder = @"请输入微信号";
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"电话"]){
            cellWithRowSev.transferTitle = @"电话";
            cellWithRowSev.transferPlaceholeder = @"请输入电话号";
        }
        
        return cellWithRowSev;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信名片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"微信名片"]){
        static NSString *cellIdengifyWithRowEig = @"cellIdengifyWithRowEig";
        GWSelectedImgTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowEig];
        if (!cellWithRowEig){
            cellWithRowEig = [[GWSelectedImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowEig];
            cellWithRowEig.delegate = self;
            cellWithRowEig.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowEig.transferSelectedImgArr = self.selectedWeChatImgArr;
        
        return cellWithRowEig;
    }
    else {
        static NSString *cellIdengifyWithRowSex = @"cellIdengifyWithRowSex";
        UITableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowSex];
        if (!cellWithRowSex){
            cellWithRowSex = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowSex];
            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowSex;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(20));
    if (section == [self cellIndexPathSectionWithcellData:@"商店名称"]){
        fixedLabel.text = @"商店信息";
    } else if (section == [self cellIndexPathSectionWithcellData:@"上传商店头像"]){
        fixedLabel.text = @"上传商店头像";
    } else if (section == [self cellIndexPathSectionWithcellData:@"上传商店图片"]){
        fixedLabel.text = @"上传商店图片";
    } else if (section == [self cellIndexPathSectionWithcellData:@"商店地址"]){
        fixedLabel.text = @"商店地址";
    } else if (section == [self cellIndexPathSectionWithcellData:@"电话"]){
        fixedLabel.text = @"联系方式";
    }
    [headerView addSubview:fixedLabel];
    
//    @[@[@"商店名称",@"商店简介"],@[@"上传商店头像"],@[@"上传商店图片"],@[@"商店地址"],@[@"电话",@"QQ",@"微信",@"微信名片"],@[@"提交"]];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店名称"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店名称"]){
        return LCFloat(44);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店简介"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店简介"]){
        return LCFloat(44);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传商店图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"上传商店图片"]){
        return [GWSelectedImgTableViewCell calculationCellHeightWithImgArr:self.selectedShopImgArr];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商店地址"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商店地址"]){
        return 50;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提交"] && indexPath.row == [self cellIndexPathRowWithcellData:@"提交"]){
        return LCFloat(50);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传商店头像"] && indexPath.row == [self cellIndexPathRowWithcellData:@"上传商店头像"] ){
        return [GWSelectedImgTableViewCell calculationCellHeightWithImgArr:self.selectedAvatarImgArr];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信名片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"微信名片"]){
        return [GWSelectedImgTableViewCell calculationCellHeightWithImgArr:self.selectedWeChatImgArr];
    } else {
        return LCFloat(50);
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"提交"]){
        return LCFloat(50);
    } else {
        return 0;
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.shopTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.shopMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.shopMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - ImgSelectDelegate
-(void)imageSelectedButtonClick:(GWSelectedImgTableViewCell *)cell{            // 图片选择
    GWSelectedImgTableViewCell * shopDetailCell = [self.shopTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"上传商店图片"] inSection:[self cellIndexPathSectionWithcellData:@"上传商店图片"]]];
    GWSelectedImgTableViewCell *shopAvatarCell = [self.shopTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"上传商店头像"] inSection:[self cellIndexPathSectionWithcellData:@"上传商店头像"]]];
    GWSelectedImgTableViewCell *shopWechatCell = [self.shopTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"微信名片"] inSection:[self cellIndexPathSectionWithcellData:@"微信名片"]]];
    
    // 添加图片
    NSInteger selectedImgCount = 0;
    if (cell == shopDetailCell){                                           // 上传商店图片banner
        selectedImgCount = 1 - self.selectedShopImgArr.count;
    } else if (cell == shopAvatarCell){                                    // 上传商店头像
        selectedImgCount = 6 - self.selectedAvatarImgArr.count;
    } else if (cell == shopWechatCell){
        selectedImgCount = 1 - self.selectedWeChatImgArr.count;
    }
    
    GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:selectedImgCount andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (cell == shopDetailCell){            // 上传商店图片banner
            [strongSelf.selectedShopImgArr addObjectsFromArray:selectedImgArr];
            // 刷新行
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"上传商店图片"] inSection:[self cellIndexPathSectionWithcellData:@"上传商店图片"]];
            [strongSelf.shopTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        } else if (cell == shopAvatarCell){
            [strongSelf.selectedAvatarImgArr addObjectsFromArray:selectedImgArr];
            // 刷新行
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"上传商店头像"] inSection:[self cellIndexPathSectionWithcellData:@"上传商店头像"]];
            [strongSelf.shopTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        } else if (cell == shopWechatCell){
            [strongSelf.selectedWeChatImgArr addObjectsFromArray:selectedImgArr];
            // 刷新行
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"微信名片"] inSection:[self cellIndexPathSectionWithcellData:@"微信名片"]];
            [strongSelf.shopTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
    [self.navigationController pushViewController:assetLibraryViewController animated:YES];

}

-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(GWSelectedImgTableViewCell *)cell{

}





#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.shopMutableArr.count ; i++){
        NSArray *dataTempArr = [self.shopMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.shopMutableArr.count ; i++){
        NSArray *dataTempArr = [self.shopMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark  tap 方法
-(void)tapManager{
    
}

#pragma mark - 通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    _shopTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    _shopTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Interface
-(void)sendRequestToUploadInfo{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue: forKey:@"name"];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:shop_add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        
    }];
}

@end
