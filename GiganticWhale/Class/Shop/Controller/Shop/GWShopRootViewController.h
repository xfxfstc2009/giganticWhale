//
//  GWShopRootViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 【商店列表】
#import "AbstractViewController.h"

@interface GWShopRootViewController : AbstractViewController

@end
