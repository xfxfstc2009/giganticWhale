//
//  GWProductsAddViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWProductsAddViewController.h"

@interface GWProductsAddViewController()
@property (nonatomic,strong)UITableView *productAddTableView;

@end

@implementation GWProductsAddViewController

-(void)viewDidLoad{
    [super viewDidLoad];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加商品";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    
}

@end
