//
//  GWShopSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWShopSingleModel <NSObject>

@end

@interface GWShopSingleModel : FetchModel

@property (nonatomic,copy)NSString *shopName;                       /**< 商店名字*/
@property (nonatomic,copy)NSString *customerName;                   /**< 商店人员名字*/
@property (nonatomic,copy)NSString *location;                       /**< 商店地址*/
@property (nonatomic,copy)NSString *shopLat;                        /**< 商店lat*/
@property (nonatomic,copy)NSString *shopLng;                        /**< 商店lng*/
@property (nonatomic,strong)NSArray *shopBanner;                    /**< 商店banner*/
@property (nonatomic,copy)NSString *shopDesc;                       /**< 商店详情*/

@end
