//
//  GWShopShopSingleCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWShopSingleModel.h"

@interface GWShopShopSingleCell : UITableViewCell

@property (nonatomic,strong)GWShopSingleModel *transferShopSingleModel;             /**< 传入的model*/

@property (nonatomic,assign)CGFloat transferCellHeight;  /**< 传入高度*/

+(CGFloat)calculationCellHeightWithModel:(GWShopSingleModel *)shopSingleModel;

@end
