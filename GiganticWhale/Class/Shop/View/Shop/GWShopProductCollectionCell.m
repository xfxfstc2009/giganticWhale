//
//  GWShopProductCollectionCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShopProductCollectionCell.h"

@interface GWShopProductCollectionCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)GWImageView *proImgView;
@property (nonatomic,strong)UILabel *productNameLabel;                      /**< 商品名字*/
@property (nonatomic,strong)GWImageView *alphaImgView;                      /**< 阴影*/

@end

@implementation GWShopProductCollectionCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建背景
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.frame = self.bounds;
    [self addSubview:self.bgView];
    
    // 2. 创建图片
    self.proImgView = [[GWImageView alloc]init];
    self.proImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.proImgView];
    
    // 3. 创建阴影
    self.alphaImgView = [[GWImageView alloc]init];
    self.alphaImgView.backgroundColor = [UIColor clearColor];
    self.alphaImgView.frame = CGRectMake(0, self.size_height - LCFloat(19), self.size_width, LCFloat(19));
    self.alphaImgView.image = [UIImage imageNamed:@"pro_bg_blackshadow"];
    [self addSubview:self.alphaImgView];
    
    // 4. 创建文字
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.productNameLabel.textColor = [UIColor whiteColor];
    self.productNameLabel.text = @"哈哈哈";
    self.productNameLabel.frame = CGRectMake(LCFloat(5), self.size_height - [NSString contentofHeight:self.productNameLabel.font], self.size_width - 2 * LCFloat(5), [NSString contentofHeight:self.productNameLabel.font]);
    [self addSubview:self.productNameLabel];
}


@end
