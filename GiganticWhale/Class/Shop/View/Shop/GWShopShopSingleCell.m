//
//  GWShopShopSingleCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShopShopSingleCell.h"
#import "PDScrollView.h"

@interface GWShopShopSingleCell()
@property (nonatomic,strong)GWImageView *shopBannerImgView;                     /**< banner的图片*/
@property (nonatomic,strong)UILabel *shopNameLabel;                             /**< 商店名字 label*/
@property (nonatomic,strong)UILabel *shopLocationLabel;                         /**< 商店地址 label*/
@property (nonatomic,strong)UILabel *shopDescLabel;                             /**< 商店详情 label*/
@property (nonatomic,strong)UILabel *customerNameLabel;                         /**< 商店经营者名字 label*/
@property (nonatomic,strong)PDScrollView *mainScrollView;           /**< 学校图片*/
@end

@implementation GWShopShopSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    GWImageView *bgView = [[GWImageView alloc]init];
    self.backgroundView.userInteractionEnabled = YES;
    bgView.image = [GWTool stretchImageWithName:@"common_card_background"];
    self.backgroundView = bgView;
    
    // 2. 创建图片
    self.shopBannerImgView = [[GWImageView alloc]init];
    self.shopBannerImgView.backgroundColor = [UIColor clearColor];
    self.shopBannerImgView.userInteractionEnabled = YES;
    [self addSubview:self.shopBannerImgView];
    
    // 2.1 创建mainScrollView
    self.mainScrollView = [[PDScrollView alloc]initWithFrame:CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200))];
    self.mainScrollView.isPageControl = YES;
    self.mainScrollView.layer.cornerRadius = LCFloat(9);
    self.mainScrollView.clipsToBounds = YES;
    [self addSubview:self.mainScrollView];

    // 3. 创建商店名字
    self.shopNameLabel = [[UILabel alloc]init];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    self.shopNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.shopNameLabel.numberOfLines = 0;
    [self addSubview:self.shopNameLabel];
    
    // 4. 创建商店地址
    self.shopLocationLabel = [[UILabel alloc]init];
    self.shopLocationLabel.backgroundColor = [UIColor clearColor];
    self.shopLocationLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.shopLocationLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferShopSingleModel:(GWShopSingleModel *)transferShopSingleModel{
    _transferShopSingleModel = transferShopSingleModel;
    
    // 1. banner
    if (transferShopSingleModel.shopBanner.count == 1){         // 【显示图片】
        self.shopBannerImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
        self.shopBannerImgView.layer.cornerRadius = LCFloat(9);
        self.shopBannerImgView.clipsToBounds = YES;
        [self.shopBannerImgView uploadImageWithURL:[transferShopSingleModel.shopBanner lastObject] placeholder:nil callback:NULL];
    } else {                                                    // 【显示banner】
        // 3. 创建图片墙
        NSMutableArray *imgTempArr = [NSMutableArray array];
        for (int i = 0 ; i < transferShopSingleModel.shopBanner.count;i++){
            GWScrollViewSingleModel *scrollViewSingleMode = [[GWScrollViewSingleModel alloc]init];
            scrollViewSingleMode.img = [transferShopSingleModel.shopBanner objectAtIndex:i];
            [imgTempArr addObject:scrollViewSingleMode];
        }
        self.mainScrollView.transferImArr = imgTempArr;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11),LCFloat(200));
        if (imgTempArr.count){
           [self.mainScrollView startTimerWithTime:8];
        }
    }
    
    // 2. 商店名字
    self.shopNameLabel.text = transferShopSingleModel.shopName;
    CGSize shopNameSize = [self.shopNameLabel.text sizeWithCalcFont:self.shopNameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.shopNameLabel.frame = CGRectMake(LCFloat(11),transferShopSingleModel.shopBanner.count > 1 ?CGRectGetMaxY(self.mainScrollView.frame) + LCFloat(11) : CGRectGetMaxY(self.shopBannerImgView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), shopNameSize.height);
    
    // 3. 创建商店详情
    self.shopLocationLabel.text = transferShopSingleModel.location;
    CGSize shopDescSize = [self.shopLocationLabel.text sizeWithCalcFont:self.shopLocationLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11),CGFLOAT_MAX)];
    self.shopLocationLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.shopNameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), shopDescSize.height);
    
}


+(CGFloat)calculationCellHeightWithModel:(GWShopSingleModel *)shopSingleModel{
    CGFloat cellHeight = 0;
    
    cellHeight += LCFloat(11);
    // 1. 图片
    cellHeight += LCFloat(200);
    // 2. 商店名字
    CGSize contentOfShopSize = [shopSingleModel.shopName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width, CGFLOAT_MAX)];
    cellHeight += LCFloat(11);
    cellHeight += contentOfShopSize.height;
    // 3. 商店地址
    cellHeight += LCFloat(11);
    CGSize contentOfShopLocationSize = [shopSingleModel.location sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += contentOfShopLocationSize.height;
    cellHeight += LCFloat(11);
    
    return cellHeight;
}

@end
