//
//  GWShopProductCollectionHeaderView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShopProductCollectionHeaderView.h"

@interface GWShopProductCollectionHeaderView()
@property (nonatomic,strong)GWImageView *avatar;                /**< 头像*/
@property (nonatomic,strong)UILabel *shopNameLabel;             /**< 商店名字*/
@property (nonatomic,strong)UILabel *contentLabel;              /**< 商品详情*/

@end

@implementation GWShopProductCollectionHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. avatar
    self.avatar = [[GWImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.frame = CGRectMake((kScreenBounds.size.width - LCFloat(150)) / 2., LCFloat(20), LCFloat(150), LCFloat(150));
    self.avatar.clipsToBounds = YES;
    self.avatar.image = [UIImage imageNamed:@"player_btn_pause_highlight"];
    self.avatar.layer.cornerRadius = self.avatar.size_width / 2.;
    [self addSubview:self.avatar];
    
    // 2. 创建名字
    self.shopNameLabel = [[UILabel alloc]init];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    self.shopNameLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"]boldFont];
    self.shopNameLabel.textAlignment = NSTextAlignmentCenter;
    self.shopNameLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.avatar.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeight:self.shopNameLabel.font]);
    [self addSubview:self.shopNameLabel];
    
    // 3. 创建详情
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.contentLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.contentLabel];
}

@end
