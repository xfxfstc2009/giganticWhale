//
//  GWShopProductCollectionCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 2016/10/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 商店商品cell
#import <UIKit/UIKit.h>

@interface GWShopProductCollectionCell : UICollectionViewCell

@end
