//
//  GWScanningModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWScanningDataModel.h"
@interface GWScanningModel : FetchModel
@property (nonatomic,copy)NSString *identify;                   /**< 唯一编码*/
@property (nonatomic,assign)NSInteger type;                     /**< 扫码类型*/
@property (nonatomic,strong)GWScanningDataModel *data;          /**< data*/

@end
