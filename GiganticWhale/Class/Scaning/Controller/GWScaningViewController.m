//
//  GWScaningViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWScaningViewController.h"
#import <ZBarSDK/ZBarSDK.h>
#import "ZBarCameraSimulator.h"
#import "GWScanningModel.h"
#import "GWAssetsLibraryViewController.h"

@interface GWScaningViewController()<ZBarReaderViewDelegate,ZBarReaderDelegate,UITextFieldDelegate>
// animation
@property (nonatomic,assign)NSInteger num;                                          /**< 动画作用的num*/
@property (nonatomic,assign)BOOL upOrDown;                                          /**< 判断是上升还是下降*/
@property (nonatomic,strong)NSTimer *timer;                                         /**< 计时器*/
// view
@property (nonatomic,strong)UIImageView *scanningTopBackgroundImageView;            /**< 顶部的view*/
@property (nonatomic,strong)UIImageView *scanningBottomBackgroundImageView;         /**< 底部的view*/
@property (nonatomic,strong)UIImageView *scanningMiddeMiddleBackgroundImageView;    /**< 中间的view*/
@property (nonatomic,strong)UIButton *inputBarCodeButton;
@property (nonatomic,strong)UIButton *lightButton;                                   /**< 灯光按钮*/
@property (nonatomic,strong)UIButton *chooseAssetButton;                            /**< 选择按钮*/
@property (nonatomic,strong)UIView *viewLine;                                       /**< 动画的view*/
@property (nonatomic,strong)UITextField *inputBarCodeTextField;                     /**< 输入框*/

@property (nonatomic,assign)BOOL isShowAll;                                         /**< 是否显示全部*/
@property (nonatomic,assign)NSInteger closeCameraTime;                              /**< 关闭照相机时间*/
@property (nonatomic,strong)NSTimer *closeCameraTimer;                              /**< 关闭照相机*/
@property (nonatomic,assign)BOOL isRedirect;                                        /**< Bool*/

@property (nonatomic,strong)UIButton *openDrawerButton;                             /**< 菜单按钮*/
@property (nonatomic,strong)ZBarReaderView *readerView;
@end

@implementation GWScaningViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.readerView){
        [self.readerView start];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self arrayWithInit];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    if (self.closeCameraTimer){
        [self.closeCameraTimer invalidate];
        self.closeCameraTimer = nil;
    }
}

-(void)dealloc{
    NSLog(@"释放了?");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createZBar];                          // 创建zbar
    [self createNavBar];                        // 创建顶部的 view
    [self createView];
    [self createSliderButton];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.num = 0;
    self.upOrDown = NO;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:.02f target:self selector:@selector(animation) userInfo:nil repeats:YES];
    if (self.closeCameraTimer){
        self.closeCameraTime = 60;
        self.closeCameraTimer = [NSTimer scheduledTimerWithTimeInterval:self.closeCameraTime target:self selector:@selector(closeCameraAnimation) userInfo:nil repeats:YES];
    }
}

#pragma mark - CreateView
-(void)createView{
    
    // 创建输入条形码按钮
    self.inputBarCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.inputBarCodeButton.backgroundColor = [UIColor clearColor];
    [self.inputBarCodeButton addTarget:self action:@selector(inputBarCodeClick) forControlEvents:UIControlEventTouchUpInside];
    [self.inputBarCodeButton setTitle:@"输入条形码" forState:UIControlStateNormal];
    if (kScreenBounds.size.height >= 480){
        self.inputBarCodeButton.frame = CGRectMake(LCFloat(40), kScreenBounds.size.height - LCFloat(85) - LCFloat(40), LCFloat(125), LCFloat(40));
    }
    [self.view addSubview:self.inputBarCodeButton];
    self.inputBarCodeButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
    self.inputBarCodeButton.layer.borderWidth = 1;
    self.inputBarCodeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.inputBarCodeButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
    [self.inputBarCodeButton makeRoundedRectangleShape];
    
    // 1. 创建light button
    self.lightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.lightButton.backgroundColor = [UIColor clearColor];
    [self.lightButton setImage:[UIImage imageNamed:@"scan-code_btn_flashlight"] forState:UIControlStateNormal];
    [self.lightButton addTarget:self action:@selector(lightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    if (kScreenBounds.size.height <= 480){
        self.lightButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(51)) / 2. , self.scanningBottomBackgroundImageView.frame.origin.y + LCFloat(32), LCFloat(51), LCFloat(51));
        
        self.inputBarCodeButton.frame = CGRectMake(LCFloat(40), CGRectGetMaxY(self.lightButton.frame) + LCFloat(32), LCFloat(125), LCFloat(40));
    } else {
        self.lightButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(51)) / 2. , CGRectGetMinY(self.inputBarCodeButton.frame) - LCFloat(32) - LCFloat(51), LCFloat(51), LCFloat(51));
    }
    self.lightButton.stringTag = @"lightButton";
    [self.view addSubview:self.lightButton];
    
    // 创建选择相册按钮
    self.chooseAssetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chooseAssetButton.backgroundColor = [UIColor clearColor];
    [self.chooseAssetButton addTarget:self action:@selector(chooseAlAssetClick) forControlEvents:UIControlEventTouchUpInside];
    [self.chooseAssetButton setTitle:@"选择相册按钮" forState:UIControlStateNormal];
    self.chooseAssetButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(40) - LCFloat(125), self.inputBarCodeButton.frame.origin.y, LCFloat(125), LCFloat(40));
    [self.view addSubview:self.chooseAssetButton];
    self.chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
    self.chooseAssetButton.layer.borderWidth = 1;
    self.chooseAssetButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
    [self.chooseAssetButton makeRoundedRectangleShape];
    
    self.inputBarCodeTextField = [[UITextField alloc]init];
    self.inputBarCodeTextField.backgroundColor = [UIColor whiteColor];
    self.inputBarCodeTextField.placeholder = @"请输入条形码";
    self.inputBarCodeTextField.delegate = self;
    self.inputBarCodeTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.inputBarCodeTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.inputBarCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.inputBarCodeTextField.frame = CGRectMake((kScreenBounds.size.width - LCFloat(265))/ 2., self.scanningTopBackgroundImageView.bounds.size.height - LCFloat(13), LCFloat(265), LCFloat(43));
    self.inputBarCodeTextField.font = [UIFont fontWithName:@"Helvetica-Bold" size:LCFloat(15 + 2)];
    self.inputBarCodeTextField.hidden = YES;
    self.inputBarCodeTextField.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self.view addSubview:self.inputBarCodeTextField];
    self.isShowAll = YES;
}

#pragma mark CreatezBar
-(void)createZBar{
    //实例化条形码扫描SDK
    if (![UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
        [[UIAlertView alertViewWithTitle:@"提示" message:@"当前设备不可用" buttonTitles:@[@"确定"] callBlock:NULL]show];
        return ;
    } else {
        self.readerView = [[ZBarReaderView alloc] init];
        self.readerView.frame = kScreenBounds;
        self.readerView.torchMode = 0;               // 0 不开摄像头 1 开
        self.readerView.readerDelegate = self;
        self.readerView.stringTag = @"readerView";
        [self.view addSubview:self.readerView];
        [self.readerView start];
    }
    
    // set backImageView
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imageView.image =[UIImage imageNamed:@"Image_bg_rectangle"];
    [self.view addSubview:imageView];
    
    self.upOrDown = NO;
    self.num = 0;
    // set imageLine
    self.viewLine = [[UIImageView alloc] init];
    self.viewLine.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [self.view addSubview:self.viewLine];
    
}

-(void)createNavBar{
    
    self.scanningTopBackgroundImageView = [[UIImageView alloc]init];
    self.scanningTopBackgroundImageView.backgroundColor = [UIColor clearColor];
    self.scanningTopBackgroundImageView.image = [UIImage imageNamed:@"scan-code_img_on"];
    self.scanningTopBackgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(150));
    [self.view addSubview:self.scanningTopBackgroundImageView];
    
    self.scanningMiddeMiddleBackgroundImageView = [[UIImageView alloc]init];
    self.scanningMiddeMiddleBackgroundImageView.backgroundColor = [UIColor clearColor];
    self.scanningMiddeMiddleBackgroundImageView.image = [UIImage imageNamed:@"scan-code_img_middle"];
    self.scanningMiddeMiddleBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(self.scanningTopBackgroundImageView.frame), kScreenBounds.size.width, LCFloat(265) - 20);
    [self.view addSubview:self.scanningMiddeMiddleBackgroundImageView];
    
    self.scanningBottomBackgroundImageView = [[UIImageView alloc]init];
    self.scanningBottomBackgroundImageView.backgroundColor = [UIColor clearColor];
    self.scanningBottomBackgroundImageView.userInteractionEnabled = YES;
    self.scanningBottomBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(self.scanningMiddeMiddleBackgroundImageView.frame),kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.scanningMiddeMiddleBackgroundImageView.frame));
    UIImage *bottomImage = [UIImage imageNamed:@"scan-code_img_down"];
    UIImage *stretchableImage = [bottomImage stretchableImageWithLeftCapWidth:0 topCapHeight:bottomImage.size.height * 0.99];
    self.scanningBottomBackgroundImageView.image = stretchableImage;
    [self.view addSubview:self.scanningBottomBackgroundImageView];
    
    // set init label
    UILabel *labelBarcodeTitle = [[UILabel alloc] init];
    labelBarcodeTitle.font = [UIFont systemFontOfCustomeSize:14.];
    labelBarcodeTitle.textColor = [UIColor colorWithCustomerName:@"白"];
    [labelBarcodeTitle setText:@"请将二维码/条形码放到框内即可进行扫描"];
    CGSize contentOfSize = [labelBarcodeTitle.text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:14.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    NSLog(@"%.2f",self.scanningTopBackgroundImageView.bounds.size.height - LCFloat(6) - 20);
    labelBarcodeTitle.frame = CGRectMake((kScreenBounds.size.width - contentOfSize.width) / 2., self.scanningTopBackgroundImageView.bounds.size.height - LCFloat(6) - 40, contentOfSize.width, 20);
    [self.view addSubview:labelBarcodeTitle];
    
    // 创建扫一扫title
    UILabel *scanningTitle = [[UILabel alloc]init];
    scanningTitle.backgroundColor = [UIColor clearColor];
    scanningTitle.text = @"扫一扫";
    scanningTitle.font = [UIFont boldSystemFontOfSize:16.];
    scanningTitle.frame = CGRectMake((kScreenBounds.size.width - 100) / 2., 20, 100, 44);
    scanningTitle.textColor = [UIColor whiteColor];
    scanningTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:scanningTitle];
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnceHideKeyBoard)];
    [self.scanningBottomBackgroundImageView addGestureRecognizer:tapForHideKeyBoard];
}


#pragma mark - ActionClick
-(void)tapOnceHideKeyBoard{
    if ([self.inputBarCodeTextField isFirstResponder]){
        [self.inputBarCodeTextField resignFirstResponder];
    }
}

-(void)lightButtonClick{
    ZBarReaderView *readerView = (ZBarReaderView *)[self.view viewWithStringTag:@"readerView"];
    readerView.torchMode = 1 - readerView.torchMode;
    if (readerView.torchMode == 0){
        [self.lightButton setImage:[UIImage imageNamed:@"scan-code_btn_flashlight"] forState:UIControlStateNormal];
    } else {
        [self.lightButton setImage:[UIImage imageNamed:@"scan-code_btn_click"] forState:UIControlStateNormal];
    }
}

-(void)inputBarCodeClick{
    if (self.isShowAll){
        self.viewLine.hidden = YES;
        [self.readerView stop];
    } else {
        self.viewLine.hidden = NO;
        [self.readerView start];
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf inputBarCodeAnimation];
    
    self.isShowAll = !self.isShowAll;
}

#pragma mark - ZBarReaderViewDelegate
- (void)readerView:(ZBarReaderView*)readerView didReadSymbols:(ZBarSymbolSet*)symbols fromImage:(UIImage*)image {
    // 得到扫描的条码内容
    const zbar_symbol_t *symbol = zbar_symbol_set_first_symbol(symbols.zbarSymbolSet);
    NSString *symbolStr = [NSString stringWithUTF8String: zbar_symbol_get_data(symbol)];
//        self.scanFinished(symbolStr);
    [readerView stop];
    [self directionalWithCode:symbolStr];
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"扫描信息" message:symbolStr buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [readerView removeFromSuperview];
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }]show];
    // set pop to root view controller animated
    //    [self.navigationController popViewControllerAnimated:YES];
    // remove from superview
    //    [readerView removeFromSuperview];
}

#pragma mark - UItextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (self.inputBarCodeTextField == textField) {
        if ([toBeString length] > 0) {
            if (toBeString.length >= 30){
                textField.text = [toBeString substringToIndex:30];
                return NO;
            }
            [self.chooseAssetButton setBackgroundColor:[UIColor colorWithCustomerName:@"粉"]];
            [self.chooseAssetButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            self.chooseAssetButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;
            return YES;
        } else {
            [self.chooseAssetButton setBackgroundColor:[UIColor clearColor]];
            [self.chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
            self.chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            return YES;
        }
    }
    return YES;
}

-(void)directionalWithCode:(NSString *)symbolStr{               // 解析
    if (symbolStr.length){
        NSDictionary *dataSource = [NSJSONSerialization JSONObjectWithData:[symbolStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
        GWScanningModel *scanningModel = [[GWScanningModel alloc]initWithJSONDict:dataSource];
        if ([symbolStr hasPrefix:@"http://"]){  // 跳转h5
           
        }
        
        if (scanningModel.type){
            [self.readerView stop];
        } else {
            if (self.isRedirect == NO){
                self.isRedirect = YES;
                [self redirectionToProductDetailWithBarCode:symbolStr];
            }
        }
    } else {
        [[UIAlertView alertViewWithTitle:nil message:@"扫描不到信息" buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
}

#pragma mark - 接口
-(void)sendRequestWithParameter:(NSDictionary *)parameter{
  
}

// 字典拼接
-(NSDictionary *)scanningParameterManager:(GWScanningModel *)scanningModel{
    return nil;
}

// 重定向到商品详情
-(void)redirectionToProductDetailWithBarCode:(NSString *)barCode{

}

#pragma mark - OtherManager
// 条形码动画
-(void)inputBarCodeAnimation{
    [UIView animateWithDuration:.5f animations:^{
        if (self.isShowAll){
            self.scanningMiddeMiddleBackgroundImageView.size_height = 10;
            self.inputBarCodeTextField.alpha = 1;
            self.lightButton.alpha = 0;
            [self.inputBarCodeButton setTitle:@"切换到扫码" forState:UIControlStateNormal];
            self.inputBarCodeButton.orgin_y = CGRectGetMaxY(self.scanningMiddeMiddleBackgroundImageView.frame) + LCFloat(20) + LCFloat(30);
            [self.chooseAssetButton setTitle:@"确定" forState:UIControlStateNormal];
            // 关闭关闭摄像头
            if (self.closeCameraTimer){
                [self.closeCameraTimer invalidate];
                self.closeCameraTimer = nil;
            }
            if (self.inputBarCodeTextField.text.length){
                [self.chooseAssetButton setBackgroundColor:[UIColor colorWithCustomerName:@"粉"]];
                [self.chooseAssetButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
                self.chooseAssetButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;
            } else {
                [self.chooseAssetButton setBackgroundColor:[UIColor clearColor]];
                [self.chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
                self.chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            }
        } else {
            self.scanningMiddeMiddleBackgroundImageView.size_height = LCFloat(265) - 20;
            self.inputBarCodeTextField.alpha = 0;
            self.lightButton.alpha = 1;
            [self.inputBarCodeButton setTitle:@"输入条形码" forState:UIControlStateNormal];
            if (kScreenBounds.size.height > 480){
                self.inputBarCodeButton.frame = CGRectMake(LCFloat(40), kScreenBounds.size.height - LCFloat(85) - LCFloat(40), LCFloat(125), LCFloat(40));
            } else {
                self.inputBarCodeButton.frame = CGRectMake(LCFloat(40), CGRectGetMaxY(self.lightButton.frame) + LCFloat(32), LCFloat(125), LCFloat(40));
            }
            [self.chooseAssetButton setTitle:@"选择相册按钮" forState:UIControlStateNormal];
            [self.chooseAssetButton setBackgroundColor:[UIColor clearColor]];
            [self.chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
            self.chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            
            self.num = 0;
            self.upOrDown = NO;
            // 重新开启关闭摄像头
            self.closeCameraTime = 60;
            self.closeCameraTimer = [NSTimer scheduledTimerWithTimeInterval:self.closeCameraTime target:self selector:@selector(closeCameraAnimation) userInfo:nil repeats:YES];
        }
        self.chooseAssetButton.orgin_y = self.inputBarCodeButton.frame.origin.y;
        self.scanningBottomBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(self.scanningMiddeMiddleBackgroundImageView.frame),kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.scanningMiddeMiddleBackgroundImageView.frame));
    } completion:^(BOOL finished) {
        if (self.isShowAll){
            self.inputBarCodeTextField.hidden = YES;
            self.lightButton.hidden = NO;
            if ([self.inputBarCodeTextField isFirstResponder]){
                [self.inputBarCodeTextField resignFirstResponder];
            }
        } else {
            self.inputBarCodeTextField.hidden = NO;
            self.lightButton.hidden = YES;
            [self.inputBarCodeTextField becomeFirstResponder];
        }
    }];
}

// 选择相册内容
-(void)chooseAlAssetClick{                  // 选择相册内容
    if (self.isShowAll){
        GWAssetsLibraryViewController *assetsLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        assetsLibraryViewController.isNotShowCamera = YES;
        __weak typeof(self)weakSelf = self;
        [assetsLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *chooseImage = [selectedImgArr lastObject];
            CGImageRef chooseImageWithRef = chooseImage.CGImage;
            ZBarReaderController *reader = [[ZBarReaderController alloc]init];
            ZBarSymbol *symbol = nil;
            for (symbol in [reader scanImage:chooseImageWithRef])
                break;
            [strongSelf directionalWithCode:symbol.data];
        }];
        [self.readerView stop];
        [self.navigationController pushViewController:assetsLibraryViewController animated:YES];
        self.num = 0;
    } else {
        [self directionalWithCode:self.inputBarCodeTextField.text];
    }
}

// Animation
- (void)animation {
    if (self.upOrDown == NO) {
        self.num++;
        [self.viewLine setFrame:CGRectMake((kScreenBounds.size.width - LCFloat(265))/2. + LCFloat(10), CGRectGetMaxY(self.scanningTopBackgroundImageView.frame) + 2 * self.num , LCFloat(265 - 20) , 1)];
        if (2 * self.num >= self.scanningMiddeMiddleBackgroundImageView.bounds.size.height) {
            self.upOrDown = YES;
        }
    } else {
        self.num--;
        [self.viewLine setFrame:CGRectMake((kScreenBounds.size.width - LCFloat(265))/2. + LCFloat(10), CGRectGetMaxY(self.scanningTopBackgroundImageView.frame) + 2 * self.num , LCFloat(265 - 20), 1)];
        if (self.num == 0) {
            self.upOrDown = NO;
        }
    }
}

-(void)closeCameraAnimation{
    [self.readerView stop];
    [UIAlertView alertViewWithTitle:@"警告" message:@"由于您长时间没有操作可能对您摄像头造成损耗已将扫码功能暂时关闭，请问是否打开？" buttonTitles:@[@"打开",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0){
            [self.readerView start];
        }
    }];
}


#pragma mark - 创建按钮
-(void)createSliderButton{
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (self != [self.navigationController.viewControllers firstObject]) {
            [[RESideMenu shareInstance] presentLeftMenuViewController];
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    [self.view addSubview:self.openDrawerButton];
}

@end
