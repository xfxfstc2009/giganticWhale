//
//  GWSettingRootCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSettingRootCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"

@interface GWSettingRootCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)GWImageView *arrowImageView;
@end

@implementation GWSettingRootCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建arrowImageView;
    self.arrowImageView = [[GWImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInfo:(NSString *)transferInfo{
    _transferInfo = transferInfo;
    self.fixedLabel.text = transferInfo;
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, self.transferCellHeight);
}



-(void)loadContentWithIndex:(NSInteger)index{
    if (index % 2) {
        self.backgroundColor = [UIColor whiteColor];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
    NSString *fullStirng = [NSString stringWithFormat:@"%02ld. %@", (long)index, self.transferInfo];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont HeitiSCWithFontSize:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.fixedLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.fixedLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.arrowImageView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.fixedLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.arrowImageView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}
@end
