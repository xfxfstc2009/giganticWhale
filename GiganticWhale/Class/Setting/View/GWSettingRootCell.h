//
//  GWSettingRootCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWSettingRootCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;         /**< 传递cell 高度*/
@property (nonatomic,copy)NSString *transferInfo;

-(void)loadContentWithIndex:(NSInteger)index;
@end
