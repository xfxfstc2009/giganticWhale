//
//  GWSettingSliderSettingViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSettingSliderSettingViewController.h"
#import "GWSliderSettingCell.h"
#import "GWSettingSliderListModel.h"

@interface GWSettingSliderSettingViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSMutableArray *settingArr;

@end

@implementation GWSettingSliderSettingViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetSliderList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"侧边栏设置";
}

-(void)arrayWithInit{
    self.settingArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.settingTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWSliderSettingCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWSliderSettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellStyleDefault;
        
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.transferSliderModel = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne switchTouchManager:^(UISwitch *mainSwitch, NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToChangeWithFlag:info info:[NSString stringWithFormat:@"%li",(long)mainSwitch.on]];
    }];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetSliderList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:base_slider_main requestParams:nil responseObjectClass:[GWSettingSliderListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWSettingSliderListModel *settingSliderListModel = (GWSettingSliderListModel *)responseObject;
            if (strongSelf.settingArr.count){
                [strongSelf.settingArr removeAllObjects];
            }
            [strongSelf.settingArr addObject:settingSliderListModel.sliderList];
            [strongSelf.settingTableView reloadData];
        }
    }];
}

#pragma mark 修改
-(void)sendRequestToChangeWithFlag:(NSString *)flag info:(NSString *)info{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:base_slider_main_change requestParams:@{@"flag":flag,@"info":info} responseObjectClass:[GWSettingSliderListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [PDHUD showHUDProgress:@"修改成功" diary:2];
        } else {
            [PDHUD showHUDProgress:@"修改失败" diary:2];
        }
    }];
}

@end
