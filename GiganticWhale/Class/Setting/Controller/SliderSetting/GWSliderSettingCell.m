//
//  GWSliderSettingCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSliderSettingCell.h"
#import <objc/runtime.h>

static char touchKey;
@interface GWSliderSettingCell()
@property (nonatomic,strong)UILabel *fixedLabel;                /**< 输入信息*/
@property (nonatomic,strong)UISwitch *mainSwitch;               /**< switch*/

@end

@implementation GWSliderSettingCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建
    self.mainSwitch = [[UISwitch alloc] init];
    [self.mainSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.mainSwitch];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSliderModel:(GWSettingSliderSingleModel *)transferSliderModel{
    self.fixedLabel.text = transferSliderModel.sign;
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, self.transferCellHeight);
    
    // switch
    self.mainSwitch.frame = CGRectMake(kScreenBounds.size.width - 61, 7, 51, 31);
    [self.mainSwitch setOn:transferSliderModel.show];
}


-(void)switchAction{
    void(^block)(UISwitch *mainSwitch,NSString *info) = objc_getAssociatedObject(self, &touchKey);
    if (block){
        block(self.mainSwitch,self.fixedLabel.text);
    }
}

-(void)switchTouchManager:(void(^)(UISwitch *mainSwitch, NSString *info))block{
    objc_setAssociatedObject(self, &touchKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



@end
