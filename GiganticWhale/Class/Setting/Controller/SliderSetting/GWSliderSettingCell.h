//
//  GWSliderSettingCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWSettingSliderSingleModel.h"
@interface GWSliderSettingCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;             /**< 传递cell高度*/
@property (nonatomic,strong)GWSettingSliderSingleModel *transferSliderModel;            /**< 传递model*/

-(void)switchTouchManager:(void(^)(UISwitch *mainSwitch, NSString *info))block;

@end
