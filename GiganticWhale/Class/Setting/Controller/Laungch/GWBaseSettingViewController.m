//
//  GWBaseSettingViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/30.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWBaseSettingViewController.h"
#import "GWBaseSettingModel.h"
#import "AliOSSManager.h"
#import "GWAssetsLibraryViewController.h"
#import "GWSettingSliderSettingViewController.h"

typedef NS_ENUM(NSInteger,uploadType) {
    uploadTypeLaungch,                      /**< 上传到Laungch*/
    uploadTypeSlider,                       /**< 上传到侧边栏*/
};

@interface GWBaseSettingViewController()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>{
    GWBaseSettingModel *baseSettingModel;
}
@property (nonatomic,strong)UITableView *baseTableView;
@property (nonatomic,strong)NSArray *baseArr;                       /**< 基础数组*/
@property (nonatomic,strong)UISwitch *isAppleSwitch;                /**< 苹果审核开关*/
@property (nonatomic,strong)UISwitch *otherAppleSwitch;             /**< 其他开关*/
@property (nonatomic,strong)UITextView *inputTextView;
@property (nonatomic,strong)GWImageView *laungchImgView;
@property (nonatomic,strong)GWImageView *sliderImgView;
@end

@implementation GWBaseSettingViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                             // 页面初始化
    [self arrayWithInit];                                           // 数组初始化
    [self createTableView];                                         // 创建tableView
    [self createNotifi];                                            // 键盘通知
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToGetBaseSettingInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"提交" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToChange];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.baseArr = @[@[@"App 审核"],@[@"其他开关"],@[@"Laungch类型",@"Laungch图片"],@[@"侧边栏图片"],@[@"Laungch标题"],@[@"侧边栏"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.baseTableView){
        self.baseTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.baseTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.baseTableView.delegate = self;
        self.baseTableView.dataSource = self;
        self.baseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.baseTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.baseTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.baseArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.baseArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Laungch标题"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Laungch标题"]){
        
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 建议textView
            self.inputTextView = [[UITextView alloc]init];
            self.inputTextView.delegate = self;
            self.inputTextView.textColor = [UIColor blackColor];
            self.inputTextView.returnKeyType = UIReturnKeyDefault;
            self.inputTextView.keyboardType = UIKeyboardTypeDefault;
            self.inputTextView.backgroundColor = [UIColor whiteColor];
            self.inputTextView.scrollEnabled = YES;
            self.inputTextView.limitMax = 100;
            self.inputTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            self.inputTextView.frame = CGRectMake(LCFloat(16),LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(150));
            [cellWithRowTwo addSubview:self.inputTextView];
            self.inputTextView.placeholder = @"输入信息";
        }
        self.inputTextView.text = baseSettingModel.cons;
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithRowOne addSubview:fixedLabel];
            
            // 2. 创建右侧的switch
            UISwitch *otherSwitch = [[UISwitch alloc] init];
            otherSwitch.frame = CGRectMake(kScreenBounds.size.width - 61, 7, 51, 31);
            [otherSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
            otherSwitch.stringTag = @"otherSwitch";
            [cellWithRowOne addSubview:otherSwitch];
            
            // 3. 创建图片
            GWImageView *imageView = [[GWImageView alloc]init];
            imageView.backgroundColor = [UIColor clearColor];
            imageView.stringTag = @"imageView";
            imageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - cellHeight - 2 * LCFloat(3), LCFloat(3), cellHeight - 2 * LCFloat(3), cellHeight - 2 * LCFloat(3));
            [cellWithRowOne addSubview:imageView];
        }
        UISwitch *otherSiwtch = (UISwitch *)[cellWithRowOne viewWithStringTag:@"otherSwitch"];
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
        GWImageView *imageView = (GWImageView *)[cellWithRowOne viewWithStringTag:@"imageView"];
        
        // fixedLabel
        fixedLabel.frame = CGRectMake(LCFloat(11), 0, LCFloat(150), cellHeight);
        fixedLabel.text = [[self.baseArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        // switch
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"App 审核"]){
            self.isAppleSwitch = otherSiwtch;
            self.isAppleSwitch.on = baseSettingModel.isApple;
            otherSiwtch.hidden = NO;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"其他开关"]){
            self.otherAppleSwitch = otherSiwtch;
            self.otherAppleSwitch.on = baseSettingModel.otherOpen;
            otherSiwtch.hidden = NO;
        } else {
            otherSiwtch.hidden = YES;
        }
        
        // imgView
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Laungch图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Laungch图片"]){
            self.laungchImgView = imageView;
            [self.laungchImgView uploadImageWithLaungchURL:baseSettingModel.laungch placeholder:nil callback:NULL];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"侧边栏图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"侧边栏图片"]){
            self.sliderImgView = imageView;
            [self.sliderImgView uploadImageWithLaungchURL:baseSettingModel.sliderBg placeholder:nil callback:NULL];
        }
        return cellWithRowOne;
    }

    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Laungch图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Laungch图片"]){
        GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *image = [selectedImgArr lastObject];
            [strongSelf uploadImageWithImage:image type:uploadTypeLaungch];
        }];
        [self.navigationController pushViewController:assetLibraryViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"侧边栏图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"侧边栏图片"]){
        GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *image = [selectedImgArr lastObject];
            [strongSelf uploadImageWithImage:image type:uploadTypeSlider];
        }];
        [self.navigationController pushViewController:assetLibraryViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"侧边栏"]){
        GWSettingSliderSettingViewController *settingViewController = [[GWSettingSliderSettingViewController alloc]init];
        [self.navigationController pushViewController:settingViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Laungch标题"]){
        return LCFloat(150) + 2 * LCFloat(11);
    } else {
        return LCFloat(44);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    titleLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    titleLabel.frame = CGRectMake(LCFloat(11), 0, 300,LCFloat(20));
    if (section == [self cellIndexPathSectionWithcellData:@"App 审核"]){
        titleLabel.text = @"苹果审核";
    } else if (section == [self cellIndexPathSectionWithcellData:@"其他开关"]){
        titleLabel.text = @"其他开关";
    } else if (section == [self cellIndexPathSectionWithcellData:@"Laungch类型"]){
        titleLabel.text = @"Laungch";
    } else if (section == [self cellIndexPathSectionWithcellData:@"侧边栏图片"]){
        titleLabel.text = @"侧边栏图片";
    } else if (section == [self cellIndexPathSectionWithcellData:@"Laungch标题"]){
        titleLabel.text = @"Laungch标题";
    }
    [headerView addSubview:titleLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [headerView addGestureRecognizer:tap];
    
    return headerView;
}

-(void)tapManager{
    if ([self.inputTextView isFirstResponder]){
        [self.inputTextView resignFirstResponder];
    }
}

#pragma mark - 上传方法
-(void)uploadImageWithImage:(UIImage *)image type:(uploadType)type{
    OSSSingleFileModel *fileModel = [[OSSSingleFileModel alloc]init];
    fileModel.objcName = [NSDate getCurrentTime];
    fileModel.objcImg = image;
    
    if (type == uploadTypeSlider){
        fileModel.objcName = [NSString stringWithFormat:@"slider%@.png",[NSDate getCurrentTimeWithSetting]];
    } else if (type == uploadTypeLaungch){
        fileModel.objcName = [NSString stringWithFormat:@"laungch%@.png",[NSDate getCurrentTimeWithSetting]];
    }
    
    __weak typeof(self)weakSelf = self;
    [AliOSSManager uploadLaungchFileWithImg:fileModel compressionQuality:1.f andBlock:^(NSString *fileName) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (type == uploadTypeLaungch){                 // 修改laungch
            strongSelf->baseSettingModel.laungch = [NSString stringWithFormat:@"Laungch/%@",fileName];
            [strongSelf.laungchImgView uploadImageWithLaungchURL:strongSelf->baseSettingModel.laungch placeholder:nil callback:NULL];
        } else if (type == uploadTypeSlider){
            strongSelf->baseSettingModel.sliderBg = [NSString stringWithFormat:@"Laungch/%@",fileName];
            [strongSelf.sliderImgView uploadImageWithLaungchURL:strongSelf->baseSettingModel.sliderBg placeholder:nil callback:NULL];
        }
    }];
}


#pragma mark - Interface
-(void)sendReqeustToGetBaseSettingInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:base_setting requestParams:nil responseObjectClass:[GWBaseSettingModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->baseSettingModel = (GWBaseSettingModel *)responseObject;
            [strongSelf.baseTableView reloadData];
        } else {
        
        }
    }];
}

#pragma mark - Action
-(void)switchAction:(UISwitch *)sender{
    if (sender == self.isAppleSwitch){          // 苹果审核
        
    }
}

#pragma mark - sendRequestToChange
-(void)sendRequestToChange{
    
    NSDictionary *dic = @{@"cons":self.inputTextView.text,@"icon":@"",@"isApple":@(self.isAppleSwitch.on),@"otherOpen":@(self.otherAppleSwitch.on),@"laungch":baseSettingModel.laungch,@"sliderBg":baseSettingModel.sliderBg};
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:base_setting_change requestParams:dic responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"修改成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.baseArr.count ; i++){
        NSArray *dataTempArr = [self.baseArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.baseArr.count ; i++){
        NSArray *dataTempArr = [self.baseArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}
#pragma mark - 键盘通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.baseTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.baseTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

@end
