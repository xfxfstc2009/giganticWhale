//
//  GWSettingRootViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSettingRootViewController.h"

#import "GWSuggestionViewController.h"              // 建议
#import "GWDrinkViewController.h"
#import "GWBaseSettingViewController.h"             // app设置
#import "HomeSliderViewController.h"
#import "GWSettingSliderSettingViewController.h"    // 管理员设置
#import "GWSettingRootCell.h"                       // cell
#import "MMHPayView.h"
#import "GWAPICloudViewController.h"


@interface GWSettingRootViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSMutableArray *settingArr;

@property (nonatomic,strong)UIButton *logoutButton;             /**< 退出登录按钮*/

@end

@implementation GWSettingRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
    [self rightBarButtonWithTitle:@"12" barNorImage:nil barHltImage:nil action:^{
        GWAPICloudViewController * Vc = [[GWAPICloudViewController alloc]init];
        [self.navigationController pushViewController:Vc animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.settingArr = [NSMutableArray array];
    NSArray *tempSettingArr;
//    if ([WXApi isWXAppInstalled]){
        tempSettingArr = @[@[@"反馈"],@[@"提醒喝水"],@[@"管理员设置"],@[@"退出登录"]];
//    } else {
//        tempSettingArr = @[@[@"反馈"],@[@"提醒喝水"],@[@"管理员设置"]];
//    }
    
    [self.settingArr addObjectsFromArray:tempSettingArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.settingTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退出登录"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        UITableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowZero.backgroundColor = [UIColor clearColor];
            
            self.logoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.logoutButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
            self.logoutButton.layer.cornerRadius = LCFloat(5);
            self.logoutButton.frame = CGRectMake(LCFloat(16), 0, kScreenBounds.size.width - 2 * LCFloat(16), cellHeight);
            __weak typeof(self)weakSelf = self;
            [self.logoutButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf logOutManager];
            }];
            [cellWithRowZero addSubview: self.logoutButton];
        }
        if ([AccountModel sharedAccountModel].hasLoggedIn){         // 登录状态
            [self.logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
            self.logoutButton.backgroundColor = [UIColor colorWithCustomerName:@"浅蓝"];
        } else {
            [self.logoutButton setTitle:@"登录" forState:UIControlStateNormal];
            self.logoutButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
        }
        
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWSettingRootCell*cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWSettingRootCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferInfo = [[self.settingArr objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];

        [cellWithRowOne loadContentWithIndex:(indexPath.section + 1)];
        return cellWithRowOne;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(20);
    } else if (section == [self cellIndexPathSectionWithcellData:@"退出登录"]){
        return LCFloat(40);
    } else {
        return LCFloat(5);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.section != [self cellIndexPathSectionWithcellData:@"退出登录"]){
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"反馈"] && indexPath.row == [self cellIndexPathRowWithcellData:@"反馈"]){
        GWSuggestionViewController *suggestionViewController = [[GWSuggestionViewController alloc]init];
        [self.navigationController pushViewController:suggestionViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"提醒喝水"] && indexPath.row == [self cellIndexPathRowWithcellData:@"提醒喝水"]){
        GWDrinkViewController *drinkViewController = [[GWDrinkViewController alloc]init];
        [self.navigationController pushViewController:drinkViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"管理员设置"] && indexPath.row == [self cellIndexPathRowWithcellData:@"管理员设置"]){
        [self showAppPwd];
    }
}


-(void)showAppPwd{
    MMHPayView *view = [[MMHPayView alloc]init];
    __weak typeof(self)weakSelf = self;
    view.passCodeBlock = ^(NSString *passcode){
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if([passcode isEqualToString:@"159671"]){       // 成功
//            if ([[AccountModel sharedAccountModel].userId isEqualToString:@"4"]){
                GWBaseSettingViewController *baseSettingViewController = [[GWBaseSettingViewController alloc]init];
                [strongSelf.navigationController pushViewController:baseSettingViewController animated:YES];
//            } else {
//                [[UIAlertView alertViewWithTitle:@"错误" message:@"您当前不是管理员账户" buttonTitles:@[@"确定"] callBlock:NULL]show];
//            }
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:@"密码输入错误" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    };
    view.resetRedirectBlock = ^(){
        [[UIAlertView alertViewWithTitle:nil message:@"调到重置" buttonTitles:@[@"确定"] callBlock:NULL]show];
    };
    view.cancelPayBlock = ^(){          // 取消支付
        
    };
    [view payViewShow];
}

#pragma mark - Other 
#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.settingArr.count ; i++){
        NSArray *dataTempArr = [self.settingArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.settingArr.count ; i++){
        NSArray *dataTempArr = [self.settingArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark - 退出登录方法
-(void)logOutManager{
    __weak typeof(self)weakSelf = self;
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){           // 登录状态
        [[AccountModel sharedAccountModel] logoutManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[UIAlertView alertViewWithTitle:@"您已退出登录" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [strongSelf.logoutButton setTitle:@"登录" forState:UIControlStateNormal];
                strongSelf.logoutButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
            }] show];
        }];
    } else {                                                        // 未登录状态
        [weakSelf loginManager];
    }
}


-(void)loginManager{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] wechatLoginManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.headerImageView = [[GWImageView alloc]init];
        strongSelf.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
        [strongSelf loginSuccessManager];
    }];
}
@end
