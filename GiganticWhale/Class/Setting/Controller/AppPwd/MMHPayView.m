//
//  MMHPayView.m
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPayView.h"
#import "MMHPayInputView.h"                 /**< 输入框*/
#import "MMHCustomKeyboard.h"               /**< 键盘*/
@interface MMHPayView()
@property (nonatomic,strong)MMHPayInputView *payInputView;          /**< 输入框*/
@property (nonatomic,strong)MMHCustomKeyboard *payCustomKeyboard;   /**< 自定义键盘*/
@property (nonatomic,strong)UIButton *alphaButton;                  /**< 蒙层*/
@property (nonatomic,strong)UITextField *inputTextField;            /**< 输入框TextField*/
@property (nonatomic,assign)BOOL keyBoardShow;                      /**< 键盘是否弹起*/
@property (nonatomic,copy)NSString *inputPassword;                  /**< 输入的密码*/
@end

@implementation MMHPayView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"释放了");
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:kScreenBounds];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        // 1. 创建蒙层
        [self createAlphaView];
        // 2. 创建键盘
        [self createCustomKeyboard];
        // 3. 创建响应者
        [self createInputTextField];
        // 4.创建输入框
        [self createInputView];
    }
    return self;
}

#pragma mark 创建蒙层
-(void)createAlphaView{
    UIButton *alphaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:alphaButton];
    
    self.alphaButton = alphaButton;
    self.alphaButton.backgroundColor = [UIColor blackColor];
    self.alphaButton.alpha = .4f;
    [self.alphaButton addTarget: self action:@selector(alphaButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark 创建输入框
-(void)createInputView{
    MMHPayInputView *payInputView = [[MMHPayInputView alloc]init];
    [self addSubview:payInputView];
    __weak typeof(self)weakSelf = self;
    payInputView.findMyPwdBlock = ^(){          // 找回密码
        weakSelf.payInputView.hidden = YES;
        [weakSelf hidenKeyboard:nil];
        [[UIAlertView alertViewWithTitle:nil message:@"是否找回密码？" buttonTitles:@[@"否",@"是"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            weakSelf.payInputView.hidden = NO;
            if (buttonIndex == 0){
                [weakSelf showKeyboard];
            } else {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.resetRedirectBlock();
                
                [weakSelf removeFromSuperview];
                [weakSelf.payInputView setNeedsDisplay];
            }
        }]show];
    };
    self.payInputView = payInputView;
    // 注册取消按钮通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cancelButtonClick) name:payInputViewCancelButtonClick object:nil];
    // 注册确定按钮通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(inputViewConfirmClick:) name:payInputViewConfirmButtonClick object:nil];
    
}

#pragma mark 创建输入框
-(void)createInputTextField{
    UITextField *payInputTextField = [[UITextField alloc]init];
    [self addSubview:payInputTextField];
    self.inputTextField = payInputTextField;
}

#pragma mark 键盘
-(void)createCustomKeyboard{
    MMHCustomKeyboard *keyboard = [[MMHCustomKeyboard alloc]init];
    [self addSubview:keyboard];
    self.payCustomKeyboard = keyboard;
    // 注册确定按钮通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ok) name:keyboardConfirmButtonClick object:nil];
}

#pragma mark Layout
-(void)layoutSubviews{
    [super layoutSubviews];
    self.alphaButton.frame = self.bounds;
}

#pragma mark - actionClick
#pragma mark 蒙层点击
-(void)alphaButtonClick{
    if (self.keyBoardShow){             // 隐藏
        [self hidenKeyboard:nil];
    } else {                            // 弹起
        [self showKeyboard];
    }
}

/** 弹出 */
- (void)payViewShow {
    [self payViewShowInView:[UIApplication sharedApplication].keyWindow];
}

-(void)showKeyboard{
    self.keyBoardShow = YES;
    CGFloat marginTop;
    if (iphone4) {
        marginTop = 42;
    } else if (iphone5) {
        marginTop = 100;
    } else if (iphone6) {
        marginTop = 120;
    } else {
        marginTop = 140;
    }
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.payCustomKeyboard.transform = CGAffineTransformMakeTranslation(0, - self.payCustomKeyboard.size_height);
        self.payInputView.transform = CGAffineTransformMakeTranslation(0, marginTop - self.payInputView.orgin_y);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)payViewShowInView:(UIView *)view{
    [view addSubview:self];
    
    /** 键盘起始frame */
    self.payCustomKeyboard.orgin_x = 0;
    self.payCustomKeyboard.orgin_y = kScreenBounds.size.height;
    self.payCustomKeyboard.size_width = kScreenBounds.size.width;
    self.payCustomKeyboard.size_height = kScreenBounds.size.width * 0.65;
    
    /** 输入框起始frame */
    self.payInputView.size_height = kScreenBounds.size.width * 0.5625 + LCFloat(20) + [NSString contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    self.payInputView.orgin_y = (self.size_height - self.payInputView.size_height) * 0.5;
    self.payInputView.size_width = kScreenBounds.size.width * 0.94375;
    self.payInputView.orgin_x = (kScreenBounds.size.width - self.payInputView.size_width) * 0.5;
    
    /** 弹出键盘 */
    [self showKeyboard];
}

- (void)hidenKeyboard:(void (^)(BOOL finished))completion {
    self.keyBoardShow = NO;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.payCustomKeyboard.transform = CGAffineTransformIdentity;
        self.payInputView.transform = CGAffineTransformIdentity;
    } completion:completion];
}

-(void)inputViewConfirmClick:(NSNotification *)note{
    // 获取密码
    NSString *pwd = note.userInfo[payInputViewPassCodeKey];
    // 通知代理\传递密码
    if ([self.payViewDelegate respondsToSelector:@selector(confirmButtonClick:)]) {
        [self.payViewDelegate confirmButtonClick:pwd];
    }
    // 回调block\传递密码
    self.passCodeBlock(pwd);
    
    // 移除自己
    [self hidenKeyboard:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


-(void)ok{
    [self hidenKeyboard:nil];
}

#pragma mark 取消输入框
-(void)cancelButtonClick{
    [self hidenKeyboard:^(BOOL finished) {
        self.payInputView.hidden = YES;

        [[UIAlertView alertViewWithTitle:@"是否取消?" message:nil  buttonTitles:@[@"否",@"是"]  callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            self.payInputView.hidden = NO;
            if (buttonIndex == 0){
                [self showKeyboard];
            } else {
                if (self.cancelPayBlock){
                    self.cancelPayBlock();
                }
                NSMutableArray *numberArr = [self.payInputView valueForKeyPath:@"numberArr"];
                [numberArr removeAllObjects];
                [self removeFromSuperview];
                [self.payInputView setNeedsDisplay];
            }
        }]show];
    }];
}

@end
