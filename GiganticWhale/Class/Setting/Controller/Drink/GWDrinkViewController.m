//
//  GWDrinkViewController.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/25.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDrinkViewController.h"
#import "GWDrinkModel.h"
@interface GWDrinkViewController()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)UITableView *drinkTableView;
@property (nonatomic,strong)NSMutableArray *drinkMutableArr;
@property (nonatomic,strong)UISwitch *mainSwitch;               /**< 滑块*/
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,strong)GWDrinkModel *drinkModel;
@end

@implementation GWDrinkViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestGetDrinkInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"提醒喝水";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"提交" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendReqeustToSettingTime:strongSelf.inputTextField.text];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.drinkMutableArr = [NSMutableArray array];
    NSArray *drinkArr = @[@[@"开关"]];
    [self.drinkMutableArr addObjectsFromArray:drinkArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.drinkTableView){
        self.drinkTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.drinkTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.drinkTableView.delegate = self;
        self.drinkTableView.dataSource = self;
        self.drinkTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.drinkTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.drinkTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.drinkMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.drinkMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        fixedLabel.numberOfLines = 1;
        fixedLabel.stringTag = @"fixedLabel";
        fixedLabel.frame = CGRectMake(LCFloat(11), 0, LCFloat(100), cellHeight);
        [cellWithRowOne addSubview:fixedLabel];
        
        // 2. 创建右侧的switch
        UISwitch *drinkSwitch = [[UISwitch alloc] init];
        drinkSwitch.frame = CGRectMake(kScreenBounds.size.width - 61, 7, 51, 31);
        [drinkSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
        drinkSwitch.stringTag = @"drinkSwitch";
        [cellWithRowOne addSubview:drinkSwitch];
        
        // 3. 创建textField
        UITextField *inputTextField = [[UITextField alloc]init];
        inputTextField.backgroundColor = [UIColor clearColor];
        inputTextField.stringTag = @"inputTextField";
        inputTextField.placeholder = @"时间";
        inputTextField.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(150), 0, LCFloat(150), cellHeight);
        [cellWithRowOne addSubview:inputTextField];
    }
    UISwitch *drinkSwitch = (UISwitch *)[cellWithRowOne viewWithStringTag:@"drinkSwitch"];
    UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
    UITextField *inputTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"inputTextField"];
    
    if (indexPath.section == [self cellIndexPathRowWithcellData:@"开关"]){
        fixedLabel.text = @"开关";
        drinkSwitch.hidden = NO;
        drinkSwitch.on = self.drinkModel.remind;
        self.mainSwitch = drinkSwitch;
        inputTextField.hidden = YES;
    } else {
        fixedLabel.text = @"设置时间";
        drinkSwitch.hidden = YES;
        inputTextField.hidden = NO;
        self.inputTextField = inputTextField;
        self.inputTextField.text = [NSString stringWithFormat:@"%li",self.drinkModel.min];
    }
    
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Action
-(void)switchAction{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToSettingDrinkOpen:weakSelf.mainSwitch.on];
}

#pragma mark 根据内容返回indexPath
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.drinkMutableArr.count ; i++){
        NSArray *dataTempArr = [self.drinkMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

#pragma mark 返回indexPath
-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = 0;
    for (int i = 0 ; i < self.drinkMutableArr.count ; i++){
        NSArray *dataTempArr = [self.drinkMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark - SendRequest
-(void)sendReqeustToSettingDrinkOpen:(BOOL)isOepn{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:other_drink_set requestParams:@{@"remind":@(isOepn)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){           // 成功
            NSInteger cellOfIndex = [strongSelf cellIndexPathSectionWithcellData:@"时间"];
            if (strongSelf.mainSwitch.on){           // 打开状态
                // 0. 判断是否有
                if (cellOfIndex == -1){       // 不存在
                    // 1. 增加条目
                    NSArray *tempArr = @[@"时间"];
                    [strongSelf.drinkMutableArr addObject:tempArr];
                    // 2. 插入数据
                    NSInteger tempCellOfIndex = [strongSelf cellIndexPathSectionWithcellData:@"开关"];
                    NSMutableIndexSet *sinceTheMentionAddressSet = [[NSMutableIndexSet alloc]initWithIndex:(tempCellOfIndex + 1)];
                    [strongSelf.drinkTableView insertSections:sinceTheMentionAddressSet withRowAnimation:UITableViewRowAnimationLeft];
                }
            } else {                        // 关闭状态
                if (cellOfIndex != -1){
                    [strongSelf.drinkMutableArr removeLastObject];
                    // 2. 删除数据
                    NSIndexSet *indexSetWithShouhuoTime = [NSIndexSet indexSetWithIndex:cellOfIndex];
                    [strongSelf.drinkTableView deleteSections:indexSetWithShouhuoTime withRowAnimation:UITableViewRowAnimationTop];
                }
            }
        } else {
        
        }
    }];
}

-(void)sendRequestGetDrinkInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:other_drink requestParams:nil responseObjectClass:[GWDrinkModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.drinkModel = (GWDrinkModel *)responseObject;
            
            if (strongSelf.drinkModel.remind){
                [strongSelf.mainSwitch setOn:YES animated:YES];
                NSInteger cellOfIndex = [strongSelf cellIndexPathSectionWithcellData:@"时间"];
                // 0. 判断是否有
                if (cellOfIndex == -1){       // 不存在
                    // 1. 增加条目
                    NSArray *tempArr = @[@"时间"];
                    [strongSelf.drinkMutableArr addObject:tempArr];
                    // 2. 插入数据
                    NSInteger tempCellOfIndex = [strongSelf cellIndexPathSectionWithcellData:@"开关"];
                    NSMutableIndexSet *sinceTheMentionAddressSet = [[NSMutableIndexSet alloc]initWithIndex:(tempCellOfIndex + 1)];
                    [strongSelf.drinkTableView insertSections:sinceTheMentionAddressSet withRowAnimation:UITableViewRowAnimationMiddle];
                }
            }
        } else {
        
        }
    }];
}

#pragma mark - SendRequest
-(void)sendReqeustToSettingTime:(NSString *)min{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:other_drink_set requestParams:@{@"min":min} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        if (isSucceeded){           // 成功
            [[UIAlertView alertViewWithTitle:@"设置成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        } else {
            
        }
    }];
}


@end
