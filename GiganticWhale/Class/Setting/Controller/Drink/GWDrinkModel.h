//
//  GWDrinkModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/25.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface GWDrinkModel : FetchModel

@property (nonatomic,assign)NSInteger hour; /**< 小时*/
@property (nonatomic,assign)NSInteger min;
@property (nonatomic,assign)NSInteger remind;
@property (nonatomic,assign)NSInteger sec;

@end
