//
//  UITextField+CustomShowBar.m
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/12.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import "UITextField+CustomShowBar.h"
#import <objc/runtime.h>
static char cancelCallbackBolckKey;
static char checkCallbackBlockKey;
static char leftCallbackBlockKey;
static char rightCallbackBlockKey;

typedef NS_ENUM(NSInteger,barType) {
    barTypeNormal,                  /**< 默认*/
    barTypeNext,                    /**< 下一步内容*/
};

@implementation UITextField (CustomShowBar)

-(void)showBarCallback:(void(^)(UITextField *textField,NSInteger buttonIndex))callBack{
    UIView *toolbarView = [self createInputAccessoryViewWithType:barTypeNormal];
    
    if (callBack){
        objc_setAssociatedObject(self, &cancelCallbackBolckKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &checkCallbackBlockKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    [self setInputAccessoryView:toolbarView];
}

-(void)showNextBarCallBack:(void(^)(UITextField *textField,NSInteger buttonIndex))callBack{
    UIView *toobarView = [self createInputAccessoryViewWithType:barTypeNext];
    if (callBack){
        objc_setAssociatedObject(self, &cancelCallbackBolckKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &leftCallbackBlockKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &rightCallbackBlockKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    [self setInputAccessoryView:toobarView];
}

#pragma mark 创建inputAccessoryView
-(UIView *)createInputAccessoryViewWithType:(barType)barType{
    UIView *toolbarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40)];
    toolbarView.backgroundColor = [UIColor hexChangeFloat:@"F0F1F2"];
    toolbarView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0, 0, kScreenBounds.size.width , .5f);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.678 alpha:1].CGColor;
    [toolbarView.layer addSublayer:topBorder];
    
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.backgroundColor = [UIColor clearColor];
    [checkButton setTitle:@"确定" forState:UIControlStateNormal];
    [checkButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
    [checkButton addTarget:self action:@selector(checkClick) forControlEvents:UIControlEventTouchUpInside];
    checkButton.frame = CGRectMake(kScreenBounds.size.width - 80, 0, 80, 40);
    [toolbarView addSubview:checkButton];
    
    if (barType == barTypeNormal){
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.backgroundColor = [UIColor clearColor];
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.frame = CGRectMake(0, 0, 80, 40);
        [toolbarView addSubview:cancelButton];
    } else if (barTypeNext){
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        leftButton.backgroundColor = [UIColor clearColor];
        [leftButton setTitle:@"<" forState:UIControlStateNormal];
        [leftButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
        [leftButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        leftButton.frame = CGRectMake(LCFloat(20), 0, LCFloat(30), 40);
        [toolbarView addSubview:leftButton];
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.backgroundColor = [UIColor clearColor];
        [rightButton setTitle:@">" forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
        [rightButton addTarget:self action:@selector(leftButtonClick) forControlEvents:UIControlEventTouchUpInside];
        rightButton.frame = CGRectMake(CGRectGetMaxX(leftButton.frame), 0, LCFloat(30), 40);
        [toolbarView addSubview:rightButton];
    }
    
    return toolbarView;
}


-(void)cancelClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &cancelCallbackBolckKey);
    if (callBack){
        callBack(self,0);
    }
}

-(void)checkClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &checkCallbackBlockKey);
    if (callBack){
        callBack(self,1);
    }
}

-(void)leftButtonClick{
    void(^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &leftCallbackBlockKey);
    if (callBack){
        callBack(self,1);
    }
}

-(void)rightButtonClick{
    void(^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &rightCallbackBlockKey);
    if (callBack){
        callBack(self,2);
    }
}
@end
