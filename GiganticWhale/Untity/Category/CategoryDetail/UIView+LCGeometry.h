//
//  UIView+LCGeometry.h
//  LaiCai
//
//  Created by SmartMin on 15/8/13.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LCGeometry)
@property (nonatomic,assign)CGFloat orgin_x;
@property (nonatomic,assign)CGFloat orgin_y;
@property (nonatomic,assign)CGFloat size_width;
@property (nonatomic,assign)CGFloat size_height;
@property (nonatomic,assign)CGFloat center_x;
@property (nonatomic,assign)CGFloat center_y;
@property (nonatomic,assign)CGPoint middlePoint;
@property (nonatomic,assign)CGFloat middleY;

- (void)makeRoundedRectangleShape;
@end
