//
//  SDWebImageManager+Memory.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/2/4.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "SDWebImageManager+Memory.h"

@implementation SDWebImageManager (Memory)

- (BOOL)memoryCachedImageExistsForURL:(NSURL *)url {
    NSString *key = [self cacheKeyForURL:url];
    return ([self.imageCache imageFromMemoryCacheForKey:key] != nil) ?  YES : NO;
}


@end
