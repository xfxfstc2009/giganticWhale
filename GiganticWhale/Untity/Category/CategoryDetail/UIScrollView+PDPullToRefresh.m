//
//  UIScrollView+PDPullToRefresh.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIScrollView+PDPullToRefresh.h"
#import "UIScrollView+PullToRefreshCoreText.h"
#import "UIScrollView+SVPullToRefresh.h"
#import <SVPullToRefresh.h>
#import <objc/runtime.h>



static BOOL isSuper = YES;

static char pageKey;
static char isXiaLaKey;              //

@implementation UIScrollView (PDPullToRefresh)

-(void)appendingPullToRefreshHandler:(void (^)())block{             // 下啦刷新
    // 修改page = 0
    self.currentPage = @"1";
    __weak typeof(self)weakSelf = self;
    if(isSuper){
        [self addPullToRefreshWithPullText:@"GiganticWhale" action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage = @"1";
            strongSelf.isXiaLa = @"YES";
            if (block){
                block();
            }
        }];
        
    } else{
        [self addPullToRefreshWithActionHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage = @"1";
            strongSelf.isXiaLa = @"YES";
            if (block){
                block();
            }
        }];
        
        SVPullToRefreshView *refreshView = (SVPullToRefreshView *)self.pullToRefreshView;
        CGRect rect = refreshView.frame;
        rect.origin.x = 30.;
        refreshView.frame = rect;
        refreshView.arrowColor = RGB(197, 197, 197, 1);
        refreshView.textColor = RGB(197, 197, 197,1);
        [refreshView setTitle:@"下拉刷新数据" forState:SVPullToRefreshStateStopped];
        [refreshView setTitle:@"释放进行刷新" forState:SVPullToRefreshStateTriggered];
        [refreshView setTitle:@"正在加载..." forState:SVPullToRefreshStateLoading];
    }
}

-(void)stopPullToRefresh{
    if(isSuper){
        [self finishLoading];
    } else {
        [self.pullToRefreshView stopAnimating];
    }
}

-(void)appendingFiniteScrollingPullToRefreshHandler:(void (^)())block{              // 上啦加载
    __weak typeof(self)weakSelf = self;
    [self addInfiniteScrollingWithActionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSString *page = strongSelf.currentPage;
        NSInteger indexPage = [page integerValue];
        NSString *pageStr = [NSString stringWithFormat:@"%li",(long)indexPage + 1];
        strongSelf.currentPage = pageStr;
        strongSelf.isXiaLa = @"NO";
        if (block){
            block();
        }
    }];
}

-(void)stopFinishScrollingRefresh{
    [self.infiniteScrollingView stopAnimating];
}



#pragma mark - Properties

-(void)setCurrentPage:(NSString *)currentPage{
    objc_setAssociatedObject(self, &pageKey, currentPage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)currentPage{
    NSString *currrentPage = objc_getAssociatedObject(self, &pageKey);
    return currrentPage;
}

-(void)setIsXiaLa:(NSString *)isXiaLa{
    objc_setAssociatedObject(self, &isXiaLaKey, isXiaLa, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)isXiaLa{
    NSString *xiala = objc_getAssociatedObject(self, &isXiaLaKey);
    return xiala;
}

@end
