//
//  UIScrollView+QSPullToRefresh.h
//  QuicklyShop
//
//  Created by 梵天 on 14-7-25.
//  Copyright (c) 2014年 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVPullToRefresh.h>

@interface UIScrollView (QSPullToRefresh)

- (void)addQSPullToRefreshWithActionHandler:(void (^)(void))actionHandler;
- (void)addQSInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler;


@end
