//
//  UIImage+RenderedImage.h
//  LaiCai
//
//  Created by SmartMin on 15/8/10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, QSImageResizeOption){
    QSImageResizeOptionFill,
    QSImageResizeOptionFit,
};


@interface UIImage (RenderedImage)

+ (UIImage *)screenShoot:(UIView *)view;
+ (UIImage*)scaleDown:(UIImage*)image withSize:(CGSize)newSize;

+ (UIImage *)imageWithRenderColor:(UIColor *)color renderSize:(CGSize)size;
- (UIImage *)imageWithNewSize:(CGSize)size andResizeOption:(QSImageResizeOption)option;

@end
