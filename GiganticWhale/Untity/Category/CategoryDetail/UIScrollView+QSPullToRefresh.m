//
//  UIScrollView+QSPullToRefresh.m
//  QuicklyShop
//
//  Created by 梵天 on 14-7-25.
//  Copyright (c) 2014年 com. All rights reserved.
//

#import "UIScrollView+QSPullToRefresh.h"

@implementation UIScrollView (QSPullToRefresh)

- (void)addQSPullToRefreshWithActionHandler:(void (^)(void))actionHandler{
    
    [self addPullToRefreshWithActionHandler:actionHandler];
    
    SVPullToRefreshView *refreshView = self.pullToRefreshView;
    CGRect rect = refreshView.frame;
    rect.origin.x = 30.;
    refreshView.frame = rect;
    refreshView.arrowColor = RGB(197, 197, 197, 1);
    refreshView.textColor = RGB(197, 197, 197, 1);
    [refreshView setTitle:@"下拉刷新数据" forState:SVPullToRefreshStateStopped];
    [refreshView setTitle:@"释放进行刷新" forState:SVPullToRefreshStateTriggered];
    [refreshView setTitle:@"正在加载..." forState:SVPullToRefreshStateLoading];
}

- (void)addQSInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler{
    
    [self addInfiniteScrollingWithActionHandler:actionHandler];

}

@end

