//
//  CategoryStaticConstans.h
//  CategoryStatic
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#ifndef CategoryStaticConstans_h
#define CategoryStaticConstans_h

#import "UIActionSheet+Customise.h"                 // 自定义sheet
#import "UIAlertView+Customise.h"                   // 自定义alert
#import "UIButton+Customise.h"                      // 自定义按钮
#import "UITextField+CustomShowBar.h"               // 自定义textField
#import "UITextView+Customise.h"                    // 自定义textView
#import "NSString+LCCalcSize.h"                     // 计算文字frame
#import "NSDate+Utilities.h"                        // date换算
#import "UIView+LCGeometry.h"                       // frame简单修改
#import "UIImage+ImageEffects.h"                    // 高斯模糊
#import "UIView+StringTag.h"                        // tag
#import "UIColor+Extended.h"                        // 主题颜色修改
#import "UIFont+Customise.h"                        // font修改
#import "UITableViewCell+LCSeparatorLine.h"        // cell 线条
#import "UIImage+RenderedImage.h"
#import "UIView+MaterialDesign.h"
#import "UINavigationController+CBackGesture.h"     // nav 退后
#import "UIScrollView+QSPullToRefresh.h"            // refresh
#import "StatusBarManager.h"
#import "UIScrollView+PDPullToRefresh.h"
#import "UIView+MMHPrompt.h"

#endif /* CategoryStaticConstans_h */
