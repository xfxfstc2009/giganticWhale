//
//  SDWebImageManager+Memory.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/2/4.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <UIImageView+WebCache.h>
@interface SDWebImageManager (Memory)

- (BOOL)memoryCachedImageExistsForURL:(NSURL *)url;

@end
