//
//  UITextView+Customise.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/29.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "UITextView+Customise.h"
#import <objc/runtime.h>

static char textViewLimitTag;
static char textViewPlaceholderTag;
static char textViewPlaceholderLabelTag;
static char textViewDidChangeBlockTag;

@interface UITextView()<UITextViewDelegate>

@end

@implementation UITextView (Customise)

-(void)textViewDidChangeWithBlock:(textViewDidChangeBlock)block{
    objc_setAssociatedObject(self, &textViewDidChangeBlockTag, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(NSString *)placeholder{
    return objc_getAssociatedObject(self, &textViewPlaceholderTag);
}

-(NSInteger)limitMax{
    return [objc_getAssociatedObject(self, &textViewLimitTag) integerValue];
}


-(void)setLimitMax:(NSInteger)limitMax{
    self.delegate = self;
    NSNumber *limitMaxNum = [NSNumber numberWithInteger:limitMax];
    objc_setAssociatedObject(self, &textViewLimitTag, limitMaxNum, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self createLimitLabelWithMax:limitMax];
}

-(void)setPlaceholder:(NSString *)placeholder{
    self.delegate = self;
    UILabel *placeLabel = [self createPlaceholderLabelWithplaceholder:placeholder];
    [self addSubview:placeLabel];
    objc_setAssociatedObject(self, &textViewPlaceholderLabelTag, placeLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(self, &textViewPlaceholderTag, placeholder, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - createLimitLabel
-(UILabel *)createLimitLabelWithMax:(NSInteger)maxLimit{
    UILabel *limitLabel = [[UILabel alloc]init];
    limitLabel.backgroundColor = [UIColor clearColor];
    limitLabel.text = [NSString stringWithFormat:@"%li/%li",(long)0,(long)maxLimit];
    limitLabel.textAlignment = NSTextAlignmentRight;
    limitLabel.textColor = [UIColor blackColor];
    limitLabel.font=[UIFont systemFontOfSize:LCFloat(13.)];
    limitLabel.textColor = [UIColor lightGrayColor];
    CGSize limitSize = [limitLabel.text sizeWithCalcFont:limitLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:limitLabel.font])];
    limitLabel.frame = CGRectMake(self.bounds.size.width - limitSize.width - LCFloat(7),self.bounds.size.height + LCFloat(7),limitSize.width,[NSString contentofHeight:limitLabel.font]);
    return limitLabel;
}


#pragma mark - createPlaceholder
-(UILabel *)createPlaceholderLabelWithplaceholder:(NSString *)placeholder{
    UILabel *placeholderLabel = [[UILabel alloc]init];
    placeholderLabel.numberOfLines = 0;
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    placeholderLabel.text = placeholder;
    placeholderLabel.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
    CGSize placeholderSize = [placeholder sizeWithCalcFont:placeholderLabel.font constrainedToSize:CGSizeMake(self.size_width, CGFLOAT_MAX)];
    placeholderLabel.size_height = placeholderSize.height;
    placeholderLabel.frame = CGRectMake(LCFloat(7), LCFloat(8), self.size_width - 2 * LCFloat(7), placeholderSize.height);
    return placeholderLabel;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSNumber *limitMaxWithNumber = objc_getAssociatedObject(self, &textViewLimitTag);
    NSInteger limitMax = [limitMaxWithNumber integerValue];
    if([text isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if([toBeString length] > limitMax) {
        textView.text = [toBeString substringToIndex:limitMax];
        return NO;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    UILabel *placeLabel = objc_getAssociatedObject(self, &textViewPlaceholderLabelTag);
    NSString *placeholder = objc_getAssociatedObject(self, &textViewPlaceholderTag);
    void(^textViewDidChangeBlock)(NSInteger currentCount) = objc_getAssociatedObject(self, &textViewDidChangeBlockTag);
    if (textView.text.length){
        placeLabel.text = @"";
    } else {
        placeLabel.text = placeholder;
    }
    if (textViewDidChangeBlock){
        textViewDidChangeBlock(textView.text.length);
    }
}
@end
