//
//  UIView+LCGeometry.m
//  LaiCai
//
//  Created by SmartMin on 15/8/13.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIView+LCGeometry.h"

@implementation UIView (LCGeometry)

-(void)setOrgin_x:(CGFloat)orgin_x{
    self.frame = CGRectMake(orgin_x,self.frame.origin.y , self.frame.size.width, self.frame.size.height);
}

-(void)setOrgin_y:(CGFloat)orgin_y{
    self.frame = CGRectMake(self.frame.origin.x, orgin_y, self.frame.size.width, self.frame.size.height);
}

-(void)setSize_width:(CGFloat)size_width{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size_width, self.frame.size.height);
}

-(void)setSize_height:(CGFloat)size_height{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, size_height);
}

-(void)setCenter_x:(CGFloat)center_x{
    self.frame = CGRectMake(center_x - self.size_width / 2., self.orgin_y, self.size_width, self.size_height);
}

-(void)setCenter_y:(CGFloat)center_y{
    self.frame = CGRectMake(self.orgin_x, center_y - self.size_height / 2., self.size_width, self.size_height);
}

-(void)setMiddlePoint:(CGPoint)middlePoint{
    self.middlePoint = middlePoint;
}

-(void)setMiddleY:(CGFloat)middleY{
    self.middleY = middleY;
}

-(CGFloat)orgin_x{
    return self.frame.origin.x;
}

-(CGFloat)orgin_y{
    return self.frame.origin.y;
}

-(CGFloat)size_width{
    return self.frame.size.width;
}

-(CGFloat)size_height{
    return self.frame.size.height;
}

-(CGFloat)center_x{
    return self.frame.origin.x + self.frame.size.width / 2.;
}

-(CGFloat)center_y{
    return self.frame.origin.y + self.frame.size.height / 2.;
}

- (CGPoint)middlePoint {
    return CGPointMake(CGRectGetWidth(self.bounds) / 2.f, CGRectGetHeight(self.bounds) / 2.f);
}

- (CGFloat)middleY {
    return CGRectGetHeight(self.bounds) / 2.f;
}

#pragma mark -切圆角
- (void)makeRoundedRectangleShape {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat length = MIN(width, height);
    self.layer.cornerRadius = length * 0.5f;
    self.clipsToBounds = YES;
}


@end
