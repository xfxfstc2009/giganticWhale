//
//  UIButton+Customise.m
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIButton+Customise.h"
#import <objc/runtime.h>

static char *buttonCallBackBlockKey;

@implementation UIButton (Customise)

- (void) buttonWithBlock:(void(^)(UIButton *button))buttonClickBlock{
    [self addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    if (buttonClickBlock){
        objc_setAssociatedObject(self, &buttonCallBackBlockKey, buttonClickBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
}


-(void)buttonClick:(UIButton *)sender{
    void(^buttonClickBlock)(UIButton *button) = objc_getAssociatedObject(sender, &buttonCallBackBlockKey);
    if (buttonClickBlock){
        buttonClickBlock(sender);
    }
}
@end
