//
//  GWStupSQLiteManager.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWMessageSingleModel.h"
@interface GWStupSQLiteManager : NSObject
+(void)steupSqlite;



+(BOOL)insertIntoSQLiteWithMusic:(NSString *)url file:(NSString *)file;
+(NSString *)getFileWithURL:(NSString *)url ;


// 【消息】
+(NSMutableArray *)getListWithMessageTableName;
+(BOOL)insertIntoSQLiteWithMessageModel:(GWMessageSingleModel *)messageModel;



@end
