//
//  GWMovieView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMovieView.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPMoviePlayerController.h>



@interface GWMovieView()
@property (nonatomic,strong) MPMoviePlayerController *player;
@property (nonatomic,assign) BOOL isLoop;
@end

@implementation GWMovieView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self ){
        [self createViewWithFrame:frame];
    }
    return self;
}

#pragma mark - createView
-(void)createViewWithFrame:(CGRect)frame{
    if (!self.player){
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"GiganticWhale"  ofType:@"mp4"]];
        self.player = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [self.player setControlStyle:MPMovieControlStyleNone];
        [self.player prepareToPlay];
        [self.player.view setFrame:frame];
        [self addSubview:self.player.view];
        [self sendSubviewToBack:self.player.view];
        self.player.scalingMode = MPMovieScalingModeAspectFill;
        self.isLoop = YES;
    }
}

#pragma mark - Notifications
- (void)moviePlayerStateChangeNotification:(NSNotification *)notification {
    MPMoviePlayerController *moviePlayer = notification.object;
    MPMoviePlaybackState playbackState = moviePlayer.playbackState;
    
    switch (playbackState) {
        case MPMoviePlaybackStatePaused:
        case MPMoviePlaybackStateStopped:
        case MPMoviePlaybackStateInterrupted:{
            if (self.isLoop) {
                moviePlayer.controlStyle = MPMovieControlStyleNone;
                [self.player play];
            }  break;
        }
        default:
            break;
    }
}

#pragma mark 开启动画
-(void)startAnimation{
    [self getPlayerNotifications];
    [self.player play];
}

-(void)stopAnimation{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    [self.player pause];
}

- (void)getPlayerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerStateChangeNotification:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
}


@end
