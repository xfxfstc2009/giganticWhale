//
//  GWMovieView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWMovieView : UIView


-(void)startAnimation;
-(void)stopAnimation;

@end
