//
//  GWLocationMethod.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 获取当前定位地址
#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

typedef void(^currentLocationBlock)(NSString *lat,NSString *lon);
@interface GWLocationMethod : NSObject

+ (GWLocationMethod *)sharedLocationManager;

// 全局的地址
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,strong)AMapAddressComponent *addressComponent;

-(void)getCurrentLocationManager:(void(^)(CGFloat lat,CGFloat lng,AMapAddressComponent *addressComponent))block;
@end
