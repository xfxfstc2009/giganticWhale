//
//  SoundsManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/25.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "SoundsManager.h"
@implementation SoundsManager

+(void)aliMessageSounds{
    SystemSoundID shakeSoundsId = [SoundsManager loadSound:@"shark.wav"];
    AudioServicesPlaySystemSound(shakeSoundsId);
}


#pragma mark - 初始化音效
+ (SystemSoundID)loadSound:(NSString *)soundName {
    NSURL *url = [[NSBundle mainBundle]URLForResource:soundName withExtension:nil];
    
    // 创建声音Id
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(url), &soundId);
    
    return soundId;
}


@end
