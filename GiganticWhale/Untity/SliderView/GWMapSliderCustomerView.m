//
//  GWMapSliderCustomerView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMapSliderCustomerView.h"

#import <objc/runtime.h>
@class GWMapSliderView;

static char *menuActionWithBlockKey;
@interface GWMapSliderCustomerView()

@end

@implementation GWMapSliderCustomerView

-(instancetype)initWithImg:(UIImage *)img block:(void(^)())block{
    self = [super init];
    if (self){
        objc_setAssociatedObject(self, &menuActionWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createViewWithImg:img];
    }
    return self;
}

-(void)createViewWithImg:(UIImage *)img{
    self.frame = CGRectMake(0, 0, 40, 40);
    __weak typeof(self)weakSelf = self;
    [weakSelf setMenuActionWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &menuActionWithBlockKey);
        if (block){
            block();
        }
    }];
    
    UIImageView *imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [imgView1 setImage:img];
    imgView1.backgroundColor = [UIColor clearColor];
    [self addSubview:imgView1];
}

@end
