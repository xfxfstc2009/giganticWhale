//
//  GWMapSliderContentView.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/5.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GWMapSliderContentView;

typedef NS_ENUM(NSInteger,SliderMenuType) {
    SliderMenuTypeMapType = 1,                  /**< 地图类型*/
    SliderMenuTypeTraffic = 2,                  /**< 是否交通*/
    SliderMenuTypeOffLine = 3,                  /**< 离线地图*/
};

@protocol  GWMapSliderContentViewDelegate<NSObject>
-(void)sliderContentViewButtonDidSelected:(SliderMenuType)buttonIndex;

@end

@interface GWMapSliderContentView : UIView

@property (nonatomic,weak)id<GWMapSliderContentViewDelegate> delegate;


@end
