//
//  GWMapSliderCustomerView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWMapSliderView.h"
@interface GWMapSliderCustomerView : UIView

-(instancetype)initWithImg:(UIImage *)img block:(void(^)())block;
@end
