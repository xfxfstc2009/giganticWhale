//
//  GWMapSliderView.h
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWMapSliderCustomerView.h"

typedef enum {
    GWSideMenuPositionLeft,
    GWSideMenuPositionRight,
    GWSideMenuPositionTop,
    GWSideMenuPositionBottom
} GWSideMenuPosition;


@interface GWMapSliderView : UIView

@property (nonatomic, assign, readonly) BOOL isOpen;
@property (nonatomic, assign) CGFloat itemSpacing;
@property (nonatomic, assign) CGFloat animationDuration;
@property (nonatomic, assign) GWSideMenuPosition menuPosition;
- (id)initWithItems:(NSArray *)items;
- (void)open;
- (void)close;

@end

@interface UIView (MenuActionHandlers)

- (void)setMenuActionWithBlock:(void (^)(void))block;

@end
