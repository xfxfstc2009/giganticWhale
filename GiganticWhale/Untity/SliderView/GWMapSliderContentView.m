//
//  GWMapSliderContentView.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/5.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWMapSliderContentView.h"


@implementation GWMapSliderContentView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    for (int i = 0 ; i < 3;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat origin_y = LCFloat(30) + (LCFloat(50) - 2 * LCFloat(7) + LCFloat(25)) * i;
        __weak typeof(self)weakSelf = self;
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.delegate sliderContentViewButtonDidSelected:i];
        }];
        button.frame = CGRectMake(LCFloat(7), origin_y, (LCFloat(50) - 2 * LCFloat(7)), (LCFloat(50) - 2 * LCFloat(7)));
        [button setBackgroundImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
        [self addSubview:button];
    }
}

@end
