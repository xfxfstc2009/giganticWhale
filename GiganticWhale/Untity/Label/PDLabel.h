//
//  PDLabel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLabel : UILabel

// 颜色光晕
@property (nonatomic, assign) CGFloat glowOutSize;                      /**< 外部光*/
@property (nonatomic, strong) UIColor *glowOutColor;                    /**< 外部光颜色*/
@property (nonatomic, assign) CGFloat glowInSize;                       /**< 内部光大小*/
@property (nonatomic, strong) UIColor *glowInColor;                     /**< 内部颜色*/
@property (nonatomic,assign)BOOL glowShow;

@end
