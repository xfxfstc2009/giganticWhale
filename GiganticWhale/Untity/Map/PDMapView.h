//
//  PDMapView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
@interface PDMapView : UIView

@property (nonatomic,assign)CLLocationCoordinate2D transferCoordinate;


@end
