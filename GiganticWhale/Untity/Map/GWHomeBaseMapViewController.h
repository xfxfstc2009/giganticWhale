//
//  GWHomeBaseMapViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 1. 首页的地图
#import "GWBaseMapViewController.h"
#import "GiganticWhaleAnnotation.h"
typedef NS_ENUM(NSInteger,AMapPOISearchType) {
    AMapPOISearchTypeKeywords,                      /**< 根据关键字搜索*/
    AMapPOISearchTypeAround,                        /**< 搜索周边*/
};


@interface GWHomeBaseMapViewController : GWBaseMapViewController


#pragma mark - 修改地图类型【标准，卫星，黑夜】
-(void)changeMapTypeWithType:(MAMapType)mapType;
#pragma mark - 修改是否交通
-(void)changeMapTrafficType:(BOOL)show;

#pragma mark - POI
// 关键字搜索
- (void)searchPoiWithType:(AMapPOISearchType)searchType keyword:(NSString *)key;


@end
