//
//  GiganticWhaleAnnotationView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface GiganticWhaleAnnotationView : MAAnnotationView

// 脉冲
@property (nonatomic, strong) UIColor *annotationColor; // default is same as MKUserLocationView
@property (nonatomic, readwrite) NSTimeInterval pulseAnimationDuration; // default is 1s
@property (nonatomic, readwrite) NSTimeInterval delayBetweenPulseCycles; // default is 1s

// 上面显示的view
@property (nonatomic, strong) UIView *calloutView;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIImage *portrait;

//@property (nonatomic, strong) UIView *calloutView;


@end
