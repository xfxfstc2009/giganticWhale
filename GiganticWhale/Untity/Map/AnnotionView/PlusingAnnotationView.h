//
//  PlusingAnnotationView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 1. 脉冲view
#import <MAMapKit/MAMapKit.h>

@interface PlusingAnnotationView : MAAnnotationView

// 脉冲
@property (nonatomic, strong) UIColor *annotationColor; // default is same as MKUserLocationView
@property (nonatomic, readwrite) NSTimeInterval pulseAnimationDuration; // default is 1s
@property (nonatomic, readwrite) NSTimeInterval delayBetweenPulseCycles; // default is 1s

// 上面显示的view
@property (nonatomic, strong) UIView *calloutView;

//-(void)createViewWithImg:(UIImage *)icon text:(NSString *)text withBlock:(void(^)())block;

@end
