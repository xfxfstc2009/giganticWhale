//
//  POIAnnotation.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapSearchKit/AMapCommonObj.h>
#import <MAMapKit/MAMapKit.h>

@interface POIAnnotation : NSObject <MAAnnotation>
- (id)initWithPOI:(AMapPOI *)poi;

@property (nonatomic, readonly, strong) AMapPOI *poi;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

/*!
 @brief 获取annotation标题
 @return 返回annotation的标题信息
 */
- (NSString *)title;

/*!
 @brief 获取annotation副标题
 @return 返回annotation的副标题信息
 */
- (NSString *)subtitle;



@end
