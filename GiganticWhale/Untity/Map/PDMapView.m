//
//  PDMapView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PDMapView.h"

#import <AMapSearchKit/AMapSearchKit.h>

@interface PDMapView()<MAMapViewDelegate, AMapSearchDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) AMapSearchAPI *search;

@end

@implementation PDMapView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self mapWithInit];
    }
    return self;
}

#pragma mark - MapWithInit
-(void)mapWithInit{
    [self initMapView];                         // 创建地图
    [self initSearch];                          // 创建搜索
}

- (void)initSearch {
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
}

- (void)initMapView {
    self.mapView = [[MAMapView alloc]initWithFrame:self.bounds];
    self.mapView.delegate = self;
    [self addSubview:self.mapView];
}

-(void)setTransferCoordinate:(CLLocationCoordinate2D)transferCoordinate{
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc]init];
    pointAnnotation.coordinate = transferCoordinate;
    [self.mapView addAnnotation:pointAnnotation];
    [self.mapView setCenterCoordinate:transferCoordinate];
    
    [self.mapView setZoomLevel:LCFloat(14) animated:YES];
}


@end
