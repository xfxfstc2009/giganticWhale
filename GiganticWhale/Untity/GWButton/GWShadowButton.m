//
//  GWShadowButton.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWShadowButton.h"

@interface GWShadowButton()
@property (nonatomic,strong)UIActivityIndicatorView *activityIndicator;     /**< 菊花*/
@property (nonatomic,strong)UIView *blackView;                              /**< 阴影*/
@property (nonatomic,copy)NSString *originalTitle;                          /**< 标题*/

@end

@implementation GWShadowButton

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        
    }
    return self;
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    [self initializeColorsAndCorners];
}

-(void)initializeColorsAndCorners{
    self.layer.cornerRadius = self.size_height / 2.;
    self.clipsToBounds = YES;
    self.tintColor = [UIColor clearColor];
}


-(void)addButtomShadow{
    self.blackView = [[UIView alloc]initWithFrame:self.bounds];
    self.blackView.backgroundColor = [UIColor clearColor];
    self.blackView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.frame cornerRadius:self.frame.size.height/2].CGPath;
    self.blackView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.blackView.layer.shadowRadius = 3;
    self.blackView.layer.shadowOffset = CGSizeMake(0, 5);
    self.blackView.layer.shadowOpacity = .25;
    self.blackView.layer.cornerRadius = self.frame.size.height/2;
    
    [self.superview insertSubview:self.blackView belowSubview:self];
}

-(void)removeBottomShadow{
    if (self.blackView){
        [self.blackView removeFromSuperview];
    }
}

@end
