//
//  GWShadowButton.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/11.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWShadowButton : UIButton

@property (nonatomic, assign) BOOL shadowEnabled;                   /**< 判断是否需要阴影*/
@property (nonatomic, assign) BOOL working;                         /**< 判断是否在工作中*/


@end
