//
//  GWSearchBar.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, GWSearchBarState) {
    GWSearchBarStateNormal = 0,
    GWSearchBarStateSearchBarVisible,           /**< 搜索栏可见*/
    GWSearchBarStateSearchBarHasContent,        /**< 搜索栏内容*/
    GWSearchBarStateTransitioning               /**< 过度*/
};

@class GWSearchBar;
@protocol GWSearchBarDelegate <NSObject>
- (CGRect)destinationFrameForSearchBar:(GWSearchBar *)searchBar;
- (void)searchBar:(GWSearchBar *)searchBar willStartTransitioningToState:(GWSearchBarState)destinationState;
- (void)searchBar:(GWSearchBar *)searchBar didEndTransitioningFromState:(GWSearchBarState)previousState;
- (void)searchBarDidTapReturn:(GWSearchBar *)searchBar;
- (void)searchBarTextDidChange:(GWSearchBar *)searchBar;

@end

@interface GWSearchBar : UIView
@property (nonatomic, readonly) GWSearchBarState state;
@property (nonatomic, readonly) UITextField *searchField;

@property (nonatomic, weak) id<GWSearchBarDelegate>	delegate;
- (void)hideSearchBar:(id)sender;
@end
