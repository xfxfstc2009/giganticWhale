//
//  WhalePulseView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "WhalePulseView.h"
#import "WaveView.h"
#import "GWPulseLayer.h"

#define k_whale_Margin 20
@interface WhalePulseView()
@property (nonatomic,strong)WaveView *whaleView;
@property (nonatomic,strong)GWPulseLayer *pulseLayer;
@end

@implementation WhalePulseView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 创建一个鲸鱼
    self.whaleView = [[WaveView alloc]init];
    self.whaleView.trackTintColor = [UIColor darkGrayColor];
    self.whaleView.progressTintColor = UUBlue;
    self.whaleView.frame = CGRectMake(0, 0, self.size_width, self.size_height);
    [self addSubview:self.whaleView];
    
    WaveView *whaleView2 = [[WaveView alloc]init];
    whaleView2.trackTintColor = [UIColor lightGrayColor];
    whaleView2.progressTintColor = [UIColor lightGrayColor];
    
    whaleView2.frame = self.whaleView.frame;
    [self addSubview:whaleView2];
    
    [self.whaleView setShape:[self sm].CGPath];
    
    [self.whaleView setProgress:.8f];
    [self.whaleView startGravity];
    
    // 增加脉冲
    _pulseLayer = [GWPulseLayer layer];
    _pulseLayer.bounds = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
    _pulseLayer.position = CGPointMake(_pulseLayer.frame.size.width / 2., _pulseLayer.frame.size.height / 2.);
    _pulseLayer.radius = LCFloat(50);
    self.whaleView.backgroundColor = [UIColor redColor];
    [self.whaleView.layer.superlayer addSublayer:self.pulseLayer];
}


-(UIBezierPath *)sm{
    //// Bezier 5 Drawing
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    
    
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(64 / k_whale_Margin, 283/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin) controlPoint1: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint2: CGPointMake(132/ k_whale_Margin, 110/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(792/ k_whale_Margin, 181/ k_whale_Margin) controlPoint1: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin) controlPoint2: CGPointMake(666/ k_whale_Margin, 172/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(792/ k_whale_Margin, 180/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(721/ k_whale_Margin, 297/ k_whale_Margin) controlPoint1: CGPointMake(792/ k_whale_Margin, 180/ k_whale_Margin) controlPoint2: CGPointMake(705/ k_whale_Margin, 269/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin)];
    [bezier5Path addLineToPoint: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin) controlPoint1: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin) controlPoint2: CGPointMake(359/ k_whale_Margin, 188/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint1: CGPointMake(61/ k_whale_Margin, 304/ k_whale_Margin) controlPoint2: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier5Path.lineWidth = 1;
    [bezier5Path stroke];
    [bezierPath appendPath:bezier5Path];
    
    
    
    //// Bezier 13 Drawing
    UIBezierPath* bezier13Path = [UIBezierPath bezierPath];
    [bezier13Path moveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin)];
    [bezier13Path addLineToPoint: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin) controlPoint1: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin) controlPoint2: CGPointMake(312/ k_whale_Margin, 261/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin) controlPoint1: CGPointMake(644/ k_whale_Margin, 261/ k_whale_Margin) controlPoint2: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin) controlPoint1: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin) controlPoint2: CGPointMake(873/ k_whale_Margin, 362/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(993/ k_whale_Margin, 900/ k_whale_Margin) controlPoint1: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin) controlPoint2: CGPointMake(1009/ k_whale_Margin, 724/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(995/ k_whale_Margin, 900/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint1: CGPointMake(995/ k_whale_Margin, 900/ k_whale_Margin) controlPoint2: CGPointMake(948/ k_whale_Margin, 861/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin) controlPoint1: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint2: CGPointMake(863/ k_whale_Margin, 859/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin) controlPoint1: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin) controlPoint2: CGPointMake(826/ k_whale_Margin, 859/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(682/ k_whale_Margin, 868/ k_whale_Margin) controlPoint1: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin) controlPoint2: CGPointMake(700/ k_whale_Margin, 853/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin) controlPoint1: CGPointMake(664/ k_whale_Margin, 883/ k_whale_Margin) controlPoint2: CGPointMake(687/ k_whale_Margin, 748/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(711/ k_whale_Margin, 605/ k_whale_Margin) controlPoint1: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin) controlPoint2: CGPointMake(855/ k_whale_Margin, 671/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint1: CGPointMake(567/ k_whale_Margin, 539/ k_whale_Margin) controlPoint2: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint2: CGPointMake(455/ k_whale_Margin, 551/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin) controlPoint2: CGPointMake(400/ k_whale_Margin, 586/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint1: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin) controlPoint2: CGPointMake(359/ k_whale_Margin, 523/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin) controlPoint1: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint2: CGPointMake(165/ k_whale_Margin, 419/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint1: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin) controlPoint2: CGPointMake(64/ k_whale_Margin, 330/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier13Path.lineWidth = 1;
    [bezier13Path stroke];
    [bezierPath appendPath:bezier13Path];
    
    
    
    //// Bezier 7 Drawing
    UIBezierPath* bezier7Path = [UIBezierPath bezierPath];
    [bezier7Path moveToPoint: CGPointMake(64.5/ k_whale_Margin, 282.5/ k_whale_Margin)];
    [bezier7Path addLineToPoint: CGPointMake(101.5/ k_whale_Margin, 350.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(379.5/ k_whale_Margin, 572.5/ k_whale_Margin) controlPoint1: CGPointMake(101.5/ k_whale_Margin, 350.5/ k_whale_Margin) controlPoint2: CGPointMake(204.5/ k_whale_Margin, 533.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(380.5/ k_whale_Margin, 571.5/ k_whale_Margin)];
    [bezier7Path addLineToPoint: CGPointMake(465.5/ k_whale_Margin, 586.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(642.5/ k_whale_Margin, 606.5/ k_whale_Margin) controlPoint1: CGPointMake(465.5/ k_whale_Margin, 586.5/ k_whale_Margin) controlPoint2: CGPointMake(524.5/ k_whale_Margin, 606.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(643.5/ k_whale_Margin, 605.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(749.5/ k_whale_Margin, 650.5/ k_whale_Margin) controlPoint1: CGPointMake(643.5/ k_whale_Margin, 605.5/ k_whale_Margin) controlPoint2: CGPointMake(722.5/ k_whale_Margin, 612.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(750.5/ k_whale_Margin, 649.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(733.5/ k_whale_Margin, 649.5/ k_whale_Margin) controlPoint1: CGPointMake(750.5/ k_whale_Margin, 649.5/ k_whale_Margin) controlPoint2: CGPointMake(753.5/ k_whale_Margin, 664.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(574.5/ k_whale_Margin, 621.5/ k_whale_Margin) controlPoint1: CGPointMake(713.5/ k_whale_Margin, 634.5/ k_whale_Margin) controlPoint2: CGPointMake(652.5/ k_whale_Margin, 615.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin) controlPoint1: CGPointMake(496.5/ k_whale_Margin, 627.5/ k_whale_Margin) controlPoint2: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(479.5/ k_whale_Margin, 695.5/ k_whale_Margin) controlPoint1: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin) controlPoint2: CGPointMake(470.5/ k_whale_Margin, 675.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(389.5/ k_whale_Margin, 601.5/ k_whale_Margin) controlPoint1: CGPointMake(488.5/ k_whale_Margin, 715.5/ k_whale_Margin) controlPoint2: CGPointMake(417.5/ k_whale_Margin, 667.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(390.5/ k_whale_Margin, 600.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(64.5/ k_whale_Margin, 282.5/ k_whale_Margin) controlPoint1: CGPointMake(390.5/ k_whale_Margin, 600.5/ k_whale_Margin) controlPoint2: CGPointMake(127.5/ k_whale_Margin, 548.5/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier7Path.lineWidth = 1;
    [bezier7Path stroke];
    [bezierPath appendPath:bezier7Path];
    return bezierPath;
}


@end
