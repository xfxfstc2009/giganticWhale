//
//  WhaleView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WaveView.h"
@interface WhaleView : UIView

@property (nonatomic,strong)WaveView *whaleView;

@end
