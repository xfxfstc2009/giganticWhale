//
//  WhaleView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#define k_whale_Margin 10

#import "WhaleView.h"

@implementation WhaleView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.whaleView = [[WaveView alloc]init];
    self.whaleView.trackTintColor = [UIColor darkGrayColor];
    self.whaleView.progressTintColor = UUBlue;
    self.whaleView.frame = CGRectMake(0, 0, self.size_width, self.size_height);
    [self addSubview:self.whaleView];
    
    WaveView *whaleView2 = [[WaveView alloc]init];
    whaleView2.trackTintColor = [UIColor lightGrayColor];
    whaleView2.progressTintColor = [UIColor lightGrayColor];
    
    whaleView2.frame = self.whaleView.frame;
    [self addSubview:whaleView2];
    
    [self.whaleView setShape:[self sm].CGPath];
    
    [self.whaleView setProgress:.8f];
    [self.whaleView startGravity];
}


-(UIBezierPath *)smartShape1{
    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(64/ k_whale_Margin, 284/ k_whale_Margin)];
    [bezier4Path addCurveToPoint: CGPointMake(564/ k_whale_Margin, 246/ k_whale_Margin) controlPoint1: CGPointMake(64/ k_whale_Margin, 284/ k_whale_Margin) controlPoint2: CGPointMake(112/ k_whale_Margin, 130/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(794/ k_whale_Margin, 183.09/ k_whale_Margin) controlPoint1: CGPointMake(564.5/ k_whale_Margin, 245.5/ k_whale_Margin) controlPoint2: CGPointMake(677.25/ k_whale_Margin, 166.45/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(719/ k_whale_Margin, 289/ k_whale_Margin) controlPoint1: CGPointMake(794/ k_whale_Margin, 183/ k_whale_Margin) controlPoint2: CGPointMake(728/ k_whale_Margin, 253/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(729/ k_whale_Margin, 312/ k_whale_Margin) controlPoint1: CGPointMake(719/ k_whale_Margin, 289/ k_whale_Margin) controlPoint2: CGPointMake(714/ k_whale_Margin, 306/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(891/ k_whale_Margin, 683/ k_whale_Margin) controlPoint1: CGPointMake(729/ k_whale_Margin, 312/ k_whale_Margin) controlPoint2: CGPointMake(875/ k_whale_Margin, 381/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(993/ k_whale_Margin, 895/ k_whale_Margin) controlPoint1: CGPointMake(891/ k_whale_Margin, 683/ k_whale_Margin) controlPoint2: CGPointMake(1009/ k_whale_Margin, 766/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(980/ k_whale_Margin, 892/ k_whale_Margin) controlPoint1: CGPointMake(994/ k_whale_Margin, 895/ k_whale_Margin) controlPoint2: CGPointMake(991/ k_whale_Margin, 902/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint1: CGPointMake(980/ k_whale_Margin, 892/ k_whale_Margin) controlPoint2: CGPointMake(971/ k_whale_Margin, 870/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(845/ k_whale_Margin, 830/ k_whale_Margin) controlPoint1: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint2: CGPointMake(865/ k_whale_Margin, 861/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(763/ k_whale_Margin, 850/ k_whale_Margin) controlPoint1: CGPointMake(845/ k_whale_Margin, 830/ k_whale_Margin) controlPoint2: CGPointMake(814/ k_whale_Margin, 860/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(684/ k_whale_Margin, 868/ k_whale_Margin) controlPoint1: CGPointMake(763/ k_whale_Margin, 852/ k_whale_Margin) controlPoint2: CGPointMake(723/ k_whale_Margin, 849/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(677/ k_whale_Margin, 854/ k_whale_Margin) controlPoint1: CGPointMake(684/ k_whale_Margin, 868/ k_whale_Margin) controlPoint2: CGPointMake(670/ k_whale_Margin, 873/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(803/ k_whale_Margin, 726/ k_whale_Margin) controlPoint1: CGPointMake(677/ k_whale_Margin, 854/ k_whale_Margin) controlPoint2: CGPointMake(674/ k_whale_Margin, 769/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(796/ k_whale_Margin, 659/ k_whale_Margin) controlPoint1: CGPointMake(803/ k_whale_Margin, 726/ k_whale_Margin) controlPoint2: CGPointMake(842/ k_whale_Margin, 712/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint1: CGPointMake(796/ k_whale_Margin, 659/ k_whale_Margin) controlPoint2: CGPointMake(714/ k_whale_Margin, 583/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 588/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint2: CGPointMake(456/ k_whale_Margin, 555/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(613/ k_whale_Margin, 604/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 588/ k_whale_Margin) controlPoint2: CGPointMake(517/ k_whale_Margin, 604/ k_whale_Margin)];
    [bezier4Path addCurveToPoint: CGPointMake(749/ k_whale_Margin, 649/ k_whale_Margin) controlPoint1: CGPointMake(709/ k_whale_Margin, 604/ k_whale_Margin) controlPoint2: CGPointMake(749/ k_whale_Margin, 649/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(739/ k_whale_Margin, 652/ k_whale_Margin) controlPoint1: CGPointMake(749/ k_whale_Margin, 649/ k_whale_Margin) controlPoint2: CGPointMake(752/ k_whale_Margin, 661/ k_whale_Margin)];
    [bezier4Path addCurveToPoint: CGPointMake(541/ k_whale_Margin, 621/ k_whale_Margin) controlPoint1: CGPointMake(726/ k_whale_Margin, 643/ k_whale_Margin) controlPoint2: CGPointMake(680/ k_whale_Margin, 613/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(466/ k_whale_Margin, 613/ k_whale_Margin) controlPoint1: CGPointMake(541/ k_whale_Margin, 620/ k_whale_Margin) controlPoint2: CGPointMake(483/ k_whale_Margin, 620/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(481/ k_whale_Margin, 695/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 613/ k_whale_Margin) controlPoint2: CGPointMake(471/ k_whale_Margin, 661/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(470/ k_whale_Margin, 695/ k_whale_Margin) controlPoint1: CGPointMake(481/ k_whale_Margin, 695/ k_whale_Margin) controlPoint2: CGPointMake(482/ k_whale_Margin, 704/ k_whale_Margin)];
    [bezier4Path addCurveToPoint: CGPointMake(389/ k_whale_Margin, 601/ k_whale_Margin) controlPoint1: CGPointMake(458/ k_whale_Margin, 686/ k_whale_Margin) controlPoint2: CGPointMake(419/ k_whale_Margin, 663/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 284/ k_whale_Margin) controlPoint1: CGPointMake(389/ k_whale_Margin, 601/ k_whale_Margin) controlPoint2: CGPointMake(110/ k_whale_Margin, 553/ k_whale_Margin)];
    
    
    return bezier4Path;
}


-(UIBezierPath *)smartPath{
    // 上面的
    UIBezierPath *bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(212/ k_whale_Margin, 243/ k_whale_Margin)];
    [bezier4Path addCurveToPoint: CGPointMake(421/ k_whale_Margin, 238/ k_whale_Margin) controlPoint1: CGPointMake(212/ k_whale_Margin, 243/ k_whale_Margin) controlPoint2: CGPointMake(298/ k_whale_Margin, 205/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(477/ k_whale_Margin, 260/ k_whale_Margin) controlPoint1: CGPointMake(421/ k_whale_Margin, 238/ k_whale_Margin) controlPoint2: CGPointMake(470/ k_whale_Margin, 250/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(470/ k_whale_Margin, 262/ k_whale_Margin) controlPoint1: CGPointMake(477/ k_whale_Margin, 260/ k_whale_Margin) controlPoint2: CGPointMake(485/ k_whale_Margin, 264/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(255/ k_whale_Margin, 251/ k_whale_Margin) controlPoint1: CGPointMake(470/ k_whale_Margin, 263/ k_whale_Margin) controlPoint2: CGPointMake(323/ k_whale_Margin, 246/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(212/ k_whale_Margin, 247/ k_whale_Margin) controlPoint1: CGPointMake(255/ k_whale_Margin, 250/ k_whale_Margin) controlPoint2: CGPointMake(225/ k_whale_Margin, 251/ k_whale_Margin)];
    
    [bezier4Path addCurveToPoint: CGPointMake(212/ k_whale_Margin, 243/ k_whale_Margin) controlPoint1: CGPointMake(212/ k_whale_Margin, 247/ k_whale_Margin) controlPoint2: CGPointMake(208/ k_whale_Margin, 247/ k_whale_Margin)];
    [bezier4Path closePath];
    
    // 下面的
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint: CGPointMake(378.5/ k_whale_Margin, 569.5/ k_whale_Margin)];
    [path addCurveToPoint: CGPointMake(100.5/ k_whale_Margin, 351.5/ k_whale_Margin) controlPoint1: CGPointMake(378.5/ k_whale_Margin, 569.5/ k_whale_Margin) controlPoint2: CGPointMake(236.5/ k_whale_Margin, 562.5/ k_whale_Margin)];
    
    [path addCurveToPoint: CGPointMake(355.5/ k_whale_Margin, 483.5/ k_whale_Margin) controlPoint1: CGPointMake(100.5/ k_whale_Margin, 351.5/ k_whale_Margin) controlPoint2: CGPointMake(234.5/ k_whale_Margin, 447.5/ k_whale_Margin)];
    
    [path addCurveToPoint: CGPointMake(377.5/ k_whale_Margin, 570.5/ k_whale_Margin) controlPoint1: CGPointMake(356.5/ k_whale_Margin, 482.5/ k_whale_Margin) controlPoint2: CGPointMake(366.5/ k_whale_Margin, 551.5/ k_whale_Margin)];
    [path closePath];
    [bezier4Path appendPath:path];
    return bezier4Path;
}



-(UIBezierPath *)smartMain{
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    //    //// Bezier 5 Drawing
    //    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    //    [bezier5Path moveToPoint: CGPointMake(64 / k_whale_Margin, 287/ k_whale_Margin)];
    //    [bezier5Path addLineToPoint: CGPointMake(211/ k_whale_Margin, 246/ k_whale_Margin)];
    //    [bezier5Path addCurveToPoint: CGPointMake(478/ k_whale_Margin, 262/ k_whale_Margin) controlPoint1: CGPointMake(211/ k_whale_Margin, 246/ k_whale_Margin) controlPoint2: CGPointMake(316/ k_whale_Margin, 186/ k_whale_Margin)];
    ////    [bezier5Path moveToPoint: CGPointMake(478/ k_whale_Margin, 262/ k_whale_Margin)];
    //    [bezier5Path addLineToPoint: CGPointMake(719/ k_whale_Margin, 296/ k_whale_Margin)];
    //    [bezier5Path addCurveToPoint: CGPointMake(793/ k_whale_Margin, 184/ k_whale_Margin) controlPoint1: CGPointMake(719/ k_whale_Margin, 296/ k_whale_Margin) controlPoint2: CGPointMake(726/ k_whale_Margin, 248/ k_whale_Margin)];
    ////    [bezier5Path moveToPoint: CGPointMake(793/ k_whale_Margin, 184/ k_whale_Margin)];
    //    [bezier5Path addCurveToPoint: CGPointMake(566/ k_whale_Margin, 245/ k_whale_Margin) controlPoint1: CGPointMake(793/ k_whale_Margin, 184/ k_whale_Margin) controlPoint2: CGPointMake(690/ k_whale_Margin, 166/ k_whale_Margin)];
    ////    [bezier5Path moveToPoint: CGPointMake(566/ k_whale_Margin, 245/ k_whale_Margin)];
    //    [bezier5Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin) controlPoint1: CGPointMake(566/ k_whale_Margin, 245/ k_whale_Margin) controlPoint2: CGPointMake(139/ k_whale_Margin, 122/ k_whale_Margin)];
    ////    [bezier5Path closePath];
    //    [path appendPath:bezier5Path];
    
    
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(64 / k_whale_Margin, 286 / k_whale_Margin)];
    [bezier5Path addLineToPoint: CGPointMake(211 / k_whale_Margin, 249 / k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(478 / k_whale_Margin, 267 / k_whale_Margin) controlPoint1: CGPointMake(211 / k_whale_Margin, 249 / k_whale_Margin) controlPoint2: CGPointMake(316 / k_whale_Margin, 191 / k_whale_Margin)];
    [bezier5Path addLineToPoint: CGPointMake(719 / k_whale_Margin, 301 / k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(793 / k_whale_Margin, 183 / k_whale_Margin) controlPoint1: CGPointMake(719 / k_whale_Margin, 301 / k_whale_Margin) controlPoint2: CGPointMake(726 / k_whale_Margin, 247 / k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(566 / k_whale_Margin, 244 / k_whale_Margin) controlPoint1: CGPointMake(793 / k_whale_Margin, 183 / k_whale_Margin) controlPoint2: CGPointMake(690 / k_whale_Margin, 165 / k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(64 / k_whale_Margin, 286 / k_whale_Margin) controlPoint1: CGPointMake(566 / k_whale_Margin, 244 / k_whale_Margin) controlPoint2: CGPointMake(139 / k_whale_Margin, 121 / k_whale_Margin)];
    [bezier5Path closePath];
    bezier5Path.usesEvenOddFillRule = NO;
    [path appendPath:bezier5Path];
    
    
    
    //// Bezier 16 Drawing
    UIBezierPath* bezier16Path = [UIBezierPath bezierPath];
    [bezier16Path moveToPoint: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin)];
    [bezier16Path addLineToPoint: CGPointMake(211/ k_whale_Margin, 246/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(478/ k_whale_Margin, 262/ k_whale_Margin) controlPoint1: CGPointMake(211/ k_whale_Margin, 246/ k_whale_Margin) controlPoint2: CGPointMake(214/ k_whale_Margin, 256/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(478/ k_whale_Margin, 262/ k_whale_Margin)];
    [bezier16Path addLineToPoint: CGPointMake(719/ k_whale_Margin, 296/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(726/ k_whale_Margin, 310/ k_whale_Margin) controlPoint1: CGPointMake(719/ k_whale_Margin, 296/ k_whale_Margin) controlPoint2: CGPointMake(717/ k_whale_Margin, 310/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(726/ k_whale_Margin, 310/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(892/ k_whale_Margin, 689/ k_whale_Margin) controlPoint1: CGPointMake(726/ k_whale_Margin, 310/ k_whale_Margin) controlPoint2: CGPointMake(875/ k_whale_Margin, 375/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(892/ k_whale_Margin, 689/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(994/ k_whale_Margin, 897/ k_whale_Margin) controlPoint1: CGPointMake(892/ k_whale_Margin, 689/ k_whale_Margin) controlPoint2: CGPointMake(1000/ k_whale_Margin, 755/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(994/ k_whale_Margin, 897/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(892/ k_whale_Margin, 864/ k_whale_Margin) controlPoint1: CGPointMake(994/ k_whale_Margin, 897/ k_whale_Margin) controlPoint2: CGPointMake(955/ k_whale_Margin, 857/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(892/ k_whale_Margin, 863/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(845/ k_whale_Margin, 830/ k_whale_Margin) controlPoint1: CGPointMake(892/ k_whale_Margin, 863/ k_whale_Margin) controlPoint2: CGPointMake(868/ k_whale_Margin, 859/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(845/ k_whale_Margin, 830/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(755/ k_whale_Margin, 850/ k_whale_Margin) controlPoint1: CGPointMake(845/ k_whale_Margin, 830/ k_whale_Margin) controlPoint2: CGPointMake(811/ k_whale_Margin, 855/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(755/ k_whale_Margin, 850/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(680/ k_whale_Margin, 868/ k_whale_Margin) controlPoint1: CGPointMake(755/ k_whale_Margin, 850/ k_whale_Margin) controlPoint2: CGPointMake(705/ k_whale_Margin, 853/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(814/ k_whale_Margin, 722/ k_whale_Margin) controlPoint1: CGPointMake(655/ k_whale_Margin, 883/ k_whale_Margin) controlPoint2: CGPointMake(684/ k_whale_Margin, 740/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(814/ k_whale_Margin, 722/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(780/ k_whale_Margin, 646/ k_whale_Margin) controlPoint1: CGPointMake(814/ k_whale_Margin, 722/ k_whale_Margin) controlPoint2: CGPointMake(836/ k_whale_Margin, 684/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(468/ k_whale_Margin, 515/ k_whale_Margin) controlPoint1: CGPointMake(724/ k_whale_Margin, 608/ k_whale_Margin) controlPoint2: CGPointMake(551/ k_whale_Margin, 521/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(468/ k_whale_Margin, 515/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin) controlPoint1: CGPointMake(468/ k_whale_Margin, 515/ k_whale_Margin) controlPoint2: CGPointMake(456/ k_whale_Margin, 524/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin) controlPoint1: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin) controlPoint2: CGPointMake(435/ k_whale_Margin, 590/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint1: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin) controlPoint2: CGPointMake(359/ k_whale_Margin, 515/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin) controlPoint1: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint2: CGPointMake(156/ k_whale_Margin, 416/ k_whale_Margin)];
    //    [bezier16Path moveToPoint: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin)];
    [bezier16Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin) controlPoint1: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin) controlPoint2: CGPointMake(69/ k_whale_Margin, 310/ k_whale_Margin)];
    bezier16Path.lineWidth = 1;
    [bezier16Path closePath];
    [path appendPath:bezier16Path];
    
    
    
    //// Bezier Drawing
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin) controlPoint1: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin) controlPoint2: CGPointMake(70/ k_whale_Margin, 312/ k_whale_Margin)];
    //    [bezierPath moveToPoint: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin) controlPoint1: CGPointMake(99/ k_whale_Margin, 349/ k_whale_Margin) controlPoint2: CGPointMake(176/ k_whale_Margin, 520/ k_whale_Margin)];
    //    [bezierPath moveToPoint: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin) controlPoint1: CGPointMake(378/ k_whale_Margin, 571/ k_whale_Margin) controlPoint2: CGPointMake(425/ k_whale_Margin, 587/ k_whale_Margin)];
    //    [bezierPath moveToPoint: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(631/ k_whale_Margin, 605/ k_whale_Margin) controlPoint1: CGPointMake(462/ k_whale_Margin, 587/ k_whale_Margin) controlPoint2: CGPointMake(529/ k_whale_Margin, 605/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(755/ k_whale_Margin, 659/ k_whale_Margin) controlPoint1: CGPointMake(733/ k_whale_Margin, 605/ k_whale_Margin) controlPoint2: CGPointMake(755/ k_whale_Margin, 659/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(751/ k_whale_Margin, 663/ k_whale_Margin) controlPoint1: CGPointMake(755/ k_whale_Margin, 659/ k_whale_Margin) controlPoint2: CGPointMake(757/ k_whale_Margin, 667/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(622/ k_whale_Margin, 623/ k_whale_Margin) controlPoint1: CGPointMake(745/ k_whale_Margin, 659/ k_whale_Margin) controlPoint2: CGPointMake(713/ k_whale_Margin, 619/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(466/ k_whale_Margin, 614/ k_whale_Margin) controlPoint1: CGPointMake(531/ k_whale_Margin, 627/ k_whale_Margin) controlPoint2: CGPointMake(473/ k_whale_Margin, 618/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(482/ k_whale_Margin, 695/ k_whale_Margin) controlPoint1: CGPointMake(459/ k_whale_Margin, 610/ k_whale_Margin) controlPoint2: CGPointMake(475/ k_whale_Margin, 682/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(462/ k_whale_Margin, 690/ k_whale_Margin) controlPoint1: CGPointMake(489/ k_whale_Margin, 708/ k_whale_Margin) controlPoint2: CGPointMake(469/ k_whale_Margin, 699/ k_whale_Margin)];
    //    [bezierPath moveToPoint: CGPointMake(462/ k_whale_Margin, 690/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(389/ k_whale_Margin, 600/ k_whale_Margin) controlPoint1: CGPointMake(462/ k_whale_Margin, 690/ k_whale_Margin) controlPoint2: CGPointMake(413/ k_whale_Margin, 661/ k_whale_Margin)];
    //    [bezierPath moveToPoint: CGPointMake(389/ k_whale_Margin, 600/ k_whale_Margin)];
    [bezierPath addCurveToPoint: CGPointMake(64/ k_whale_Margin, 287/ k_whale_Margin) controlPoint1: CGPointMake(389/ k_whale_Margin, 600/ k_whale_Margin) controlPoint2: CGPointMake(152/ k_whale_Margin, 573/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezierPath.lineWidth = 1;
//    [bezierPath stroke];
    [bezierPath closePath];
    [path appendPath:bezierPath];
    
    return path;
}


-(UIBezierPath *)sm{
    //// Bezier 5 Drawing
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    
    
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(64 / k_whale_Margin, 283/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin) controlPoint1: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint2: CGPointMake(132/ k_whale_Margin, 110/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(792/ k_whale_Margin, 181/ k_whale_Margin) controlPoint1: CGPointMake(566/ k_whale_Margin, 244/ k_whale_Margin) controlPoint2: CGPointMake(666/ k_whale_Margin, 172/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(792/ k_whale_Margin, 180/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(721/ k_whale_Margin, 297/ k_whale_Margin) controlPoint1: CGPointMake(792/ k_whale_Margin, 180/ k_whale_Margin) controlPoint2: CGPointMake(705/ k_whale_Margin, 269/ k_whale_Margin)];
    //    [bezier5Path moveToPoint: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin)];
    [bezier5Path addLineToPoint: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin) controlPoint1: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin) controlPoint2: CGPointMake(359/ k_whale_Margin, 188/ k_whale_Margin)];
    [bezier5Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint1: CGPointMake(61/ k_whale_Margin, 304/ k_whale_Margin) controlPoint2: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier5Path.lineWidth = 1;
//    [bezier5Path stroke];
    [bezierPath appendPath:bezier5Path];
    
    
    
    //// Bezier 13 Drawing
    UIBezierPath* bezier13Path = [UIBezierPath bezierPath];
    [bezier13Path moveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin)];
    [bezier13Path addLineToPoint: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(478/ k_whale_Margin, 261/ k_whale_Margin) controlPoint1: CGPointMake(210/ k_whale_Margin, 246/ k_whale_Margin) controlPoint2: CGPointMake(312/ k_whale_Margin, 261/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin) controlPoint1: CGPointMake(644/ k_whale_Margin, 261/ k_whale_Margin) controlPoint2: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin) controlPoint1: CGPointMake(719/ k_whale_Margin, 297/ k_whale_Margin) controlPoint2: CGPointMake(873/ k_whale_Margin, 362/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(993/ k_whale_Margin, 900/ k_whale_Margin) controlPoint1: CGPointMake(892/ k_whale_Margin, 685/ k_whale_Margin) controlPoint2: CGPointMake(1009/ k_whale_Margin, 724/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(995/ k_whale_Margin, 900/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint1: CGPointMake(995/ k_whale_Margin, 900/ k_whale_Margin) controlPoint2: CGPointMake(948/ k_whale_Margin, 861/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin) controlPoint1: CGPointMake(897/ k_whale_Margin, 864/ k_whale_Margin) controlPoint2: CGPointMake(863/ k_whale_Margin, 859/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin) controlPoint1: CGPointMake(844/ k_whale_Margin, 829/ k_whale_Margin) controlPoint2: CGPointMake(826/ k_whale_Margin, 859/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(682/ k_whale_Margin, 868/ k_whale_Margin) controlPoint1: CGPointMake(753/ k_whale_Margin, 851/ k_whale_Margin) controlPoint2: CGPointMake(700/ k_whale_Margin, 853/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin) controlPoint1: CGPointMake(664/ k_whale_Margin, 883/ k_whale_Margin) controlPoint2: CGPointMake(687/ k_whale_Margin, 748/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(711/ k_whale_Margin, 605/ k_whale_Margin) controlPoint1: CGPointMake(813/ k_whale_Margin, 720/ k_whale_Margin) controlPoint2: CGPointMake(855/ k_whale_Margin, 671/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint1: CGPointMake(567/ k_whale_Margin, 539/ k_whale_Margin) controlPoint2: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 515/ k_whale_Margin) controlPoint2: CGPointMake(455/ k_whale_Margin, 551/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin) controlPoint1: CGPointMake(465/ k_whale_Margin, 587/ k_whale_Margin) controlPoint2: CGPointMake(400/ k_whale_Margin, 586/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint1: CGPointMake(379/ k_whale_Margin, 573/ k_whale_Margin) controlPoint2: CGPointMake(359/ k_whale_Margin, 523/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin) controlPoint1: CGPointMake(359/ k_whale_Margin, 484/ k_whale_Margin) controlPoint2: CGPointMake(165/ k_whale_Margin, 419/ k_whale_Margin)];
    //    [bezier13Path moveToPoint: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin)];
    [bezier13Path addCurveToPoint: CGPointMake(64/ k_whale_Margin, 283/ k_whale_Margin) controlPoint1: CGPointMake(101/ k_whale_Margin, 351/ k_whale_Margin) controlPoint2: CGPointMake(64/ k_whale_Margin, 330/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier13Path.lineWidth = 1;
//    [bezier13Path stroke];
    [bezierPath appendPath:bezier13Path];
    
    
    
    //// Bezier 7 Drawing
    UIBezierPath* bezier7Path = [UIBezierPath bezierPath];
    [bezier7Path moveToPoint: CGPointMake(64.5/ k_whale_Margin, 282.5/ k_whale_Margin)];
    [bezier7Path addLineToPoint: CGPointMake(101.5/ k_whale_Margin, 350.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(379.5/ k_whale_Margin, 572.5/ k_whale_Margin) controlPoint1: CGPointMake(101.5/ k_whale_Margin, 350.5/ k_whale_Margin) controlPoint2: CGPointMake(204.5/ k_whale_Margin, 533.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(380.5/ k_whale_Margin, 571.5/ k_whale_Margin)];
    [bezier7Path addLineToPoint: CGPointMake(465.5/ k_whale_Margin, 586.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(642.5/ k_whale_Margin, 606.5/ k_whale_Margin) controlPoint1: CGPointMake(465.5/ k_whale_Margin, 586.5/ k_whale_Margin) controlPoint2: CGPointMake(524.5/ k_whale_Margin, 606.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(643.5/ k_whale_Margin, 605.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(749.5/ k_whale_Margin, 650.5/ k_whale_Margin) controlPoint1: CGPointMake(643.5/ k_whale_Margin, 605.5/ k_whale_Margin) controlPoint2: CGPointMake(722.5/ k_whale_Margin, 612.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(750.5/ k_whale_Margin, 649.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(733.5/ k_whale_Margin, 649.5/ k_whale_Margin) controlPoint1: CGPointMake(750.5/ k_whale_Margin, 649.5/ k_whale_Margin) controlPoint2: CGPointMake(753.5/ k_whale_Margin, 664.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(574.5/ k_whale_Margin, 621.5/ k_whale_Margin) controlPoint1: CGPointMake(713.5/ k_whale_Margin, 634.5/ k_whale_Margin) controlPoint2: CGPointMake(652.5/ k_whale_Margin, 615.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin) controlPoint1: CGPointMake(496.5/ k_whale_Margin, 627.5/ k_whale_Margin) controlPoint2: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(479.5/ k_whale_Margin, 695.5/ k_whale_Margin) controlPoint1: CGPointMake(464.5/ k_whale_Margin, 615.5/ k_whale_Margin) controlPoint2: CGPointMake(470.5/ k_whale_Margin, 675.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(389.5/ k_whale_Margin, 601.5/ k_whale_Margin) controlPoint1: CGPointMake(488.5/ k_whale_Margin, 715.5/ k_whale_Margin) controlPoint2: CGPointMake(417.5/ k_whale_Margin, 667.5/ k_whale_Margin)];
    //    [bezier7Path moveToPoint: CGPointMake(390.5/ k_whale_Margin, 600.5/ k_whale_Margin)];
    [bezier7Path addCurveToPoint: CGPointMake(64.5/ k_whale_Margin, 282.5/ k_whale_Margin) controlPoint1: CGPointMake(390.5/ k_whale_Margin, 600.5/ k_whale_Margin) controlPoint2: CGPointMake(127.5/ k_whale_Margin, 548.5/ k_whale_Margin)];
    [UIColor.blackColor setStroke];
    bezier7Path.lineWidth = 1;
//    [bezier7Path stroke];
    [bezierPath appendPath:bezier7Path];
    return bezierPath;
}



@end
