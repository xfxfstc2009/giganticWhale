//
//  WebViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "WebViewController.h"
#import "GWWebView.h"

@interface WebViewController()<GWWebViewDelegate>
@property (nonatomic,strong)GWWebView *mainWebView;

@end

@implementation WebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark createWebView
-(void)createWebView{
    self.mainWebView = [[GWWebView alloc]initWithFrame:self.view.bounds];
    self.mainWebView.delegate = self;
    self.mainWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.mainWebView];
}

// 跳转
-(void)webViewControllerWithAddress:(NSString *)url{
    if (!self.mainWebView){
        [self createWebView];
    }
    [self.mainWebView loadURLString:url];
}


- (void)webViewDidStartLoad:(GWWebView *)webview {
    NSLog(@"页面开始加载");
}

- (void)webView:(GWWebView *)webview shouldStartLoadWithURL:(NSURL *)URL {
    NSLog(@"截取到URL：%@",URL);
}

- (void)webView:(GWWebView *)webview didFinishLoadingURL:(NSURL *)URL {
    NSString *title = @"";
    if (webview.uiWebView){
        title = [webview.uiWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    } else {
        title = webview.wkWebView.title;
    }
    self.barMainTitle = title;
}

- (void)webView:(GWWebView *)webview didFailToLoadURL:(NSURL *)URL error:(NSError *)error {
    NSLog(@"加载出现错误");
}


@end
