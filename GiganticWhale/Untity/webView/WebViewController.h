//
//  WebViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"

@interface WebViewController : AbstractViewController

-(void)webViewControllerWithAddress:(NSString *)url;

@end
