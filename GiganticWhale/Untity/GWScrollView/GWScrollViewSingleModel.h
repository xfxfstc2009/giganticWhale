//
//  GWScrollViewSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface GWScrollViewSingleModel : FetchModel

@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *content;

@end
