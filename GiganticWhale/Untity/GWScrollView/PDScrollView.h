//
//  PDScrollView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/14.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWScrollViewSingleModel.h"
@interface PDScrollView : UIView

@property (nonatomic,assign)BOOL isPageControl;         /**< 是否page*/
@property (nonatomic,strong)NSArray *transferImArr;     /**< 传入数组*/

-(void)startTimerWithTime:(NSInteger)time;
-(void)removeTimer;

-(void)bannerImgTapManagerWithInfoblock:(void(^)(GWScrollViewSingleModel *))block;

@end

