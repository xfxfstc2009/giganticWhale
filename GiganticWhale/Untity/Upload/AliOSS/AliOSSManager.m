//
//  AliOSSManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/22.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "AliOSSManager.h"

@interface AliOSSManager()

@end

@implementation AliOSSManager

-(void)dealloc{
    NSLog(@"释放");
}

#pragma mark - 初始化 OSS
+(OSSClient *)clientWithInit{
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:ossAccessKey secretKey:ossSecretKey];
    
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 3;                                     // 网络请求遇到异常失败后的重试次数
    conf.timeoutIntervalForRequest = 30;                        // 网络请求的超时时间
    conf.timeoutIntervalForResource = 24 * 60 * 60;             // 允许资源传输的最长时间
    
    OSSClient *client = [[OSSClient alloc] initWithEndpoint:ossEndpoint credentialProvider:credential clientConfiguration:conf];
    return client;
}

#pragma mark  上传 Laungch
+(void)uploadLaungchFileWithImg:(OSSSingleFileModel *)objc compressionQuality:(CGFloat)compressionQuality andBlock:(void (^)(NSString *fileName))block{
    OSSPutObjectRequest *putRequest = [[OSSPutObjectRequest alloc]init];
    putRequest.bucketName = ossBucketName;
    
    putRequest.objectKey = [NSString stringWithFormat:@"Laungch/%@",objc.objcName];
    
    UIImage *image = objc.objcImg;
    NSData *objcData = UIImageJPEGRepresentation(image,compressionQuality);
    putRequest.uploadingData = objcData;
    
    putRequest.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
    };
    
    OSSTask *putTask = [[AliOSSManager clientWithInit] putObject:putRequest];
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            
        } else {
            NSLog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
//    [putTask waitUntilFinished];
    
    if (block){
        block(objc.objcName);
    }
}

#pragma mark - 上传文件
+(void)uploadFileWithImgArr:(NSArray<OSSSingleFileModel> *)objcArr uploadType:(OSSUploadType)type compressionRatio:(CGFloat)compressionRatio block:(void(^)(NSArray *fileUrlArr))fileArrBlock{
    NSMutableArray *fileUrlArr = [NSMutableArray array];
    for (OSSSingleFileModel *singleModel in objcArr){
        __weak typeof(self)weakSelf = self;
        [weakSelf uploadFileWithObjctype:type name:singleModel.objcName objImg:singleModel.objcImg block:^(NSString *fileUrl) {
            if (!weakSelf){
                return ;
            }
            [fileUrlArr addObject:fileUrl];
        } progress:^(CGFloat progress) {
            NSLog(@"%.2f",progress);
        }];
    }
    if (fileArrBlock){
        fileArrBlock(fileUrlArr);
    }
}




#pragma msrk 上传文件
+(void)uploadFileWithObjctype:(OSSUploadType)type name:(NSString *)objcName objImg:(UIImage *)img block:(void(^)(NSString *fileUrl))block progress:(void(^)(CGFloat progress))progressBlock{
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    
    put.bucketName = ossBucketName;
    if (!objcName.length){
        objcName = [NSDate getCurrentTimeWithSetting];
    }
    
    NSData *fileData = UIImageJPEGRepresentation(img,0.7);
    if (type == OSSUploadTypePlayerAblum){
        put.objectKey = [NSString stringWithFormat:@"player/ablum/%@%li.png",objcName,(long)(arc4random() % 7)] ;
    }else if (type == OSSUploadTypePlayerInfo){
        put.objectKey = [NSString stringWithFormat:@"player/info/%@%li.png",objcName,(long)(arc4random() % 7)] ;
    } else if (type == OSSUploadTypeDiary){
        put.objectKey = [NSString stringWithFormat:@"diary/%@-%@",objcName,[NSDate getCurrentTimeWithYotobe]];
        fileData = UIImageJPEGRepresentation(img,1.0);
    }

    put.uploadingData = fileData; // 直接上传NSData
    
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {    // 上传进度
//        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        if (totalByteSent == totalBytesExpectedToSend){

        } else {
            NSInteger totalByteSentInteger = [[NSString stringWithFormat:@"%lld",totalByteSent] integerValue];
            NSInteger totalBytesExpectedToSendInteger = [[NSString stringWithFormat:@"%lld",totalBytesExpectedToSend] integerValue];
            CGFloat progress = totalByteSentInteger * 1.f / totalBytesExpectedToSendInteger;
            if (progressBlock){
                progressBlock(progress);
            }
        }
    };
    
    OSSTask * putTask = [[AliOSSManager clientWithInit] putObject:put];
    
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
//            [PDHUD showHUDSuccess:@"上传成功"];
        } else {
            NSLog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
    
    // 可以等待任务完成
    [putTask waitUntilFinished];
    
    if (block){
        block([NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,put.objectKey]);
    }
}

//#pragma mark 上传文件到Laungch
//-(void)uploadLaungchFileWithImg:(OSSSingleFileModel *)objc andBlock:(void (^)(NSString *fileName))block{
//    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
//    
//    put.bucketName = ossBucketName;
//    if (objc.objcName.length){
//        put.objectKey = objc.objcName;
//    } else {
//        put.objectKey = [NSString stringWithFormat:@"%@.png",[NSDate getCurrentTimeWithFileName]];
//    }
//
//    
//    UIImage *fileImg = objc.objcImg;
//    NSData *data = UIImageJPEGRepresentation(fileImg, 1);
//    put.uploadingData = data;
//    
//    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//        NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
//        [StatusBarManager statusBarWithText:@"Laungch上传中" progress:(totalByteSent / totalBytesExpectedToSend)];
//    };
//    
//    OSSTask * putTask = [[AliOSSManager clientWithInit] putObject:put];
//    
//    [putTask continueWithBlock:^id(OSSTask *task) {
//        if (block){
//            block(put.objectKey);
//        }
//        if (!task.error) {
//            [StatusBarManager statusBarHidenWithText:@"Laungch页面上传成功"];
//        } else {
//            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"Laungch页面上传失败%@",task.error.localizedDescription]];
//        }
//        return nil;
//    }];
//    
//    // 可以等待任务完成
//    [putTask waitUntilFinished];
//}
//
//
//-(void)uploadFileWithImgArr:(NSArray<OSSSingleFileModel *> *)objcArr compressionRatio:(CGFloat)compressionRatio{
//    for(int i = 0 ; i < objcArr.count; i++){
//        OSSSingleFileModel *fileModel = [objcArr objectAtIndex:i];
//        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
//        put.bucketName = ossBucketName;
//        if (fileModel.objcName.length){
//            put.objectKey = fileModel.objcName;
//        } else {
//            put.objectKey = [NSString stringWithFormat:@"%@",[NSDate getCurrentTimeWithFileName]];
//        }
//        
//        UIImage *fileImg = fileModel.objcImg;
//        if (compressionRatio >1){
//            [[UIAlertView alertViewWithTitle:@"错误" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
//        }
//        NSData *data = UIImageJPEGRepresentation(fileImg, compressionRatio);
//        put.uploadingData = data;
//        
//        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//            
//            NSLog(@"第%li张, %lld, %lld, %lld", (long)i,bytesSent, totalByteSent, totalBytesExpectedToSend);
//        };
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//          OSSTask * putTask = [[AliOSSManager clientWithInit] putObject:put];
//            if (data != nil) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    
//                    [putTask continueWithBlock:^id(OSSTask *task) {
//                        if (!task.error) {
//                            NSLog(@"upload object success!");
//                        } else {
//                            NSLog(@"upload object failed, error: %@" , task.error);
//                        }
//                        return nil;
//                    }];
//                    
//                    // 可以等待任务完成
//                    [putTask waitUntilFinished];
//                });
//            } else {
//
//            }
//        });
//    }
//}




@end
