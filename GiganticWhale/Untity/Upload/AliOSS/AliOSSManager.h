//
//  AliOSSManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/22.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSSSingleFileModel.h"
#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>

typedef NS_ENUM(NSInteger,OSSUploadType) {
    OSSUploadTypePlayerInfo,                       /**< 视频详情*/
    OSSUploadTypePlayerAblum,                      /**< 视频专辑*/
    OSSUploadTypeDiary,                            /**< 日记*/
};

@interface AliOSSManager : NSObject

+(OSSClient *)clientWithInit;

#pragma mark - 上传图片数组
+(void)uploadFileWithImgArr:(NSArray<OSSSingleFileModel> *)objcArr uploadType:(OSSUploadType)type compressionRatio:(CGFloat)compressionRatio block:(void(^)(NSArray *fileUrlArr))fileArrBlock;

#pragma mark  上传 Laungch
+(void)uploadLaungchFileWithImg:(OSSSingleFileModel *)objc compressionQuality:(CGFloat)compressionQuality andBlock:(void (^)(NSString *fileName))block;

#pragma msrk 上传文件
+(void)uploadFileWithObjctype:(OSSUploadType)type name:(NSString *)objcName objImg:(UIImage *)img block:(void(^)(NSString *fileUrl))block progress:(void(^)(CGFloat progress))progressBlock;
@end
