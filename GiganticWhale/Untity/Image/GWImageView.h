//
//  GWImageView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWImageView : UIImageView


#define Laungch
-(void)uploadImageWithLaungchURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
-(void)uploadWithNormal:(NSString *)url size:(CGSize)size back:(void(^)(UIImage *img))back;

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
-(void)uploadImgWithNormal:(NSString *)url placehorder:(UIImage *)placeholder back:(void(^)(UIImage *img))back;


#pragma mark - 视频播放器
-(void)uploadImageWithPlayerURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *image))callbackBlock;
-(void)uploadPlayerListImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;


#pragma mark - 关于我
-(void)uploadAboutProductURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock;

#pragma mark - 关于我的公司
-(void)uploadAboutCompanyURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock;

#pragma mark - 音乐播放器
-(void)uploadMusicAblumListImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadMusicAblumListImageWithURL1:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;


-(void)uploadImageWithURL1:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;

#pragma mark - 音乐播放器
-(void)uploadImageWithMusicrURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *image))callbackBlock;

#pragma mark - 消息
-(void)uploadImageWithMessageURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock;
@end

