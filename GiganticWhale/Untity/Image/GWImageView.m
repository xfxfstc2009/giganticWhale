//
//  GWImageView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWImageView.h"
#import <UIImageView+WebCache.h>


@implementation GWImageView

#define Laungch
-(void)uploadImageWithLaungchURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)kScreenBounds.size.height,(long)kScreenBounds.size.width]];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"empty_placeholder"];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadWithNormal:(NSString *)url size:(CGSize)size back:(void(^)(UIImage *img))back{
    if (![url isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:url];
    if ([url hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:url];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,url,(long)size.width,(long)size.height]];
    }
    
    UIImage *placeholder = [UIImage imageNamed:@"giganticWhale"];
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image){
            image = placeholder;
        }
        if (back){
            back(image);
        }
    }];
}

-(void)uploadImgWithNormal:(NSString *)url placehorder:(UIImage *)placeholder back:(void(^)(UIImage *img))back{
    if (![url isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:url];
    if ([url hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:url];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,url,(long)self.size_width,(long)self.size_height]];
    }
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"giganticWhale"];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image){
            image = placeholder;
        }
        if (back){
            back(image);
        }
    }];
}

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placeholder"];
    }
    if ([urlString hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:urlString];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)kScreenBounds.size.width,(long)(kScreenBounds.size.height / 1.7)]];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - 视频播放器
-(void)uploadImageWithPlayerURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL;
    
    if ([urlString hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:urlString];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)kScreenBounds.size.width,(long)(kScreenBounds.size.height / 1.7)]];
    }
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_video"];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - playerList
-(void)uploadPlayerListImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"image1_320x210"];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image != nil){
            self.image = image;
        } else {
            self.image = placeholder;
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - About
#pragma mark - 关于我商品
-(void)uploadAboutProductURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    
    [self sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"123"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - 关于我的公司
-(void)uploadAboutCompanyURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@0o_0l_%liw_90q.png",aliyunOSS_BaseURL,urlString,(long)self.size_height]];
    
    [self sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"123"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}


#pragma mark - 音乐播放器
-(void)uploadMusicAblumListImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"image1_320x210"];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image != nil){
            self.image = image;
        } else {
            self.image = placeholder;
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}


-(void)uploadMusicAblumListImageWithURL1:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    
    NSURL *imgURL;
 
    if ([urlString hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:urlString];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image != nil){
            self.image = image;
        } else {
            self.image = placeholder;
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

// 更新图片
-(void)uploadImageWithURL1:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placeholder"];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - 音乐播放器
-(void)uploadImageWithMusicrURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"empty_placeholder"];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"123"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (callbackBlock){
            if (image == nil){
                image = [UIImage imageNamed:@"bg"];
            }
            callbackBlock(image);
        }
    }];
}

#pragma mark - 消息
-(void)uploadImageWithMessageURL:(NSString *)urlString callback:(void (^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw.png",aliyunOSS_BaseURL,urlString,(long)self.size_height,(long)self.size_width]];
    
    [self sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"123"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}
@end
