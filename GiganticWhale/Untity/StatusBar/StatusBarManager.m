//
//  StatusBarManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/30.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "StatusBarManager.h"
#import <WTStatusBar/WTStatusBar.h>
@implementation StatusBarManager

+(void)statusBarWithClear{
    [WTStatusBar setBackgroundColor:UUBlue];
    [WTStatusBar clearStatusAnimated:YES];
}

+(void)statusBarShowWithText:(NSString *)text{
    [WTStatusBar setBackgroundColor:UUBlue];
    [WTStatusBar setStatusText:text animated:YES];
}

+(void)statusBarHidenWithText:(NSString *)text{
    [WTStatusBar setBackgroundColor:UUBlue];
    [WTStatusBar setStatusText:text timeout:3 animated:YES];
}

+(void)statusBarWithText:(NSString *)text progress:(CGFloat)progress{
    if (progress < 1.0){
        [WTStatusBar setProgressBarColor:UUBlue];
        [WTStatusBar setStatusText:text animated:YES];
        [WTStatusBar setProgress:progress animated:YES];
    } else {
        [WTStatusBar setProgressBarColor:UUBlue];
    }
}


+(void)testStatusBarWithText:(NSString *)text progress:(CGFloat)progress{
    [WTStatusBar setBackgroundColor:UUBlue];
    [WTStatusBar setProgress:progress animated:YES];
}

@end
