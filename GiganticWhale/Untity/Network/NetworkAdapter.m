//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"

@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;
    
    NetworkEngine *networkEngine;
    if ([path hasPrefix:@"Class"]){
        NSString *baseURLString = @"";
        if (API_PORT.length){
            baseURLString = [NSString stringWithFormat:@"http://%@:%@/%@/",API_HOST,API_PORT,API_VER];
        } else {
            baseURLString = [NSString stringWithFormat:@"http://%@/%@",API_HOST,API_VER];
        }
        
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:baseURLString]];
    } else if ([path hasPrefix:@"api"]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",BaoBab_HOST]]];
    } else if ([path hasPrefix:yinyuetai_info]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",Yinyuetai_Host]]];
    } else if ([path hasPrefix:tuling_info]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",Tuling_Host]]];
    }
    
    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                if ([path hasPrefix:@"api"]){
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:responseObject];
                    block(YES,responseModelObject,nil);
                    return;
                }
                
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"errCode"];
                if ([path hasPrefix:yinyuetai_info]){
                    errorNumber = [responseObject objectForKey:@"error"];
                } else if ([path hasPrefix:tuling_info]){
                    errorNumber = [responseObject objectForKey:@"code"];
                }
                if (errorNumber.integerValue == 200 || (errorNumber.integerValue == 0 && [path hasPrefix:yinyuetai_info]) || (errorNumber.integerValue == 100000 && [path hasPrefix:tuling_info])){           // 请求成功
                    if (responseObjectClass == nil){
                        NSDictionary *dic;
                        if ([path hasPrefix:yinyuetai_info]){
                            dic = responseObject;
                        } else if ([path hasPrefix:tuling_info]){
                          dic = responseObject;
                        } else {
                            dic = [responseObject objectForKey:@"data"];
                        }
                        block(YES,dic,nil);
                        return;
                    }
                    NSDictionary *responseMaxObj;
                    if ([path hasPrefix:yinyuetai_info]){
                        responseMaxObj = responseObject;
                    } else if ([path hasPrefix:tuling_info]){
                        responseMaxObj = responseObject;
                    } else {
                        responseMaxObj =[responseObject objectForKey:@"data"];
                    }
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:responseMaxObj];
                    block(YES,responseModelObject,nil);
                } else {                                        // 业务错误
                    NSString *errorInfo = [responseObject objectForKey:@"errMsg"];
                    if (!errorInfo.length){
                        errorInfo = @"系统错误，请联系管理员";
                    }
                    NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                    NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                    [[UIAlertView alertViewWithTitle:@"系统出错" message:errorInfo buttonTitles:@[@"确定"] callBlock:NULL]show];
                    block(NO,responseObject,bizError);
                    return;
                }
            }
        }
    }];
}




@end

