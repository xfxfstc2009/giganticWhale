//
//  NetErrorMap.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetErrorMap : NSObject

+ (NSString *)networkErrorMapWithErrCode:(NSString *)errCode;

@end
