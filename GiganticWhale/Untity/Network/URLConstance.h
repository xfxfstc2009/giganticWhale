//
//  URLConstance.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试           // 【JAVA】
#define API_HOST @"120.92.167.62"//192.168.0.163
#define API_PORT @"8001"
#else               // 线上
#define API_HOST  @"120.92.167.62"
#define API_PORT  @"8001"
#endif


#define WECHAT_HOST @"api.weixin.qq.com/sns"

#define BaoBab_HOST @"baobab.wandoujia.com"

#define Yinyuetai_Host @"www.yinyuetai.com/api/info"
// 【音悦台】
static NSString *const yinyuetai_info = @"get-video-urls";

// 【图灵】
#define Tuling_Host @"www.tuling123.com"
static NSString *const tuling_info = @"openapi/api";


// 【基础】
static NSString *const base_setting = @"Class/Base/Controller/Base_Setting.ashx";
static NSString *const base_setting_change = @"Class/Base/Controller/Base_Setting_Change.ashx";
static NSString *const base_suggest = @"Class/Other/suggest/suggest.ashx";
static NSString *const base_slider_main = @"Class/Other/Slider/Base_Setting_Slider.ashx";
static NSString *const base_slider_main_change = @"Class/Other/Slider/Base_Setting_Slider_change.ashx";

// 【播放器】
static NSString *const player_ablum = @"Class/Player/Controller/player_ablum_list.ashx";
static NSString *const player_detail_list = @"Class/Player/Controller/player_info.ashx";
static NSString *const player_yotobe_detail = @"api/v1/feed.bak";                               /**< yotobe*/
static NSString *const player_detail_add = @"Class/Player/Controller/player_info_add.ashx";     // 视频添加

// 【学习】
static NSString *const study_info_list = @"Class/Study/Controller/Study_info_list.ashx";

static NSString *const study_item_list = @"Class/Study/Controller/Study_menu_list.ashx";
static NSString *const study_item_add = @"Class/Study/Controller/Study_menu_add.ashx";           /**< 添加item*/
static NSString *const study_item_delete = @"Class/Study/Controller/Study_item_delete.ashx";          /**< 添加信息*/

static NSString *const study_item_edit_list = @"Class/Study/Controller/Study_menu_list.ashx";
static NSString *const study_info_add = @"Class/Study/Controller/Study_info_add.ashx";          /**< 添加信息*/

// 【登录】
static NSString *const user_login = @"Class/User/Controller/user_login.ashx";                                     /**< 三方登录*/
static NSString *const user_location = @"Class/User/Controller/user_location.ashx";

// 【关于我】
static NSString *const about_Products = @"Class/about/controller/about_product.ashx";       /**< 关于我的作品*/
static NSString *const about_Company = @"Class/About/Controller/about_company.ashx";        /**< 关于我的公司*/
static NSString *const about_Me = @"Class/about/controller/about_me.ashx";                  /**< 关于我*/
static NSString *const about_MeMail = @"/Class/Other/Mail/Mail.ashx";                       /**< 关于我发送邮件*/
static NSString *const about_School = @"Class/about/controller/about_school.ashx";          /**< 关于我的学校*/

// 【音乐】
static NSString *const music_AblumList = @"Class/Music/Controller/music_ablum.ashx";        /**< 音乐列表*/
static NSString *const music_list = @"Class/Music/Controller/music_List.ashx";              /**< 音乐列表*/
static NSString *const music_Single = @"Class/Music/Controller/music_single.ashx";          /**< 单一一个音乐*/

// 【其他】
static NSString *const other_drink_set = @"/Class/Other/Drink/other_drink_set.ashx";        /**< 喝水*/
static NSString *const other_drink = @"/Class/Other/Drink/other_drink.ashx";                /**< 获取喝水信息*/

// 【微信】
static NSString *const weChat = @"/userinfo";
static NSString *const weChat_Token = @"/oauth2/access_token";

// 【日记】
static NSString *const diary_add = @"Class/Diary/Controller/Diary_Info_Add.ashx";
static NSString *const diary_list = @"Class/Diary/Controller/Diary_list.ashx";

// 【获取天气】
static NSString *const weather_get = @"http://apicloud.mob.com/v1/weather/query";

// 【商店】
static NSString *const shop_add = @"Class/Shop/Controller/shop_root_add.ashx";

#endif /* URLConstance_h */
