//
//  BaseNetworkConstants.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#ifndef BaseNetworkConstants_h
#define BaseNetworkConstants_h

// 【接口环境】
// 【版本号】
#define API_VER @""
#define APP_VER        [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"]
#define BUILD_VER      [[NSBundle mainBundle].infoDictionary objectForKey:(NSString *)kCFBundleVersionKey]

// 【Socket】
#ifdef DEBUG
#define Socket_API_HOST     @"www.giganticwhale.pw"
#define Socket_API_PORT     @""
#else
#define Socket_API_HOST     @""
#define Socket_API_PORT     @""
#endif


static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL netLogAlert = NO;
static NSInteger timeoutInterval = 10;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#import "FetchModelProperty.h"

// Base
#import "FetchModel.h"
#import "FetchFileModel.h"
#import "AFNetworking.h"

#import "NetworkEngine.h"
#import "RequestSerializer.h"
#import "ResponseSerializer.h"
#import "NetworkAdapter.h"
#import "URLConstance.h"
#import "GWImageView.h"
#import "PDHUD.h"




#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif



#endif /* BaseNetworkConstants_h */
