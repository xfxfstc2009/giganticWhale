//
//  NetErrorMap.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetErrorMap.h"

@implementation NetErrorMap

+ (NSString *)networkErrorMapWithErrCode:(NSString *)errCode{
    NSDictionary *dic = @{@"200":@"成功",
                          @"300":@"参数错误",
                          @"301":@"用户登录失败",
                          @"302":@"数据校验失败-数据缺失",
                          @"303":@"数据校验失败-MD5校验不通过",
                          @"305":@"因业务导致禁止访问，例如：数据库未存在的数据",
                          @"401":@"token失效或登录失败",
                          @"403":@"禁止访问",
                          @"500":@"数据库错误",
                          @"501":@"绑定召唤师出错",
                          @"502":@"重置密码不能与当前密码相同",
                          @"503":@"验证码发送次数超过限制",
                          };
    
    NSString *errMsg = [dic objectForKey:errCode];
    return errMsg.length?errMsg:@"";
}

@end
