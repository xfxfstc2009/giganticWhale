//
//  GWConstants.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#ifndef GWConstants_h
#define GWConstants_h

// System Default Sets
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]


// System
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)
#define IS_IOS10_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 9.99)

#define iphone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iphone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iphone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define iphone4  ([UIScreen mainScreen].bounds.size.height == 480)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)

// Frame
#define kScreenBounds               [[UIScreen mainScreen] bounds]
#define kScreenWidth                kScreenBounds.size.width
#define kScreenHeight               kScreenBounds.size.height

//rightMenuCellHeight
#define RIGHTMENU_CELL_HEIGHT 40

// System Style
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR       RGB(239, 239, 244,1)
#define NAVBAR_COLOR RGB(255, 255, 255,1)

// log
#define debugMethod() PDOLLog(@"%s", __func__)
#ifdef DEBUG
#define PDOLLog(...)  NSLog(__VA_ARGS__)
#else
#define PDOLLog(...)
#endif


// Notification_Name
#define WX_PAY_NOTIFICATION   @"WXPayNotification"
#define ALI_PAY_NOTIFICATION  @"AliPayNotification"

// alipay Notification
static NSString *const PDAlipayNotification = @"PDAlipayNotification";/**< 支付宝支付通知*/

// NSUserDefaults constants
#define UD_CLUB_CAN_SHOW      @"clubProtocolIsShow"

// Cell Margins
#define ConfigCellMargin(value) \
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexpath { \
cell.separatorInset = UIEdgeInsetsMake(0, value, 0, value); \
cell.layoutMargins = UIEdgeInsetsMake(0, value, 0, value); \
cell.preservesSuperviewLayoutMargins = NO; \
}


// deviCeId
#define DeviceId @"device"

// 【主题】
#define Main_Color [UIColor colorWithRed:71/255.0 green:165/255.0 blue:254/255.0 alpha:1]

// 【阿里OSS】
#define ossEndpoint   @"http://oss-cn-shanghai.aliyuncs.com"
#define ossAccessKey  @"QYAXIE691g96Nkde"
#define ossSecretKey  @"IGa4h7Anl8SHFiP0JKpTD3xWLrS57e"
#define ossBucketName @"smartmin"
#define aliyunOSS_BaseURL @"http://smartmin.img-cn-shanghai.aliyuncs.com/"

// 【阿里百川】
#define AliChattingIdentify @"23320846"                                     // 百川chattingAPI
// 【阿里推送】
static NSString *const aliPushAppKey = @"23320846";
static NSString *const aliPushAppSecret = @"1e8538ebe7b7aa098396df842e576986";
// 【图灵机器人】
static NSString *const tulingKey = @"ef7f43e2d19e4352a7d6c9b7b8648f52";


// 【map】
#define mapIdentify @"e4f58b027dfcd596b10b2219b0e7f0c7"


// 【slider】
#define sliderImg @"Laungch/slider.png"

// 【wechat】
#define WeChatAppID         @"wxeb1a9d5cd948e494"
#define WeChatAppSecret     @"d6ca7e52ea33bc119547dd7a99a8db36"

// 【百度语音】
#define baiduYuyinKey  @"03VPP2apwYRr40lFUuZfpytZEt1DnirV"
#define baiduSecret @"4f4sFWUMerGqb717mC89F51tFN4H2KnA"
// 【自动登录】
#define AUTO_Login_User @"AUTO_Login_User"
#define AUTO_Login_Type @"AUTO_Login_Type"
// 【slider】
#define Main_Slider @"Main_Slider"



// 【GWUserCurrentInfoGetManager】
#import "GWUserCurrentInfoGetManager.h"         // 全局的单利
#import "GWViewTool.h"

// 通知
static NSString *const GWMusicNotification = @"GWMusicNotification";                // 音乐通知
// 消息通知
static NSString *const GWPushNotificationWithLocation = @"GWPushNotificationWithLocation";  /**< 地址信息通知*/
// 日记通知
static NSString *const GWDiaryNotification = @"GWDiaryNotification";                            /**< 日记通知*/






#endif /* GWConstants_h */
